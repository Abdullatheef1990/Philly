<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db142587_philydev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kfvl)=E`dX2?o=0[[!.s1JPe/4y+}tdr jN%;CAV{42,,,ic}it=PDM_RHKXxg+m');
define('SECURE_AUTH_KEY',  'M.dp6K&R^u2t5H]WQwZY|5gqQ1_*h#*Ea;*C +C]PMw^+Ht79glsYH+%2*2n6Y(m');
define('LOGGED_IN_KEY',    'q1txN1~zTtPKkZVY85j[]Ve99<PvORsk %RkX8P)WFB@6ls=MkIh}Rina5)Re/(t');
define('NONCE_KEY',        'hlar-vp%NVl7e8A}*Wbx8i qUq0}mUICQfeV#t~chkDG&vrJO0Y@]CzP`>G<GRj;');
define('AUTH_SALT',        '~@[=!a#L$ 5nyX3e>YS4T)B&>Tp=&mz~>*-4E8BMLz}mYQPYH|WGfWSqK#E I40m');
define('SECURE_AUTH_SALT', 'y*9@puyu*#MtlBf}ieeOv{m$JI.:pdBQ_61`9/6lz}N:9r@yn}vPn~qv#8m$[p)F');
define('LOGGED_IN_SALT',   'UDriS$$_fzrX-$!6FR5C6+#wN$N.q<|E30L-51|,`.~Jp~6ge`W*?%Gm# )=6/jW');
define('NONCE_SALT',       'SQ_rs{ag|]Oz>+8Z+}#qjv/_@mTig0,*4zDGus~!tP}<-PVi3}jvtC/@6)rQj6;|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
define('WP_HOME','http://local.sites/wp');
define('WP_SITEURL','http://local.sites/wp');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
