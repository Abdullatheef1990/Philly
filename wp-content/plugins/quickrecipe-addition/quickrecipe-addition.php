<?php
/*
Plugin Name: Quick Recipe Post Types and Profile Fields
Plugin URI: 
Description: A Custom Post Type Plugin To Use With Quick Recipe Theme.
Version: 1.0.0
Author: OngoingThemes
Author URI: http://ongoingthemes.com/
License: 
*/
include_once('custom-ajax-auth.php');	
include_once('framework/user.php');

// action to loaded the plugin translation file
add_action('plugins_loaded', 'quickrecipe_plugins');
if( !function_exists('quickrecipe_plugins') ){
	function quickrecipe_plugins() {
		load_plugin_textdomain( 'quickrecipe-addition', false, dirname(plugin_basename( __FILE__ ))  . '/languages/' ); 
	}
}

if(!function_exists('quickrecipe_create_posttypes')){

    add_action( 'init', 'quickrecipe_create_posttypes' );

    function quickrecipe_create_posttypes() {

        $labels = array(
            'name' => __('Recipes', 'quickrecipe'),
            'singular_name' => __('Recipe', 'quickrecipe'),
            'add_new' => __('Add New', 'quickrecipe'), __('Recipe', 'quickrecipe'),
            'add_new_item' => __('Recipe', 'quickrecipe'),
            'edit_item' => __('Edit Recipe', 'quickrecipe'),
            'new_item' => __('New Recipe', 'quickrecipe'),
            'view_item' => __('View Recipe', 'quickrecipe'),
            'search_items' => __('Search Recipes', 'quickrecipe'),
            'not_found' =>  __('No Recipes found', 'quickrecipe'),
            'not_found_in_trash' => __('No Recipes found in Trash', 'quickrecipe'),
            'parent_item_colon' => ''
        );

        $args = array(
		        'labels'             => $labels,
		        'public'             => true,
		        'publicly_queryable' => true,
		        'show_ui'            => true,
		        'show_in_menu'       => true,
		        'query_var'          => true,
		        'rewrite'            => array( 'slug' => __('recipe', 'quickrecipe') ),
		        'capability_type'    => 'post',
		        'has_archive'        => true,
		        'hierarchical'       => false,
		        'menu_position'      => 5,
                'taxonomies'         => array( 'recipe-type', 'post_tag' ),
                'menu_icon'          => 'dashicons-welcome-write-blog',
		        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
        );

        register_post_type( 'recipe', $args );

		$post_type_labels = array(
			'name' => __( 'Testimonials', 'quickrecipe' ),
			'singular_name' => __( 'Testimonial', 'quickrecipe' ),
			'add_new' => __( 'Add New', 'quickrecipe' ),
			'add_new_item' => __( 'Add New Testimonial', 'quickrecipe' ),
			'edit_item' => __( 'Edit Testimonial', 'quickrecipe' ),
			'new_item' => __( 'New Testimonial', 'quickrecipe' ),
			'view_item' => __( 'View Testimonial', 'quickrecipe' ),
			'search_items' => __( 'Search Testimonials', 'quickrecipe' ),
			'not_found' =>  __( 'No Testimonials found', 'quickrecipe' ),
			'not_found_in_trash' => __( 'No Testimonials found in the trash', 'quickrecipe' ),
			'parent_item_colon' => ''
		);

		register_post_type( 'testimonial',
			array(
				 'labels' => $post_type_labels,
				 'singular_label' => __( 'Testimonial', 'quickrecipe' ),
				 'public' => true,
				 'show_ui' => true,
				 '_builtin' => false,
				 '_edit_link' => 'post.php?post=%d',
				 'capability_type' => 'post',
				 'hierarchical' => false,
				 'rewrite' => array( 'slug' => 'testimonial' ),
				 'query_var' => 'testimonial',
				 'supports' => array( 'title', 'editor', 'thumbnail' ),
                 'menu_icon'     => 'dashicons-testimonial',
				 'menu_position' => 5
			)
		);


		$post_type_labels = array(
			'name' => __( 'Home Page Slides', 'quickrecipe' ),
			'singular_name' => __( 'Slide', 'quickrecipe' ),
			'add_new' => __( 'Add New', 'quickrecipe' ),
			'add_new_item' => __( 'Add New Slide', 'quickrecipe' ),
			'edit_item' => __( 'Edit Slide', 'quickrecipe' ),
			'new_item' => __( 'New Slide', 'quickrecipe' ),
			'view_item' => __( 'View Slide', 'quickrecipe' ),
			'search_items' => __( 'Search Slide', 'quickrecipe' ),
			'not_found' =>  __( 'No slide found', 'quickrecipe' ),
			'not_found_in_trash' => __( 'No testimonials found in the trash', 'quickrecipe' ),
			'parent_item_colon' => ''
		);

		register_post_type( 'homepage_slide',
			array(
				 'labels' => $post_type_labels,
				 'singular_label' => __( 'Slide', 'quickrecipe' ),
				 'public' => true,
				 'show_ui' => true,
				 '_builtin' => false,
				 '_edit_link' => 'post.php?post=%d',
				 'capability_type' => 'post',
				 'hierarchical' => false,
				 'rewrite' => array( 'slug' => 'homepage_slide' ),
				 'query_var' => 'homepage_slide',
				 'supports' => array( 'title', 'editor', 'thumbnail' ),
                 'menu_icon'     => 'dashicons-slides',
				 'menu_position' => 5
			)
		);

		$post_type_labels = array(
			'name' => __( 'We Offer', 'quickrecipe' ),
			'singular_name' => __( 'Offer', 'quickrecipe' ),
			'add_new' => __( 'Add New', 'quickrecipe' ),
			'add_new_item' => __( 'Add New Offer', 'quickrecipe' ),
			'edit_item' => __( 'Edit Offer', 'quickrecipe' ),
			'new_item' => __( 'New Offer', 'quickrecipe' ),
			'view_item' => __( 'View Offer', 'quickrecipe' ),
			'search_items' => __( 'Search Offer', 'quickrecipe' ),
			'not_found' =>  __( 'No offer found', 'quickrecipe' ),
			'not_found_in_trash' => __( 'No offer found in the trash', 'quickrecipe' ),
			'parent_item_colon' => ''
		);

		register_post_type( 'we_offer',
			array(
				 'labels' => $post_type_labels,
				 'singular_label' => __( 'Offer', 'quickrecipe' ),
				 'public' => true,
				 'show_ui' => true,
				 '_builtin' => false,
				 '_edit_link' => 'post.php?post=%d',
				 'capability_type' => 'post',
				 'hierarchical' => false,
				 'rewrite' => array( 'slug' => 'we_offer' ),
				 'query_var' => 'we_offer',
				 'supports' => array( 'title', 'editor', 'thumbnail' ),
                 'menu_icon' => 'dashicons-megaphone',
				 'menu_position' => 5
			)
		);

    }
}



if(!function_exists('quickrecipe_create_taxonomies')){
    add_action( 'init', 'quickrecipe_create_taxonomies', 0 );

    function quickrecipe_create_taxonomies() {

        $recipe_type_labels = array(
            'name' => __('Recipe Types', 'quickrecipe'),
            'singular_name' => __('Recipe Type', 'quickrecipe'),
            'search_items' => __('Search Recipe Types', 'quickrecipe'),
            'all_items' => __('All Recipe Types', 'quickrecipe'),
            'parent_item' => __('Parent Recipe Type', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Recipe Type:', 'quickrecipe'),
            'edit_item' => __('Edit Recipe Type', 'quickrecipe'),
            'update_item' => __('Update Recipe Type', 'quickrecipe'),
            'add_new_item' => __('Add New Recipe Type', 'quickrecipe'),
            'new_item_name' => __('Recipe Type Name', 'quickrecipe'),
            'menu_name' => __('Recipe Types', 'quickrecipe')
        );

	    $recipe_type_args = array(
		    'hierarchical'      => true,
		    'labels'            => $recipe_type_labels,
		    'show_ui'           => true,
		    'show_admin_column' => true,
		    'query_var'         => false,
		    'rewrite'           => array( 'slug' => __('recipe-type', 'quickrecipe') ),
	    );
        register_taxonomy( 'recipe_type', 'recipe', $recipe_type_args );


        $ingredient_labels = array(
            'name' => __('Main Ingredients', 'quickrecipe'),
            'singular_name' => __('Main Ingredient', 'quickrecipe'),
            'search_items' => __('Search Ingredients', 'quickrecipe'),
            'all_items' => __('All Ingredients', 'quickrecipe'),
            'parent_item' => __('Parent Ingredient', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Ingredient:', 'quickrecipe'),
            'edit_item' => __('Edit Ingredient', 'quickrecipe'),
            'update_item' => __('Update Ingredient', 'quickrecipe'),
            'add_new_item' => __('Add New Ingredient', 'quickrecipe'),
            'new_item_name' => __('Ingredient Name', 'quickrecipe'),
            'menu_name' => __('Main Ingredients', 'quickrecipe')
        );

        register_taxonomy(
            'ingredient',
            'recipe',
            array(
                'hierarchical' => true,
                'labels' => $ingredient_labels,
                'query_var' => false,
                'rewrite' => array( 'slug' => __('ingredient', 'quickrecipe') )
            )
        );

        
        $cuisine_labels = array(
            'name' => __('Cuisines', 'quickrecipe'),
            'singular_name' => __('Cuisine', 'quickrecipe'),
            'search_items' => __('Search Cuisines', 'quickrecipe'),
            'all_items' => __('All Cuisines', 'quickrecipe'),
            'parent_item' => __('Parent Cuisine', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Cuisine:', 'quickrecipe'),
            'edit_item' => __('Edit Cuisine', 'quickrecipe'),
            'update_item' => __('Update Cuisine', 'quickrecipe'),
            'add_new_item' => __('Add New Cuisine', 'quickrecipe'),
            'new_item_name' => __('Cuisine Name', 'quickrecipe'),
            'menu_name' => __('Cuisines', 'quickrecipe')
        );

        register_taxonomy(
            'cuisine',
            'recipe',
            array(
                'hierarchical' => true,
                'labels' => $cuisine_labels,
                'query_var' => false,
                'rewrite' => array( 'slug' => __('cuisine', 'quickrecipe') )
            )
        );

        
        $course_labels = array(
            'name' => __('Courses - Meal Types', 'quickrecipe'),
            'singular_name' => __('Course - Meal Type', 'quickrecipe'),
            'search_items' => __('Search Courses', 'quickrecipe'),
            'all_items' => __('All Courses', 'quickrecipe'),
            'parent_item' => __('Parent Course', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Course:', 'quickrecipe'),
            'edit_item' => __('Edit Course', 'quickrecipe'),
            'update_item' => __('Update Course', 'quickrecipe'),
            'add_new_item' => __('Add New Course', 'quickrecipe'),
            'new_item_name' => __('Course Name', 'quickrecipe'),
            'menu_name' => __('Courses - Meal Types', 'quickrecipe')
        );

        register_taxonomy(
            'course',
            'recipe',
            array(
                'hierarchical' => true,
                'labels' => $course_labels,
                'query_var' => false,
                'rewrite' => array( 'slug' => __('course', 'quickrecipe') )
            )
        );

        
        $allergen_labels = array(
            'name' => __('Allergens', 'quickrecipe'),
            'singular_name' => __('Allergen', 'quickrecipe'),
            'search_items' => __('Search Allergens', 'quickrecipe'),
            'all_items' => __('All Allergens', 'quickrecipe'),
            'parent_item' => __('Parent Allergen', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Allergen:', 'quickrecipe'),
            'edit_item' => __('Edit Allergen', 'quickrecipe'),
            'update_item' => __('Update Allergen', 'quickrecipe'),
            'add_new_item' => __('Add New Allergen', 'quickrecipe'),
            'new_item_name' => __('Allergen Name', 'quickrecipe'),
            'menu_name' => __('Allergens', 'quickrecipe')
        );

        register_taxonomy(
            'allergen',
            'recipe',
            array(
                'hierarchical' => true,
                'labels' => $allergen_labels,
                'query_var' => false,
                'rewrite' => array( 'slug' => __('allergen', 'quickrecipe') )
            )
        );

        
        $level_labels = array(
            'name' => __('Recipe Difficulty Levels', 'quickrecipe'),
            'singular_name' => __('Difficulty Level', 'quickrecipe'),
            'search_items' => __('Search Difficulty Levels', 'quickrecipe'),
            'all_items' => __('All Difficulty Levels', 'quickrecipe'),
            'parent_item' => __('Parent Difficulty Level', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Difficulty Level:', 'quickrecipe'),
            'edit_item' => __('Edit Difficulty Level', 'quickrecipe'),
            'update_item' => __('Update Difficulty Level', 'quickrecipe'),
            'add_new_item' => __('Add New Difficulty Level', 'quickrecipe'),
            'new_item_name' => __('Difficulty Level Name', 'quickrecipe'),
            'menu_name' => __('Recipe Difficulty Levels', 'quickrecipe')
        );

        register_taxonomy(
            'difficulty_level',
            'recipe',
            array(
                'hierarchical' => true,
                'labels' => $level_labels,
                'query_var' => false,
                'rewrite' => array( 'slug' => __('difficulty-level', 'quickrecipe') )
            )
        );

        
        $season_labels = array(
            'name' => __('Seasons', 'quickrecipe'),
            'singular_name' => __('Season', 'quickrecipe'),
            'search_items' => __('Search Seasons', 'quickrecipe'),
            'all_items' => __('All Seasons', 'quickrecipe'),
            'parent_item' => __('Parent Season', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Season:', 'quickrecipe'),
            'edit_item' => __('Edit Season', 'quickrecipe'),
            'update_item' => __('Update Season', 'quickrecipe'),
            'add_new_item' => __('Add New Season', 'quickrecipe'),
            'new_item_name' => __('Season Name', 'quickrecipe'),
            'menu_name' => __('Seasons', 'quickrecipe')
        );

        register_taxonomy(
            'season',
            'recipe',
            array(
                'hierarchical' => true,
                'labels' => $season_labels,
                'query_var' => false,
                'rewrite' => array( 'slug' => __('season', 'quickrecipe') )
            )
        );

        $recipe_badge_labels = array(
            'name' => __('Recipe Badges', 'quickrecipe'),
            'singular_name' => __('Recipe Badge', 'quickrecipe'),
            'search_items' => __('Search Recipe Badges', 'quickrecipe'),
            'all_items' => __('All Recipe Badges', 'quickrecipe'),
            'parent_item' => __('Parent Recipe Badge', 'quickrecipe'),
            'parent_item_colon' =>__('Parent Recipe Badge:', 'quickrecipe'),
            'edit_item' => __('Edit Recipe Badge', 'quickrecipe'),
            'update_item' => __('Update Recipe Badge', 'quickrecipe'),
            'add_new_item' => __('Add New Recipe Badge', 'quickrecipe'),
            'new_item_name' => __('Recipe Badge Name', 'quickrecipe'),
            'menu_name' => __('Recipe Badges', 'quickrecipe')
        );

        register_taxonomy(
            'recipe_badge',
            'recipe',
            array(
                'hierarchical' => true,
                'labels' => $recipe_badge_labels,
                'query_var' => true,
                'rewrite' => array( 'slug' => __('recipe_badge', 'quickrecipe') )
            )
        );

		/*
		Testimonial Taxonomy
		*/
		
		$taxonomy_labels = array(
			'name' => __( 'Testimonial Categories', 'quickrecipe' ),
			'singular_name' => __( 'Testimonial Category', 'quickrecipe' ),
			'search_items' =>  __( 'Search Testimonial Categories', 'quickrecipe' ),
			'all_items' => __( 'All Testimonial Categories', 'quickrecipe' ),
			'parent_item' => __( 'Parent Testimonial Categories', 'quickrecipe' ),
			'parent_item_colon' => __( 'Parent Testimonial Category', 'quickrecipe' ),
			'edit_item' => __( 'Edit Testimonial Category', 'quickrecipe' ),
			'update_item' => __( 'Update Testimonial Category', 'quickrecipe' ),
			'add_new_item' => __( 'Add New Testimonial Category', 'quickrecipe' ),
			'new_item_name' => __( 'New Testimonial Category', 'quickrecipe' ),
			'menu_name' => __( 'Categories', 'quickrecipe' )
	  );

		register_taxonomy( 'testimonial_category', 'testimonial', array(
				'hierarchical' => true,
				'labels' => $taxonomy_labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => 'testimonials' )
			)
		);


    }
}


if(!function_exists('quickrecipe_edit_columns')){
    function quickrecipe_edit_columns($columns){

        /*$columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __( 'Recipe Title', 'quickrecipe' ),
            "thumb" => __( 'Thumbnail', 'quickrecipe' ),
            "id" => __( 'Recipe ID', 'quickrecipe' ),			 
            "date" => __( 'Publish Time', 'quickrecipe' )
        );*/
		
		$columns['cb'] = "<input type='checkbox' />";
        $columns['title'] = "Recipe Title";
        $columns['thumb'] = "Thumbnail";
        $columns['id'] = "Recipe ID";
        $columns['date'] = "Publish Time";
        

        return $columns;
    }
    add_filter("manage_edit-recipe_columns", "quickrecipe_edit_columns");
}


if(!function_exists('quickrecipe_custom_columns')){
    function quickrecipe_custom_columns($column){
        global $post;
        switch ($column)
        {
            case 'thumb':
                if(has_post_thumbnail($post->ID))
                {
                    $image_id = get_post_thumbnail_id();
                    $image_url = wp_get_attachment_url($image_id);
                    ?>
                    <a href="<?php the_permalink(); ?>" target="_blank">
                        <?php // the_post_thumbnail('sidebar-tabs'); ?>
                         <?php the_post_thumbnail('medium'); ?>
                    </a>
                <?php
                }
                else
                {
                    esc_html_e('No Thumbnail', 'quickrecipe');
                }
                break;

            case 'id':
                echo $post->ID;
                break;
        }
    }
    add_action("manage_posts_custom_column",  "quickrecipe_custom_columns");
}

function quickrecipe_user_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['user_phone'] = __('Phone Number', 'quickrecipe');
	$profile_fields['facebook'] = __('Facebook Url', 'quickrecipe');
	$profile_fields['twitter'] = __('Twitter Username (without the @)', 'quickrecipe');
	$profile_fields['google'] = __('Google+ Url', 'quickrecipe');
	$profile_fields['linkedin'] = __('LinkedIn Url', 'quickrecipe');
	$profile_fields['pinterest'] = __('Pinterest Url', 'quickrecipe');
	$profile_fields['instagramm'] = __('Instagram Username', 'quickrecipe');

	return $profile_fields;
}
add_filter('user_contactmethods', 'quickrecipe_user_contact_methods');


// Add the option to show or hide the email address for post authors
add_action( 'show_user_profile', 'quickrecipe_extra_profile_fields' );
add_action( 'edit_user_profile', 'quickrecipe_extra_profile_fields' );

function quickrecipe_extra_profile_fields( $user ) { ?>

	<h3><?php esc_html_e('Extra Profile Information', 'quickrecipe'); ?></h3>

	<table class="form-table">

		<tr>
			<th><label for="user_title"><?php esc_html_e('Title', 'quickrecipe'); ?></label></th>

			<td>
				<input type="text" name="user_title" id="user_title" value="<?php echo esc_attr(get_the_author_meta( "user_title", $user->ID )); ?>" />	
			</td>
		</tr>

		<tr>
			<th><label for="showemail"><?php esc_html_e('Show email', 'quickrecipe'); ?></label></th>

			<td>
				<input type="checkbox" name="showemail" id="showemail" value="yes" <?php if (esc_attr( get_the_author_meta( "showemail", $user->ID )) == "yes") echo "checked"; ?> />	
				<span class="description"><?php esc_html_e('Check if you want to display your email address in single posts and the contributors page template.', 'quickrecipe'); ?></span>
			</td>
		</tr>

	</table>
<?php }

add_action( 'personal_options_update', 'quickrecipe_update_profile_fields' );
add_action( 'edit_user_profile_update', 'quickrecipe_update_profile_fields' );

function quickrecipe_update_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_user_meta( $user_id, 'showemail', $_POST['showemail'] );
	update_user_meta( $user_id, 'user_title', $_POST['user_title'] );

}



function quickrecipe_email_new_password( $user_id, $new_password ){
    

	$user = get_userdata( $user_id );
    $email_address = get_option('quickrecipe_email_address');

	//if( !$user || !empty($new_password) ) return false;

    $user_display_name = $user->display_name;
    $user_email = $user->user_email;

	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/plain; charset=utf-8";
	$headers[] = "From: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "Reply-To: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "X-Mailer: PHP/".phpversion();

    $subject = get_bloginfo( 'name' ) . ": " . __( 'New Password', 'quickrecipe' );

    $message = __('Hi ','quickrecipe') . $user_display_name;
    $message .= "\n";
    $message .= __("Your password has been reset successfully! Please use the new password to login.", 'quickrecipe');
    $message .= "\n";
    $message .= __('Your new password is: ','quickrecipe') . $new_password;

    if ( wp_mail($user_email, $subject, $message, $headers) ) {
		return true;
	} else {
		return false;
	}
}

function quickrecipe_email_remind_username( $user_id ){
    

	$user = get_userdata( $user_id );
    $email_address = get_option('quickrecipe_email_address');

	if( !$user ) return false;

	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/plain; charset=utf-8";
	$headers[] = "From: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "Reply-To: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "X-Mailer: PHP/".phpversion();

    $subject = get_bloginfo( 'name' ) . ": " . __( 'Remind Username', 'quickrecipe' );

    $message = __('Hi ','quickrecipe') . $user->display_name;
    $message .= "\n";
    $message .= __('Your username is: ','quickrecipe') . $user->user_login;

    if ( wp_mail($user->user_email, $subject, $message, $headers) ) {
		return true;
	} else {
		return false;
	}
}


function quickrecipe_email_reset_password( $user_id ){
    

	$user = get_userdata( $user_id );
	if( !$user || !$user->user_generated_password ) return false;
    
    $enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);
	
    $lost_password_page_id = get_option('quickrecipe_lost_password_page_url', '');
	$lost_password_page_url = get_permalink($lost_password_page_id);
	if (!$enable_custom_login || !$lost_password_page_url)
		$lost_password_page_url = wp_login_url();
	
	$email_address = get_option( 'quickrecipe_email_address' );
	
	$lostpassword_url = add_query_arg( 
		array( 
			'action' => 'resetpassword',
			'user_id' => $user->ID,
			'generated_password' => $user->user_generated_password
		), 
		$lost_password_page_url
	);

	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/plain; charset=utf-8";
	$headers[] = "From: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "Reply-To: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "X-Mailer: PHP/".phpversion();

	$subject = get_bloginfo( 'name' ) . ": " . __( 'Reset Password', 'quickrecipe' );
	
    $rn = "\r\n";
    $message = __( 'To reset your password, visit the following address: ', 'quickrecipe' ) . $rn;
	$message .= $lostpassword_url . $rn;
	$message .= __( 'You are receiving this e-mail because you have requested a password reset. If you did not request this password reset, then please ignore this email.', 'quickrecipe' ) . $rn;
	$message .= __( 'The link will remain possible during the next 24 hours.', 'quickrecipe' );
	
	if( wp_mail( $user->user_email, $subject, $message, $headers ) ) {
	//if( mail( $user->user_email, $subject, $message, implode( "\r\n", $headers ), '-f ' . $email_address ) ){
		return true;
	} else {
		return false;
	}
}


function quickrecipe_email_submitted_recipe( $user_id, $title ){
	global $current_user, $post;

    $to_address = get_option('quickrecipe_email_address');

    if(!empty($to_address)){
        $current_user = wp_get_current_user();
        $user_display_name = $current_user->display_name;
        $user_email = $current_user->user_email;

	    $headers   = array();
	    $headers[] = "MIME-Version: 1.0";
	    $headers[] = "Content-type: text/plain; charset=utf-8";
	    $headers[] = "From: " . get_bloginfo( 'name' ) . " <" . $to_address . ">";
	    $headers[] = "Reply-To: " . get_bloginfo( 'name' ) . " <" . $to_address . ">";
	    $headers[] = "X-Mailer: PHP/".phpversion();

        $subject = __('QuickRecipe: New Recipe Added', 'quickrecipe');
        $message = __('Recipe Title: ','quickrecipe') . $title;

        wp_mail($to_address, $subject, $message, $headers);
        return true;
    }
    return false;
}

function quickrecipe_email_activation_link( $user_id ){

	$user = get_userdata( $user_id );
		
	if( !$user ) return false;
		
	$user_activation_key = get_user_meta($user_id, 'user_activation_key', true);

	if (empty($user_activation_key))
		return false;

    $enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);
	
	$register_page_id = get_option('quickrecipe_register_page_url', '');
	$register_page_url = get_permalink($register_page_id);
	if (!$enable_custom_login || !$register_page_url) $register_page_url = wp_login_url();
	
	$activation_url = add_query_arg( 
		array( 
			'action' => 'activate',
			'user_id' => $user->ID,
			'activation_key' => $user_activation_key
		), 
		$register_page_url
	);
	$email_address = get_option( 'quickrecipe_email_address' );

	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/plain; charset=utf-8";
	$headers[] = "From: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "Reply-To: " . get_bloginfo( 'name' ) . " <" . $email_address . ">";
	$headers[] = "X-Mailer: PHP/".phpversion();

	$subject = get_bloginfo( 'name' ) . ": " . __( 'Account Activation', 'quickrecipe' );
	$message = __( 'To activate your account, please click the link in the activation email.', 'quickrecipe' ) . "\r\n" . $activation_url;
	
	if( wp_mail( $user->user_email, $subject, $message, $headers ) ) {
		return true;
	} else {
		return false;
	}
}

function quickrecipe_email_successful_activation( $user_id ){

	
		
	$user = get_userdata( $user_id );
		
	if( !$user  ) return false;
		
    $enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);

    $login_page_id = get_option('quickrecipe_login_page_url', '');
    $login_page_url = get_permalink($login_page_id);
    if (!$enable_custom_login || !$login_page_url)  $login_page_url = wp_login_url();
		
	$subject = get_bloginfo( 'name' ) . ": " . __( 'User Activation Success ', 'quickrecipe' );
	$new_password = get_user_meta($user_id, 'user_pass', true);
	$message = __( 'Thank you for activating your account. You may now log in using the following credentials:', 'quickrecipe' ) . "\r\n";
	$message .= sprintf(__('Username: %s', 'quickrecipe'), $user->user_login) . "\r\n";
	$message .= sprintf(__('Password: %s', 'quickrecipe'), $new_password) . "\r\n";
	$message .= sprintf(__('Login url: %s', 'quickrecipe'), $login_page_url) . "\r\n";

	delete_user_meta( $user_id, 'user_pass' );
		
	$message .= "\r\n";

	$admin_email = get_option( 'quickrecipe_email_address' );
		
	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/plain; charset=utf-8";
	$headers[] = "From: " . get_bloginfo( 'name' ) . " <" . $admin_email . ">";
	$headers[] = "Reply-To: " . get_bloginfo( 'name' ) . " <" . $admin_email . ">";
	$headers[] = "X-Mailer: PHP/".phpversion();
		
	if( wp_mail( $user->user_email, $subject, $message, $headers ) ) {
		return true;
	} else {
		return false;
	}

}

function quickrecipe_redirect_login_page() {
    $login_page  = esc_url( home_url( '/login/' ) );
    $page_viewed = basename($_SERVER['REQUEST_URI']);
 
    if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($login_page);
        exit;
    }
}
add_action('init','quickrecipe_redirect_login_page');

function quickrecipe_login_failed() {
    $login_page  = esc_url( home_url( '/login/' ) );
    $page_viewed = basename($_SERVER['REQUEST_URI']);
 
    if( $page_viewed == "wp-login.php" || is_page( 'login' )) {
        wp_redirect( $login_page . '?login=failed' );
        exit;
    }
}
add_action( 'wp_login_failed', 'quickrecipe_login_failed' );
 
function quickrecipe_verify_username_password( $user, $username, $password ) {
	$enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);
	if ($enable_custom_login) {
      $login_page  = esc_url( home_url( '/login/' ) );
        if( $username == "" || $password == "" ) {
            wp_redirect( $login_page . "?login=empty" );
            exit;
        }
    }
}
add_filter( 'authenticate', 'quickrecipe_verify_username_password', 1, 3);

function quickrecipe_logout_page() {
	$redirect_to_after_logout_page_id = get_option('quickrecipe_redirect_to_after_logout', '');
	$redirect_to_after_logout_url = get_permalink($redirect_to_after_logout_page_id);
			
	$login_page_id = get_option('quickrecipe_login_page_url', '');
	$login_page_url = get_permalink($login_page_id);
	if (!empty($redirect_to_after_logout_url)) {
		wp_redirect( $redirect_to_after_logout_url );
		exit;
	}
    else {
        if (!empty($login_page_url)) {
			wp_redirect( $login_page_url );
			exit;
        } else {
            wp_redirect( home_url( '/' ) );
            exit;
        }
    }
}
add_action('wp_logout','quickrecipe_logout_page');

function quickrecipe_login_register_popup() {
    $terms_page_url_id = get_option('quickrecipe_terms_page_url', '');
    $terms_page_url = get_permalink($terms_page_url_id);

    $privacy_policy_page_url_id = get_option('quickrecipe_privacy_policy_page_url', '');
    $privacy_policy_page_url = get_permalink($privacy_policy_page_url_id);

    ?>
    
<?php  if (ICL_LANGUAGE_CODE=='en') {  ?>

   <!-- begin:modal-signin -->
    <form id="login" class="ajax-auth" action="login" method="post">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php esc_html_e('LOG IN', 'quickrecipe'); ?></h4>
              </div>
              <div class="modal-body">
                  <p class="status"></p>  
                  <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>  
                  <div class="form-group">
                    <label for="username"><?php esc_html_e('Username', 'quickrecipe'); ?></label>
                    <input type="text" name="username" id="username" class="required form-control input-lg" placeholder="<?php esc_html_e('Enter Username', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <label for="password"><?php esc_html_e('Password', 'quickrecipe'); ?></label>
                    <input type="password" name="password" class="required form-control input-lg" id="password" placeholder="<?php esc_html_e('Password', 'quickrecipe'); ?>">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="remember_me"> <?php esc_html_e('Keep me logged in', 'quickrecipe'); ?>
                    </label>
                  </div>
              </div>
              <a href="http://philydev.bluebarracudame.com/wp-login.php?loginFacebook=1&redirect=http://philydev.bluebarracudame.com" onclick="window.location = 'http://philydev.bluebarracudame.com/wp-login.php?loginFacebook=1&redirect='+window.location.href; return false;"> <img src="HereComeTheImage" /> </a>
              <div class="modal-footer">
				  <div class="text-box bb"><?php esc_html_e('Don\'t have account ?', 'quickrecipe'); ?> <a id="pop_signup" class="text-link" href="<?php echo wp_registration_url(); ?>"><?php esc_html_e('Sign up here.', 'quickrecipe'); ?></a></div>
               <div class="text-box">
				   <?php esc_html_e('Forgot your password?', 'quickrecipe'); ?> <a id="pop_forgot" class="text-link" href="<?php echo wp_lostpassword_url(); ?>"><?php esc_html_e('Reset it here.', 'quickrecipe'); ?></a></div>
               <div class="text-box">
				   <input type="submit" name="login" value="<?php esc_html_e('Sign in', 'quickrecipe'); ?>" class="submit_button btn btn-success btn-block btn-lg"></div>
              </div>
            </div>
          </div>
          <!--
        </div>
        -->
    </form>
    <!-- end:modal-signin -->

    <form id="register" class="ajax-auth"  action="register" method="post">
        <!-- begin:modal-signup -->
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php esc_html_e('Sign up', 'quickrecipe'); ?></h4>
              </div>
              <div class="modal-body">
                  <p class="status"></p>
                  <?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?>         
                  <div class="form-group">
                    <input id="signonname" type="text" name="signonname" class="required form-control input-lg" placeholder="<?php esc_html_e('Enter username', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <input type="email" class="required form-control input-lg" placeholder="<?php esc_html_e('Enter email', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <input type="password" class="required form-control input-lg" placeholder="<?php esc_html_e('Password', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <input id="signonpassword" name="signonpassword" type="password" class="required form-control input-lg" placeholder="<?php esc_html_e('Confirm Password', 'quickrecipe'); ?>">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="agree"> <?php echo sprintf(__('Agree to our Terms of use and privacy policy', 'quickrecipe'), esc_url($terms_page_url), esc_url($privacy_policy_page_url)); ?>
                    </label>
                  </div>
              </div>
              <div class="modal-footer">
				  <div class="text-box"><?php esc_html_e('Already have account ?', 'quickrecipe'); ?> <a id="pop_login" class="text-link" href="<?php echo wp_login_url(); ?>"><?php esc_html_e('Sign in here.', 'quickrecipe'); ?></a></div>
                <div class="text-box">
					<input type="submit" class="submit_button btn btn-success btn-block btn-lg" value="<?php esc_html_e('Sign up', 'quickrecipe'); ?>"></div>
              </div>
            </div>
          </div>
        <!-- end:modal-signup -->
    </form>


    <form id="forgot_password" class="ajax-auth" action="forgot_password" method="post">    
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php esc_html_e('Forgot Password', 'quickrecipe'); ?></h4>
              </div>
              <div class="modal-body">
                <p class="status"></p>  
                <?php wp_nonce_field('ajax-forgot-nonce', 'forgotsecurity'); ?>  
                <div class="form-group">
                    <label for="user_signedin"><?php esc_html_e('Username or E-mail', 'quickrecipe'); ?></label>
                    <input id="user_signedin" type="text" class="required form-control input-lg" name="user_signedin" placeholder="<?php esc_html_e('Username or E-mail', 'quickrecipe'); ?>">
                </div>
              </div>
              <div class="modal-footer">
				  <div class="text-box"><?php esc_html_e('Already have account ?', 'quickrecipe'); ?> <a id="pop_login2" class="text-link" href="<?php echo wp_login_url(); ?>"><?php esc_html_e('Sign in here.', 'quickrecipe'); ?></a></div>
               <div class="text-box">
				   <input type="submit" class="submit_button btn btn-success btn-block btn-lg" value="<?php esc_html_e('Sign up', 'quickrecipe'); ?>"></div>
              </div>
            </div>
          </div>
        <!-- end:modal-signup -->
    </form>
    

<? } elseif (ICL_LANGUAGE_CODE=='ar') {?>


   <!-- begin:modal-signin -->
    <form id="login" class="ajax-auth" action="login" method="post">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php esc_html_e('تسجيل الدخول', 'quickrecipe'); ?></h4>
              </div>
              <div class="modal-body">
                  <p class="status"></p>  
                  <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>  
                  <div class="form-group">
                    <label for="username"><?php esc_html_e('اسم المستخدم', 'quickrecipe'); ?></label>
                    <input type="text" name="username" id="username" class="required form-control input-lg" placeholder="<?php esc_html_e('أدخلي اسم المستخدم', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <label for="password"><?php esc_html_e('كلمة السر', 'quickrecipe'); ?></label>
                    <input type="password" name="password" class="required form-control input-lg" id="password" placeholder="<?php esc_html_e('كلمة السر', 'quickrecipe'); ?>">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="remember_me"> <?php esc_html_e('أبقني مسجلا', 'quickrecipe'); ?>
                    </label>
                  </div>
              </div>
              <div class="modal-footer">
				  <div class="text-box bb"><?php esc_html_e('ليس لديك حساب معنا؟', 'quickrecipe'); ?> <a id="pop_signup" class="text-link" href="<?php echo wp_registration_url(); ?>"><?php esc_html_e('قومي بالتسجيل هنا', 'quickrecipe'); ?></a></div>
               <div class="text-box">
				   <?php esc_html_e('نسيت كلمة السر؟', 'quickrecipe'); ?> <a id="pop_forgot" class="text-link" href="<?php echo wp_lostpassword_url(); ?>"><?php esc_html_e('قومي بإسترجاع كلمة السر هنا!', 'quickrecipe'); ?></a></div>
               <div class="text-box">
				   <input type="submit" name="login" value="<?php esc_html_e('تسجيل الدخول', 'quickrecipe'); ?>" class="submit_button btn btn-success btn-block btn-lg"></div>
              </div>
            </div>
          </div>
          <!--
        </div>
        -->
    </form>
    <!-- end:modal-signin -->

    <form id="register" class="ajax-auth"  action="register" method="post">
        <!-- begin:modal-signup -->
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php esc_html_e('قومي بالتسجيل', 'quickrecipe'); ?></h4>
              </div>
              <div class="modal-body">
                  <p class="status"></p>
                  <?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?>         
                  <div class="form-group">
                    <input id="signonname" type="text" name="signonname" class="required form-control input-lg" placeholder="<?php esc_html_e('أدخلي اسم المستخدم', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <input type="email" class="required form-control input-lg" placeholder="<?php esc_html_e('يرجى ادخال البريد الإلكتروني', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <input type="password" class="required form-control input-lg" placeholder="<?php esc_html_e('يرجى ادخال كلمة السر', 'quickrecipe'); ?>">
                  </div>
                  <div class="form-group">
                    <input id="signonpassword" name="signonpassword" type="password" class="required form-control input-lg" placeholder="<?php esc_html_e('يرجى تأكييد كلمة السر', 'quickrecipe'); ?>">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="agree"> <?php echo sprintf(__('أوافق على سياسة الخصوصية وشروط الاستخدام', 'quickrecipe'), esc_url($terms_page_url), esc_url($privacy_policy_page_url)); ?>
                    </label>
                  </div>
              </div>
              <div class="modal-footer">
				  <div class="text-box"><?php esc_html_e('لديك حساب معنا؟', 'quickrecipe'); ?> <a id="pop_login" class="text-link" href="<?php echo wp_login_url(); ?>"><?php esc_html_e('قومي بتسجيل الدخول هنا', 'quickrecipe'); ?></a></div>
                <div class="text-box">
					<input type="submit" class="submit_button btn btn-success btn-block btn-lg" value="<?php esc_html_e('قومي بالتسجيل', 'quickrecipe'); ?>"></div>
              </div>
            </div>
          </div>
        <!-- end:modal-signup -->
    </form>


    <form id="forgot_password" class="ajax-auth" action="forgot_password" method="post">    
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php esc_html_e('نسيت كلمة السر', 'quickrecipe'); ?></h4>
              </div>
              <div class="modal-body">
                <p class="status"></p>  
                <?php wp_nonce_field('ajax-forgot-nonce', 'forgotsecurity'); ?>  
                <div class="form-group">
                    <label for="user_signedin"><?php esc_html_e('اسم المستخدم أو البريد الإلكتروني', 'quickrecipe'); ?></label>
                    <input id="user_signedin" type="text" class="required form-control input-lg" name="user_signedin" placeholder="<?php esc_html_e('اسم المستخدم أو البريد الإلكتروني', 'quickrecipe'); ?>">
                </div>
              </div>
              <div class="modal-footer">
				  <div class="text-box"><?php esc_html_e('لديك حساب معنا؟', 'quickrecipe'); ?> <a id="pop_login2" class="text-link" href="<?php echo wp_login_url(); ?>"><?php esc_html_e('قومي بتسجيل الدخول هنا', 'quickrecipe'); ?></a></div>
               <div class="text-box">
				   <input type="submit" class="submit_button btn btn-success btn-block btn-lg" value="<?php esc_html_e('قومي بالتسجيل', 'quickrecipe'); ?>"></div>
              </div>
            </div>
          </div>
        <!-- end:modal-signup -->
    </form>
    

<?php } ?>
   
 
    
<?php
}

function quickrecipe_contact_map() {
    global $post;
?>
    <div id="contact-block" class="contact-block">
		<?php if (!empty($address_longitude) && !empty($address_latitude)) { ?>
	    <script>
		    window.address_latitude = '<?php echo esc_html($address_latitude); ?>';
		    window.address_longitude = '<?php echo esc_html($address_longitude); ?>';
		    window.company_address = '<?php echo esc_html($company_address); ?>';
	    </script>
		<!--map-->
		<div class="map-wrap">
			<div class="gmap" id="map_canvas"></div>
		</div>
		<!--//map-->
		<?php } 
        else {
            echo get_post_meta($post->ID, 'pageoptions_contact_map', true); 
        } ?>                
        <div id="contact-block-inner" class="contact-block-inner">
        </div>
    </div>
<?php
}
?>