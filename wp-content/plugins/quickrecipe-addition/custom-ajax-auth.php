<?php
function quickrecipe_ajax_auth_init(){	
    wp_enqueue_style('ajax-auth-style', plugins_url('/css/ajax-auth-style.css', __FILE__));
	
    wp_enqueue_script('ajax-auth-script', plugins_url('/js/ajax-auth-script.js', __FILE__), array('jquery'));

	
	
	 if (ICL_LANGUAGE_CODE=='en') { 
		 $loadingmessage='Sending user info, please wait...';
	 } elseif (ICL_LANGUAGE_CODE=='ar') { 
		 $loadingmessage='معلومات المستخدم قيد الإرسال، الرجاء الإنتظار';
	 } 
	
    wp_localize_script( 'ajax-auth-script', 'ajax_auth_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => esc_url( home_url( '/' ) ),
        'loadingmessage' => __($loadingmessage, 'quickrecipe')
    ));
	
	
   

    // Enable the user with no privileges to run quickrecipe_ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'quickrecipe_ajax_login' );
	// Enable the user with no privileges to run quickrecipe_ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxregister', 'quickrecipe_ajax_register' );
	// Enable the user with no privileges to run quickrecipe_ajax_forgotPassword() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxforgotpassword', 'quickrecipe_ajax_forgotPassword' );
}

// Execute the action only if the user isn't logged in
//if (!is_user_logged_in()) {
    add_action('init', 'quickrecipe_ajax_auth_init');
//}
  
function quickrecipe_ajax_login(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
  	// Call quickrecipe_auth_user_login
	quickrecipe_auth_user_login($_POST['username'], $_POST['password'], 'Login'); 
	
    die();
}

function quickrecipe_ajax_register(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-register-nonce', 'security' );
		
    // Nonce is checked, get the POST data and sign user on
    $info = array();
  	$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user($_POST['username']) ;
    $info['user_pass'] = sanitize_text_field($_POST['password']);
	$info['user_email'] = sanitize_email( $_POST['email']);
	
	// Register the user
    $user_register = wp_insert_user( $info );
 	if ( is_wp_error($user_register) ){	
		$error  = $user_register->get_error_codes()	;
	
		if (ICL_LANGUAGE_CODE=='en') { 
		 $username_exist_error='This username is already registered.';
		 $email_exist_error='This email address is already registered.';
	 } elseif (ICL_LANGUAGE_CODE=='ar') { 
		 $username_exist_error='This username is already registered.';
		 $email_exist_error='This email address is already registered.';	
	 } 
		
		if(in_array('empty_user_login', $error))
			echo json_encode(array('loggedin'=>false, 'message'=>$user_register->get_error_message('empty_user_login')));
		elseif(in_array('existing_user_login',$error))
			echo json_encode(array('loggedin'=>false, 'message'=>__($username_exist_error, 'quickrecipe')));
		elseif(in_array('existing_user_email',$error))
        echo json_encode(array('loggedin'=>false, 'message'=>__($email_exist_error, 'quickrecipe')));
    } else {
	  quickrecipe_auth_user_login($info['nickname'], $info['user_pass'], 'Registration');       
    }

    die();
}

function quickrecipe_auth_user_login($user_login, $password, $login)
{
	$info = array();
    $info['user_login'] = $user_login;
    $info['user_password'] = $password;
    $info['remember'] = true;
	
	$user_signon = wp_signon( $info, false );
	
	if (ICL_LANGUAGE_CODE=='en') { 
		 $username_pass_error='Wrong username or password.';
		 $login_success_msg='Login successful, redirecting...';		
	} elseif (ICL_LANGUAGE_CODE=='ar') { 
		 $username_pass_error='Wrong username or password.';
		 $login_success_msg='Login successful, redirecting...';				
		
	 } 
	
    if ( is_wp_error($user_signon) ){
		echo json_encode(array('loggedin'=>false, 'message'=>__($username_pass_error, 'quickrecipe')));
    } else {
		wp_set_current_user($user_signon->ID); 
        echo json_encode(array('loggedin'=>true, 'message'=>__($login_success_msg, 'quickrecipe')));
    }
	
	die();
}

function quickrecipe_ajax_forgotPassword(){
	 
	// First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-forgot-nonce', 'security' );
	
	global $wpdb;
	
	$account = $_POST['user_login'];
	
	if (ICL_LANGUAGE_CODE=='en') { 
		 $username_pass_msg='Enter an username or e-mail address.';
		 $email_error_msg='There is no user registered with that email address.';		
		 $user_error_msg='There is no user registered with that username.';				
		 $invalid_user_email_msg='Invalid username or e-mail address.';						
	} elseif (ICL_LANGUAGE_CODE=='ar') { 
		  $username_pass_msg='Enter an username or e-mail address.';
		 $email_error_msg='There is no user registered with that email address.';		
		 $user_error_msg='There is no user registered with that username.';				
		 $invalid_user_email_msg='Invalid username or e-mail address.';					
		
	 } 
	
	if( empty( $account ) ) {
		$error = __($username_pass_msg, 'quickrecipe');
	} else {
		if(is_email( $account )) {
			if( email_exists($account) ) 
				$get_by = 'email';
			else	
				$error = __($email_error_msg, 'quickrecipe');			
		}
		else if (validate_username( $account )) {
			if( username_exists($account) ) 
				$get_by = 'login';
			else	
				$error = __($user_error_msg, 'quickrecipe');				
		}
		else
			$error =  __($invalid_user_email_msg, 'quickrecipe');		
	}	
	
	if(empty ($error)) {
		// lets generate our new password
		//$random_password = wp_generate_password( 12, false );
		$random_password = wp_generate_password();

			
		// Get user data by field and data, fields are id, slug, email and login
		$user = get_user_by( $get_by, $account );
			
		$update_user = wp_update_user( array ( 'ID' => $user->ID, 'user_pass' => $random_password ) );
			
		// if  update user return true then lets send user an email containing the new password
		if( $update_user ) {
			
			$from = 'admin@phillyarabia.com'; // Set whatever you want like mail@yourdomain.com
			
			if(!(isset($from) && is_email($from))) {		
				$sitename = strtolower( $_SERVER['SERVER_NAME'] );
				if ( substr( $sitename, 0, 4 ) == 'www.' ) {
					$sitename = substr( $sitename, 4 );					
				}
				$from = 'admin@'.$sitename; 
			}
			
			$to = $user->user_email;
			$subject = __('Your new password', 'quickrecipe');
			$sender = __('From: ', 'quickrecipe').get_option('name').' <'.$from.'>' . "\r\n";
			
			$message = __('Your new password is: ', 'quickrecipe').$random_password;
				
			$headers[] = 'MIME-Version: 1.0' . "\r\n";
			$headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers[] = "X-Mailer: PHP \r\n";
			$headers[] = $sender;
				
			$mail = wp_mail( $to, $subject, $message, $headers );
			if( $mail ) 
				$success = __('Check your email address for you new password.', 'quickrecipe');
			else
				$error = __('System is unable to send you mail containg your new password.', 'quickrecipe');						
		} else {
			$error = __('Oops! Something went wrong while updaing your account.', 'quickrecipe');
		}
	}
	
	if( ! empty( $error ) )
		echo json_encode(array('loggedin'=>false, 'message'=>$error));
			
	if( ! empty( $success ) )
		echo json_encode(array('loggedin'=>false, 'message'=>$success));
				
	die();
}