<?php
/*	
*	User File
*/	

function quickrecipe_add_user(){
    $errors = array();

	// user data array
	$userdata = array(
		'user_login' => sanitize_user( $_POST['user_login'] ),
		'user_email' => sanitize_email( $_POST['user_email'] ),
		'user_pass' => wp_generate_password( 8, false ),
		'confirm_pass' => $userdata['user_pass'],
		'first_name' => '',
		'last_name' => '',
		'role' => 'pending'
	);

    if ( isset( $_POST['term_and_condition'] ) ) 
        $term_and_condition = '1';
	else
		$errors[] = esc_html__( 'You must agree to our terms &amp; conditions.', 'quickrecipe' );

	if ( empty( $userdata['user_login'] ) ) {
		$errors[] = esc_html__( 'Username is required.', 'quickrecipe' );
	}
	else if ( strlen( $userdata['user_login'] ) < 5 ) {
		$errors[] = esc_html__( 'Username must be at least 5 characters.', 'quickrecipe' );
	}
	else if ( !validate_username( $userdata['user_login'] ) ) {
		$errors[] = esc_html__( 'Username is invalid.', 'quickrecipe' );
	}
	else if ( username_exists( $userdata['user_login'] ) ) {
		$errors[] = esc_html__( 'Username already exists.', 'quickrecipe' );
	}

	if ( !is_email( $userdata['user_email'] ) ) {
		$errors[] = esc_html__( 'You must enter a valid email address.', 'quickrecipe' );
	}
	else if ( email_exists( $userdata['user_email'] ) ) {
		$errors[] = esc_html__( 'The email address you entered is already in use.', 'quickrecipe' );
	}

	if( empty( $errors ) ){
		
		$user_id = wp_insert_user( $userdata );
		
        do_action('user_register', $user_id);

		update_user_meta($user_id, 'user_pass', $userdata['user_pass']);
		
        update_user_meta($user_id, 'term_and_condition', $term_and_condition);

        $user_activation_key = wp_generate_password( 20, false );
        update_user_meta($user_id, 'user_activation_key', $user_activation_key);

        quickrecipe_email_activation_link( $user_id );

		wp_redirect( add_query_arg( array( 'action' => 'registered' ), get_permalink() ) );

		//exit;
	}
    return $errors;
}

function quickrecipe_activate_user(){
    $user_id = intval( $_GET['user_id'] );
    $activation_key = (isset($_GET['activation_key'])) ? $_GET['activation_key'] : null;

    $errors = array();

	if ($activation_key && $user_id)
	{
        $user_info = get_userdata($user_id);
        $user_activation_key = get_user_meta($user_id, 'user_activation_key', true);

	    if ($user_info && !empty($user_activation_key) && $user_activation_key === $activation_key) {
		    $userdata = array(
			    'ID' => $user->ID,
			    'role' => 'pending'
		    );

		    wp_update_user( $userdata );
		    delete_user_meta( $user_info->ID, 'user_activation_key' );

            quickrecipe_email_successful_activation($user_id);

            //$success = esc_html__( 'Your account has been successfully activated. Thank you for activating your account.', 'quickrecipe' );
		} else {
            $errors[] = esc_html__( 'An error was encountered while activating your account.', 'quickrecipe' );
		}
	}
    else
    {
        $errors[] = __( 'Activation key was not found.', 'quickrecipe' );
    }
    return $errors;
}

function quickrecipe_update_user(){
    global $current_user;

    $error = array();    

    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
        if ( $_POST['pass1'] == $_POST['pass2'] )
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
        else
            $error[] = esc_html__('The passwords you entered do not match.  Your password was not updated.', 'quickrecipe');
    }

    /* Update user information. */
    if ( !empty( $_POST['url'] ) )
        wp_update_user( array( 'ID' => $current_user->ID, 'user_url' => esc_url( $_POST['url'] ) ) );
    if ( !empty( $_POST['email'] ) ){
        if (!is_email(esc_attr( $_POST['email'] )))
            $error[] = esc_html__('The Email you entered is not valid.  please try again.', 'quickrecipe');
        elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->id )
            $error[] = esc_html__('This email is already used by another user.  try a different one.', 'quickrecipe');
        else{
            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if ( !empty( $_POST['first-name'] ) )
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
    if ( !empty( $_POST['last-name'] ) )
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
    if ( !empty( $_POST['description'] ) )
        update_user_meta( $current_user->ID, 'description', esc_attr( $_POST['description'] ) );
    if ( !empty( $_POST['facebook'] ) )
        update_user_meta( $current_user->ID, 'facebook', esc_attr( $_POST['facebook'] ) );
    if ( !empty( $_POST['twitter'] ) )
        update_user_meta( $current_user->ID, 'twitter', esc_attr( $_POST['twitter'] ) );
    if ( !empty( $_POST['google'] ) )
        update_user_meta( $current_user->ID, 'google', esc_attr( $_POST['google'] ) );
    if ( !empty( $_POST['linkedin'] ) )
        update_user_meta( $current_user->ID, 'linkedin', esc_attr( $_POST['linkedin'] ) );
    if ( !empty( $_POST['pinterest'] ) )
        update_user_meta( $current_user->ID, 'pinterest', esc_attr( $_POST['pinterest'] ) );
    if ( !empty( $_POST['instagramm'] ) )
        update_user_meta( $current_user->ID, 'instagramm', esc_attr( $_POST['instagramm'] ) );

    /* Redirect so the page will show updated info.*/
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', $current_user->ID);
        wp_redirect( get_permalink() );
        exit;
    }
    return $error;
}

function quickrecipe_forgot_username(){
    $user_login = $_POST['user_login'];

	$errors = array();

    $user = get_user_by('email', $user_login);

	if ( empty( $user_login ) ) {
		$errors[] = esc_html__( 'Email address is required.', 'quickrecipe' );
	}	
    else if ( !is_email($user_login) && !$user ) {
        $errors[] = esc_html__( 'You must enter a valid email address.', 'quickrecipe' );
	} 
    else if ( !email_exists($user_login) ) {
		$errors[] = esc_html__( 'That E-mail doesn\'t belong to any registered users on this site.', 'quickrecipe' );
	}
		
	if( empty( $errors ) ){

        quickrecipe_email_remind_username( $user->ID );

		// refresh
		wp_redirect( add_query_arg( array( 'action' => 'notify' ), get_permalink() ) );
		exit;
	}
    return $errors;
}

function quickrecipe_reset_password(){
	$user_login = $_POST['user_login'];

	$errors = array();

	if ( empty( $user_login ) ) {
		$errors[] = esc_html__( 'Username or email address is required.', 'quickrecipe' );
	}	
    else if ( !is_email($user_login) ) {
		$user = get_user_by('login', $user_login);
		if (!$user) {
            $errors[] = esc_html__( 'You must enter a valid and existing email address or username.', 'quickrecipe' );
        }
	} 
    else if ( !email_exists($user_login) ) {
		$errors[] = esc_html__( 'That E-mail doesn\'t belong to any registered users on this site.', 'quickrecipe' );
	}
		
	if( empty($errors) ){

		$user = get_user_by('email', $user_login);

		$user_meta = array(
			'user_generated_password' => wp_generate_password( 20, false ),
			'user_generatedpassword_time' => date('Y-m-d H:i:s', time() )
		);	

		foreach ( $user_meta as $key => $value ) {
			update_user_meta( $user->ID, $key, $value );
		}

        if(function_exists('quickrecipe_email_reset_password')) {
		    quickrecipe_email_reset_password( $user->ID );
        }

		wp_redirect( add_query_arg( array( 'action' => 'notify' ), get_permalink() ) );
		exit;
	}
    return $errors;
}

function quickrecipe_generate_password(){
    $user_id = $_GET['user_id'];
	$generated_password = $_GET['generated_password'];
    $errors = array();

	$user_info = get_userdata( $user_id );

	if ( $user_info->user_generated_password === $generated_password ) {

		if ( $user_info->user_generatedpassword_time || strtotime( $user_info->user_generatedpassword_time ) > time() - ( 24 * 60 * 60 ) ) {

		    $userdata = array(
			    'ID' => $user_info->ID,
			    'user_pass' => wp_generate_password( 8, false )
		    );

		    wp_update_user( $userdata );
		    delete_user_meta( $user_info->ID, 'user_generated_password' );
		
        }
    }
	if( $userdata['user_pass'] && quickrecipe_email_new_password( $user_id, $userdata['user_pass'] ) ){
	} else {
        $errors[] = esc_html__( 'We encountered an error when attempting to reset your password.', 'quickrecipe' );
	}
    return $errors;
}
?>