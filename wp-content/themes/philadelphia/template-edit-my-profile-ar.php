<?php
/**
 * Template Name: Edit My Profile AR
 *
 * Allow users to update their profiles from Frontend.
 *
 */
if (!is_user_logged_in()) { auth_redirect(); }

/* Get user info. */
global $current_user;
wp_get_current_user();

$error = array();    
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    if(function_exists('quickrecipe_update_user')) {
	    $error = quickrecipe_update_user();
    } else {
        $error[] = esc_html__( 'You should install and activate to quickrecipe addition plugin.', 'quickrecipe' );
    }

}

$page_by_title = get_page_by_title( get_the_title() );
$the_excerpt = (!empty($page_by_title->post_excerpt)) ? $page_by_title->post_excerpt : $page_by_title->post_content;

get_header();

$slide_intro = get_field('vp_header_title');		
$slide_image = get_field('vp_header_image');
?>

<div class="row full-width header-box">
	<div class="main-image" style="background-image: url(<?php echo $slide_image; ?>)">
		<div class="master-slider ms-skin-default" id="masterslider">
			<div class="ms-slide">
				<img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt=""/>
				<div class="row main-container align-center full-width ms-layer ms-caption">
					<div class="column large-6 column align-self-middle">
						<?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>
                        
 					<?php } ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>

<?php
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <!-- begin:content -->
 <div id="content" class="user-account-box row">
          <!-- begin:article -->
          	<div class="columns full-width user-account-header"><?php the_title(); ?></div>
			<div class="columns large-9 small-12">	
				
                    <?php if ( !is_user_logged_in() ) : ?>
                            <p class="warning">
                                <?php esc_html_e('You must be logged in to edit your profile.', 'quickrecipe'); ?>
                            </p><!-- .warning -->
                    <?php else : ?>
                        <?php if ( count($error) > 0 ) echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>

					<form method="post" id="adduser" action="<?php the_permalink(); ?>" enctype="multipart/form-data">
						<div class="row submit-profile-form">
							<div class="columns large-12 small-12 description-text">
              						 <?php echo esc_html($the_excerpt); ?>
            					</div>
						<div class="columns large-6 small-12">
                            <label for="first-name"><?php esc_html_e('الاسم الأول', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
						</div>
						<div class="columns large-6 small-12">
                            <label for="last-name"><?php esc_html_e('الاسم الأخير', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
						</div>
						<div class="columns large-6 small-12">
                            <label for="email"><?php esc_html_e('البريد الإلكتروني *', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
						</div>
						<div class="columns large-6 small-12">
                            <label for="url"><?php esc_html_e('Website', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="url" type="text" id="url" value="<?php the_author_meta( 'user_url', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
						</div>
						<div class="columns large-6 small-12">
                            <label for="pass1"><?php esc_html_e('كلمة السر *', 'quickrecipe'); ?> </label>
                            <input class="form-control input-lg" name="pass1" type="password" id="pass1" />
                            <p class="sub-text"><?php esc_html_e('كلمة السر ', 'quickrecipe'); ?></p>
						</div>
						<div class="columns large-6 small-12">
                            <label for="pass2"><?php esc_html_e('أعد كتابة كلمة السر *', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="pass2" type="password" id="pass2" />
                            <p class="sub-text"><?php esc_html_e('تأكيد كلمة السر', 'quickrecipe'); ?></p>
						</div>
						  <div class="columns large-12 small-12">
                            <label for="description"><?php esc_html_e('المعلومات الشخصية', 'quickrecipe') ?></label>
                            <textarea class="form-control input-lg" name="description" id="description" rows="10" cols="30"><?php the_author_meta( 'description', $current_user->ID ); ?></textarea>
                            <p class="sub-text"><?php esc_html_e('أبقيها قصيرة ومعبرة', 'quickrecipe'); ?></p>
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
						</div>
						<div class="columns large-12 small-12">
                            <label for="user_title"><?php esc_html_e('لقبك', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="user_title" type="text" id="user_title" value="<?php the_author_meta( 'user_title', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
						</div>
						<div class="columns large-12 small-12">
                            <label for="facebook"><?php esc_html_e('فيس بوك', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="facebook" type="text" id="facebook" value="<?php the_author_meta( 'facebook', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
						</div>
						<div class="columns large-12 small-12">
                            <label for="twitter"><?php esc_html_e('تويتر', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="twitter" type="text" id="twitter" value="<?php the_author_meta( 'twitter', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
						</div>
                        <div class="columns large-12 small-12">
                            <label for="google"><?php esc_html_e('Google+', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="google" type="text" id="google" value="<?php the_author_meta( 'google', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
                        </div>
                        <div class="columns large-12 small-12">
                            <label for="linkedin"><?php esc_html_e('Linkedin', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="linkedin" type="text" id="linkedin" value="<?php the_author_meta( 'linkedin', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
                        </div>
                        <div class="columns large-12 small-12">
                            <label for="pinterest"><?php esc_html_e('Pinterest', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="pinterest" type="text" id="pinterest" value="<?php the_author_meta( 'pinterest', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
                        </div>
                        <div class="columns large-12 small-12">
                            <label for="instagramm"><?php esc_html_e('حساب انستاغرام', 'quickrecipe'); ?></label>
                            <input class="form-control input-lg" name="instagramm" type="text" id="instagramm" value="<?php the_author_meta( 'instagramm', $current_user->ID ); ?>" />
                            <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
                        </div>
                        <?php 
                            //action hook for plugin and extra fields
                            //do_action('edit_user_profile',$current_user);                     
                        ?>
                       <div class="columns large-12 small-12">
                            <label for="avatar"><?php esc_html_e('Avatar', 'quickrecipe'); ?></label>
                        <?php 
                            $my_avatar = new simple_local_avatars();
                            $delete_avatar = add_query_arg('avatar', get_the_ID(), get_permalink(wp_get_referer()));
                            if(isset($_GET['avatar'])) {
                                $my_avatar->avatar_delete($current_user->ID);
                            }
                            wp_nonce_field( 'simple_local_avatar_nonce', '_simple_local_avatar_nonce', false );
                        ?>
                        </div>
                        <div class="form-group col-sm-12">
                            <div class="upload">
                                <?php /*?><input type="button" class="uploadButton" value="<?php esc_html_e('Browse', 'quickrecipe'); ?>" /><?php */?>
                                <input type="file" name="simple-local-avatar" id="simple-local-avatar" accept="image/*" class="standard-text" />
                                <?php /*?><span class="filename"><?php esc_html_e('Select file...', 'quickrecipe'); ?></span><?php */?>
                                <p class="sub-text">
										<?php esc_html_e('Optional', 'quickrecipe'); ?>
									</p>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <p class="form-avatar">
                            </p><!-- .form-avatar -->
                        </div>
						<!-- / column -->
						<div class="col-sm-12">
							<button name="updateuser" type="submit" id="updateuser" class="submit button form-submit-btn">
								<?php esc_html_e('تحديث الملف الشخصي', 'quickrecipe'); ?> <i class="fa fa-arrow-left"></i>
							</button>
                            <?php wp_nonce_field( 'update-user' ) ?>
                            <input name="action" type="hidden" id="action" value="update-user" />
						</div>
						</div>
					</form>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php else: ?>
            <p class="no-data">
                <?php esc_html_e('Sorry, no page matched your criteria.', 'quickrecipe'); ?>
            </p><!-- .no-data -->
        <?php endif; ?>

					<!-- / form -->
				</div>
				
			
          <!-- end:article -->


         <!-- begin:sidebar -->
			<div class="columns large-3 small-12">
			<?php quickrecipe_get_user_sidebar(); ?>
			</div>
			<!-- end:sidebar -->

      
    </div>
    <!-- end:content -->


<?php get_footer(); ?>