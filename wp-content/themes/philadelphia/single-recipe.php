<?php
if ( ! defined( 'ABSPATH' ) ) exit;

// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header();



if ( have_posts() ) { $count = 0;
    while ( have_posts() ) { the_post(); $count++;
        $cover_image = get_post_meta($post->ID, 'quickrecipe_cover_image', true);
        if (!empty($cover_image)) {
            $cover_image_url = wp_get_attachment_image_src( $cover_image, 'large');
        }
        else {
            $cover_image_url[0] = get_template_directory_uri() . '/img/img01.jpg';   
        }
        $is_recipe = ( is_singular( 'recipe' ) ) ? true : false;
?>


    <!-- begin:header -->
     <div class="row full-width header-box">
    <div class="main-image" style="background-image: url(<?php echo esc_url($cover_image_url[0]); ?>)">

        <div class="row main-container align-center full-width">
            <div class="column large-6 column align-self-middle">
                <h1 class="heading"><?php the_title(); ?></h1>
                <?php if( get_author_role()=='editor' || get_author_role()=='administrator' ) { } else{ ?> 
    
<p class="recipe-date"><?php the_date(); ?></p>
                        <?php /*?><div class="recipe-submitted-img">
                            <img src=""/>
                        </div><?php */?>
                        <p class="submitted-by">By <?php the_author(); ?> </p>
                

            <?php } ?>

            </div>
        </div>

    </div>

</div>
   
   
    <!-- end:header -->

    <!-- begin:content -->
    
    <div class="row recipe-video align-center">

    <div class="columns large-10 small-12">
         <?php
                        $is_include_a_video = get_post_meta($post->ID, 'quickrecipe_is_include_a_video', true);
                        if($is_include_a_video == 'on')
                        {
                            $video_url = get_post_meta($post->ID, 'quickrecipe_embed_a_video', true);
                            ?>
                            <div id="slider-recipe" class="carousel">
                                <?php echo wp_oembed_get($video_url); ?>
                            </div>
                            <?php
                        } 
                        else 
                        {
                            $recipe_images = get_post_meta($post->ID, 'quickrecipe_more_images_recipe', false);
                            if ( count($recipe_images) ) {
                            ?>
                                <div id="slider-recipe" class="carousel slide" data-ride="carousel">
                                 <?php /*?>   <ol class="carousel-indicators">
                                <?php
                                $count = 0;
                                foreach ( $recipe_images as $recipe_image ) {
                                    $recipe_image_url = wp_get_attachment_image_src($recipe_image); 
                                ?>
                                    <li data-target="#slider-recipe" data-slide-to="<?php echo esc_attr($count); ?>" class="<?php if ($count == 0) echo 'active'; ?>"><img src="<?php echo esc_url($recipe_image_url[0]); ?>"></li>
                                <?php
                                    $count++;
                                }
                                ?>
                                    </ol><?php */?>

                                <div class="carousel-inner">
                                <?php
                                $count = 0;
                                foreach ( $recipe_images as $recipe_image ) {
                                    $recipe_image_url = wp_get_attachment_image_src($recipe_image, 'large'); 
                                ?>
                                    <div class="item <?php if ($count == 0) echo 'active'; ?>"><img src="<?php echo esc_url($recipe_image_url[0]); ?>"></div>
                                <?php
                                    $count++;
                                }
                                ?>
                                </div>
                                <?php if ($count > 1) { ?>
                                <a class="left carousel-control" href="#slider-recipe" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#slider-recipe" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                                <?php } ?>
                            </div>    
                            <?php          
                            }
                            elseif ( has_post_thumbnail() ) {
                            ?>
                            <div id="slider-recipe" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active"><?php the_post_thumbnail(); ?></div>
                                </div>
                            </div>    
                            <?php
                            }
                        }
                        ?>

    </div>
  <div class="columns large-4 small-12 recipe-servings-box">
        <div class="recipe-servings-wrapper">
        <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true);
            if (!empty($servings)) { 
            
                ?>
            <div class="recipe-servings">   
             <?php for ( $count=1; $count<=sprintf(wp_kses(__('%s', 'quickrecipe'), ''), esc_attr($servings)); $count++) {  
                
                ?>  
   
        
            <div class="recipe-serving"></div>
              <?php } ?>
        </div>
      
            <div class="recipe-servings-caption"> <?php if (ICL_LANGUAGE_CODE=='en') { ?>
<?php echo sprintf(wp_kses(__('Serves %s', 'quickrecipe'), ''), esc_attr($servings)); ?>

<?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
<?php echo sprintf(wp_kses(__('المحتوى %s', 'quickrecipe'), ''), esc_attr($servings)); ?>
<?php } ?></div>
            
            <?php } ?>  
        </div>
    </div>
  
  
    <div class="columns large-6 small-12 recipe-buttons-box">
        <?php 
                                $postURL = urlencode(get_permalink());
                                $postTitle = str_replace( ' ', '%20', get_the_title());
                                $mailto = "mailto:?subject=".str_replace( ' ', '%20', get_bloginfo('name')).'-'.$postTitle."&amp;body=".$postTitle.":".$postURL; 
                                ?>
                               
       <ul class="recipe-video-buttons">
           <li>
            <?php if ( $is_recipe ) echo quickrecipe_get_favorite_button(); ?>
           </li>
            <li>
               
                <a href="#popup-social<?php echo esc_attr($post->ID); ?>" class="share recipe-video-btn">
                                <i class="fa fa-share-square-o"></i> <?php if (ICL_LANGUAGE_CODE=='en') { 
                                esc_html_e( 'SHARE', 'quickrecipe' );  
                        } 
                         elseif (ICL_LANGUAGE_CODE=='ar'){ 
                                esc_html_e( 'حقيقة', 'quickrecipe' ); 
                        } ?></a>
                            <?php get_template_part( 'includes/html/popup-social' ); ?>
            </li>
            <li>
                <a href="<?php echo esc_url($mailto); ?>" class="email recipe-video-btn"><i class="fa fa-envelope"></i> <?php if (ICL_LANGUAGE_CODE=='en') { 
                                esc_html_e( 'E-Mail', 'quickrecipe' );  
                        } 
                         elseif (ICL_LANGUAGE_CODE=='ar'){ 
                                esc_html_e( 'حقيقة', 'quickrecipe' ); 
                        } ?></a>
              
            </li>
            <li>
                <a id="printcontent" rel="nofollow" class="print print-friendly recipe-video-btn" href="javascript:void(0);"><i class="fa fa-print"></i> <?php if (ICL_LANGUAGE_CODE=='en') { 
                                esc_html_e( 'PRINT', 'quickrecipe' );  
                        } 
                         elseif (ICL_LANGUAGE_CODE=='ar'){ 
                                esc_html_e( 'حقيقة', 'quickrecipe' ); 
                        } ?></a> 
                
            </li>
        </ul>
    </div>
    
</div>

<?php /******* RECIPE DECSRIPTION  **********/?>
 
  <div class="row align-center">

   <div class="columns large-10 small-12 printable">
    
                       
                        
          <?php 
                        if ( $is_recipe ) {
                        ?>
                        
                         <div class="recipe-title">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/desc-icon.png" class="title_icon"/>
                        <?php /*?><?php if (ICL_LANGUAGE_CODE=='en') { 
                                esc_html_e( 'Recipe Description', 'quickrecipe' );  
                        } 
                         elseif (ICL_LANGUAGE_CODE=='ar'){ 
                                esc_html_e( 'حقيقة مثبتة منذ زمن', 'quickrecipe' ); 
                        } ?><?php */?>
                    <?php the_title(); ?>
                 </div>
    <?php } ?>
    <div class="recipe-description">
        <?php the_content(); ?>
    </div>
    
      </div>
   
</div>
 
 <?php /******* INGREDIENTS/INSTRUCTION  **********/?>
  
  <div class="row align-center">

    <div class="columns large-4 small-12 recipe-ingredients">
        <h2><img src="<?php echo get_template_directory_uri(); ?>/img/ingredients-icon.png" class="title_icon"/> <?php if (ICL_LANGUAGE_CODE=='en') { 
                                esc_html_e( 'INGREDIENTS', 'quickrecipe' );
                        } 
                         elseif (ICL_LANGUAGE_CODE=='ar'){ 
                                esc_html_e( 'حقيقة مثبتة منذ زمن', 'quickrecipe' ); 
                        } ?></h2>
         <?php echo quickrecipe_list_ingredients(); ?>
    </div>
    <div class="columns large-6 small-12 recipe-instruction">
        
                               
        <h2><img src="<?php echo get_template_directory_uri(); ?>/img/instructions-icon.png" class="title_icon"/><?php if (ICL_LANGUAGE_CODE=='en') { 
                                esc_html_e( 'INSTRUCTION', 'quickrecipe' ); 
                        } 
                         elseif (ICL_LANGUAGE_CODE=='ar'){ 
                                esc_html_e( 'حقيقة مثبتة منذ زمن', 'quickrecipe' ); 
                        } ?></h2>
        <?php echo quickrecipe_list_method_steps(); ?>
    </div>

</div>
   

   <?php /******* NEXT PREVIOS RECIPES **********/?>   
   
   <div class="row recipe-grid prev-next-recipes align-center hide-for-small-only">
 <?php
                    $prev_post = get_previous_post();
                    $prev_label = ( $is_recipe ) ? esc_html__('Previous:', 'quickrecipe') : esc_html__('Previous Article:', 'quickrecipe');
                            
                    if (!empty( $prev_post )): ?>
    <div class="columns large-6 small-12 recipes-grid-element"
         style="background-image: url('<?php echo get_the_post_thumbnail_url($prev_post->ID); ?>')">
        <div class="row full-width grid-inner">

            <div class="column large-6 small-6 transparent-block panel">
                
                                            
                        

                <h2><?php echo esc_attr( get_the_title($prev_post)  ); ?></h2>

                <a href="<?php echo esc_url(get_permalink( $prev_post->ID )); ?>" class="transparent-button">PREVIOUS</a>
            
            </div>
            <div class="column large-6 small-6 panel">

            </div>
        </div>

    </div>
    <?php endif; ?>
 <?php
                    $next_post = get_next_post();
                    $next_label = ( $is_recipe ) ? esc_html__('Next:', 'quickrecipe') : esc_html__('Next Article:', 'quickrecipe');
                    if (!empty( $next_post )): ?>   
    <div class="columns large-6 small-12 recipes-grid-element"
         style="background-image: url('<?php echo get_the_post_thumbnail_url($prev_post->ID); ?>')">
        <div class="row full-width grid-inner">
            <div class="column large-6 small-6 panel">

            </div>
            <div class="column large-6 small-6 transparent-block panel">
                
                                            
                        
                    
                              
                <h2><?php echo esc_attr( get_the_title($next_post)  ); ?></h2>

                <a href="<?php echo esc_url(get_permalink( $next_post->ID )); ?>" class="transparent-button">NEXT</a>
            </div>
            
            
        </div>

    </div>
     <?php endif; ?>
</div>

 <div class="row recipe-grid prev-next-recipes align-center hide-for-medium hide-for-large">
 <?php
                    $prev_post = get_previous_post();
                    $prev_label = ( $is_recipe ) ? esc_html__('Previous:', 'quickrecipe') : esc_html__('Previous Article:', 'quickrecipe');
                            
                    if (!empty( $prev_post )): ?>
    <div class="columns small-6 prev-btn">
        <a href="">PREVIOUS RECIPE</a>
    </div>
 <?php endif; ?>
 <?php
                    $next_post = get_next_post();
                    $next_label = ( $is_recipe ) ? esc_html__('Next:', 'quickrecipe') : esc_html__('Next Article:', 'quickrecipe');
                    if (!empty( $next_post )): ?>   
    <div class="columns small-6 next-btn">
        <a href="">NEXT RECIPE</a>
    </div>
     <?php endif; ?>
</div>


<?php /******* COMMENTS **********/?>

       
<?php comments_template(); ?>



<?php /******* YOU MAY ALSO LIKE **********/?>

 <?php   get_template_part('includes/recipepage/related-recipes');    ?>
            
<?php /******* BE OUR CHEF **********/?>   
      <?php
                            if (ICL_LANGUAGE_CODE=='ar') {
                                
                                $be_chef_title = get_field('be_chef_title', 556);
                $be_chef_intro = get_field('be_chef_intro', 556);
                $be_chef_link_title = get_field('be_chef_link_title', 556);
                $be_chef_link_url = get_field('be_chef_link_url', 556);
                $be_chef_image = get_field('be_chef_image', 556);
                                
                             } elseif (ICL_LANGUAGE_CODE=='en'){ 
                            
                $be_chef_title = get_field('be_chef_title', 315);
                $be_chef_intro = get_field('be_chef_intro', 315);
                $be_chef_link_title = get_field('be_chef_link_title', 315);
                $be_chef_link_url = get_field('be_chef_link_url', 315);
                $be_chef_image = get_field('be_chef_image', 315);
    }
                            if (!empty( $be_chef_title )){ ?>     
       
          <div class="row full-width recipe-grid be-our-chef">

    <div class="columns large-12 small-12 recipes-grid-element"
         style="background-image: url(' <?php echo $be_chef_image ?>');">
        <div class="row full-width grid-inner">
            <div class="column large-6 small-6 panel">

            </div>
            <div class="column large-6 small-6 transparent-block panel">

                <div class="column large-7">

                    <h2><?php echo $be_chef_title ?></h2>
                    <p> <?php echo $be_chef_intro ?></p>
                   <?php if (!empty( $be_chef_link_title )){ ?> 
                    <a href="<?php echo $be_chef_link_url ?>" class="transparent-button"><?php echo $be_chef_link_title ?></a> 
                    <?php } ?>
                </div>

            </div>
        </div>

    </div>
</div>
    <?php } } }?>
  
  <?php // get_sidebar(); ?>
  

<?php get_footer(); ?>
