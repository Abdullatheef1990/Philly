<?php
/*
*   : Home Page
*/
// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header();
?>
    <!-- begin:header -->
    <div id="header" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
	    <?php
        $args = array('post_type' => 'homepage_slide', 'order' => 'ASC');
        	
        $query=new WP_Query($args);
        	
        if ( $query->have_posts() ) {
            $count = 0;
            while($query->have_posts()){
	            $query->the_post();
                $count++;
                $attachment_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                $related_link_page_id = get_post_meta($post->ID, 'quickrecipe_related_link', true);
                $related_link_page_url = get_permalink($related_link_page_id);

	    ?>
                <div class="item <?php if ($count == 1) echo 'active'; ?>" style="background-image: url(<?php echo esc_url($attachment_url); ?>);">
                  <div class="carousel-caption hidden-xs">
                    <h3><?php the_title(); ?></h3>
                    <p><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></p>
                    <div class="view-btn">
                      <a href="<?php echo esc_url($related_link_page_url); ?>" class="btn btn-success btn-lg"><?php esc_html_e( 'View Recipe', 'quickrecipe' ); ?></a>
                    </div>
                  </div>
                </div>
	    <?php
            } // End WHILE Loop
            wp_reset_postdata();
        }
        ?>
      </div>
      <a class="left carousel-control" href="#header" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#header" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </div>
    <!-- end:header -->

<?php

	$show_search_area = get_option('quickrecipe_show_search_area', 1); 
	$show_latest_recipes = get_option('quickrecipe_show_latest_recipes', 1); 
	$show_offers = get_option('quickrecipe_show_offers', 1); 
	$show_most_favorited = get_option('quickrecipe_show_most_favorited', 1); 
	$show_editor_choice = get_option('quickrecipe_show_editor_choice', 1); 
	$show_latest_news = get_option('quickrecipe_show_latest_news', 1); 
	$show_our_editors = get_option('quickrecipe_show_our_editors', 1); 
	$show_testimonials = get_option('quickrecipe_show_testimonials', 1); 
	$show_most_read = get_option('quickrecipe_show_most_read', 1); 
	
 	//get_template_part('includes/parts/post', 'latest'); 
	if ($show_search_area) {
		get_template_part( 'includes/mainpage/search-area' );
	}
	
	if ($show_latest_recipes) {
		get_template_part( 'includes/mainpage/latest-recipes' );
	}

	if ($show_offers) {
		get_template_part( 'includes/mainpage/offers' );
	}

	if ($show_most_favorited) {
		get_template_part( 'includes/mainpage/most-favorited' );
	}

	if ($show_latest_news) {
		get_template_part( 'includes/mainpage/latest-news' );
	}


	if ($show_editor_choice) {
		get_template_part( 'includes/mainpage/editor-choice' );
	}

	if ($show_our_editors) {
		get_template_part( 'includes/mainpage/our-editors' );
	}

	
	if ($show_testimonials) {
		get_template_part( 'includes/mainpage/testimonials' );
	}

	if ($show_most_read) {
		get_template_part( 'includes/mainpage/most-read' );
	}

get_footer(); 
?>