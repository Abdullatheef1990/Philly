<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Page Template
 */
get_header();

?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

            <?php
        	    if ( have_posts() ) { $count = 0;
        		    while ( have_posts() ) { the_post(); $count++;
            ?>
                <article <?php post_class(); ?>>

                    <section class="entry">
                	    <?php the_content(); ?>

					    <?php wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'quickrecipe' ), 'after' => '</div>' ) ); ?>
               	    </section>

                </article>

                <?php
			            if ( comments_open() || get_comments_number() ) {
				            comments_template();
			            }
				    }
			    } else {
		    ?>
                <p><strong><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'quickrecipe' ); ?></strong></p>
            <?php } ?>


          </div>
        </div>
      </div>
    </div>
    <!-- end:content -->


<?php get_footer(); ?>