<?php
if ( ! defined( 'ABSPATH' ) ) exit;

get_header(); 

?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
          <!-- begin:article -->
          <div class="col-md-9">


            <!-- begin:recipe -->
            <div class="row container-recipe">
		    <?php if (have_posts()) : $count = 0; ?>
			<?php while ( have_posts() ) : the_post(); $count++; 
                    $term_names = wp_get_post_terms($post->ID, 'course', array("fields" => "names"));
                    $term_list = array();
                    if (is_array($term_names)) {
                        foreach($term_names as $term_name) {
                            $term_list[] = $term_name;
                        }
                        $term_name_list = join( ", ", $term_list );
                    } else {
                        $term_name_list = '';
                    }
                    $recipe_badge = wp_get_post_terms($post->ID, 'recipe_badge', array("fields" => "names"));            
            ?>

              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item-container">
                  <div class="item-img-box">
			        <?php
			        if ( has_post_thumbnail( $post->ID ) ) {
				        echo get_the_post_thumbnail( $post->ID, array(400,400) );
			        }
                    ?>
                    <a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><div class="hover-button"><?php esc_html_e('View Recipe', 'quickrecipe'); ?></div></a>
                  </div>
                  <div class="item-meta-box">
                  <?php if ( is_tag() || 'post' == get_post_type($post->ID)) : ?>
                    <span><i class="fa fa-comments"></i> <?php comments_number( '0 comment', 'one comment', '% comments' ); ?></span>
                    <span><i class="fa fa-eye"></i> <?php echo esc_html(quickrecipe_get_post_views(get_the_ID())); ?></span>
                    <span><i class="fa fa-folder"></i> <?php $category = get_the_category ( get_the_ID() ); if (is_object($category)) { echo esc_html($category[0]->cat_name); } else { echo esc_html__('None', 'quickrecipe'); } ?></span>
                  <?php else: ?>
                    <span><i class="fa fa-clock-o"></i> <?php $prep_time = intval( get_post_meta(get_the_ID(), 'quickrecipe_prep_time', true) ); echo esc_attr(quickrecipe_get_mins_to_hours($prep_time)); ?></span>
                    <span><i class="fa fa-user"></i> <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true); if (!empty($servings)) { echo sprintf(wp_kses(__('%s People', 'quickrecipe'), ''), esc_attr($servings)); } ?></span>
                    <span><i class="fa fa-spoon"></i> <?php $yield = get_post_meta($post->ID, 'quickrecipe_yield', true); if (!empty($servings)) { echo sprintf(wp_kses(__('%s Yield', 'quickrecipe'), ''), esc_attr($yield)); } ?></span>
                  <?php endif; ?>
                  </div>
                  <div class="item-content-box">
                    <h3><a href="<?php the_permalink()?>"><?php echo esc_html(wp_trim_words(get_the_title(), 5, '...')); ?></a> <small><?php echo esc_html($term_name_list); ?></small></h3>
                    <div class="avatar">
                        <div class="clearfix">
                            <div class="pull-left">
                                <?php echo get_avatar( get_the_author_meta( 'ID' ), '25' ); ?>
                                <?php esc_html_e('By ', 'quickrecipe'); ?> <?php the_author_posts_link(); ?>
                            </div>
                            <span class="user-ratings pull-right">
                                <?php echo average_rating(); ?>
                            </span>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- break -->
              <?php endwhile; ?>
	        <?php else: ?>
	        <h1><?php echo esc_html__( 'Nothing Found', 'quickrecipe' ); ?></h1>
	        <p><?php echo esc_html__( 'Sorry, but nothing matched your search terms. Please try again with some different keywords or settings.', 'quickrecipe' ); ?></p>
	        <?php endif; ?>
            </div>
            <!-- end:recipe -->

            <!-- begin:pagination -->
            <?php quickrecipe_pagination_links( ); ?>
            <!-- end:pagination -->
          </div>
          <!-- end:article -->

          <!-- begin:sidebar -->
          <?php get_sidebar(); ?>
          <!-- end:sidebar -->
          
        </div>
      </div>
    </div>
    <!-- end:content -->

<?php get_footer(); ?>