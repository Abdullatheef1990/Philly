<?php 
/*

* Template Name: Members

*/ 

get_header(); 
?>
    <!-- begin:content -->
    <div id="latest">
      <div class="container">
        <div class="row">
        <?php
            $number     = get_option('posts_per_page');
            $paged      = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $offset     = ($paged - 1) * $number;
            $query      = get_users( 'blog_id=1&orderby=nicename&role=editor' );
            $users      = get_users('blog_id=1&orderby=nicename&role=editor&offset='.$offset.'&number='.$number);
            $total_users = count($query);
            $total_query = count($users);
            $total_pages = intval($total_users / $number) + 1;

        	if ( count($query) ) {
        		$count = 0;
                foreach ( $users as $user ) {
                    $count++;
                    $comments_count = $wpdb->get_var( $wpdb->prepare( 
	                    "
		                    SELECT SUM(comment_count)
		                    FROM $wpdb->posts 
		                    WHERE comment_count != '0' AND post_author = %s
	                    ", 
	                    $user->ID
                    ) );
        ?>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="item-container">
              <div class="item-img-box">
                <?php echo get_avatar( $user->ID, 400 ); ?>
                <a href="<?php echo esc_url(get_author_posts_url( $user->ID, get_the_author_meta( 'user_nicename' ) )); ?>"><div class="hover-button"><?php esc_html_e('Recipes', 'quickrecipe'); ?></div></a>
              </div>
              <div class="item-meta-box">
                <span><i class="fa fa-spoon"></i> <?php echo esc_html(count_user_posts( $user->ID , 'recipe' )); ?> <?php esc_html_e( 'Recipes', 'quickrecipe' ); ?></span>
                <span><i class="fa fa-comment-o"></i> <?php echo (($comments_count) ? esc_html($comments_count) : 0); ?> <?php esc_html_e( 'Comment', 'quickrecipe' ); ?></span>
              </div>
              <div class="item-content-box">
                <div class="author-details">
                    <h3 class="title"><a href="<?php echo esc_url(get_author_posts_url( $user->ID, get_the_author_meta( 'user_nicename' ) )); ?>"><?php echo esc_html( $user->display_name ); ?></a> <small><?php echo esc_html($user->user_title); ?></small></h3>
                    <?php if ( !empty($user->user_email) ) { ?>
                    <div class="contact">
                        <i class="fa fa-envelope-o"></i><a href="mailto:<?php echo antispambot($user->user_email,1); ?>"><?php echo antispambot($user->user_email); ?></a></div>
                    <?php } ?>
                    <?php if ( !empty($user->user_url) ) { ?>
                    <div class="contact">
                        <i class="fa fa-globe"></i><a href="<?php echo esc_url($user->user_url); ?>"><?php echo esc_html($user->user_url); ?></a></div>
                    <?php } ?>
                    <?php if ( !empty($user->user_phone) ) { ?>
                    <div class="contact">
                        <i class="fa fa-mobile"></i><?php echo esc_html($user->user_phone); ?></div>
                    <?php } ?>
                </div>
              </div>              
              <div class="item-button-box">
                <?php echo quickrecipe_get_author_social($user->ID); ?>
              </div>
            </div>
          </div>
          <!-- break -->
		<?php
        	} 
        } else {
        ?>
            <div class="noarticle">
	            <h1><?php esc_html_e( 'Nothing Found', 'quickrecipe' ); ?></h1>
	            <p><?php esc_html_e( 'There is no member available.', 'quickrecipe' ); ?></p>
            </div>
	    <?php } ?> 

        </div>
        <!-- end:latest -->
       </div>
    </div>

    <!-- end:content -->

    <?php
    if ($total_users > $total_query) {
        echo '<div class="pagination">';
            $current_page = max(1, get_query_var('paged'));
            echo paginate_links(array(
                'base' => get_pagenum_link(1) . '%_%',
                'format' => 'page/%#%/',
                'current' => $current_page,
                'total' => $total_pages,
                'prev_next'    => false,
                'mid_size'      => 5,
            ));
        echo '</div>';
    }
    ?>


<?php get_footer(); ?>