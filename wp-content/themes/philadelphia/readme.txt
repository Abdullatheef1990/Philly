Theme Name: QuickRecipe
Theme URI: http://www.ongoingthemes.com/quickrecipe/
Description: Quick recipe is a clean and responsive WordPress recipe theme for those who want to share their own or someone else's recipes. It allows you to publish your own recipe, customize the home page sections, build rating system, add to favorites and much more...
Author: OngoingThemes
Author URI: http://www.ongoingthemes.com
Version: 1.0
Copyright: (c) 2015-2016 Ongoing Themes.
License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, right-sidebar, custom-background, custom-colors, custom-header, custom-menu, editor-style, featured-images, flexible-header, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready
Text Domain: quickrecipe