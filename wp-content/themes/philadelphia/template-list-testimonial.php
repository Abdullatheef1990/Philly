<?php 
/*

* Template Name: List Testimonial 

*/ 



$page_id = $post->ID;
if (!is_user_logged_in()) { auth_redirect(); }
get_header(); 

$page_by_title = get_page_by_title( get_the_title() );
$the_excerpt = (!empty($page_by_title->post_excerpt)) ? $page_by_title->post_excerpt : $page_by_title->post_content;

?>
    <!-- begin:content -->
    <div id="content" class="recipe-form">
      <div class="container">
        <div class="row">
          <!-- begin:article -->
          <div class="col-md-9">
          <div class="row">
            <div class="col-xs-12 single-page">
				<div class="header text-left">
					<h2><?php the_title(); ?></h2>
					<p><?php echo esc_html($the_excerpt); ?></p>
					<hr>
				</div>
				<div class="content">
				<?php
				if(is_user_logged_in())
				{
                    $query = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' =>'-1', 'author' => get_current_user_id(), 'post_status' => array('publish', 'pending', 'draft', 'private', 'trash') ) );
                    if ($query->have_posts()) : 
                ?>
                <div class="table-scroll">
		        <table>
                    <thead>
			            <tr>
				            <th><?php esc_html_e( 'Recipe', 'quickrecipe' ); ?></th>
				            <th><?php esc_html_e( 'Status', 'quickrecipe' ); ?></th>
				            <th><?php esc_html_e( 'Actions', 'quickrecipe' ); ?></th>
			            </tr>
                    </thead>
                    <tbody>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
			            <tr>
				            <td class="small"><h4><?php echo get_the_title(); ?></h4><?php esc_html(the_excerpt()); ?></td>
				            <td><?php echo get_post_status( get_the_ID() ); ?></td>

				            <?php 
                                $submit_testimonial_page_id = get_option('quickrecipe_submit_testimonial_url', '');
                                $edit_post = add_query_arg('post', get_the_ID(), get_permalink($submit_testimonial_page_id)); 
                            ?>

				            <td>
					            <a href="<?php echo esc_url($edit_post); ?>" class="btn-success small"><i class="fa fa-pencil"></i> <?php esc_html_e( 'Edit', 'quickrecipe' ); ?></a>

                                <?php

                                    $current_user = wp_get_current_user();
                                    $author = get_the_author_meta('ID');

                                    if ( current_user_can('delete_posts') && ( $current_user->ID == $author ) ) {
		                                if( !(get_post_status() == 'trash') ) : ?>

			                                <a onclick="return confirm('Are you sure you wish to delete post: <?php echo esc_attr(get_the_title()); ?>?')" href="<?php echo esc_url(get_delete_post_link( get_the_ID() )); ?>" class="btn-warning small"><?php esc_html_e( 'Delete', 'quickrecipe' ); ?></a>

		                                <?php endif;
                                    }

                                ?>
				            </td>
			            </tr>

		            <?php endwhile; ?>
                    </tbody>
                </table>
                </div>	
	                <?php else: ?>
	                   <strong><?php esc_html_e( 'There are no testimonials available at this time.', 'quickrecipe' ); ?></strong>
	                <?php endif;
                 } ?>
				</div>
				<!-- end:content -->
			</div>
            </div>
          </div>
          <!-- end:article -->

          <!-- begin:sidebar -->
          <?php quickrecipe_get_user_sidebar($page_id); ?>
          <!-- end:sidebar -->

        </div>
      </div>
    </div>
    <!-- end:content -->

<?php get_footer(); ?>