<?php
if ( ! defined( 'ABSPATH' ) ) exit;

get_header();



if ( have_posts() ) { $count = 0;
    while ( have_posts() ) { the_post(); $count++;
        $cover_image = get_post_meta($post->ID, 'quickrecipe_cover_image', true);
        if (!empty($cover_image)) {
            $cover_image_url = wp_get_attachment_image_src( $cover_image, 'large');
        }
        else {
            $cover_image_url[0] = get_template_directory_uri() . '/img/img01.jpg';   
        }
        $is_recipe = ( is_singular( 'recipe' ) ) ? true : false;
?>


    <!-- begin:header -->
    <div id="header" class="page-heading" style="background-image: url(<?php echo esc_url($cover_image_url[0]); ?>);">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 col-sm-12">
            <div class="single-title-wrap">
                <div class="single-title-meta">
                    <div>
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <span><?php the_date(); ?></span>
                    
                    <?php
                        $posttags = get_the_tags($post->ID);
                        if (is_array($posttags)) {
                            ?>
                            <span class="tags">
                            <?php
                            $numItems = count($posttags);
                            $i = 0;
                            foreach ( $posttags as $tag ) {
                                $tag_link = get_tag_link( $tag->term_id );
                                echo '<a href="'.esc_url($tag_link).'" title="'.esc_attr($tag->name).'" class="'.esc_attr($tag->slug).'">'.esc_html($tag->name).'</a>';
                                if(++$i !== $numItems) { echo ', '; }
                            }
                            ?>
                            </span>
                            <?php
                        }
                    ?>
                </div>
                <div class="author-detail">
                    <figure class="single-title-author-img">
                        <?php echo get_avatar( get_the_author_meta( 'ID' ), '100' ); ?>
                    </figure>
                    <h4 class="cook-name"><?php the_author_posts_link(); ?></h4>
                </div>
            </div>
            <?php quickrecipe_breadcrumbs(); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- end:header -->

    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
          <!-- begin:article -->
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-12 single-post">
                <div id="single-content" class="inner-content">
                    <div class="row">
                      <div class="col-md-12">
                        <?php
                        $is_include_a_video = get_post_meta($post->ID, 'quickrecipe_is_include_a_video', true);
                        if($is_include_a_video == 'on')
                        {
                            $video_url = get_post_meta($post->ID, 'quickrecipe_embed_a_video', true);
                            ?>
                            <div id="slider-recipe" class="carousel">
                                <?php echo wp_oembed_get($video_url); ?>
                            </div>
                            <?php
                        } 
                        else 
                        {
                            $recipe_images = get_post_meta($post->ID, 'quickrecipe_more_images_recipe', false);
                            if ( count($recipe_images) ) {
                            ?>
                                <div id="slider-recipe" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                <?php
                                $count = 0;
                                foreach ( $recipe_images as $recipe_image ) {
                                    $recipe_image_url = wp_get_attachment_image_src($recipe_image); 
                                ?>
                                    <li data-target="#slider-recipe" data-slide-to="<?php echo esc_attr($count); ?>" class="<?php if ($count == 0) echo 'active'; ?>"><img src="<?php echo esc_url($recipe_image_url[0]); ?>"></li>
                                <?php
                                    $count++;
                                }
                                ?>
                                    </ol>

                                <div class="carousel-inner">
                                <?php
                                $count = 0;
                                foreach ( $recipe_images as $recipe_image ) {
                                    $recipe_image_url = wp_get_attachment_image_src($recipe_image, 'large'); 
                                ?>
                                    <div class="item <?php if ($count == 0) echo 'active'; ?>"><img src="<?php echo esc_url($recipe_image_url[0]); ?>"></div>
                                <?php
                                    $count++;
                                }
                                ?>
                                </div>
                                <a class="left carousel-control" href="#slider-recipe" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#slider-recipe" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>    
                            <?php          
                            }
                            elseif ( has_post_thumbnail() ) {
                            ?>
                            <div id="slider-recipe" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active"><?php the_post_thumbnail(); ?></div>
                                </div>
                            </div>    
                            <?php
                            }
                        }
                        ?>

                        <div class="recipe-meta">
                            <ul>
                                <?php 
                                if ( $is_recipe ) {
                                ?>

                                <li><?php esc_html_e( 'Prep Time', 'quickrecipe' ); ?><strong>
                                    <i class="fa fa-clock-o"></i> <?php echo esc_html(get_post_meta($post->ID, 'quickrecipe_prep_time', true)); ?> <?php esc_html_e( 'minutes', 'quickrecipe' ); ?></strong> </li>
                                <li><?php esc_html_e( 'Cook Time', 'quickrecipe' ); ?><strong>
                                    <i class="fa fa-clock-o"></i> <?php echo esc_html(get_post_meta($post->ID, 'quickrecipe_cook_time', true)); ?> <?php esc_html_e( 'minutes', 'quickrecipe' ); ?></strong> </li>
                                <li><?php esc_html_e( 'Serving', 'quickrecipe' ); ?><strong>
                                    For <?php echo esc_html(get_post_meta($post->ID, 'quickrecipe_servings', true)); ?> <?php esc_html_e( 'People', 'quickrecipe' ); ?> </strong></li>
                                <li><?php esc_html_e( 'Difficulty', 'quickrecipe' ); ?><strong>
                                    <?php 
                                        $terms = get_the_terms( get_the_ID(), 'difficulty_level' );
                         
                                        if ( $terms && ! is_wp_error( $terms ) ) : 
 
                                            $difficulty_levels = array();
 
                                            foreach ( $terms as $term ) {
                                                $difficulty_levels[] = $term->name;
                                            }
                         
                                            $difficulty_level_text = join( ", ", $difficulty_levels );
                                            ?>
                                            <?php printf( esc_html__( '%s', 'quickrecipe' ), esc_html( $difficulty_level_text ) ); ?>
                                        <?php endif; ?>                                    
                                    </strong> 
                                </li>
                                <?php 
                                } 
                                else { ?>
                                <li><?php esc_html_e( 'Views', 'quickrecipe' ); ?><strong><?php echo esc_html(quickrecipe_get_post_views(get_the_ID())); ?></strong> </li>
                                <li><?php esc_html_e( 'Comments', 'quickrecipe' ); ?><strong><i class="fa fa-comments-o"></i> <?php comments_number( 'no responses', 'one response', '% responses' ); ?></strong> </li>
                                <?php } ?>
                            </ul>
                            <div class="button-box">
                                <?php 
                                $postURL = urlencode(get_permalink());
                                $postTitle = str_replace( ' ', '%20', get_the_title());
                                $mailto = "mailto:?subject=".str_replace( ' ', '%20', get_bloginfo('name')).'-'.$postTitle."&amp;body=".$postTitle.":".$postURL; 
                                ?>
                                <a id="printcontent" rel="nofollow" class="print print-friendly"><i class="fa fa-print"></i> <?php esc_html_e( 'Print', 'quickrecipe' ); ?></a> 
                                <a href="<?php echo esc_url($mailto); ?>" class="email"><i class="fa fa-envelope"></i> <?php esc_html_e( 'E-mail', 'quickrecipe' ); ?></a>
                                <?php if ( $is_recipe ) echo quickrecipe_get_favorite_button(); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="printable">
                        <?php 
                        if ( $is_recipe ) {
                        ?>
                        <h3 class="section-title"><?php esc_html_e( 'Recipe Description', 'quickrecipe' ); ?></h3>
                        <?php } ?>
                        <?php the_content(); ?>

                        <?php 
                        $page_links_args = array(
	                        'before' => '<ul class="pagination">',
	                        'after' => '</ul>',
	                        'before_link' => '<li>',
	                        'after_link' => '</li>',
	                        'current_before' => '<li class="active">',
	                        'current_after' => '</li>',
	                        'previouspagelink' => '&laquo;',
	                        'nextpagelink' => '&raquo;'
                        );
                        quickrecipe_bootstrap_link_pages( $page_links_args );
                        if ( $is_recipe ) {
                        ?>

                        <div class="row">
                          <div class="col-md-6 col-sm-6">
                            <h3 class="section-title"><?php esc_html_e( 'Recipe Ingredient', 'quickrecipe' ); ?></h3>
                            <div class="ingredients-list">
                                <?php echo quickrecipe_list_ingredients(); ?>
					        </div>
                           </div>
                           <div class="col-md-6 col-sm-6">
                            <div class="directions">
                            <!-- Directions -->
                                <h3 class="section-title"><?php esc_html_e( 'Instructions', 'quickrecipe' ); ?></h3>
                                <div class="instructions">
                                    <?php echo quickrecipe_list_method_steps(); ?>
                                </div>
                            </div>
                           </div>                    
                        </div>
                        <div class="recipe-footer">
                            <a href="#popup-nutrition<?php echo esc_attr($post->ID); ?>">
                                <i class="fa fa-info"></i> <?php esc_html_e( 'Nutrition', 'quickrecipe' ); ?>
                            </a>
                            <a href="#popup-allergen<?php echo esc_attr($post->ID); ?>" class="allergen">
                                <i class="fa fa-exclamation-triangle"></i> <?php esc_html_e( 'Allergen', 'quickrecipe' ); ?>
                            </a>
                            <a href="#popup-social<?php echo esc_attr($post->ID); ?>" class="share">
                                <i class="fa fa-share"></i> <?php esc_html_e( 'Share', 'quickrecipe' ); ?>
                            </a>
                        </div>
                        <?php get_template_part( 'includes/html/popup-social' ); ?>
                        <div id="popup-nutrition<?php echo esc_attr($post->ID); ?>" class="overlay">
                            <div class="popup">
                                <h2>
                                    <?php esc_html_e( 'Nutrition Facts', 'quickrecipe' ); ?>
                                </h2>
                                <a class="close-popup" href="#">&times;</a>
                                <div class="content">
                                    <?php echo quickrecipe_list_nutritions(); ?>
                                </div>
                            </div>
                        </div>
                        <div id="popup-allergen<?php echo esc_attr($post->ID); ?>" class="overlay">
                            <div class="popup">
                                <h2>
                                    <?php esc_html_e( 'Allergens', 'quickrecipe' ); ?>
                                </h2>
                                <a class="close-popup" href="#">&times;</a>
                                <div class="content">
                                <?php
                                $allergen_terms = get_the_terms( $post->ID, 'allergen' );
                                if( $allergen_terms && !is_wp_error( $allergen_terms ))
                                {
                                ?>
                                    <ul>
                                    <?php
	                                foreach($allergen_terms as $allergen)
	                                {
                                    ?>
                                        <li><?php echo esc_html($allergen->name); ?></li>
                                    <?php
	                                }
                                    ?>
                                    </ul>
                                <?php
                                }
                                ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        </div>
            <?php
                    get_template_part('includes/related-recipes');
            ?>
            <div class="navigation post-navigation clearfix">
				<div class="alignleft post-nav-links prev-link-wrapper">
					<?php
					$prev_post = get_previous_post();
                    $prev_label = ( $is_recipe ) ? esc_html__('Previous Recipe:', 'quickrecipe') : esc_html__('Previous Article:', 'quickrecipe');
					if (!empty( $prev_post )): ?>								
						<a href="<?php echo esc_url(get_permalink( $prev_post->ID )); ?>" title="<?php echo esc_attr($prev_label) . ' ' . esc_attr( get_the_title($prev_post) ); ?>" rel="prev"><div class="prev-link"><span><?php echo esc_html($prev_label); ?></span> <?php echo esc_attr( wp_trim_words(get_the_title($prev_post), 8, '...') ); ?></div></a>
					<?php endif; ?>
                </div>
				<div class="alignright post-nav-links next-link-wrapper">
					<?php
					$next_post = get_next_post();
                    $next_label = ( $is_recipe ) ? esc_html__('Next Recipe:', 'quickrecipe') : esc_html__('Next Article:', 'quickrecipe');
					if (!empty( $next_post )): ?>									
						<a href="<?php echo esc_url(get_permalink( $next_post->ID )); ?>" title="<?php echo esc_attr($next_label) . ' ' . esc_attr( get_the_title($next_post) ); ?>" rel="next"><div class="next-link"><span><?php echo esc_html($next_label); ?></span> <?php echo esc_attr( wp_trim_words(get_the_title($next_post), 8, '...') ); ?></div></a>
					<?php endif; ?>

                </div>
			</div><!-- end .navigation -->

            <?php
            		comments_template();
				} // End WHILE Loop
                quickrecipe_set_post_views(get_the_ID());
			} else {
		?>
	        <h1><?php esc_html_e( 'Nothing Found', 'quickrecipe' ); ?></h1>
	        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'quickrecipe' ); ?></p>
       	<?php } ?>


                      </div>

                    </div>

                  <!-- break -->
                </div>
              </div>
            </div>

          </div>
          <!-- end:article -->

          <!-- begin:sidebar -->
         <?php get_sidebar(); ?>
          <!-- end:sidebar -->
          
        </div>
      </div>
    </div>
    <!-- end:content -->

<?php get_footer(); ?>
