<?php 
/*

* Template Name: Submit tips EN

*/ 
if (!is_user_logged_in()) { auth_redirect(); }

$successful = false;

$tip_type_id = 0;
$occassion_id = 0;
$content = $excerpt = null;

if (isset($_POST['submitted']) && $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {

	$title = sanitize_text_field($_POST['title']);
	$description = sanitize_text_field($_POST['mycustomeditor']);

	$tip_info = array(
		'post_title'	=>	$title,
		'post_content'	=>	$description,
		'post_status'	=>	'pending',
		'post_type'	    =>	'tips'
		);

	if(is_user_logged_in())
	{
		global $current_user;
		wp_get_current_user();
		$tip_info['post_author'] = $current_user->ID;
	}
	if(isset($_GET['post'])) {
		$tip_info['ID'] = intval($_GET['post']);
		$tip_id = wp_update_post($tip_info);
	}
	else
	{
		$tip_id = wp_insert_post($tip_info);
	}

	foreach($_POST['steps'] as $key => $value) {
        $field_value[] = array( "steps"	=> $value );
    }
    $field_steps_key = "field_5979eb6bdabb8";
    update_field( $field_steps_key, $field_value, $tip_id );

	if(isset($_POST['tip_type']) && ($_POST['tip_type'] != "-1") )
		wp_set_object_terms( $tip_id , intval($_POST['tip_type']), 'tip_type' );

	if(isset($_POST['occassion']) && ($_POST['occassion'] != "-1"))
		wp_set_object_terms( $tip_id , intval($_POST['occassion']), 'occassion' );

	if($_FILES)
	{
		foreach ($_FILES as $file => $array)
		{
			if ($file == 'featured_image') {
				$size = intval( $_FILES['featured_image']['size'] );
				if ( $size > 0 )
				{
					$thumbnail_id = quickrecipe_insert_attachment($file,$tip_id, true);
					set_post_thumbnail( $tip_id, $thumbnail_id );
				}
			}
		}
	}

	$field_video_key = "field_597b433e9472a";
	$field_video_value = $_POST['embed_a_video'];
    update_field( $field_video_key, $field_video_value, $tip_id );

	$successful = true;

	if(function_exists('quickrecipe_email_submitted_recipe')) {
		quickrecipe_email_submitted_recipe( $current_user->ID,  $title);
	}
}

$post_id = isset($_GET['post']) ? intval(wp_kses($_GET['post'], '')) : '';

$query = new WP_Query(array('p' => $post_id, 'post_type' => 'tips', 'posts_per_page' =>'-1', 'post_status' => array('publish', 'pending', 'draft', 'private', 'trash') ) );

if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();

if(!empty($post_id)) {
	if($post_id == $post->ID)
	{
		$current_post = $post->ID;
		$title = get_the_title();
		$content = get_the_content();
		$excerpt = get_the_excerpt();
		if( have_rows('tip_steps') ):
		    while( have_rows('tip_steps') ) : the_row();		        
		        $post_meta['steps'] = get_sub_field('steps');
		    endwhile;
		endif;
		 
		$embeded_video = get_post_meta($post->ID, 'quickrecipe_embed_a_video', true);

		$tip_type_terms = get_the_terms( $post->ID, 'tip_type' );
		$tip_type_id = $recipe_type_terms[0]->term_id;

		$occassion_terms = get_the_terms( $post->ID, 'occassion' );
		$occassion_id = $course_terms[0]->term_id;
	}
} else {
    $post_meta['steps'] = null;
}
endwhile; endif;
wp_reset_postdata();

global $current_post;

do_action('wp_insert_post', 'wp_insert_post');

get_header(); 

$page_by_title = get_page_by_title( get_the_title() );
$the_excerpt = (!empty($page_by_title->post_excerpt)) ? $page_by_title->post_excerpt : $page_by_title->post_content;
$slide_intro = get_field('vp_header_title');		
$slide_image = get_field('vp_header_image');
?>
<div class="row full-width header-box">
	<div class="main-image" style="background-image: url(<?php echo $slide_image; ?>)">
		<div class="master-slider ms-skin-default" id="masterslider">
			<div class="ms-slide">
				<img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt=""/>
				<div class="row main-container align-center full-width ms-layer ms-caption">
					<div class="column large-6 column align-self-middle">
						<?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>                        
 					<?php } ?>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>
    <!-- begin:content -->
<div id="content" class="user-account-box row">
	<div class="columns full-width user-account-header">SUBMIT TIPS</div>
    	<div class="columns large-9 small-12">
			<div class="content">
				<?php
				if(is_user_logged_in())
				{
					if(!$successful)
					{
						$recipe_images = get_post_meta($post->ID, 'quickrecipe_more_images_recipe', false);
					?>
						<form id="recipe-form" name="recipe-form" action="<?php echo get_permalink(); ?>" method="post" enctype="multipart/form-data" class="row submit-recipe-form">
							<div class="columns large-12 small-12">
								<label><?php esc_html_e('Tip Name','quickrecipe');?></label>
								<input type="text" id="title" value="<?php echo esc_attr($title); ?>" tabindex="1" name="title" class="form-tip-title form-control input-lg required" placeholder="" />
								<div class="sub-text">
									<?php esc_html_e('Keep it short and descriptive', 'quickrecipe'); ?>
								</div>
							</div>
							
							<div class="columns large-12 small-12">
								<label><?php esc_html_e('Tip Photo','quickrecipe');?></label>
                            	<div class="upload">
                               		<?php if (has_post_thumbnail( $post->ID ) ) { ?>
                                		<input type="file" name="featured_image" id="featured_image" class="form-tip-image form-control input-lg" />
                                	<?php } else { ?>
                                		<input type="file" name="featured_image" id="featured_image" class="form-tip-image form-control input-lg required" />
                                	<?php } ?>
                                	<input type="hidden" name="hidden_tip_image" class="hidden-tip-image" id="hidden-tip-image" />
                            	</div>
                            	<div class="sub-text">
                            		<?php esc_html_e('Recommended size: 2000px by 600px or larger', 'quickrecipe'); ?>
                            	</div>                      
                        	</div>
                        	
                        	<div class="columns large-12 small-12">
                        		<label>
                        			<?php esc_html_e('Tip Video','quickrecipe');?>
                        		</label>
                        		<input type="text" class="form-tip-video form-control input-lg" name="embed_a_video" id="embed-a-video" value="<?php echo esc_attr(isset($embeded_video)); ?>"  placeholder="Please enter an url for recipe video">
                        		<div class="sub-text">
                        			<?php esc_html_e('OPTIONAL:', 'quickrecipe'); ?> 
                        			<?php esc_html_e('You can embed a video on your recipe page and then people can watch your video. If you have your recipe video on Youtube, then you\'ll want to use the field above. In the pop-up window, you\'ll see a field containing an embed code for the video. Just copy the that code and paste it into above field.','quickrecipe');?>
                        		</div>
                        	</div>

                        	<div class="columns large-4 small-12">
                        		<label>
                        			<?php esc_html_e('By Occasion','quickrecipe');?>
                        		</label>
	                        	<?php
	                        	$course_args = array(
	                        		'show_option_none' => __( ' - Choose Occassion -', 'quickrecipe' ),
	                        		'show_count'       => 0,
	                        		'option_none_value'  => '0',
	                        		'hide_empty'         => 0, 
	                        		'orderby'          => 'SLUG',
	                        		'echo'             => 1,
	                        		'selected'           => $occassion_id,
	                        		'name'             => 'occassion',
	                        		'id'               => '',
	                        		'class'            => 'form-tip-occassion form-control input-lg',
	                        		'hierarchical'     => 1,		
	                        		'depth'            => 0,
	                        		'tab_index'        => 1,
	                        		'taxonomy'         => 'occassion',
	                        		);
	                        	wp_dropdown_categories( $course_args );
	                        	?>
                        	</div>

                        	<div class="columns large-4 small-12">
                        		<label>
                        			<?php esc_html_e('By Type','quickrecipe');?>
                        		</label>
	                        	<?php
	                        	$season_args = array(
	                        		'show_option_none' => __( ' - Choose Tip Type - ', 'quickrecipe' ),
	                        		'show_count'       => 0,
	                        		'option_none_value'  => '0',
	                        		'hide_empty'         => 0, 
	                        		'orderby'          => 'name',
	                        		'echo'             => 1,
	                        		'selected'           => $tip_type_id,
	                        		'name'             => 'tip_type',
	                        		'id'               => '',
	                        		'class'            => 'form-tip-type form-control input-lg',
	                        		'depth'            => 0,
	                        		'tab_index'        => 0,
	                        		'taxonomy'         => 'tip_type',
	                        		);
	                        	wp_dropdown_categories( $season_args );
	                        	?>
                        	</div>

                        	<!-- <div class="columns large-4 small-12">
                        		<label>
                        			<?php esc_html_e('Submitted By','quickrecipe');?>
                        		</label>
	                        	<?php
	                        	$season_args = array(
	                        		'show_option_none' => __( ' - Choose Season - ', 'quickrecipe' ),
	                        		'show_count'       => 0,
	                        		'option_none_value'  => '0',
	                        		'hide_empty'         => 0, 
	                        		'orderby'          => 'name',
	                        		'echo'             => 1,
	                        		'selected'          => $season_id,
	                        		'name'             => 'season',
	                        		'id'               => '',
	                        		'class'            => 'form-control input-lg',
	                        		'depth'            => 0,
	                        		'tab_index'        => 0,
	                        		'taxonomy'         => 'season',
	                        		);
	                        	wp_dropdown_categories( $season_args );
	                        	?>
	                        </div> -->

                        	<div class="form-group col-sm-12">
                        		<label>
                        			TIP DESCRIPTION
                        			<span class="sub-title">(optional)</span>
                        		</label>
	                        	<?php
	                        	$content = '';
	                        	$editor_id = 'mycustomeditor';
	                        	wp_editor( $content, $editor_id );
	                        	?>
	                        </div>

                        	<div class="form-group col-sm-12">
                        		<label>
                        			<?php esc_html_e('Tip Steps', 'quickrecipe'); ?>
                        		</label>
                        		<div class="steps-textarea">
	                        		<?php
	                        		if ( count($post_meta['steps']) ) {
	                        			foreach($post_meta['steps'] as $ingredient)
	                        			{
	                        				?>
	                        				<input type="text" name="steps[]" id="steps" class="form-tip-steps form-control input-lg" size="50" value="<?php echo esc_attr($ingredient); ?>">
	                        				<?php
	                        			}
	                        		}
	                        		else {
	                        			?>
	                        			<input type="text" name="steps[]" id="steps" class="form-tip-steps form-control input-lg" size="50" class="required" value="<?php echo esc_attr($post_meta['steps']); ?>">
	                        			<?php 
	                        		}
	                        		?>
	                        		<div class="more-field"></div>
	                        	</div>
                        		<div class="sub-text">
                        			<?php esc_html_e('Add Steps, one step at a time.','quickrecipe'); ?>
                        		</div>
                        		<span class="button add-ingredient add-more"><?php esc_html_e('Add Method steps','quickrecipe'); ?></span>
	                        	<br>
	                        	<br>
	                        	<br>
                        		<span id="show-preview" class="button add-ingredient"><?php esc_html_e('Preview','quickrecipe'); ?></span>
                        	</div>

	                        <div class="col-sm-12">
	                        	<button type="submit" name="submit" class="submit button form-submit-btn">
	                        		<?php esc_html_e('Submit','quickrecipe'); ?> <i class="fa fa-arrow-right"></i>
	                        	</button>
	                        	<div class="sub-text col-sm-12 text-right">
	                        		<strong>
	                        			<?php esc_html_e('Are you sure you want save the changes.','quickrecipe');?>
									</strong>
								</div>
	                        </div>
                        	<?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>
                        	<input type="hidden" name="submitted" id="submitted" value="true" />
                    	</form>
            		<?php } else { ?>
                	<div class="successful-update">
                		<h3><?php esc_html_e('Thank you for submitting your Tip','quickrecipe');?></h3>
                		<span><?php echo __('We will publish your tip after review.','quickrecipe');?></span>
                	</div>
                	<?php
                }
            }
            ?>
        </div>
        <!-- end:content -->
    </div>
    <!-- end:article -->
    <!-- begin:sidebar -->
    <div class="columns large-3 small-12">
    	<?php quickrecipe_get_user_sidebar(); ?>
    </div>
    <!-- end:sidebar -->
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#featured_image").change(function(){
	        readURL(this);
	    });

	  	jQuery('#show-preview').click(function() {
	    	jQuery("#myModal").modal('show');
	    	if(jQuery('.form-tip-title').val() != "") {
	    		jQuery('.tip-title').removeClass("hide");
	    		var tip_title = jQuery('.form-tip-title').val();	
	    	} else {
	    		jQuery('.tip-title').addClass("hide");
	    	}	    	
	    	
	    	if(jQuery('.hidden-tip-image').val() != "" ) {
	    		jQuery('.tip-image').removeClass("hide");
	    		var tip_image = jQuery('.hidden-tip-image').val();	
	    	} else {
	    		jQuery('.tip-image').addClass("hide");
	    	}
	    	
	    	if(jQuery('.form-tip-video').val() == "") {
	    		jQuery('.tip-video').addClass("hide");
	    	} else {
	    		jQuery('.tip-video').removeClass("hide");
	    		var tip_video = jQuery('.form-tip-video').val();	
	    	}
	    	
	    	if(jQuery('.hidden-tip-image').val() == "" && jQuery('.form-tip-video').val() == "") {
	    		jQuery('#myCarousel-tips-preview').addClass("hide");
	    		jQuery('.shareflies').addClass("hide");
	    	} else if(jQuery('.hidden-tip-image').val() != "" || jQuery('.form-tip-video').val() != "") {
	    		jQuery('#myCarousel-tips-preview').removeClass("hide");
	    		jQuery('.shareflies').removeClass("hide");
	    	} 

	    	if(jQuery('.hidden-tip-image').val() != "" && jQuery('.form-tip-video').val() != "") {
	    		var carousel_inner = "<div id='myCarousel-tips-preview' class='carousel slide' data-ride='carousel'><div class='carousel-inner'><div class='item active'><img class='tip-image' src='"+tip_image+"' alt='your image' /></div><div class='item'><iframe class='tip-video' id='tip-video' width='100%' height='400' src='https://www.youtube.com/embed/"+tip_video+"' frameborder='0' allowfullscreen></iframe></div></div><a class='left carousel-control' href='#myCarousel-tips-preview' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span><span class='sr-only'>Previous</span></a><a class='right carousel-control' href='#myCarousel-tips-preview' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span><span class='sr-only'>Next</span></a></div>";
	    	} else if(jQuery('.hidden-tip-image').val() != "" && jQuery('.form-tip-video').val() == "") {
	    		var carousel_inner = "<div id='myCarousel-tips-preview' class='carousel slide' data-ride='carousel'><div class='carousel-inner'><div class='item active'><img class='tip-image' src='"+tip_image+"' alt='your image' /></div></div><a class='left carousel-control' href='#myCarousel-tips-preview' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span><span class='sr-only'>Previous</span></a><a class='right carousel-control' href='#myCarousel-tips-preview' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span><span class='sr-only'>Next</span></a></div>";
	    	} else if(jQuery('.hidden-tip-image').val() == "" && jQuery('.form-tip-video').val() != "") {
	    		var carousel_inner = "<div id='myCarousel-tips-preview' class='carousel slide' data-ride='carousel'><div class='carousel-inner'><div class='item active'><iframe class='tip-video' id='tip-video' width='100%' height='400' src='https://www.youtube.com/embed/"+tip_video+"1' frameborder='0' allowfullscreen></iframe></div></div><a class='left carousel-control' href='#myCarousel-tips-preview' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span><span class='sr-only'>Previous</span></a><a class='right carousel-control' href='#myCarousel-tips-preview' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span><span class='sr-only'>Next</span></a></div>";
	    	}

	    	var tip_occassion_selected = jQuery('#occassion option:selected');
	    	if(jQuery('.form-tip-occassion').val() != 0) {
	    		var tip_occassion = tip_occassion_selected.html();	
	    	}
	    	
	    	var tip_type_selected = jQuery('#tip_type option:selected');
	    	if(jQuery('.form-tip-type').val() != 0) {
	    		var tip_type = tip_type_selected.html();
	    	}
	    	
	    	var tip_description = tinymce.get('mycustomeditor').getContent();
	    	
	    	var tip_steps = jQuery(".form-tip-steps");
	    	if(jQuery(tip_steps[0]).val() == "") {
	    		jQuery('.instruction').addClass("hide");
	    	} else {
	    		jQuery('.instruction').removeClass("hide");
	    	}
	    	var stepsDiv = "<ol>";
	    	for(var i = 0; i < tip_steps.length; i++) {
			    var steps_values = jQuery(tip_steps[i]).val();
			    if(steps_values != "") {
			    	stepsDiv += '<li class="tip-steps">'+steps_values+'</li>';	
			    }
			}
	    	stepsDiv += "</ol>";
	    	jQuery('.tip-title').html(tip_title);
	    	jQuery('.tip-occassion').html(tip_occassion);
	    	jQuery('.tip-type').html(tip_type);
	    	jQuery('.tip-description').html(tip_description);
	    	jQuery('.tip-steps').html(stepsDiv);
	    	jQuery('.carousel-list').html(carousel_inner);
	  	});
	});

	function readURL(input) {
		if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                jQuery('.hidden-tip-image').attr('value', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<div class="modal fade tips-model" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<div class="carousel-list"></div>
				<div class="shareflies">
					<ul>
						<li><a class="share" href=""></a></li>
						<li><a class="email" href=""></a></li>
						<li><a class="print" href=""></a></li>

					</ul>
				</div>
				<div class="tip-title"></div>
				<div class="tip-description"></div>
				<h2 class="instruction">Instruction</h2>
				<span class="tip-steps"></span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn footer-btn" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- end:content -->
<?php get_footer(); ?>