<?php 
/*

* Template Name: Submit Recipe  AR

*/ 

if (!is_user_logged_in()) { auth_redirect(); }

$fields = array('prep_time', 'cook_time', 'servings', 'yield', 'ready_in', 'ingredients', 'method_steps', 'is_include_a_video', 'embed_a_video');

$successful = false;

$course_id = 0;
$recipe_type_id = 0;
$cuisine_id = 0;
$skill_level_id = 0;
$allergen_id = 0;
$season_id = 0;
$ingredient_id = 0;
$content = $excerpt = null;

if (isset($_POST['submitted']) && $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {

    $title = sanitize_text_field($_POST['title']);
	$short_description = sanitize_text_field($_POST['short-description']);
	$description = sanitize_text_field($_POST['description']);

	$recipe_info = array(
		'post_title'	=>	$title,
		'post_content'	=>	$description,
		'post_excerpt'	=>	$short_description,
		'post_status'	=>	'pending',
		'post_type'	    =>	'recipe'
	);

	if(is_user_logged_in())
	{
		global $current_user;
		wp_get_current_user();
		$recipe_info['post_author'] = $current_user->ID;
	}
    if(isset($_GET['post'])) {
        $recipe_info['ID'] = intval($_GET['post']);
        $recipe_id = wp_update_post($recipe_info);
    }
	else
    {
        $recipe_id = wp_insert_post($recipe_info);
    }

	// Attache Post Meta
    foreach($_POST as $key => $value) {
        if ( in_array( $key, $fields ) ) {
            $value = wp_kses($value, '');
            if(!empty($value)) {
                if(isset($_GET['post'])) {
                    update_post_meta($recipe_id, 'quickrecipe_'.$key, $value);
                }
                else {
                    add_post_meta($recipe_id, 'quickrecipe_'.$key, $value, true);
                }
            }
        }
    }

	if(isset($_POST['recipe-type']) && ($_POST['recipe-type'] != "-1") )
		wp_set_object_terms( $recipe_id , intval($_POST['recipe-type']), 'recipe_type' );

	if(isset($_POST['difficulty-level']) && ($_POST['difficulty-level'] != "-1"))
		wp_set_object_terms( $recipe_id , intval($_POST['difficulty-level']), 'difficulty_level' );

	if(isset($_POST['cuisine']) && ($_POST['cuisine'] != "-1"))
		wp_set_object_terms( $recipe_id , intval($_POST['cuisine']), 'cuisine' );

	if(isset($_POST['course']) && ($_POST['course'] != "-1"))
		wp_set_object_terms( $recipe_id , intval($_POST['course']), 'course' );

	if(isset($_POST['allergen']) && ($_POST['allergen'] != "-1"))
		wp_set_object_terms( $recipe_id , intval($_POST['allergen']), 'allergen' );

	if(isset($_POST['season']) && ($_POST['season'] != "-1"))
		wp_set_object_terms( $recipe_id , intval($_POST['season']), 'season' );

	if(isset($_POST['ingredient']) && ($_POST['ingredient'] != "-1"))
		wp_set_object_terms( $recipe_id , intval($_POST['ingredient']), 'ingredient' );

	if($_FILES)
	{
		foreach ($_FILES as $file => $array)
		{
            if ($file == 'featured-image') {
		        $size = intval( $_FILES['featured-image']['size'] );
		        if ( $size > 0 )
		        {
			        $thumbnail_id = quickrecipe_insert_attachment($file,$recipe_id, true);
                    set_post_thumbnail( $recipe_id, $thumbnail_id );
                }
            }

            if ($file == 'cover-image') {
		        $size = intval( $_FILES['cover-image']['size'] );
		        if ( $size > 0 )
		        {
				    $attach_id = quickrecipe_insert_attachment($file,$recipe_id, false);
                    update_post_meta($recipe_id,'quickrecipe_cover_image',$attach_id);
                }
            }

            if ($file == 'recipe-image') {
		        $size = intval( $_FILES['recipe-image']['size'] );
		        if ( $size > 0 )
		        {
				    $attach_id = quickrecipe_insert_attachment($file,$recipe_id, false);
                    update_post_meta($recipe_id,'quickrecipe_more_images_recipe',$attach_id);
                }
            }


		}
	}

	$successful = true;

    if(function_exists('quickrecipe_email_submitted_recipe')) {
        quickrecipe_email_submitted_recipe( $current_user->ID,  $title);
    }

}
//if(isset($_GET['post'])) {
	$post_id = isset($_GET['post']) ? intval(wp_kses($_GET['post'], '')) : '';

    $query = new WP_Query(array('p' => $post_id, 'post_type' => 'recipe', 'posts_per_page' =>'-1', 'post_status' => array('publish', 'pending', 'draft', 'private', 'trash') ) );

    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
	
	    if(!empty($post_id)) {
		
		    if($post_id == $post->ID)
		    {
			    $current_post = $post->ID;

			    $title = get_the_title();
			    $content = get_the_content();
                $excerpt = get_the_excerpt();
                foreach($fields as $field) {
                    $post_meta[$field] = get_post_meta($current_post, 'quickrecipe_'.$field, true);
                }

                $recipe_type_terms = get_the_terms( $post->ID, 'recipe_type' );
                $recipe_type_id = $recipe_type_terms[0]->term_id;

                $course_terms = get_the_terms( $post->ID, 'course' );
                $course_id = $course_terms[0]->term_id;

                $cuisine_terms = get_the_terms( $post->ID, 'cuisine' );
                $cuisine_id = $cuisine_terms[0]->term_id;

                $skill_level_terms = get_the_terms( $post->ID, 'difficulty_level' );
                $skill_level_id = $skill_level_terms[0]->term_id;

                $allergen_terms = get_the_terms( $post->ID, 'allergen' );
                $allergen_id = $allergen_terms[0]->term_id;

                $season_terms = get_the_terms( $post->ID, 'season' );
                $season_id = $season_terms[0]->term_id;

                $ingredient_terms = get_the_terms( $post->ID, 'ingredient' );
                $ingredient_id = $ingredient_terms[0]->term_id;

		    }
	    }
        else {
            foreach($fields as $field) {
                $post_meta[$field] = null;
            }
        }

    endwhile; endif;
    wp_reset_postdata();

    global $current_post;
//}

do_action('wp_insert_post', 'wp_insert_post');

get_header(); 

$page_by_title = get_page_by_title( get_the_title() );
$the_excerpt = (!empty($page_by_title->post_excerpt)) ? $page_by_title->post_excerpt : $page_by_title->post_content;

$slide_intro = get_field('vp_header_title');		
$slide_image = get_field('vp_header_image');
?>
   <div class="row full-width header-box">
	<div class="main-image" style="background-image: url(<?php echo $slide_image; ?>)">
		<div class="master-slider ms-skin-default" id="masterslider">
			<div class="ms-slide">
				<img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt=""/>
				<div class="row main-container align-center full-width ms-layer ms-caption">
					<div class="column large-6 column align-self-middle">
						<?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>
                        
 					<?php } ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>
    <!-- begin:content -->
    <div id="content" class="user-account-box row">
    		 <div class="columns full-width user-account-header">SUBMIT RECIPE
					<?php /*?><h2><?php esc_html_e('Welcome back, Recipe Contributor!','quickrecipe');?></h2>
					<p><?php echo esc_html($the_excerpt); ?></p>
					<hr><?php */?>
				</div>
      
           <div class="columns large-9 small-12">
				
				<div class="content">
				<?php
				if(is_user_logged_in())
				{
					if(!$successful)
					{
					?>

					<form id="recipe-form" name="recipe-form" action="<?php echo get_permalink(); ?>" method="post" enctype="multipart/form-data" class="row submit-recipe-form">
						<div class="columns large-6 small-12">
							<label><?php esc_html_e('عنوان الوصفة','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-title - a class -->
							<input type="text" id="title" value="<?php echo esc_attr($title); ?>" tabindex="1" name="title" class="form-recipe-title form-control input-lg required" placeholder="" />
							<!-- VnD Dev // END -->
							<div class="sub-text"><?php esc_html_e('أبقيها قصيرة وسهلة', 'quickrecipe'); ?></div>
						</div>
						<div class="columns large-6 small-12">
							<label><?php esc_html_e('صورة الوصفة','quickrecipe');?></label>
                            <div class="upload">
                            	<!-- VnD Dev // START 
                            	Added 'form-recipe-image - a class -->
                                <input type="file" value="" name="featured-image" id="featured-image" class="form-recipe-image form-control input-lg required">
                                <!-- VnD Dev // END -->
                              <?php /*?>  <span class="filename" style="-webkit-user-select: none;"><?php esc_html_e('No file selected', 'quickrecipe'); ?></span>
                                <input type="button" class="uploadButton" value="<?php esc_html_e('Browse', 'quickrecipe'); ?>" /><?php */?>
                                <!-- VnD Dev // START -->
                                <input type="hidden" name="hidden_recipe_image" class="hidden-recipe-image" id="hidden-recipe-image" />
                                <!-- VnD Dev // END -->
                            </div>
							<div class="sub-text"><?php esc_html_e('الحجم المقترح', 'quickrecipe'); ?></div>                      
						</div>
						 <div class="columns large-12 small-12">
							<label><?php esc_html_e('فيديو الوصفة','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-video - a class -->
							<input type="text" class="form-recipe-video form-control input-lg" name="embed_a_video" id="embed-a-video"  placeholder="يرجى ادخال رابط Youtube للوصفة">
							<!-- VnD Dev // END -->
							 <div class="sub-text"><?php esc_html_e('OPTIONAL:', 'quickrecipe'); ?> <?php esc_html_e('You can embed a video on your recipe page and then people can watch your video. If you have your recipe video on Youtube, then you\'ll want to use the field above. In the pop-up window, you\'ll see a field containing an embed code for the video. Just copy the that code and paste it into above field.','quickrecipe');?></div>
						</div>
						<?php /*?><div class="columns large-4 small-12">
							<label>Recipe Category</label>
                            <?php 
                            $recipe_type_args = array(
	                            'show_option_all'    => '',
	                            'show_option_none'   => __(' - Choose Category - ','quickrecipe'),
	                            'option_none_value'  => '0',
	                            'orderby'            => 'NAME', 
	                            'order'              => 'ASC',
	                            'show_count'         => 0,
	                            'hide_empty'         => 0, 
	                            'child_of'           => 0,
	                            'exclude'            => '',
	                            'echo'               => 1,
	                            'selected'           => $recipe_type_id,
	                            'hierarchical'       => 0, 
	                            'name'               => 'recipe-type',
	                            'id'                 => '',
	                            'class'              => 'form-control input-lg',
	                            'depth'              => 0,
	                            'tab_index'          => 0,
	                            'taxonomy'           => 'recipe_type',
	                            'hide_if_empty'      => false,
	                            'value_field'	     => 'term_id',	
                            ); 
                            wp_dropdown_categories( $recipe_type_args );
                            ?>
						</div><?php */?>
						<?php /*?><div class="columns large-4 small-12">
							<label><?php esc_html_e('Recipe Cuisine','quickrecipe');?></label>
		                    <?php
		                    $cuisine_args = array(
			                    'show_option_none' => __( ' - Choose Cuisine - ', 'quickrecipe' ),
			                    'show_count'       => 0,
	                            'option_none_value'  => '0',
	                            'hide_empty'         => 0, 
			                    'orderby'          => 'name',
			                    'echo'             => 1,
	                            'selected'           => $cuisine_id,
	                            'name'             => 'cuisine',
	                            'id'               => '',
	                            'class'            => 'form-control input-lg',
	                            'depth'            => 0,
	                            'tab_index'        => 0,
	                            'taxonomy'         => 'cuisine',
		                    );
                            wp_dropdown_categories( $cuisine_args );
		                    ?>
						</div><?php */?>
						<div class="columns large-4 small-12">
							<label><?php esc_html_e('صنف الوصفة','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-course - a class -->
		                    <?php
		                    $course_args = array(
			                    'show_option_none' => __( ' - Choose Course -', 'quickrecipe' ),
			                    'show_count'       => 0,
	                            'option_none_value'  => '0',
	                            'hide_empty'         => 0, 
			                    'orderby'          => 'SLUG',
			                    'echo'             => 1,
	                            'selected'           => $course_id,
	                            'name'             => 'course',
	                            'id'               => '',
	                            'class'            => 'form-recipe-course form-control input-lg',
								'hierarchical'     => 1,		
	                            'depth'            => 0,
	                            'tab_index'        => 1,
	                            'taxonomy'         => 'course',
		                    );
                            wp_dropdown_categories( $course_args );
		                    ?>
		                    <!-- VnD Dev // END -->
						</div>
						<?php /*?><div class="columns large-4 small-12">
							<label><?php esc_html_e('Difficulty Level','quickrecipe');?></label>
		                    <?php
		                    $difficulty_level_args = array(
			                    'show_option_none' => __( '- Difficulty Level - ', 'quickrecipe' ),
			                    'show_count'       => 0,
	                            'option_none_value'  => '0',
	                            'hide_empty'         => 0, 
			                    'orderby'          => 'name',
			                    'echo'             => 1,
	                            'selected'           => $skill_level_id,
	                            'name'             => 'difficulty-level',
	                            'id'               => '',
	                            'class'            => 'form-control input-lg',
	                            'depth'            => 0,
	                            'tab_index'        => 0,
	                            'taxonomy'         => 'difficulty_level',
		                    );
                            wp_dropdown_categories( $difficulty_level_args );
		                    ?>
						</div><?php */?>
						<div class="columns large-4 small-12">
							<label><?php esc_html_e('Season','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-season - a class -->
		                    <?php
		                    $season_args = array(
			                    'show_option_none' => __( ' - Choose Season - ', 'quickrecipe' ),
			                    'show_count'       => 0,
	                            'option_none_value'  => '0',
	                            'hide_empty'         => 0, 
			                    'orderby'          => 'name',
			                    'echo'             => 1,
	                            'selected'           => $season_id,
	                            'name'             => 'season',
	                            'id'               => '',
	                            'class'            => 'form-recipe-season form-control input-lg',
	                            'depth'            => 0,
	                            'tab_index'        => 0,
	                            'taxonomy'         => 'season',
		                    );
                            wp_dropdown_categories( $season_args );
		                    ?>
		                    <!-- VnD Dev // END -->
						</div>
						<?php /*?><div class="columns large-4 small-12">
							<label><?php esc_html_e('Allergen','quickrecipe');?></label>
		                    <?php
		                    $season_args = array(
			                    'show_option_none' => __( ' - Choose Allergen - ', 'quickrecipe' ),
			                    'show_count'       => 0,
	                            'option_none_value'  => '0',
	                            'hide_empty'         => 0, 
			                    'orderby'          => 'name',
			                    'echo'             => 1,
	                            'selected'           => $allergen_id,
	                            'name'             => 'allergen',
	                            'id'               => '',
	                            'class'            => 'form-control input-lg',
	                            'depth'            => 0,
	                            'tab_index'        => 0,
	                            'taxonomy'         => 'allergen',
		                    );
                            wp_dropdown_categories( $season_args );
		                    ?>
						</div><?php */?>
						 <?php /*?><div class="columns large-12 small-12">
							<label for="description"><?php esc_html_e('وصف','quickrecipe');?></label>
							<?php
								$settings = array( 'media_buttons' => false );
								wp_editor( $content, 'description', $settings );
							?>
						</div><?php */?>

						<?php /*?><div class="form-group col-sm-12">
							<label for="short-description"><?php esc_html_e('Short Description','quickrecipe');?></label>
							<textarea id="short-description" tabindex="2" name="short-description" cols="30" rows="10" class="form-control input-lg tinymce-enabled required"><?php echo esc_textarea($excerpt); ?></textarea>
						</div><?php */?>
						<div class="form-group col-sm-4">
							<label for="prep_time"><?php esc_html_e('وقت التحضير','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-preparation - a class -->
							<input type="text" id="prep_time" value="<?php echo esc_attr($post_meta['prep_time']); ?>" tabindex="21" name="prep_time" class="form-recipe-preparation form-control input-lg" placeholder="" />
							<!-- VnD Dev // END -->
							<div class="sub-text"><?php esc_html_e('ex. for 1 hour 15 minutes, just write 75','quickrecipe');?></div>
						</div>
						<div class="form-group col-sm-4">
							<label for="cook-time"><?php esc_html_e('Cook Time','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-cook-time - a class -->
							<input type="text" id="cook_time" value="<?php echo esc_attr($post_meta['cook_time']); ?>" tabindex="22" name="cook_time" class="form-recipe-cook-time form-control input-lg" />
							<!-- VnD Dev // END -->
							<div class="sub-text"><?php esc_html_e('ex. for 1 hour 15 minutes, just write 75','quickrecipe');?></div>
						</div>
						<div class="form-group col-sm-4">
							<label for="ready-in"><?php esc_html_e('Ready In','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-ready-in - a class -->
							<input type="text" id="ready_in" value="<?php echo esc_attr($post_meta['ready_in']); ?>" tabindex="23" name="ready_in" class="form-recipe-ready-in form-control input-lg" />
							<!-- VnD Dev // END -->
							<div class="sub-text"><?php esc_html_e('ex. for 1 hour 15 minutes, just write 75','quickrecipe');?></div>
						</div>
						<?php /*?><div class="form-group col-sm-6">
							<label for="yield"><?php esc_html_e('Yield','quickrecipe');?></label>
							<input type="text" id="yield" value="<?php echo esc_attr($post_meta['yield']); ?>" tabindex="19" name="yield" class="form-control input-lg" />
							<div class="sub-text"><?php esc_html_e('ex. 4 Servings, 3 Cups, 6 Bowls, etc.','quickrecipe');?></div>
						</div><?php */?>
						<div class="form-group col-sm-4">
							<label for="servings"><?php esc_html_e('تكفي لعدد أشخاص','quickrecipe');?></label>
							<!-- VnD Dev // START 
							Added 'form-recipe-servings - a class -->
							<input type="text" id="servings" value="<?php echo esc_attr($post_meta['servings']); ?>" tabindex="20" name="servings" class="form-recipe-servings form-control input-lg" />
							<!-- VnD Dev // END -->
						</div>
						<div class="form-group col-sm-12">
                            <label><?php esc_html_e('المكونات', 'quickrecipe'); ?></label>
                            <!-- VnD Dev // START 
							Added 'form-recipe-ingredients - a class -->
							<div class="ingredient-textarea">
                            <?php
                            if ( count($post_meta['ingredients']) ) {
                                foreach($post_meta['ingredients'] as $ingredient)
	                            {
                                ?>
                                <input type="text" name="ingredients[]" id="ingredients" class="form-recipe-ingredients form-control input-lg" size="50" value="<?php echo esc_attr($ingredient); ?>">
		                        <?php
	                            }
                            }
                            else {
                            ?>
                            <input type="text" name="ingredients[]" id="ingredients" class="form-recipe-ingredients form-control input-lg" size="50" class="required" value="<?php echo esc_attr($post_meta['ingredients']); ?>">
                            <?php 
                            }
                            ?>
                            <!-- VnD Dev // END -->
                            <div class="more-field"></div>
							</div>
							<div class="sub-text"><?php esc_html_e('أدخلي المكونات، كل على حدة','quickrecipe'); ?></div>
                            <span class="button add-ingredient add-more"><?php esc_html_e('أدخلي المكونات','quickrecipe'); ?></span>
                           
						</div>
						<div class="form-group col-sm-12">
                            <label><?php esc_html_e('التعليمات', 'quickrecipe'); ?></label>
							<div class="ingredient-textarea">
							<!-- VnD Dev // START 
							Added 'form-recipe-ingredients-steps - a class -->
                            <?php
                            if ( count($post_meta['method_steps']) ) {
                                foreach($post_meta['method_steps'] as $method_step)
	                            {
                                ?>
                          <textarea name="method_steps[]" id="method_steps" class="form-recipe-ingredients-steps form-control input-lg" cols="60" rows="3"><?php echo esc_textarea($method_step); ?></textarea>
		                        <?php
	                            }
                            }
                            else {
                            ?>
                            <textarea name="method_steps[]" id="method_steps" class="form-recipe-ingredients-steps form-control input-lg" cols="60" class="required" rows="3"></textarea>
                            <?php 
                            }
                            ?>
                            <!-- VnD Dev // END -->
                            <div class="more-field"></div>
							</div>
							<div class="sub-text"><?php esc_html_e('جميع خطوات الطبخ، كل على حدة','quickrecipe'); ?></div>
                            <span class="add-more button add-method"><?php esc_html_e('إضافة خطوة','quickrecipe'); ?></span>
                            <br>
		                    <br>
		                    <br>
		                    <!-- VnD Dev // START -->
		                    <span id="show-preview" class="button add-ingredient"><?php esc_html_e('معاينة','quickrecipe'); ?></span>
		                    <!-- VnD Dev // END -->
						</div>
						<!-- / column -->
						<div class="col-sm-12">
							<button type="submit" name="submit" class="submit button form-submit-btn">
								<?php esc_html_e('تحميل الوصفة','quickrecipe'); ?> <i class="fa fa-arrow-left"></i>
							</button>
							<div class="sub-text col-sm-12 text-left"><strong><?php esc_html_e('Are you sure you want save the changes.','quickrecipe');?></strong></div>
						</div>
                        <?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>
                        <input type="hidden" name="submitted" id="submitted" value="true" />
					</form>
					<!-- / form -->
					<?php
					}
					else
					{
						?>
						<div class="successful-update">
							<h3><?php esc_html_e('Thank you for submitting your recipe','quickrecipe');?></h3>
							<span><?php echo __('We will publish your recipe after review.','quickrecipe');?></span>
						</div>
						<?php
					}
				}
				?>
				</div>
				<!-- end:content -->
			</div>
            
          <!-- end:article -->

          <!-- begin:sidebar -->
          <div class="columns large-3 small-12">
          <?php quickrecipe_get_user_sidebar(); ?>
          <!-- end:sidebar -->
			</div>
        </div>
      
    <!-- end:content -->
<!-- VnD Dev // START -->
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#featured-image").change(function(){
            readURL(this);
        });

        jQuery('#show-preview').click(function() {
            jQuery("#myModal").modal('show');
            if(jQuery('.form-recipe-title').val() != "") {
                jQuery('.recipe-title').removeClass("hide");
                var recipe_title = jQuery('.form-recipe-title').val();
            } else {
                jQuery('.recipe-title').addClass("hide");
            }
            
            
            if(jQuery('.hidden-recipe-image').val() != "" ) {
                jQuery('.recipe-image').removeClass("hide");
                var recipe_image = jQuery('.hidden-recipe-image').val();    
            } else {
                jQuery('.recipe-image').addClass("hide");
            }
            
            if(jQuery('.form-recipe-video').val() == "") {
                jQuery('.recipe-video').addClass("hide");
            } else {
                jQuery('.recipe-video').removeClass("hide");
                var recipe_video = jQuery('.form-recipe-video').val();  
            }
            
            if(jQuery('.hidden-recipe-image').val() == "" && jQuery('.form-recipe-video').val() == "") {
                jQuery('#myCarousel-recipe-preview').addClass("hide");
                jQuery('.shareflies').addClass("hide");
            } else if(jQuery('.hidden-recipe-image').val() != "" || jQuery('.form-recipe-video').val() != "") {
                jQuery('#myCarousel-recipe-preview').removeClass("hide");
                jQuery('.shareflies').removeClass("hide");
            } 

            if(jQuery('.hidden-recipe-image').val() != "" && jQuery('.form-recipe-video').val() != "") {
                var carousel_inner = "<div id='myCarousel-recipe-preview' class='carousel slide' data-ride='carousel'><div class='carousel-inner'><div class='item active'><img class='tip-image' src='"+recipe_image+"' alt='your image' /></div><div class='item'><iframe class='tip-video' id='tip-video' width='100%' height='400' src='https://www.youtube.com/embed/"+recipe_video+"' frameborder='0' allowfullscreen></iframe></div></div><a class='left carousel-control' href='#myCarousel-recipe-preview' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span><span class='sr-only'>Previous</span></a><a class='right carousel-control' href='#myCarousel-recipe-preview' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span><span class='sr-only'>Next</span></a></div>";
            } else if(jQuery('.hidden-recipe-image').val() != "" && jQuery('.form-recipe-video').val() == "") {
                var carousel_inner = "<div id='myCarousel-recipe-preview' class='carousel slide' data-ride='carousel'><div class='carousel-inner'><div class='item active'><img class='tip-image' src='"+recipe_image+"' alt='your image' /></div></div><a class='left carousel-control' href='#myCarousel-recipe-preview' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span><span class='sr-only'>Previous</span></a><a class='right carousel-control' href='#myCarousel-recipe-preview' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span><span class='sr-only'>Next</span></a></div>";
            } else if(jQuery('.hidden-recipe-image').val() == "" && jQuery('.form-recipe-video').val() != "") {
                var carousel_inner = "<div id='myCarousel-recipe-preview' class='carousel slide' data-ride='carousel'><div class='carousel-inner'><div class='item active'><iframe class='tip-video' id='tip-video' width='100%' height='400' src='https://www.youtube.com/embed/"+recipe_video+"?' frameborder='0' allowfullscreen></iframe></div></div><a class='left carousel-control' href='#myCarousel-recipe-preview' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span><span class='sr-only'>Previous</span></a><a class='right carousel-control' href='#myCarousel-recipe-preview' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span><span class='sr-only'>Next</span></a></div>";
            }

            var recipe_course_selected = jQuery('#course option:selected');
            if(jQuery('.form-recipe-course').val() != 0) {
                var recipe_course = recipe_course_selected.html();  
            }
            
            var recipe_season_selected = jQuery('#season option:selected');
            if(jQuery('.form-recipe-season').val() != 0) {
                var recipe_season = recipe_season_selected.html();
            }
            
            var recipe_preparation_time = jQuery('.form-recipe-preparation-time').val();
            
            var recipe_cook_time = jQuery('.form-recipe-cook-time').val();
            
            var recipe_ready_in = jQuery('.form-recipe-ready-in').val();
            
            if(jQuery('.form-recipe-servings').val() != "") {
                var recipe_servings_count = parseInt(jQuery('.form-recipe-servings').val());
                if(recipe_servings_count != 'NaN') {
                    var recipe_servings_image = "";
                    console.log(recipe_servings_count);
                    for(var i = 0; i < recipe_servings_count; i++) {
                        recipe_servings_image += '<div class="recipe-serving"></div>';
                    }
                }
                var recipe_servings = "Serves " + jQuery('.form-recipe-servings').val();    
            } else {
                var recipe_servings = "";
            }
            

            var recipe_ingredients = jQuery(".form-recipe-ingredients");
            if(jQuery(recipe_ingredients[0]).val() == "") {
                jQuery('.recipe-ingredients-content').addClass("hide");
            } else {
                jQuery('.recipe-ingredients-content').removeClass("hide");
            }
            var ingredientsDiv = "<ul>";
            for(var i = 0; i < recipe_ingredients.length; i++) {
                var ingredients_values = jQuery(recipe_ingredients[i]).val();
                if(ingredients_values != "") {
                    ingredientsDiv += '<li class="recipe-ingredients">'+ingredients_values+'</li>';
                }
            }
            ingredientsDiv += "</ul>";

            var recipe_ingredients_steps = jQuery(".form-recipe-ingredients-steps");
            if(jQuery(recipe_ingredients_steps[0]).val() == "") {
                jQuery('.recipe-instruction-content').addClass("hide");
            } else {
                jQuery('.recipe-instruction-content').removeClass("hide");
            }
            var ingredientsstepsDiv = "<ol>";
            for(var i = 0; i < recipe_ingredients_steps.length; i++) {
                var ingredients_steps_values = jQuery(recipe_ingredients_steps[i]).val();
                if(ingredients_steps_values != "") {
                    ingredientsstepsDiv += '<li class="recipe-ingredients">'+ingredients_steps_values+'</li>';
                }
            }
            ingredientsstepsDiv += "</ol>";
            
            jQuery('.recipe-title').html(recipe_title);
            jQuery('.recipe-course').html(recipe_course);
            jQuery('.recipe-season').html(recipe_season);
            jQuery('.recipe-preparation-time').html(recipe_preparation_time);
            jQuery('.recipe-cook-time').html(recipe_cook_time);
            jQuery('.recipe-ready-in').html(recipe_ready_in);
            jQuery('.recipe-servings').html(recipe_servings);
            jQuery('.recipe-ingredients').html(ingredientsDiv);
            jQuery('.recipe-ingredients-steps').html(ingredientsstepsDiv);
            jQuery('.carousel-list').html(carousel_inner);
            jQuery('.recipe-serving-image').html(recipe_servings_image);
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                jQuery('.hidden-recipe-image').attr('value', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<div class="modal fade model-recipe" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">

                <div class="carousel-list"></div>
                <div class="recipe-serving-image"></div>
                <div class="recipe-servings"></div>
                <div class="shareflies">
                    <ul>
                        <li><a class="share" href="#"></a></li>
                        <li><a class="email" href="#"></a></li>
                        <li><a class="print" href="#"></a></li>

                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="recipe-title"></div>
                <div class="recipe-ingredients-content">
                    <h2>INGREDIENTS</h2>
                    <span class="recipe-ingredients"></span>
                </div>
                <div class="recipe-instruction-content">
                    <h2>INSTRUCTION</h2>
                    <span class="recipe-ingredients-steps"></span>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn footer-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- VnD Dev // END -->
<?php get_footer(); ?>