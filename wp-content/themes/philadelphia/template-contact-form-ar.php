<?php
// Template Name: Contact Page AR



if ( ! defined( 'ABSPATH' ) ) exit;
	get_header();
    
    //$address_latitude =  get_option('quickrecipe_latitude', '');
    //$address_longitude =  get_option('quickrecipe_longitude', '');
   // $contact_phone_number = get_option('quickrecipe_phone_number', '');
   // $contact_email = get_option('quickrecipe_email_address', '');
    //$company_name = get_option('quickrecipe_company_name', '');
    //$company_address = get_option('quickrecipe_company_address', '');

?>
   <div class="row full-width header-box">
    <div class="main-image" style="background-image: url(<?php the_field('page_header_image'); ?>)">

        <div class="row main-container align-center full-width">
            <div class="column large-6 column align-self-middle">
                <h1 class="heading">
                   <?php  the_field('page_header_title'); ?>
                </h1>

            </div>
        </div>

    </div>

</div>
   <div class="full-width white">
<div class="row align-center contact-boxes">

   <?php if( have_rows('contact_address') ):	 ?>
    <div class="columns large-4 small-12 contact-box">
        <div class="row">
            <div class="columns large-12"><i class="fa fa-map-marker"></i><?php esc_html_e( 'ADDRESS', 'quickrecipe' ); ?></div>
           <?php /*?> <div class="columns large-12 small-12 contact-box-text">
                 <?php echo esc_html($company_address); ?>
            </div><?php */?>
            
            
	  <?php
		 while ( have_rows('contact_address') ) : the_row();
               
                $contact_add = get_sub_field('country_address');
				
	    ?>
               <div class="columns large-12 small-12 contact-box-text">


                   <?php echo $contact_add; ?>

                </div>
             
            
 		 <?php  
    endwhile;

?>
        </div>

    </div>
    <?php 
		endif;
	    wp_reset_postdata();
       
        ?>
    
    
    <div class="columns large-4 small-12 contact-box">
        <div class="row">
        <div class="columns large-12"><i class="fa fa-phone"></i><?php esc_html_e( 'PHONE', 'quickrecipe' ); ?></div>
        <?php /*?><div class="columns large-12 small-12 contact-box-text">
              <?php echo esc_html($contact_phone_number); ?>
        </div><?php */?>
        <?php
		
		if( have_rows('contact_phone_numbers') ):	   
		 while ( have_rows('contact_phone_numbers') ) : the_row();
               
                
			$contact_phone_title = get_sub_field('phone_title');
			 $contact_phone = get_sub_field('phone_number');
				
	    ?>
               <div class="columns large-12 small-12 contact-box-text">

					<?php echo $contact_phone_title; ?><br>
                     <?php echo $contact_phone; ?>

                </div>
             
            
 		 <?php 
    
    endwhile;

endif;
	  
            wp_reset_postdata();
       
        ?>
        </div>
    </div>
    <div class="columns large-4 small-12 contact-box">

        <div class="row">
            <div class="columns large-12"><i class="fa fa-envelope"></i><?php esc_html_e( 'EMAIL', 'quickrecipe' ); ?></div>
            <?php /*?><div class="columns large-12 small-12 contact-box-text">
                <a href="mailto:<?php echo esc_html($contact_email); ?>"> <?php echo esc_html($contact_email); ?></a>
            </div><?php */?>
              <?php
		
		if( have_rows('contact_emails') ):	   
		 while ( have_rows('contact_emails') ) : the_row();
               
                $contact_email_title = get_sub_field('email_title');
			    $contact_email = get_sub_field('email');
				
	    ?>
               <div class="columns large-12 small-12 contact-box-text">

						<?php echo $contact_email_title; ?><br>
                          <a href="mailto:<?php echo esc_html($contact_email); ?>"> <?php echo esc_html($contact_email); ?></a>

                </div>
             
            
 		 <?php 
    
    endwhile;

endif;
	  
            wp_reset_postdata();
       
        ?>
        </div>
    </div>

</div>
    </div>
     
              <div class="row contact-form align-center">
                   
                      <div class="columns large-12 small-12 contact-headline">اتصل بنا</div>
                   
                    <div class="row clearfix large-12 small-12">

                       <?php /*?> <?php
        	                if ( have_posts() ) { $count = 0;
        		                while ( have_posts() ) { the_post(); $count++;
                                    the_content();
				                } // End WHILE Loop
			                } else {
		                ?>
                            <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'quickrecipe' ); ?></p>
                        <?php } // End IF Statement ?><?php */?>
                        <?php echo do_shortcode( '[contact-form-7 id="593" title="Contact Form AR"] ') ?>
                       

                    </div>

                </div>

             
             

             <?php if (get_field('location_map_short_code')!='') { ?>
              <div class="full-width white">

    <div class="row align-center">

        <div class="columns large-12 small-12 map-headline">تجد لنا</div>

        <div class="columns large-12 small-12 map">
             <?php echo do_shortcode( get_field('location_map_short_code')); ?> 
        </div>

    </div>

</div>
      <?php }       ?>

              
               <?php /*?> <?php
                if(function_exists('quickrecipe_contact_map')) {
                    quickrecipe_contact_map();
                }
                ?><?php */?>

        
    <!-- end:content -->


<?php get_footer(); ?>