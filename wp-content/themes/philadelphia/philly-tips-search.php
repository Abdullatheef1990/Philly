<?php
if ( ! defined( 'ABSPATH' ) ) exit;
error_reporting(E_ALL);
ini_set("display_errors", "Off");
get_header();
?>
<?php 
$search_string = '';
if (isset($_GET['s'])) {
    $search_string = wp_kses($_GET['s'], '');   
}

$search_tip = '';
if (isset($_GET['post_type'])) {
    $search_tip = wp_kses($_GET['post_type'], '');   
}

$tip_type = 0;
if (isset($_GET['tip_type'])) {
    $tip_type = intval($_GET['tip_type']);
    $tip_type_term = get_term_by( 'id', intval($_GET['tip_type']), 'tip_type' );
    $tip_type_name = $tip_type_term->name;
}

$occassion = 0;
if (isset($_GET['occassion'])) {
    $occassion = intval($_GET['occassion']);
    $occassion_term = get_term_by( 'id', intval($_GET['occassion']), 'occassion' );
    $occassion_name = $occassion_term->name;
} 
if (isset($_GET['sort'])) {
    $tip_string = wp_kses($_GET['sort'], '');    
}
?>  
<?php 
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$ids = get_users( array('role__not_in' => 'subscriber' ,'fields' => 'ID') );
if(!empty($tip_string)) {
$query_args = array(
    'post_type' => array('post', 'tips'),
    'post_status' => 'publish',
    's' => $search_string,
    'orderby' => 'title',
    'order'   => 'ASC', 
    'posts_per_page' => intval(get_option('posts_per_page')),
    'paged' => $paged,
    'author' => implode(',', $ids)
    );
}else{
    $query_args = array(
        'post_type' => array('post', 'tips'),
        'post_status' => 'publish',
        's' => $search_string,
        'orderby' => 'title',
        'order'   => 'DESC',
        'posts_per_page' => intval(get_option('posts_per_page')),
        'paged' => $paged,
        'author' => implode(',', $ids)
        );
}

if(!empty($tip_type)) {
    $query_args['tax_query'][]=array(
        'taxonomy' => 'tip_type',
        'field' => 'term_id',
        'terms' => $tip_type,                
        );
}     
if(!empty($occassion)) {
    $query_args['tax_query'][]=array(
        'taxonomy' => 'occassion',
        'field' => 'term_id',
        'terms' => $occassion,                
        );
}

$query=new WP_Query($query_args);
?>
<?php     
/********* RECIPE SEARCH RESULT **********/
if(!empty($search_tip)) { ?>
    <?php if (ICL_LANGUAGE_CODE=='en') { ?>
    <div class="recipes">
        <div class="row full-width header-box">
            <div class="main-image" style="background-image: url(<?php the_field('page_header_image',315); ?>)">
                <div class="row main-container align-center full-width">
                    <div class="column large-6 column align-self-middle">
                        <h1 class="heading">
                           <?php  the_field('page_header_title'); ?>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="recipe-search-box columns align-center">
                <?php get_template_part( 'includes/philly-tips/search-area' ); ?>
            </div>
        </div>
        <div class="row">
            <div class="columns large-12 recipes-headline">
                YOU ARE VIEWING <br/> <?php  echo $tip_type_name; if((!empty($occassion_name) || !empty($search_string)) && !empty($tip_type_name) ) { echo ' > '; } ?> <?php  echo $occassion_name; if(!empty($search_string) && !empty($occassion_name) ) { echo ' > '; } ?> <?php echo $search_string ?> 
            </div>
        </div>
        <?php  if ( $query->have_posts() ) : ?>
            <div class="row recipes-grid all-recipes load-more-wrapper">
                <div class="recipes-filter">
                    <select name="recipe-sort" onchange="location = this.value;">
                        <option value="">Sort by</option>
                        <option value="/philly-tips/">All</option>
                        <option value="<?php echo site_url('/')?>?post_type=tips<?php if(!empty($tip_type)){ ?>&amp;tip_type=<?php echo $tip_type; } ?><?php if(!empty($occassion)){ ?>&amp;occassion=<?php echo $occassion; } ?>&amp;s=<?php echo $search_string; ?>&amp;sort=rating">Rating</option>
                        <option value="<?php echo site_url('/')?>?post_type=tips<?php if(!empty($tip_type)){ ?>&amp;tip_type=<?php echo $tip_type; } ?><?php if(!empty($occassion)){ ?>&amp;occassion=<?php echo $occassion; } ?>&amp;s=<?php echo $search_string;  ?>&amp;sort=name">Newest</option>
                    </select>
                </div>
                <?php while($query->have_posts()) :
                    $query->the_post();
                    $term_names = wp_get_post_terms($post->ID, 'tip_type', array("fields" => "names"));
                    $term_list = array();

                    if (is_array($term_names)) {
                        foreach($term_names as $term_name) {
                            $term_list[] = $term_name;
                        }
                        $term_name_list = join( ", ", $term_list );
                    } else {
                        $term_name_list = '';
                    }
                ?>
                    <div class="columns large-6 small-12  load-more-article recipes-grid-element" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
                        <div class="row full-width grid-inner">
                            <div class="column large-6 small-6 panel"></div>
                            <div class="column large-6 small-6 transparent-block panel">
                                <h2><?php the_title(); ?></h2>
                                <a href='<?php echo get_permalink(); ?>'  class="transparent-button">START TIP</a>
                            </div>
                        </div>
                    </div>
                <?php endwhile;
                wp_reset_postdata();
        else : ?>
            <div class="row">
                <div class="search-results-wrapper load-more-wrapper">    
                    <div class="columns large-12 search-results-headline">
                        <p><?php esc_html_e( 'Nothing Found', 'quickrecipe' ); ?></p>
                        <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again.', 'quickrecipe' ); ?></p>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
    <div class="recipes">
        <div class="row full-width header-box">
            <div class="main-image" style="background-image: url(<?php the_field('page_header_image',552); ?>)">
                <div class="row main-container align-center full-width">
                    <div class="column large-6 column align-self-middle">
                        <h1 class="heading">
                           <?php  the_field('page_header_title'); ?>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="recipe-search-box columns align-center">
                <?php get_template_part( 'includes/philly-tips/search-area' ); ?>
            </div>
        </div>
        <div class="row">
            <div class="columns large-12 recipes-headline">
                استمر العصبة ضرب قد <br/> <?php  echo $tip_type_name ; if((!empty($occassion_name) || !empty($search_string)) && !empty($tip_type_name) ) { echo ' > '; } ?> <?php  echo $occassion_name; if(!empty($search_string) && !empty($occassion_name) ) { echo ' > '; } ?> <?php echo $search_string ?> 
            </div>
        </div>
        <?php  if ( $query->have_posts() ) : ?>
            <div class="row recipes-grid all-recipes load-more-wrapper">
                <div class="recipes-filter">
                    <select name="recipe-sort" onchange="location = this.value;">
                        <option value="">الصفحة</option>
                        <option value="/وصفات/?lang=ar">الصفحة</option>
                        <option value="<?php echo site_url('/?lang=ar')?>?post_type=tips<?php if(!empty($tip_type)){ ?>&amp;tip_type=<?php echo $tip_type; } ?><?php if(!empty($occassion)){ ?>&amp;occassion=<?php echo $occassion; } ?>&amp;s=<?php echo $search_string; ?>&amp;sort=rating">الصفحة</option>
                        <option value="<?php echo site_url('/?lang=ar')?>?post_type=tips<?php if(!empty($tip_type)){ ?>&amp;tip_type=<?php echo $tip_type; } ?><?php if(!empty($occassion)){ ?>&amp;occassion=<?php echo $occassion; } ?>&amp;s=<?php echo $search_string;  ?>&amp;sort=name">الصفحة</option>
                    </select>
                </div>
                <?php while($query->have_posts()) : 
                    $query->the_post();
                    $term_names = wp_get_post_terms($post->ID, 'tip_type', array("fields" => "names"));
                    $term_list = array();

                    if (is_array($term_names)) {
                        foreach($term_names as $term_name) {
                            $term_list[] = $term_name;
                        }
                        $term_name_list = join( ", ", $term_list );
                    } else {
                        $term_name_list = '';
                    } 
                ?>
                    <div class="columns large-6 small-12  load-more-article recipes-grid-element" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
                        <div class="row full-width grid-inner">
                            <div class="column large-6 small-6 panel"></div>
                            <div class="column large-6 small-6 transparent-block panel">
                                <h2><?php the_title(); ?></h2>
                                <a href='<?php echo get_permalink(); ?>'  class="transparent-button">شكل توضع الفقرات</a>
                            </div>
                        </div>
                    </div>
                <?php endwhile;          
            wp_reset_postdata();
            else : ?>
                <div class="row">
                    <div class="search-results-wrapper load-more-wrapper">    
                        <div class="columns large-12 search-results-headline">
                            <p><?php esc_html_e( 'شكل توضع الفقرات', 'quickrecipe' ); ?></p>
                            <p><?php esc_html_e( 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي', 'quickrecipe' ); ?></p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>              
<?php }
} 
/********* WEBSITE SEARCH RESULT**********/
else if(!empty($search_string)) { ?>
    <?php if (ICL_LANGUAGE_CODE=='en') { ?>
        <div class="row">
            <div class="columns large-12 search-results-headline">
                Results for  <?php  echo $tip_type_name ; if((!empty($occassion_name) || !empty($search_string)) && !empty($tip_type_name) ) { echo ' > '; } ?> <?php  echo $occassion_name; if(!empty($search_string) && !empty($occassion_name) ) { echo ' > '; } ?> <?php echo $search_string ?> 
            </div>
        </div>
        <div class="search-results-wrapper load-more-wrapper">            
            <?php   
            if ( $query->have_posts() ) :
                while($query->have_posts()) :
                    $query->the_post();
                    $term_names = wp_get_post_terms($post->ID, 'tip_type', array("fields" => "names"));
                    $term_list = array();

                    if (is_array($term_names)) {
                        foreach($term_names as $term_name) {
                            $term_list[] = $term_name;
                        }
                        $term_name_list = join( ", ", $term_list );
                    } else {
                        $term_name_list = '';
                    } 
            ?>     
                    <div class="row search-results-box load-more-article">
                        <?php  if ( has_post_thumbnail( $post->ID ) ) { ?>
                            <div class="columns search-result-image large-3 small-4 medium-4">
                                <div class="thumb" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">

                                </div>
                            </div>
                        <?php }  ?>
                        <div class="columns search-result-text large-9 small-8 medium-8">
                            <div class="columns large-12 medium-12 small-12">
                                <h2><?php echo get_the_title(); ?></h2>
                                <p><?php echo wp_trim_words( get_the_content(), 15, '...' ); ?></p>
                            </div>
                        </div>
                        <a href="<?php the_permalink()?>" class="read-more-btn">READ MORE</a>
                    </div>
                <?php endwhile;
                wp_reset_postdata();
            else : ?>
            <div class="row">
                <div class="search-results-wrapper load-more-wrapper">    
                    <div class="columns large-12 search-results-headline">
                        <p><?php esc_html_e( 'Nothing Found', 'quickrecipe' ); ?></p>
                        <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again.', 'quickrecipe' ); ?></p>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
 <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
        <div class="row">
            <div class="columns large-12 search-results-headline">
                أنت تعرض  <?php  echo $tip_type_name ; if((!empty($occassion_name) || !empty($search_string)) && !empty($tip_type_name) ) { echo ' > '; } ?> <?php  echo $occassion_name; if(!empty($search_string) && !empty($occassion_name) ) { echo ' > '; } ?> <?php echo $search_string ?> 
            </div>
        </div>
        <div class="search-results-wrapper load-more-wrapper">            
        <?php
            if ( $query->have_posts() ) : 
                while($query->have_posts()) :
                    $query->the_post();
                    $term_names = wp_get_post_terms($post->ID, 'tip_type', array("fields" => "names"));
                    $term_list = array();

                    if (is_array($term_names)) {
                        foreach($term_names as $term_name) {
                            $term_list[] = $term_name;
                        }
                        $term_name_list = join( ", ", $term_list );
                    } else {
                        $term_name_list = '';
                    } 
            ?>     
                    <div class="row search-results-box load-more-article">
                        <?php  if ( has_post_thumbnail( $post->ID ) ) { ?>
                            <div class="columns search-result-image large-3 small-4 medium-4">
                                <div class="thumb" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
                                </div>
                            </div>
                        <?php }  ?>
                        <div class="columns search-result-text large-9 small-8 medium-8">
                            <div class="columns large-12 medium-12 small-12">
                                <h2><?php echo get_the_title(); ?></h2>
                                <p><?php echo wp_trim_words( get_the_content(), 15, '...' ); ?></p>
                            </div>
                        </div>
                        <a href="<?php the_permalink()?>" class="read-more-btn">أنت تعرض</a>
                    </div>
                <?php endwhile;
            wp_reset_postdata();
        else : ?>
            <div class="row">
                <div class="search-results-wrapper load-more-wrapper">    
                    <div class="columns large-12 search-results-headline">
                        <p><?php esc_html_e( 'أنت تعرض', 'quickrecipe' ); ?></p>
                        <p><?php esc_html_e( 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي', 'quickrecipe' ); ?></p>
                    </div>
                </div>
            </div>
        <?php endif; ?>
       </div>
    <?php } ?>
<?php } ?>
<?php quickrecipe_pagination_links( ); ?>
<?php get_footer(); ?>