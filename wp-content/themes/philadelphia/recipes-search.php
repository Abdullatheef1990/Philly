<?php
if ( ! defined( 'ABSPATH' ) ) exit;

get_header();
?>

  <?php /*?><div id="header" class="page-heading" style="background-image: url(<?php echo esc_url(get_template_directory_uri() . '/img/img01.jpg'); ?>);">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 col-sm-12">
            <form method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <div class="quick-search">
              <div class="row">
                  <?php get_template_part( 'includes/mainpage/search-fields' ); ?>
              </div>
              <div class="button-group">
                  <div class="action-buttons">
                      <button class="btn btn-success"><?php esc_html_e( 'Search', 'quickrecipe' ); ?></button>
                  </div>
              </div>
            </div>
            </form>
            <?php //quickrecipe_breadcrumbs(); ?> 
          </div>
        </div>
      </div>
    </div><?php */?>
    
    <?php 
            $search_string = '';
            if (isset($_GET['s'])) {
	            $search_string = wp_kses($_GET['s'], '');	
            }

            $recipe_type = 0;
            if (isset($_GET['recipe-type'])) {
	            $recipe_type = intval($_GET['recipe-type']);
            }

            $cuisine = 0;
            if (isset($_GET['cuisine'])) {
	            $cuisine = intval($_GET['cuisine']);
            }

            $course = 0;
            if (isset($_GET['course'])) {
	            $course = intval($_GET['course']);
				//$course_term = get_term_by('name', 'course', 'category');
				$course_term = get_term_by( 'id', intval($_GET['course']), 'course' );
				$course_name = $course_term->name;
            }

            $season = 0;
            if (isset($_GET['season'])) {
	            $season = intval($_GET['season']);
				$season_term = get_term_by( 'id', intval($_GET['season']), 'season' );
				$season_name =  $season_term->name;
            }

            $ingredient = 0;
            if (isset($_GET['ingredient'])) {
	            $ingredient = intval($_GET['ingredient']);
            }

            $allergen = 0;
            if (isset($_GET['allergen'])) {
	            $allergen = intval($_GET['allergen']);
            }

            $difficulty_level = 0;
            if (isset($_GET['difficulty_level'])) {
	            $difficulty_level = intval($_GET['difficulty_level']);
            }
?>
            <div class="row">

    <div class="columns large-12 search-results-headline">
ABCCC Results for  <?php  echo $course_name ; if((!empty($season_name) || !empty($search_string)) && !empty($course_name) ) { echo ' > '; } ?> <?php  echo $season_name; if(!empty($search_string) && !empty($season_name) ) { echo ' > '; } ?> <?php echo $search_string ?> 
    </div>

</div>
<div class="search-results-wrapper load-more-wrapper">
    
		   <?php 
            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
        	$query_args = array(
                                'post_type' => array('post', 'recipe'),
                                'post_status' => 'publish',
                                's' => $search_string,
                                'orderby' => 'title',
                                'order'   => 'DESC',
        						'posts_per_page' => intval(get_option('posts_per_page')),
                                'paged' => $paged,
        					);
		    if(!empty($recipe_type)) {
			    $query_args['tax_query'][]=array(
				    'taxonomy' => 'recipe_type',
				    'field' => 'term_id',
				    'terms' => $recipe_type,				
			    );
		    }     
		    if(!empty($cuisine)) {
			    $query_args['tax_query'][]=array(
				    'taxonomy' => 'cuisine',
				    'field' => 'term_id',
				    'terms' => $cuisine,				
			    );
		    }
		    if(!empty($course)) {
			    $query_args['tax_query'][]=array(
				    'taxonomy' => 'course',
				    'field' => 'term_id',
				    'terms' => $course,				
			    );
		    }   	
		    if(!empty($season)) {
			    $query_args['tax_query'][]=array(
				    'taxonomy' => 'season',
				    'field' => 'term_id',
				    'terms' => $season,				
			    );
		    }   
		    if(!empty($ingredient)) {
			    $query_args['tax_query'][]=array(
				    'taxonomy' => 'ingredient',
				    'field' => 'term_id',
				    'terms' => $ingredient,				
			    );
		    }   
		    if(!empty($allergen)) {
			    $query_args['tax_query'][]=array(
				    'taxonomy' => 'allergen',
				    'field' => 'term_id',
				    'terms' => $allergen,				
			    );
		    }   
		    if(!empty($difficulty_level)) {
			    $query_args['tax_query'][]=array(
				    'taxonomy' => 'difficulty_level',
				    'field' => 'term_id',
				    'terms' => $difficulty_level,				
			    );
		    }   

            $query=new WP_Query($query_args);
        
            if ( $query->have_posts() ) : 

	            while($query->have_posts()) :
		            $query->the_post();

                    $term_names = wp_get_post_terms($post->ID, 'course', array("fields" => "names"));
                    $term_list = array();
 
                    if (is_array($term_names)) {
                        foreach($term_names as $term_name) {
                            $term_list[] = $term_name;
                        }
                        $term_name_list = join( ", ", $term_list );
                    } else {
                        $term_name_list = '';
                    }
            ?>
              
              
 <div class="row search-results-box load-more-article">

       <?php  if ( has_post_thumbnail( $post->ID ) ) { ?>
       
        <div class="columns search-result-image large-3 small-4 medium-4">
            <div class="thumb" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">

            </div>
        </div>
        <?php }  ?>
					
        <div class="columns search-result-text large-9 small-8 medium-8">
            <div class="columns large-12 medium-12 small-12">
                <h2><?php echo get_the_title(); ?></h2>
				<p><?php echo wp_trim_words( get_the_content(), 15, '...' ); ?></p>
            </div>
        </div>


        <a href="<?php the_permalink()?>" class="read-more-btn">READ MORE</a>


    </div>
            <?php
	
  
            endwhile;
		  
            wp_reset_postdata();
			else : ?>
         <div class="row">
          <div class="columns large-12 search-results-headline">
	        <p><?php esc_html_e( 'Nothing Found', 'quickrecipe' ); ?></p>
	        <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again.', 'quickrecipe' ); ?></p>
			 </div></div>
            <?php endif; ?>
         
            <?php quickrecipe_pagination_links( ); ?>
         
          <?php // get_sidebar(); ?>          
</div>    
<?php get_footer(); ?>