<?php
/* Template Name: Reset Password Page */
 
if ( is_user_logged_in() ) {
	wp_redirect( esc_url(get_home_url()) );
	exit;
}


$success= '';
$errors = array();

if( isset($_POST['reset']) && $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['resetpass_field_nonce']) && wp_verify_nonce( $_POST['resetpass_field_nonce'], 'resetpass_field' ) ){
    if(function_exists('quickrecipe_reset_password')) {
	    $errors = quickrecipe_reset_password();
    } else {
        $errors[] = esc_html__( 'You should install and activate to quickrecipe addition plugin.', 'quickrecipe' );
    }

}

$enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);
	
$login_page_id = get_option('quickrecipe_login_page_url', '');
$login_page_url = get_permalink($login_page_id);
if (!$enable_custom_login || !$login_page_url) {
	$login_page_url = wp_login_url();
}

$register_page_id = get_option('quickrecipe_register_page_url', '');
$register_page_url = get_permalink($register_page_id);
if (!$enable_custom_login || !$register_page_url) {
	$register_page_url = wp_registration_url();
}

$forgot_username_page_id = get_option('quickrecipe_forgot_username_page_url', '');
$forgot_username_page_url = get_permalink($forgot_username_page_id);
if (!$enable_custom_login || !$forgot_username_page_url) {
    $forgot_username_page_url = wp_registration_url();
}

get_header(); 

$action  = $_GET['action'];

switch ($action)
{
	case 'notify':
        $success = esc_html__( 'You are requested to Reset your Password by clicking the link \'Forget Password\'.<br> Please confirm the request to reset your password by clicking the link provided in your email.', 'quickrecipe' );
	break;

	case 'resetpassword':

        if(function_exists('quickrecipe_generate_password')) {
	        $errors = quickrecipe_generate_password();
            if( empty($errors) ){
                $success = esc_html__( 'Your password was successfully reset. We have sent the new password to your email address.', 'quickrecipe' );
            }
        } else {
            $errors[] = esc_html__( 'You should install and activate to quickrecipe addition plugin.', 'quickrecipe' );
        }

	break;

	case 'sendactivation':
        if(function_exists('quickrecipe_email_activation_link')) {
		    if ( quickrecipe_email_activation_link($_GET['user_id']) ) 
                $success = esc_html__( 'Activation link was successfully sent.', 'quickrecipe' );
		    else
                $errors[] = esc_html__( 'An error was encountered while sending activation notification email.', 'quickrecipe' );
        }
	break;

}
?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
            <div class="login-wrap">
                <div class="login ">
                    <div class="page-header">
                        <h1><?php esc_html_e('Reset password', 'quickrecipe'); ?></h1>
                    </div>
                    <div class="login-description">
				        <p>
				        <?php esc_html_e('Don\'t have an account yet?', 'quickrecipe'); ?> <a href="<?php echo esc_url($register_page_url); ?>"><?php esc_html_e('Sign up', 'quickrecipe'); ?></a>.
				        </p>
				        <p>	
				        <?php esc_html_e('Already a member?', 'quickrecipe'); ?> <?php echo sprintf(wp_kses(__('Proceed to <a href="%s">login</a> page', 'quickrecipe'), array(  'a' => array( 'href' => array() ) ) ), esc_url($login_page_url)); ?>.
				        </p>
				        <p>	
				        <?php esc_html_e('Forgot your username?', 'quickrecipe'); ?> <?php echo sprintf(wp_kses(__('Proceed to <a href="%s">remind</a> page', 'quickrecipe'), array(  'a' => array( 'href' => array() ) ) ), esc_url($forgot_username_page_url)); ?>.
				        </p>
			        <?php if (!empty($errors)) { ?>
				        <p class="error"><?php echo implode('<br />', $errors); ?></p>
			        <?php } ?> 
				        <p class="success">
					        <?php echo esc_html($success); ?>
				        </p>
                    </div>
                    <form id="reset_password_form" method="post" action="<?php get_permalink(); ?>">
                    <fieldset>
                        <div class="form-group">
                            <label for="user_login"><?php esc_html_e('Username or email address', 'quickrecipe'); ?><span class="star">&nbsp;*</span></label>
                            <input type="text" name="user_login" id="user_login" class="form-control input-lg" value="<?php echo isset($user_login) ? esc_attr($user_login) : ''; ?>" />
                        </div>
                        <div class="form-group">
                            <div class="form-btn">
				                <?php wp_nonce_field( 'resetpass_field', 'resetpass_field_nonce' ) ?>
				                <input type="submit" id="reset" name="reset" value="<?php esc_html_e('Reset password', 'quickrecipe'); ?>" class="btn btn-primary"/>
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div><!-- end:login-wrap -->
        </div>
      </div>
    </div>
    <!-- end:content -->



<?php get_footer(); ?>