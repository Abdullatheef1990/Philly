<?php
/*
*   Template Name: Home Page
*/
// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header();
?>
    <!-- begin:header -->
    
   
    <div class="row full-width">
    <div class="main-image">
        
        <div class="master-slider ms-skin-default" id="home-masterslider">
            
            
            
	

	<?php
		
		 $args = array('post_type' => 'home_page_slider', 'order' => 'ASC');
		
		 $query=new WP_Query($args);
        	
        if ( $query->have_posts() ) {
            
            while($query->have_posts()){
	            $query->the_post();
               
                $slide_intro = get_field('hp_slider_intro');
				$slide_button_text = get_field('hp_slider_link_button_text');
                $slide_button_link = get_field('hp_slider_link_button_link');
                $slide_image = get_field('hp_slider_image');
				$slide_video = get_field('hp_slider_youtube_video_id');
	    ?>
              
             
               
                <div class="ms-slide">

                
                <img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt="<?php echo $slide_intro; ?>"/>

                
                <div class="row main-container align-center full-width ms-layer ms-caption">
                   
                       <div class="column large-6 column align-self-middle">
                        <?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>
                         <?php } ?>
                         <?php if ($slide_button_link!=''){ ?>
                        <a href="<?php echo esc_url($slide_button_link); ?>" class="transparent-button">
                           <?php echo $slide_button_text; ?>
                        </a>
                          <?php } ?>
                    </div>
                   
                </div>
				 <?php if ($slide_video!=''){ ?>
                <iframe class="yt-video" src="https://www.youtube.com/embed/<?php echo $slide_video; ?>?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=<?php echo $slide_video; ?>&modestbranding=1" frameborder="0" allowfullscreen></iframe>
                 <?php } ?>

            </div>
            
 		<?php
            } // End WHILE Loop
            wp_reset_postdata();
        }
        ?>
         
            

        </div>
        



    </div>
</div>
   
   <div class="row full-width homepage-grid">
   <div class="row collapse full-width">
  
   
   <?php 
	   
	   // check if the repeater field has rows of data

if( have_rows('blocks') ):	   
	   
  // loop through the rows of data

  // add a counter
 
 // $group = 0;
  
  while ( have_rows('blocks') ) : the_row();
  
    // vars
    $hp_block_title = get_sub_field('hp_block_title');
    $hp_block_intro = get_sub_field('hp_block_intro');
	$hp_block_content_img = get_sub_field('hp_block_content_img');
	$hp_block_content_position = get_sub_field('hp_block_content_position');
	$hp_block_position = get_sub_field('hp_block_position');	   
	$hp_block_transparency = get_sub_field('hp_block_content_bg_transparency');    
	$hp_block_button_text = get_sub_field('hp_block_button_text');
    $hp_block_button_link = get_sub_field('hp_block_button_link');
    $hp_block_image = get_sub_field('hp_block_image');
	   

	   
    //if ($count % 2 == 0) {
      
      ?>
        
        <div class="columns large-6 small-12  homepage-grid-element <?php if($hp_block_position == 'Left') { ?> left<?php } else { ?> right<?php } ?>"  style="background-image: url(<?php echo $hp_block_image; ?>);">
        <div class="row full-width grid-inner">
        <?php  if($hp_block_content_position == 'Right') {?>
        		 <div class="column large-6 small-6 panel"></div>
     			<div class="column large-6 small-6 <?php if($hp_block_transparency != 'Transparent') { ?> white-block <?php } else { ?> transparent-block <?php } ?> panel <?php if($hp_block_content_img != '') { ?> align-self-middle image-block <?php } ?>">
      			 <?php if($hp_block_content_img != '') { ?><img src="<?php echo $hp_block_content_img; ?>" class="philly-image"/><?php } ?>
                   <h2><?php echo $hp_block_title; ?></h2>
                    <p><?php echo $hp_block_intro; ?></p>
                    <a href="<?php echo $hp_block_button_link; ?>" class="transparent-button"><?php echo $hp_block_button_text; ?></a>
			</div>
     				
     	<?php } else { ?>
     			
     			<div class="column large-6 small-6 <?php if($hp_block_transparency != 'Transparent') { ?> white-block <?php } else { ?> transparent-block <?php } ?> panel">
      			<h2><?php echo $hp_block_title; ?></h2>
                    <p><?php echo $hp_block_intro; ?></p>
                    <a href="<?php echo $hp_block_button_link; ?>" class="transparent-button"><?php echo $hp_block_button_text; ?></a>
			</div>	
    		<div class="column large-6 small-6 panel"></div>
     	
     	<?php } ?>																		
      				
		 </div>
    </div>
       
   
	   
    <?php 
    
    endwhile;

endif;
	     wp_reset_postdata();
	   ?>
   
   
   
   
	   </div>
   
</div>
    <?php /*?><div id="header" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
	    <?php
        $args = array('post_type' => 'homepage_slide', 'order' => 'ASC');
        	
        $query=new WP_Query($args);
        	
        if ( $query->have_posts() ) {
            $count = 0;
            while($query->have_posts()){
	            $query->the_post();
                $count++;
                $attachment_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                $related_link_page_id = get_post_meta($post->ID, 'quickrecipe_related_link', true);
                $related_link_page_url = get_permalink($related_link_page_id);

	    ?>
                <div class="item <?php if ($count == 1) echo 'active'; ?>" style="background-image: url(<?php echo esc_url($attachment_url); ?>);">
                  <div class="carousel-caption hidden-xs">
                    <h3><?php the_title(); ?></h3>
                    <p><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></p>
                    <div class="view-btn">
                      <a href="<?php echo esc_url($related_link_page_url); ?>" class="btn btn-success btn-lg"><?php esc_html_e( 'View Recipe', 'quickrecipe' ); ?></a>
                    </div>
                  </div>
                </div>
	    <?php
            } // End WHILE Loop
            wp_reset_postdata();
        }
        ?>
      </div>
      <a class="left carousel-control" href="#header" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#header" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </div> <?php */?>
    <!-- 1 end:header -->

<?php

	$show_search_area = get_option('quickrecipe_show_search_area', 1); 
	$show_latest_recipes = get_option('quickrecipe_show_latest_recipes', 1); 
	$show_offers = get_option('quickrecipe_show_offers', 1); 
	$show_most_favorited = get_option('quickrecipe_show_most_favorited', 1); 
	$show_editor_choice = get_option('quickrecipe_show_editor_choice', 1); 
	$show_latest_news = get_option('quickrecipe_show_latest_news', 1); 
	$show_our_editors = get_option('quickrecipe_show_our_editors', 1); 
	$show_testimonials = get_option('quickrecipe_show_testimonials', 1); 
	$show_most_read = get_option('quickrecipe_show_most_read', 1); 
	
 	//get_template_part('includes/parts/post', 'latest'); 
	if ($show_search_area) {
		get_template_part( 'includes/mainpage/search-area' );
	}
	
	if ($show_latest_recipes) {
		get_template_part( 'includes/mainpage/latest-recipes' );
	}

	if ($show_offers) {
		get_template_part( 'includes/mainpage/offers' );
	}

	if ($show_most_favorited) {
		get_template_part( 'includes/mainpage/most-favorited' );
	}

	if ($show_latest_news) {
		get_template_part( 'includes/mainpage/latest-news' );
	}


	if ($show_editor_choice) {
		get_template_part( 'includes/mainpage/editor-choice' );
	}

	if ($show_our_editors) {
		get_template_part( 'includes/mainpage/our-editors' );
	}

	
	if ($show_testimonials) {
		get_template_part( 'includes/mainpage/testimonials' );
	}

	if ($show_most_read) {
		get_template_part( 'includes/mainpage/most-read' );
	}

get_footer(); 
?>