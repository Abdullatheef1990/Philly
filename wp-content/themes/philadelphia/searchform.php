<?php
/**
 * The template for displaying search forms in quickrecipe
 *
 * @package quickrecipe
 */
?>
<form id="search_form" method="get" class="search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label>
		<input type="search" class="form-control input-lg" placeholder="<?php echo esc_attr_x( 'Type and hit enter to search...', 'placeholder', 'quickrecipe' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
    </label>
    <input type="hidden" name="post_type" value="post">
</form>
