<?php 
/* Template Name: Register Page */

if ( is_user_logged_in() ) {
	wp_redirect( esc_url(get_home_url()) );
	exit;
}


$errors = array();
$success = '';

if( isset($_POST['register']) && $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['registration_form_nonce']) && wp_verify_nonce( $_POST['registration_form_nonce'], 'registration_form' ) ){
    if(function_exists('quickrecipe_add_user')) {
	    $errors = quickrecipe_add_user();
    } else {
        $errors[] = esc_html__( 'You should install and activate to quickrecipe addition plugin.', 'quickrecipe' );
    }
}

$terms_page_id = get_option('quickrecipe_terms_page_url', '');
$terms_page_url = get_permalink($terms_page_id);

$enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);

$login_page_id = get_option('quickrecipe_login_page_url', '');
$login_page_url = get_permalink($login_page_id);
if (!$enable_custom_login || !$login_page_url)  $login_page_url = wp_login_url();

$lost_password_page_id = get_option('quickrecipe_lost_password_page_url', '');
$lost_password_page_url = get_permalink($lost_password_page_id);
if (!$enable_custom_login || !$lost_password_page_url) $lost_password_page_url = wp_lostpassword_url();

$forgot_username_page_id = get_option('quickrecipe_forgot_username_page_url', '');
$forgot_username_page_url = get_permalink($forgot_username_page_id);
if (!$enable_custom_login || !$forgot_username_page_url) $forgot_username_page_url = wp_registration_url();

get_header(); 

$action  = $_GET['action'];

switch ($action)
{
	case 'registered':
        $success = esc_html__( 'Account was successfully created. Please click the activation link in your email before your first log in.', 'quickrecipe' );
	break;

	case 'activate':
        if(function_exists('quickrecipe_activate_user')) {
	        $errors = quickrecipe_activate_user();
            if( empty($errors) ){
                $success = esc_html__( 'Your account has been successfully activated. Thank you for activating your account.', 'quickrecipe' );
            }
        } else {
            $errors[] = esc_html__( 'You should install and activate to quickrecipe addition plugin.', 'quickrecipe' );
        }

	break;

	case 'sendactivation':
        if(function_exists('quickrecipe_email_activation_link')) {
		    if ( quickrecipe_email_activation_link($_GET['user_id']) ) 
                $success = esc_html__( 'Activation link was successfully sent.', 'quickrecipe' );
		    else
                $errors[] = esc_html__( 'An error was encountered when attempting to send the activation link. Please try again later.', 'quickrecipe' );
        }
	break;

}
?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
            <div class="login-wrap">
                <div class="login ">
                    <div class="page-header">
                        <h1><?php esc_html_e('Register', 'quickrecipe'); ?></h1>
                    </div>
                    <div class="login-description">
				        <p>	
				        <?php esc_html_e('Already a member?', 'quickrecipe'); ?> <?php echo sprintf( wp_kses( __( 'Proceed to <a href="%s">login</a> page', 'quickrecipe' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $login_page_url ) ); ?>.<br> <?php esc_html_e('Forgotten your password?', 'quickrecipe'); ?> <a href="<?php echo esc_url($lost_password_page_url); ?>"><?php esc_html_e('Reset it here', 'quickrecipe'); ?></a>.
				        </p>
				        <p class="success">
					        <?php echo esc_html($success); ?>
				        </p>
				        <?php
				        if (count($errors) > 0) {
					        ?>
					        <div class="error">
						        <p><?php esc_html_e( 'Errors were encountered while processing...', 'quickrecipe' ) ?></p>
			                    <?php if (!empty($errors)) { ?>
				                    <p class="error"><?php echo implode('<br />', $errors); ?></p>
			                    <?php } ?> 
					        </div>
					        <?php
				        }
				        ?>
                    </div>
                    <form id="register_form" method="post" action="<?php echo get_permalink(); ?>">
                    <fieldset>
                        <div class="form-group">
					        <label for="user_login"><?php esc_html_e('Username', 'quickrecipe'); ?><span class="star">&nbsp;*</span></label>
					        <input tabindex="1" type="text" id="user_login" name="user_login" class="form-control input-lg" value="<?php echo esc_attr($_POST['user_login']); ?>" />
                        </div>
                        <div class="form-group">
					        <label for="email"><?php esc_html_e('Email', 'quickrecipe'); ?><span class="star">&nbsp;*</span></label>
					        <input tabindex="2" type="email" id="user_email" name="user_email" class="form-control input-lg" value="<?php echo esc_attr($_POST['user_email']); ?>" />
                        </div>
                        <div class="form-group">
                            <div class="form-checkbox">
                                <div class="checkbox">
						            <label>
                                        <input type="checkbox" value="" id="term_and_condition" name="term_and_condition">
                                        <?php echo sprintf(wp_kses(__('I agree to the <a href="%s">terms &amp; conditions</a>.', 'quickrecipe'), array(  'a' => array( 'href' => array() ) ) ), esc_url($terms_page_url)); ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-btn">
				                <?php wp_nonce_field( 'registration_form', 'registration_form_nonce' ) ?>
				                <input type="submit" id="register" name="register" value="<?php esc_html_e('Register', 'quickrecipe'); ?>" class="btn btn-primary"/>
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- end:content -->



	
<?php get_footer(); ?>