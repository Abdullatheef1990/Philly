<?php
// Template Name: Page With Sidebar
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Page Template
 */
	get_header();
?>

<div id="content" class="page column-full">

    <div class="content-wrapper">
		<section id="main" class="column-left">
            
            <div id="content-block" class="content-block">
        <?php
        	if ( have_posts() ) { $count = 0;
        		while ( have_posts() ) { the_post(); $count++;
        ?>
            <article <?php post_class('article-content'); ?>>
                <h2 class="post-title"><?php the_title(); ?></h2>

                <section class="entry">
                	<?php the_content(); ?>

					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'quickrecipe' ), 'after' => '</div>' ) ); ?>
               	</section>

            </article>

            <?php

				}
			} else {
		?>
			<article <?php post_class(); ?>>
            	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'quickrecipe' ); ?></p>
            </article>
        <?php } ?>
            
            </div>
		</section>
        <?php get_sidebar(); ?>
    </div>
</div>

<?php get_footer(); ?>