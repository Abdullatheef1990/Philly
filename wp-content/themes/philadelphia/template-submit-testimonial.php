<?php 
/* Template Name: Submit Testimonial */ 

if (!is_user_logged_in()) { auth_redirect(); }



$post_id = (isset($_GET['post'])) ? intval($_GET['post']) : 0;

$title = null;
$content = null;

$query = new WP_Query(array('p' => $post_id, 'post_type' => 'testimonial', 'posts_per_page' =>'-1', 'post_status' => array('publish', 'pending', 'draft', 'private', 'trash') ) );

if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
	
	if ($post_id) {
		
		if($post_id == $post->ID)
		{
			$current_post = $post->ID;

			$title = get_the_title();
			$content = get_the_content();

			$rating = get_post_meta($current_post, 'quickrecipe_rating', true);
		}
	}

endwhile; endif;
wp_reset_postdata();

global $current_post;

$errors = array();

if(isset($_POST['submitted']) && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {

	if (empty($_POST['post_title'])) {
		$errors['post_title'] = __( 'Please enter a title.', 'quickrecipe' );
	} else {
		$post_title = wp_kses(trim($_POST['post_title']), '');
	}

	if( empty( $errors ) ) {

	    $post_information = array(
		    'post_title' => esc_attr(strip_tags($_POST['post_title'])),
		    'post_content' => esc_attr(strip_tags($_POST['post_content'])),
		    'post_type' => 'testimonial',
		    'post_status' => 'pending'
	    );

        if ($current_post) {
            $post_information['ID'] = $current_post;
	        $new_post_id = wp_update_post($post_information);
        }
        else {
	        $new_post_id = wp_insert_post($post_information);
        }

	    if($new_post_id)
	    {
		    // Update Custom Meta
		    update_post_meta($new_post_id, 'quickrecipe_rating', esc_attr(strip_tags($_POST['rating'])));
        
	        $list_testimonial_page_id = get_option('quickrecipe_list_testimonial_url', '');
	        $list_testimonial_url = get_permalink($list_testimonial_page_id);
		    wp_redirect( $list_testimonial_url ); 
            exit;
	    }
    }
}

$page_by_title = get_page_by_title( get_the_title() );
$the_excerpt = (!empty($page_by_title->post_excerpt)) ? $page_by_title->post_excerpt : $page_by_title->post_content;

get_header();
?>
    <!-- begin:content -->
    <div id="content" class="recipe-form">
      <div class="container">
        <div class="row">
          <!-- begin:article -->
          <div class="col-md-9">
            <div class="row">
            <div class="col-xs-12 single-page">
				<div class="header text-left">
					<h2><?php the_title(); ?></h2>
					<p><?php echo esc_html($the_excerpt); ?></p>
					<hr>
				</div>
				<div class="content">
				<?php
				if(is_user_logged_in())
				{
                ?>
				<?php
				if (count($errors) > 0) {
					?>
					<div>
						<span class="error"><?php esc_html_e( 'Errors were encountered while processing...', 'quickrecipe' ) ?></span>
					</div>
					<?php
				}
				?>

		        <form action="" id="primaryPostForm" method="POST" class="row">
						<div class="form-group col-sm-6">
				            <label for="post_title"><?php esc_html_e('Testimonial Title', 'quickrecipe') ?></label>
				            <input type="text" name="post_title" id="post_title" value="<?php echo esc_attr($title); ?>" class="form-control input-lg required" placeholder="" />
			                <?php if (!empty($errors['post_title'])) { ?>
				                <span class="error"><?php echo esc_html($errors['post_title']); ?></span>
			                <?php } ?>
							<p class="description"><?php esc_html_e('Keep it short and descriptive', 'quickrecipe') ?></p>
						</div>
						<div class="form-group col-sm-12">
				            <label for="post_content"><?php esc_html_e('Testimonial Content', 'quickrecipe') ?></label>
				            <textarea class="form-control input-lg" name="post_content" id="post_content" rows="10" cols="30"><?php echo esc_textarea($content); ?></textarea>
						</div>
						<!-- / column -->
						<div class="col-sm-12">
				            <?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>

				            <input type="hidden" name="submitted" id="submitted" value="true" />
							<button type="submit" class="submit">
								<?php esc_html_e('Submit', 'quickrecipe') ?> <i class="fa fa-arrow-right"></i>
							</button>
						</div>
					</form>
                    <?php } ?>
					<!-- end:form -->
				</div>
				<!-- end:content -->
			</div>
            </div>
          </div>
          <!-- end:article -->

          <!-- begin:sidebar -->
          <?php quickrecipe_get_user_sidebar(); ?>
          <!-- end:sidebar -->

        </div>
      </div>
    </div>
    <!-- end:content -->


<?php get_footer(); ?>