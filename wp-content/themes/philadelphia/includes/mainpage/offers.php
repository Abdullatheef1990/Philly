<?php


$offers_limit = get_option('quickrecipe_offers_limit', 4); 
$offers_title = get_option('quickrecipe_offers_title', ''); 
$offers_title_desc = get_option('quickrecipe_offers_title_desc', ''); 

?>
    <!-- begin:offer -->
    <div id="offer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <?php if ( !empty($offers_title) ): ?>
              <div class="heading">
                  <h2>
                      <?php echo esc_attr( stripslashes( $offers_title ) ); ?><?php if ( $offers_title_desc ): ?>
                      <small>
                          <?php echo esc_attr( stripslashes( $offers_title_desc ) ); ?>
                      </small><?php endif; ?>
                  </h2>
              </div>	
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
			<?php
        	$query_args = array(
        						'post_type' => 'we_offer', 
        						'posts_per_page' => intval( $offers_limit )
        					);
        	
            $query=new WP_Query($query_args);
        	
        	if ( $query->have_posts() ) {
                while($query->have_posts()){
	                $query->the_post();
			?>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="offer-container">
              <div class="offer-icon">
                <i class="flaticon-circle <?php echo get_post_meta($post->ID, 'quickrecipe_we_offer', true); ?>"></i>
              </div>
              <div class="offer-content">
                <h3><?php the_title(); ?></h3>
                <p><?php echo wp_trim_words(get_the_excerpt(), 48, '...'); ?></p>
              </div>
            </div>
          </div>
          <!-- break -->
			<?php
        		} // End WHILE Loop
        	    wp_reset_postdata();
        	} else {
	        ?>
	        <article <?php post_class(); ?>>
	            <p class="noarticle"><?php esc_html_e( 'Offers are not available.', 'quickrecipe' ); ?></p>
	        </article><!-- /.post -->
	        <?php } ?> 

        </div>
      </div>  
    </div>
    <!-- end:offer -->
