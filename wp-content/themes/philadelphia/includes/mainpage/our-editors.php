<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$our_editors_limit = get_option('quickrecipe_our_editors_limit', 4); 
$our_editors_title = get_option('quickrecipe_our_editors_title', ''); 
$our_editors_title_desc = get_option('quickrecipe_our_editors_title_desc', ''); 

?>
    <div id="top_authors">
      <div class="container">
        <!-- begin:latest -->
        <div class="row">
          <div class="col-md-12">
          <?php if ( !empty($our_editors_title) ): ?>
            <div class="heading">
              <?php
	              if ( !empty($our_editors_title) ): ?><h2><?php echo esc_html( stripslashes( $our_editors_title ) ); ?></h2><?php endif;
	              if ( !empty($our_editors_title_desc) ): ?><p><?php echo esc_html( stripslashes( $our_editors_title_desc ) ); ?></p><?php endif; ?>	
            </div>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
        <?php
            $number     = $our_editors_limit;
            $paged      = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $offset     = ($paged - 1) * $number;
            $query      = get_users( 'blog_id=1&orderby=nicename&role=Editor' ); //role=subscriber
            $users      = get_users('blog_id=1&orderby=nicename&role=Editor&offset='.$offset.'&number='.$number);
            $total_users = count($query);
            $total_query = count($users);
            $total_pages = intval($total_users / $number) + 1;

        	if ( count($query) ) {
                foreach ( $users as $user ) {
                    $comments_count = $wpdb->get_var( $wpdb->prepare( 
	                    "
		                    SELECT SUM(comment_count)
		                    FROM $wpdb->posts 
		                    WHERE comment_count != '0' AND post_author = %s
	                    ", 
	                    $user->ID
                    ) );
        ?>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="item-container">
              <div class="item-img-box">
                <?php echo get_avatar( $user->ID, 400 ); ?>
                <a href="<?php echo esc_url(get_author_posts_url( $user->ID, get_the_author_meta( 'user_nicename' ) )); ?>"><div class="hover-button"><?php esc_html_e('Recipes', 'quickrecipe'); ?></div></a>
              </div>
              <div class="item-meta-box">
                <span><i class="fa fa-spoon"></i> <?php echo count_user_posts( $user->ID , 'recipe' ); ?> <?php esc_html_e( 'Recipes', 'quickrecipe' ); ?></span>
                <span><i class="fa fa-comment-o"></i> <?php echo (($comments_count) ? $comments_count : 0); ?> <?php esc_html_e( 'Comment', 'quickrecipe' ); ?></span>
              </div>
              <div class="item-content-box">
                <div class="author-details">
                    <h3 class="title"><a href="<?php echo esc_url(get_author_posts_url( $user->ID )); ?>"><?php echo esc_html( $user->display_name ); ?></a> <small><?php echo esc_html($user->user_title); ?></small></h3>
                    <?php if ( !empty($user->user_email) ) { ?>
                    <div class="contact">
                        <i class="fa fa-envelope-o"></i><a href="mailto:<?php echo antispambot($user->user_email, 1); ?>"><?php echo antispambot($user->user_email); ?></a></div>
                    <?php } ?>
                    <?php if ( !empty($user->user_url) ) { ?>
                    <div class="contact">
                        <i class="fa fa-globe"></i><a href="<?php echo esc_url($user->user_url); ?>"><?php echo esc_html($user->user_url); ?></a></div>
                    <?php } ?>
                    <?php if ( !empty($user->user_phone) ) { ?>
                    <div class="contact">
                        <i class="fa fa-mobile"></i><?php echo esc_html($user->user_phone); ?></div>
                    <?php } ?>
                </div>
              </div>              
              <div class="item-button-box">
                <?php echo quickrecipe_get_author_social($user->ID); ?>
              </div>
            </div>
          </div>
          <!-- break -->
		<?php
        	} 
        } else {
	    ?>
	        <article <?php post_class(); ?>>
	            <p class="noarticle"><?php esc_html_e( 'There is no member available.', 'quickrecipe' ); ?></p>
	        </article>
	    <?php } ?> 

        </div>
        <!-- end:latest -->
       </div>
    </div>

