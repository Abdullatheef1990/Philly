<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$testimonials_limit = get_option('quickrecipe_testimonials_limit', 4); 
$testimonials_title = get_option('quickrecipe_testimonials_title', ''); 
$testimonials_title_desc = get_option('quickrecipe_testimonials_title_desc', ''); 

?>

	<div id="testimonial" style="background-image: url(<?php echo get_template_directory_uri() . '/img/img01.jpg'; ?>);">
		<div class="container">
    		<?php if ( $testimonials_title != '' ): ?>
				<?php if ( !empty($testimonials_title) ): ?><h3><?php echo esc_html( stripslashes( $testimonials_title ) ); ?></h3><?php endif; ?>
				<?php if ( !empty($testimonials_title_desc) ): ?><p><?php echo esc_html( stripslashes( $testimonials_title_desc ) ); ?></p><?php endif; ?>
			<?php endif; ?>		

			<div class="row">
                <?php
        	    $query_args = array(
        						    'post_type' => 'testimonial', 
        						    'posts_per_page' => intval( $testimonials_limit )
        					    );
        	
                $query=new WP_Query($query_args);
        	
        	    if ( $query->have_posts() ) {
                ?>
				<div id="testimonial-slide" class="owl-carousel owl-theme testimonial-slide">
				<?php
                    while($query->have_posts()){
	                    $query->the_post();
				?>

				  <div class="item">
				     <div class="testimonial-quote-item">
                            <?php echo get_avatar( get_the_author_meta( 'ID' ), '80', null, null, array( 'class' => array( 'testimonial-thumb' ) ) ); ?>
				         <span class="quote-text">
				           <?php echo wp_trim_words(get_the_excerpt(), 48, '...'); ?>
				         </span>
				         <span class="quotes-author"><?php echo get_the_author(); ?>, </span>
				         <span class="quotes-subtext"><?php the_author_meta( 'user_title' ); ?></span>
				       	
				     </div>
				  </div><!--/ Item end -->
			    <?php
        		    } // End WHILE Loop
        	        wp_reset_postdata();
	            ?>
				</div><!--/ Testimonial carousel end-->
                <?php
        	    } else {
                // In the "Theme Options > Appearance > Testimonials" section, select the "Yes" option from the list to show it on the homepage
	            ?>
	                <article <?php post_class(); ?>>
	                    <p class="noarticle"><?php esc_html_e( 'Testimonials are not available.', 'quickrecipe' ); ?></p>
	                </article><!-- /.post -->
	            <?php } ?> 

			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</div><!--/ Testimonial end -->


