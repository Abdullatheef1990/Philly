<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$editor_choice_limit = get_option('quickrecipe_editor_choice_limit', 3); 
$editor_choice_title = get_option('quickrecipe_editor_choice_title', ''); 
$editor_choice_title_desc = get_option('quickrecipe_editor_choice_title_desc', ''); 

?>

    <!-- begin:content -->
    <div id="top-rated">
      <div class="container">
        <!-- begin:latest -->
        <div class="row">
          <div class="col-md-12">
            <?php if ( !empty($editor_choice_title) ): ?>
            <div class="heading">
              <?php 
	              if ( !empty($editor_choice_title) ): ?><h2><?php echo esc_html( stripslashes( $editor_choice_title ) ); ?></h2><?php endif;
	              if ( !empty($editor_choice_title_desc) ): ?><p><?php echo esc_html( stripslashes( $editor_choice_title_desc ) ); ?></p><?php endif; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
            <div class="row">
		        <?php
                $query_args = array(
        					        'post_type' => 'recipe', 
        					        'posts_per_page' => intval( $editor_choice_limit ),
	                                'orderby'    => 'meta_value',
	                                'order'      => 'DESC',
	                                'meta_query' => array(
		                                array(
			                                'key'     => 'quickrecipe_editor_choice_recipe',
			                                'value'   => 'true',

		                                ),
	                                )
        				        );
        	
                $query=new WP_Query($query_args);
        	
                if ( $query->have_posts() ) {
                    while($query->have_posts()){
	                    $query->the_post();

		        ?>
                <div class="col-sm-4">
                    <div class="thumb">
				        <?php
                        $recipe_image = get_post_meta($post->ID, 'quickrecipe_editor_choice_recipe_image', true);
                        if (!empty($recipe_image)) {
                            echo wp_get_attachment_image( $recipe_image, 'large' );
                        }
                        else {
			                if ( has_post_thumbnail( $post->ID ) ) {
				                echo get_the_post_thumbnail( $post->ID, 'large' );
			                }
                            else {
                            ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/img01.jpg'; ?>" alt="<?php the_title(); ?>">
                            <?php
                            }
                        }
				        ?>
                        <div class="thumb-overlay">
                            <div class="inner">
                                <div class="thumb-row">
                                    <div class="thumb-cell">
                                        <h4 class="thumb-heading">
                                            <a class="thumb_text" href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><?php the_title(); ?></a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
		        <?php
        	        }
                    wp_reset_postdata();
        	
                } else {
	            ?>
	                <article <?php post_class(); ?>>
	                    <p class="noarticle"><?php esc_html_e( 'Top recipes are not available.', 'quickrecipe' ); ?></p>
	                </article>
	            <?php } ?> 

            </div>
        </div>
    </div>
