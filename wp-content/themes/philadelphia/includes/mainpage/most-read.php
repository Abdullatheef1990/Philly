<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$most_read_limit = get_option('quickrecipe_most_read_limit', 4); 
$most_read_title = get_option('quickrecipe_most_read_title', ''); 
$most_read_title_desc = get_option('quickrecipe_most_read_title_desc', ''); 

?>

	<div id="most-read">
		<div class="container">
            <div class="row">
              <div class="col-md-12">
                <?php if ( !empty($most_read_title) ): ?>
                <div class="heading">
				       <?php if ( !empty($most_read_title) ): ?><h2><?php echo esc_html( stripslashes( $most_read_title ) ); ?></h2><?php endif; ?>
				       <?php if ( !empty($most_read_title_desc) ): ?><p><?php echo esc_html( stripslashes( $most_read_title_desc ) ); ?></p><?php endif; ?>
                </div>
                <?php endif; ?>
              </div>
            </div>

			<div class="row">
				<?php
        	    $query_args = array(
        						    'post_type' => 'recipe', 
        						    'posts_per_page' => intval( $most_read_limit ),
	                                'orderby'    => 'meta_value_num', //meta_value
                                    'meta_key'  => 'post_views_count',
	                                'order'      => 'DESC',
	                                'meta_query' => array(
		                                array(
			                                'key'     => 'quickrecipe_editor_choice_recipe',
			                                'value'   => 'true',
			                                'compare' => 'NOT IN',
		                                ),
		                                array(
			                                'key'     => 'post_views_count',
			                                'value'   => '0',
			                                'compare' => '>',
		                                ),
	                                )
        					    );
        	
                $query=new WP_Query($query_args);
        	
        	    if ( $query->have_posts() ) {
                ?>
                <div id="article-slide" class="owl-carousel owl-theme article-slide">
                <?php
        		    while($query->have_posts()){
	                    $query->the_post();
                        $term_names = wp_get_post_terms($post->ID, 'course', array("fields" => "names"));
                        $term_list = array();
                        if (is_array($term_names)) {
                            foreach($term_names as $term_name) {
                                $term_list[] = $term_name;
                            }
                            $term_name_list = join( ", ", $term_list );
                        } else {
                            $term_name_list = '';
                        }
				?>

				  <div class="item">
                      <div class="col-sm-12 col-xs-12">
                        <div class="item-container">
                          <div class="item-content-box-list">
                            <div class="item-img-box-list">
						      <?php
						      if ( has_post_thumbnail( $post->ID ) ) {
							     echo get_the_post_thumbnail( $post->ID, 'large' );
						      }
						      ?>
                              <a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><div class="hover-button"><?php esc_html_e('View Recipe', 'quickrecipe'); ?></div></a>
                              <div class="item-meta-box">
                                <span><i class="fa fa-clock-o"></i> <?php $prep_time = intval( get_post_meta(get_the_ID(), 'quickrecipe_prep_time', true) ); echo esc_attr(quickrecipe_get_mins_to_hours($prep_time)); ?></span>
                                <span><i class="fa fa-user"></i> <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true); if (!empty($servings)) { echo sprintf(__('%s People', 'quickrecipe'), esc_attr($servings)); } ?></span>
                                <span><i class="fa fa-spoon"></i> <?php $yield = get_post_meta($post->ID, 'quickrecipe_yield', true); if (!empty($servings)) { echo sprintf(__('%s Yield', 'quickrecipe'), esc_attr($yield)); } ?></span>
                              </div>
                            </div>
                            <div class="item-text">
                              <h3><a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><?php the_title(); ?></a> <small><?php echo esc_html($term_name_list); ?></small></h3>
                              <p><?php echo wp_trim_words(get_the_excerpt(), 12, '...'); ?></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- break -->
				  </div><!--/ Item end -->
			    <?php
        		    } 
        	        wp_reset_postdata();
                ?>
				</div><!--/ Testimonial carousel end-->
                <?php
        	    } else {
	            ?>
	                <article <?php post_class(); ?>>
	                    <p class="noarticle"><?php esc_html_e( 'Most popular recipes are not available.', 'quickrecipe' ); ?></p>
	                </article>
	            <?php } ?> 
			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</div><!--/ Testimonial end -->
