<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$latest_recipe_limit = get_option('quickrecipe_latest_recipe_limit', 4); 
$latest_recipe_title = get_option('quickrecipe_latest_recipe_title', ''); 
$latest_recipe_title_desc = get_option('quickrecipe_latest_recipe_title_desc', ''); 

?>

    <!-- begin:content -->
    <div id="latest">
      <div class="container">
        <!-- begin:latest -->
        <?php if ( $latest_recipe_title != '' ): ?>
        <div class="row">
          <div class="col-md-12">
            <div class="heading">
			  <?php if ( !empty($latest_recipe_title) ): ?><h2><?php echo esc_html( stripslashes( $latest_recipe_title ) ); ?></h2><?php endif; ?>
			  <?php if ( !empty($latest_recipe_title_desc) ): ?><p><?php echo esc_html( stripslashes( $latest_recipe_title_desc ) ); ?></p><?php endif; ?>
            </div>
          </div>
        </div>
        <?php endif; ?>
        <div class="row">
		<?php
        $query_args = array(
        					'post_type' => 'recipe', 
        					'posts_per_page' => intval( $latest_recipe_limit ),
                            'post_status' => 'publish',
	                        'meta_query' => array(
		                        array(
			                        'key'     => 'quickrecipe_editor_choice_recipe',
			                        'value'   => 'true',
			                        'compare' => 'NOT IN',
		                        ),
			                    array(
				                    'key' => '_thumbnail_id',
			                    ),
	                        ),

        				);
        	
        $query=new WP_Query($query_args);
        	
        if ( $query->have_posts() ) {
            while($query->have_posts()){
	            $query->the_post();
                $term_names = wp_get_post_terms($post->ID, 'course', array("fields" => "names"));
                $term_list = array();
                if (is_array($term_names)) {
                    foreach($term_names as $term_name) {
                        $term_list[] = $term_name;
                    }
                    $term_name_list = join( ", ", $term_list );
                } else {
                    $term_name_list = '';
                }
                $recipe_badge = '';

		?>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="item-container">
              <div class="item-img-box">
                <?php
				if ( has_post_thumbnail( $post->ID ) ) {
					echo get_the_post_thumbnail( $post->ID, 'large' );
				}
	            ?>
                <a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><div class="hover-button"><?php esc_html_e('View Recipe', 'quickrecipe'); ?></div></a>
                <?php if ($recipe_badge) { ?>
                <div class="item-badge">
                  <span><?php echo esc_html($recipe_badge); ?></span>
                </div>
                <?php } ?>
              </div>
              <div class="item-meta-box">
                <span><i class="fa fa-clock-o"></i> <?php $prep_time = intval( get_post_meta(get_the_ID(), 'quickrecipe_prep_time', true) ); echo esc_attr(quickrecipe_get_mins_to_hours($prep_time)); ?></span>
                <span><i class="fa fa-user"></i> <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true); if (!empty($servings)) { echo sprintf( wp_kses(__('%s People', 'quickrecipe'), ''), esc_attr($servings)); } ?></span>
                <span><i class="fa fa-spoon"></i> <?php $yield = get_post_meta($post->ID, 'quickrecipe_yield', true); if (!empty($servings)) { echo sprintf( wp_kses(__('%s Yield', 'quickrecipe'), ''), esc_attr($yield)); } ?></span>
              </div>
              <div class="item-content-box">
                <h3><a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><?php the_title(); ?></a> <small><?php echo esc_html($term_name_list); ?></small></h3>
                <div class="avatar">
                    <div class="clearfix">
                        <div class="pull-left">
                            <?php echo get_avatar( get_the_author_meta( 'ID' ), '25' ); ?>
                            <?php esc_html_e('By ', 'quickrecipe'); ?> <?php the_author_posts_link(); ?>
                        </div>
                        <span class="user-ratings pull-right">
                            <?php echo average_rating(); ?>
                        </span>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <!-- break -->
        <?php
            } // End WHILE Loop
            wp_reset_postdata();
        	
        } else {
        ?>
	        <article <?php post_class(); ?>>
	            <p class="noarticle"><?php esc_html_e( 'Latest recipes are not available.', 'quickrecipe' ); ?></p>
	        </article><!-- /.post -->
        <?php } ?> 

        </div>
        <!-- end:latest -->
       </div>
    </div>
