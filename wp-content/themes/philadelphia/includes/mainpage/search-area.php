<?php
if ( ! defined( 'ABSPATH' ) ) exit;


?>
    <!-- begin:search -->
    <div class="main-search">
      <div class="container">
      <form method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
      <div class="search-box">
        <div class="row">
            <?php get_template_part( 'includes/mainpage/search-fields' ); ?>
        </div>
        <div class="button-group">
            <div class="action-buttons">
                <button type="submit"  class="btn btn-success"><i class="fa fa-search"></i> <?php echo esc_attr_x( 'Search', 'submit button', 'quickrecipe' ); ?></button>
                <button type="reset" class="btn"><i class="fa fa-times"></i> <?php esc_html_e('Clear', 'quickrecipe'); ?></button>
            </div>
        </div>
        </div>
        </form>
      </div>
    </div>
    <!-- end:search -->
