<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$latest_news_limit = get_option('quickrecipe_latest_news_limit', 2); 
$latest_news_title = get_option('quickrecipe_latest_news_title', ''); 
$latest_news_title_desc = get_option('quickrecipe_latest_news_title_desc', ''); 

?>

    <div id="latest-news">
      <div class="container">
        <!-- begin:for-latest-news -->
        <div class="row">
          <div class="col-md-12">
            <div class="heading">
    		<?php if ( !empty($latest_news_title) ): 
				    if ( !empty($latest_news_title) ): ?><h2><?php echo esc_html( stripslashes( $latest_news_title ) ); ?></h2><?php endif;
				    if ( !empty($latest_news_title_desc) ): ?><p><?php echo esc_html( stripslashes( $latest_news_title_desc ) ); ?></p><?php endif; 
                endif; ?>		
            </div>
          </div>
        </div>
        <div class="row">
	    <?php
            $args = array('post_type'=>'post', 'posts_per_page'=> intval( $latest_news_limit ));

            $get_posts_query = new WP_Query($args);
            if($get_posts_query->have_posts()):
                while($get_posts_query->have_posts()):
                    $get_posts_query->the_post();
                ?>
                  <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="item-container">
                      <div class="item-content-box-list">
                        <div class="item-img-box-list">
                          <?php
                          if ( has_post_thumbnail() ) {
	                          the_post_thumbnail( 'large' );
                          }
                          ?>
                          <div class="item-meta-box">
                            <span><i class="fa fa-comments"></i> <?php comments_number( 'no responses', 'one response', '% responses' ); ?></span>
                            <span><i class="fa fa-eye"></i> <?php echo quickrecipe_get_post_views(get_the_ID()); ?></span>
                            <span><i class="fa fa-folder"></i> <?php $category = get_the_category ( get_the_ID() ); echo esc_html($category[0]->cat_name); ?></span>
                          </div>
                        </div>
                        <div class="item-text">
                          <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a><small><i class="fa fa-calendar"></i> <?php echo get_the_date(); ?></small></h3>
                          <p><?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?></p>
                            <div class="avatar">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <?php echo get_avatar( get_the_author_meta( 'ID' ), '25' ); ?>
                                        <?php esc_html_e('By ', 'quickrecipe'); ?> <?php the_author_posts_link(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- break -->
                <?php
                endwhile;
            else:
	        ?>
	            <article <?php post_class(); ?>>
	                <p class="noarticle"><?php esc_html_e( 'Editor\'s choice recipes are not available.', 'quickrecipe' ); ?></p>
	            </article><!-- /.post -->
	        <?php
            endif;
        ?> 

	    <?php wp_reset_postdata(); ?>

        </div>
        <!-- end:for-latest-news -->
      </div>
    </div>
    <!-- end:content -->
