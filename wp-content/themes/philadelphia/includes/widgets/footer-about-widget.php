<?php

add_action( 'widgets_init', 'quickrecipe_about_widget' );
if( !function_exists('quickrecipe_about_widget') ){
	function quickrecipe_about_widget() {
		register_widget( 'Footer_About_Widget' );
	}
}


if(!class_exists('Footer_About_Widget')){
    class Footer_About_Widget extends WP_Widget {

        public function __construct() {
            $widget_ops = array( 'classname' => 'Footer_About_Widget', 'description' => __('This widget is used to display information about your site.', 'quickrecipe'));
            parent::__construct( 'footer_about_widget', __('Quick Recipe: Footer About Widget', 'quickrecipe'), $widget_ops );
        }

        function widget($args,  $instance) {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);

            $about_text =  $instance['about_text'];

            echo $before_widget;
            echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';
            ?>
            <p><?php echo wp_trim_words(esc_html($about_text), 100, '...' ); ?></p>

        <?php
            echo $after_widget;
        }


        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array('about_text' => 'text', 'title' => __('About Us', 'quickrecipe') ) );

            $title= $instance['title'];
            $about_text =  $instance['about_text'];

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('about_text')); ?>"><?php esc_html_e('About Us', 'quickrecipe'); ?></label>
                <textarea class="widefat" rows="3" id="<?php echo esc_attr($this->get_field_id('about_text')); ?>" name="<?php echo esc_attr($this->get_field_name('about_text')); ?>"><?php echo esc_attr($about_text); ?></textarea>
            </p>
        <?php
        }


        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['about_text'] = strip_tags($new_instance['about_text']);

            return $instance;
        }

    }
}
?>