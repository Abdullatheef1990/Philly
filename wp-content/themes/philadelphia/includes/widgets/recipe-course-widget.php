<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'widgets_init', 'quickrecipe_course_widget' );
if( !function_exists('quickrecipe_course_widget') ){
	function quickrecipe_course_widget() {
		register_widget( 'Recipe_Courses_Widget' );
	}
}


if(!class_exists('Recipe_Courses_Widget')){
    class Recipe_Courses_Widget extends WP_Widget {

        public function __construct() {
            $widget_ops = array( 'classname' => 'Recipe_Courses_Widget', 'description' => __('Display a list of recipe courses.', 'quickrecipe'));
            parent::__construct( 'Recipe_Courses_Widget', __('Quick Recipe: Recipe Courses', 'quickrecipe'), $widget_ops );
        }

        function widget($args,  $instance)
        {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);
            if ( empty($title) )
                $title = false;

            $show_count = 0; // 1 for yes, 0 for no
            $pad_counts = 0; // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => 'course',
                'title_li'     => '',
                'show_count' => $show_count,
                'pad_counts' => $pad_counts,
            );

            echo $before_widget;
		    echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';

            echo '<ul>';
            wp_list_categories( $args );
            echo '</ul>';

            echo $after_widget;
        }


	    public function form( $instance ) 
        {
		    $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Recipe Courses', 'quickrecipe' );
		    ?>
		    <p>
		        <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Widget Title:', 'quickrecipe' ); ?></label> 
		        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		    </p>
		    <?php 
	    }

	    public function update( $new_instance, $old_instance ) 
        {
		    $instance = array();
		    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : $old_instance;

		    return $instance;
	    }

    }
}

?>