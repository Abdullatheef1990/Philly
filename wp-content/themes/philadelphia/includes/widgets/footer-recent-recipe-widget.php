<?php

add_action( 'widgets_init', 'quickrecipe_recent_recipe_widget' );
if( !function_exists('quickrecipe_recent_recipe_widget') ){
	function quickrecipe_recent_recipe_widget() {
		register_widget( 'Recent_Recipe_Widget' );
	}
}

if(!class_exists('Recent_Recipe_Widget')){
    class Recent_Recipe_Widget extends WP_Widget {
	    /**
	     * Sets up the widgets name etc
	     */
	    public function __construct() {
		    $widget_ops = array( 
			    'classname' => 'Recent_Recipe_Widget',
			    'description' => __('This widget displays a list of recent recipes.', 'quickrecipe'),
		    );
		    parent::__construct( 'Recent_Recipe_Widget', __('Quick Recipe: Recent Recipes Widget', 'quickrecipe'), $widget_ops );
	    }

        public function widget($args, $instance) {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);
            if ( empty($title) )
                $title = false;

            $num_words = $instance['num_words'];
            $posts_per_page =  $instance['posts_per_page'];

            echo $before_widget;
            echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';
            $args = array( 'post_type'=>'recipe', 'posts_per_page' => $posts_per_page );

            $the_query = new WP_Query($args);

            if($the_query->have_posts()):
                while($the_query->have_posts()):
                    $the_query->the_post();
                    ?>
                    <div class="item">
                        <figure class="figure">
                            <?php if(has_post_thumbnail()){ 
                                the_post_thumbnail(); 
                            } ?>
                        </figure>
                        <div class="content">
                            <h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php echo wp_trim_words( esc_html(get_the_title()), $num_words, '...' ); ?></a></h4>
                            <p class="post-content"><?php echo wp_trim_words( esc_html(get_the_content()), 16, '...' ); ?></p>
                            <ul class="meta list-inline">
                                <li><?php echo esc_html(get_the_date()); ?></li>
                                <li><?php echo esc_html(get_the_author()); ?></li>
                            </ul>
                        </div><!-- content-->
                    </div>  
                <?php
                endwhile;
            endif;
            
            echo $after_widget;
        }


        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array( 'title' => __('Recent Recipes', 'quickrecipe'),'posts_per_page' => 4,'num_words' => 4 ) );

            $title= $instance['title'];
            $num_words = $instance['num_words'];
            $posts_per_page =  $instance['posts_per_page'];

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('num_words')); ?>"><?php esc_html_e('Number of Words', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('num_words')); ?>" name="<?php echo esc_attr($this->get_field_name('num_words')); ?>" type="text" value="<?php echo esc_attr($num_words); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('posts_per_page')); ?>"><?php esc_html_e('Number of Posts Per Page', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('posts_per_page')); ?>" name="<?php echo esc_attr($this->get_field_name('posts_per_page')); ?>" type="text" value="<?php echo esc_attr($posts_per_page); ?>" />
            </p>
        <?php
        }

        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['num_words'] = intval($new_instance['num_words']);
            $instance['posts_per_page'] = intval($new_instance['posts_per_page']);

            return $instance;

        }

    }
}

?>