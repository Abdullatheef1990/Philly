<?php

defined('ABSPATH') or die('<meta http-equiv="refresh" content="0;url='.get_site_url().'">');

add_action( 'widgets_init', 'quickrecipe_adsense_widget' );
if( !function_exists('quickrecipe_adsense_widget') ){
	function quickrecipe_adsense_widget() {
		register_widget( 'Google_Adsense_Widget' );
	}
}

class Google_Adsense_Widget extends WP_Widget {
	
	public function __construct() {
		parent::__construct( 'google_adsense_widget', __('Quick Recipe: Adsense Box', 'quickrecipe'), $widget_options = array(
			'classname'   => 'google_adsense_widgets',
			'description' => __('Show an google adsense.', 'quickrecipe')
		) );
	}

	public function widget( $args, $instance ) {
		
		$title  	= apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$show_title = empty( $instance['show_title'] ) ? 0 : absint( $instance['show_title'] );
		$code 		= $this->get_code( $instance );
		
		echo $args['before_widget'];
		
		if ( $title != '' && $show_title ) :
			echo $args['before_title'].$title.$args['after_title'];
		endif;
		
		echo $code;
		
		echo $args['after_widget'];
				
	}
	
	function update( $new_instance, $instance ) {
		$instance['title']  	= strip_tags( $new_instance['title'] );
		$instance['show_title'] = empty( $new_instance['show_title'] ) ? 0 : absint($new_instance['show_title']);
		$instance['code'] 		= empty( $new_instance['code'] ) ? '' : $new_instance['code'];
		$instance['before'] 	= empty( $new_instance['before'] ) ? '' : $new_instance['before'];
		$instance['after'] 		= empty( $new_instance['after'] ) ? '' : $new_instance['after'];
		
		return $instance;
	}

	function form( $instance ) {
		$title  	= apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$show_title = empty( $instance['show_title'] ) ? 0 : absint( $instance['show_title'] );
		$code 		= $this->get_code( $instance );
		$before 	= empty( $instance['before']) ? '' : $instance['before'];
		$after 		= empty( $instance['after']) ? '' : $instance['after'];
		$before_more = empty( $instance['before_more']) ? '' : $instance['before_more'];
		$after_more = empty( $instance['after_more']) ? '' : $instance['after_more'];
		
		?>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'quickrecipe' ); ?></label></p>
			<p><input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>" /></p>
			<p><input type="checkbox" value="1" id="<?php echo esc_attr( $this->get_field_id( 'show_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_title' ) ); ?>" <?php echo $show_title?'checked':'';?> /><label for="<?php echo esc_attr( $this->get_field_id( 'show_title' ) ); ?>"><?php _e( 'Show Title', 'quickrecipe' ); ?></label></p>
			<p><label for="<?php echo $this->get_field_id( 'code' ); ?>"><?php _e( 'Adsense Code', 'quickrecipe' ); ?>:</label></p>
			<p><textarea id="<?php echo $this->get_field_id( 'code' ); ?>" name="<?php echo $this->get_field_name( 'code' ); ?>" rows="5" style="width:100%; height: 100px;"><?php echo $code; ?></textarea></p>
		<?php
		
	}
	
	private function get_code( $instance ) {
		
		$ad_code = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><ins class="adsbygoogle" style="display:block" data-ad-client="<your code>" data-ad-slot="4868992390" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>';
		
		$code = empty( $instance['code']) ? $ad_code : $instance['code'];
		
		return $code;
	}
	
}
