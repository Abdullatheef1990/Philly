<?php

add_action( 'widgets_init', 'quickrecipe_related_cuisines_widget' );
if( !function_exists('quickrecipe_related_cuisines_widget') ){
	function quickrecipe_related_cuisines_widget() {
		register_widget( 'Related_Cuisines_Sidebar_Widget' );
	}
}


if(!class_exists('Related_Cuisines_Sidebar_Widget')){
    class Related_Cuisines_Sidebar_Widget extends WP_Widget {

        public function __construct() {
            $widget_ops = array( 'classname' => 'Related_Cuisines_Sidebar_Widget', 'description' => __('Display a list of related cuisines.', 'quickrecipe'));
            parent::__construct( 'related_cuisines_sidebar_widget', __('Quick Recipe: Related Cuisines Sidebar', 'quickrecipe'), $widget_ops );
        }


        function widget($args, $instance) {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);
            if ( empty($title) )
                $title = false;

            $num_words = $instance['num_words'];
            $posts_per_page =  $instance['posts_per_page'];

            echo $before_widget;
            echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';

            echo '<div class="popular-post">
                <ul>';

            $post_id = get_the_ID();

	        $cuisine_slugs = array();
	        $cuisine_terms = get_the_terms( $post_id, 'cuisine' );
	        if( $cuisine_terms && !is_wp_error( $cuisine_terms ))
	        {
		        foreach($cuisine_terms as $term)
		        {
			        $cuisine_slugs[] = $term->slug;
		        }
	        }

	        $recipe_type_slugs = array();
	        $recipe_type_terms = get_the_terms( $post_id, 'recipe_type' );
	        if( $recipe_type_terms && !is_wp_error( $recipe_type_terms ))
	        {
		        foreach($recipe_type_terms as $term)
		        {
			        $recipe_type_slugs[] = $term->slug;
		        }
	        }

            $args = array(  'post_type' => 'recipe',
							'posts_per_page' => $posts_per_page,
							'post__not_in' => array($post_id),
                            
							'tax_query' => array(
												array(
													'taxonomy' => 'cuisine',
													'field' => 'slug',
													'terms' => $cuisine_slugs
												),
												array(
													'taxonomy' => 'recipe_type',
													'field' => 'slug',
													'terms' => $recipe_type_slugs
												)
											),

							'meta_query' => array(
													array(
													 'key' => '_thumbnail_id',
													 'value' => '0',
													 'compare' => '>',
													 'type' => 'NUMERIC'
													)
											)
							);

            $the_query = new WP_Query($args);

            if($the_query->have_posts()):
                while($the_query->have_posts()):
                    $the_query->the_post();
                    $image_id = get_post_thumbnail_id();
                    $image_url = wp_get_attachment_image_src($image_id,'full-size', true);
                    ?>
						
							<li class="img-cap-effect">
								<div class="img-box">
                                <?php if(has_post_thumbnail()){ 
                                    the_post_thumbnail('thumbnail', array( 'class' => 'recipe-thumb' )); 
                                } ?>
								</div>
								<div class="content">
									<a href="<?php the_permalink(); ?>"><h4><?php echo wp_trim_words(get_the_title(),$num_words,'...'); ?></h4></a>
									<span><i class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span>
								</div>
							</li>
						
                <?php
                endwhile;
            endif;
            echo '</ul>
            </div>';
            echo $after_widget;
        }


        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array( 'title' => __('Recent Recipes', 'quickrecipe'),'posts_per_page' => 4,'num_words' => 4 ) );

            $title= esc_attr($instance['title']);
            $num_words = $instance['num_words'];
            $posts_per_page =  $instance['posts_per_page'];

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('num_words')); ?>"><?php esc_html_e('Number of Words', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('num_words')); ?>" name="<?php echo esc_attr($this->get_field_name('num_words')); ?>" type="text" value="<?php echo esc_attr($num_words); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('posts_per_page')); ?>"><?php esc_html_e('Number of Posts Per Page', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('posts_per_page')); ?>" name="<?php echo esc_attr($this->get_field_name('posts_per_page')); ?>" type="text" value="<?php echo esc_attr($posts_per_page); ?>" />
            </p>
        <?php
        }


        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['num_words'] = intval($new_instance['num_words']);
            $instance['posts_per_page'] = intval($new_instance['posts_per_page']);

            return $instance;

        }

    }
}

?>