<?php

add_action( 'widgets_init', 'quickrecipe_footer_contact_widget' );
if( !function_exists('quickrecipe_footer_contact_widget') ){
	function quickrecipe_footer_contact_widget() {
		register_widget( 'Footer_Contact_Widget' );
	}
}


if(!class_exists('Footer_Contact_Widget')){
    class Footer_Contact_Widget extends WP_Widget {

        public function __construct() {
            $widget_ops = array( 'classname' => 'Footer_Contact_Widget', 'description' => __('This widget is used to display information about your site.', 'quickrecipe'));
            parent::__construct( 'footer_contact_widget', __('Quick Recipe: Footer Contact Widget', 'quickrecipe'), $widget_ops );
        }

        function widget($args,  $instance) {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);
            
            $contact_phone_number = get_option('quickrecipe_phone_number', '');
            $contact_email = get_option('quickrecipe_email_address', '');
            $company_name = get_option('quickrecipe_company_name', '');
            $company_address = get_option('quickrecipe_company_address', '');

            echo $before_widget;
            echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';
            ?>
                <div class="row">
                    <p class="col-md-12 col-sm-4"><i class="fa fa-home"></i> <span><?php echo esc_html($company_address); ?></span></p>
                    <p class="col-md-12 col-sm-4"><i class="fa fa-phone"></i> <span><?php echo esc_html($contact_phone_number); ?></span></p>
                    <p class="col-md-12 col-sm-4"><i class="fa fa-envelope"></i> <span><?php echo esc_html($contact_email); ?></span></p> 
                </div> 
        <?php
            echo $after_widget;
        }


        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array('about_text' => 'text', 'title' => __('Contact', 'quickrecipe') ) );

            $title      = $instance['title'];
            $about_text = $instance['about_text'];

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
        <?php
        }


        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['about_text'] = strip_tags($new_instance['about_text']);

            return $instance;
        }

    }
}
?>