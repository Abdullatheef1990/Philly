<?php
if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'widgets_init', 'quickrecipe_cuisines_widget' );
if( !function_exists('quickrecipe_cuisines_widget') ){
	function quickrecipe_cuisines_widget() {
		register_widget( 'Recipe_Cuisines_Widget' );
	}
}


if(!class_exists('Recipe_Cuisines_Widget')){
    class Recipe_Cuisines_Widget extends WP_Widget {

	    public function __construct() {
		    $widget_ops = array( 
			    'classname' => 'Recipe_Cuisines_Widget',
			    'description' => __('Display a list of recipe cuisines.', 'quickrecipe'),
		    );
		    parent::__construct( 'Recipe_Cuisines_Widget', __('Quick Recipe: Recipe Cuisines', 'quickrecipe'), $widget_ops );
	    }

        function widget($args,  $instance)
        {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);
            if ( empty($title) )
                $title = false;

            $pad_counts = 1; // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => 'cuisine',
                'title_li'     => '',
                'pad_counts' => $pad_counts,
            );

            echo $before_widget;
		    echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';

            echo '<ul class="list-check">';
            wp_list_categories( $args );
            echo '</ul>';

            echo $after_widget;
        }


        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array( 'title' => __('Recipe Cuisines', 'quickrecipe') ) );

            $title= esc_attr($instance['title']);

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
        <?php
        }


        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            return $instance;
        }

    }
}
?>