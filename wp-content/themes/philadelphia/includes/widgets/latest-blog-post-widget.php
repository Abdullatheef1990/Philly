<?php

add_action( 'widgets_init', 'quickrecipe_latest_posts_widget' );
if( !function_exists('quickrecipe_latest_posts_widget') ){
	function quickrecipe_latest_posts_widget() {
		register_widget( 'Latest_Blog_Posts_Widget' );
	}
}

if(!class_exists('Latest_Blog_Posts_Widget')){
    class Latest_Blog_Posts_Widget extends WP_Widget {

        public function __construct() {
            $widget_ops = array( 'classname' => 'Latest_Blog_Posts_Widget', 'description' => __('Display a list of latest blog posts on the footer.', 'quickrecipe'));
            parent::__construct( 'Latest_Blog_Posts_Widget', __('Quick Recipe: Latest Blog Posts', 'quickrecipe'), $widget_ops );
        }


        function widget($args,  $instance) {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);
            if ( empty($title) )
                $title = false;

            $num_words =  $instance['num_words'];
            $posts_per_page =  $instance['posts_per_page'];

            $args = array('post_type'=>'post', 'posts_per_page'=> $posts_per_page);
            
            echo $before_widget;
            echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';

            $query = new WP_Query($args);
            if($query->have_posts()):
                while($query->have_posts()):
                    $query->the_post();
                    ?>
                    <div class="item">

                        <div class="content">
                            <h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php echo wp_trim_words( get_the_title(), $num_words, '...' ); ?></a></h4>
                            <?php if ($show_excerpt) {?>
                            <p class="post-content"><?php echo wp_trim_words( get_the_excerpt(), $num_words, '...' ); ?></p>
                            <?php } ?>
	                        <div class="meta">
		                        <span><i class="fa fa-eye"></i> <?php echo quickrecipe_get_post_views(get_the_ID()); ?></span>&nbsp;&nbsp;| &nbsp;&nbsp;<span><i class="fa fa-comments"></i> <?php echo get_comments_number( get_the_ID() ) . __(' Comments', 'quickrecipe'); ?></span>
	                        </div>
                        </div><!-- content-->
                    </div>  
                <?php
                endwhile;
            endif;
            echo $after_widget;

        }

        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array( 'title' => __('Latest Blog Posts', 'quickrecipe'),'posts_per_page' => 4,'num_words' => 4 ) );

            $title = $instance['title'];
            $posts_per_page =  $instance['posts_per_page'];
            $num_words =  $instance['num_words'];

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('num_words')); ?>"><?php esc_html_e('Number of Words', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('num_words')); ?>" name="<?php echo esc_attr($this->get_field_name('num_words')); ?>" type="text" value="<?php echo esc_attr($num_words); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('posts_per_page')); ?>"><?php esc_html_e('Number of Posts Per Sidebar ', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('posts_per_page')); ?>" name="<?php echo esc_attr($this->get_field_name('posts_per_page')); ?>" type="text" value="<?php echo esc_attr($posts_per_page); ?>" />
            </p>
        <?php
        }


        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;

            $instance['title'] = strip_tags($new_instance['title']);
            $instance['num_words'] = $new_instance['num_words'];
            $instance['posts_per_page'] = $new_instance['posts_per_page'];

            return $instance;
        }
    }
}

?>