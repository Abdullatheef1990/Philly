<?php

add_action( 'widgets_init', 'quickrecipe_tag_cloud_widget' );
if( !function_exists('quickrecipe_tag_cloud_widget') ){
	function quickrecipe_tag_cloud_widget() {
		register_widget( 'Footer_TagCloud_Widget' );
	}
}


if(!class_exists('Footer_TagCloud_Widget')){
    class Footer_TagCloud_Widget extends WP_Widget {

        public function __construct() {
            $widget_ops = array( 'classname' => 'Footer_TagCloud_Widget', 'description' => __('This widget is used to display information about your site.', 'quickrecipe'));
            parent::__construct( 'footer_tagcloud_widget', __('Quick Recipe: Footer Tag Cloud Widget', 'quickrecipe'), $widget_ops );
        }

        function widget($args,  $instance) {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);

            $tag_number =  $instance['tag_number'];
            echo $before_widget;
            echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';

            $tags = get_tags(array('number'=>$tag_number));
            $html = '<div class="tagcloud">';
            foreach ( $tags as $tag ) {
	            $tag_link = get_tag_link( $tag->term_id );
			
	            $html .= "<a href='".esc_url($tag_link)."' title='".esc_attr($tag->name)."' class='".esc_attr($tag->slug)."'>" . esc_html($tag->name) . "</a>";
            }
            $html .= '</div>';
            echo $html;
            echo $after_widget;
            ?>
        <?php
        }


        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array('tag_number' => '10', 'title' => __('Tags Cloud', 'quickrecipe') ) );

            $title  = $instance['title'];
            $tag_number =  $instance['tag_number'];

            ?>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('tag_number')); ?>"><?php esc_html_e('Number of Tags', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('tag_number')); ?>" name="<?php echo esc_attr($this->get_field_name('tag_number')); ?>" type="text" value="<?php echo esc_attr($tag_number); ?>" />
            </p>
        <?php
        }


        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['tag_number'] = strip_tags($new_instance['tag_number']);

            return $instance;
        }

    }
}
?>