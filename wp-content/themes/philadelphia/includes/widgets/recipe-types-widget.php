<?php

add_action( 'widgets_init', 'quickrecipe_type_widget' );
if( !function_exists('quickrecipe_type_widget') ){
	function quickrecipe_type_widget() {
		register_widget( 'Recipe_Types_Widget' );
	}
}


if(!class_exists('Recipe_Types_Widget')){
    class Recipe_Types_Widget extends WP_Widget {

	    public function __construct() {
		    $widget_ops = array( 
			    'classname' => 'Recipe_Types_Widget',
			    'description' => __('Display a list of recipe types.', 'quickrecipe'),
		    );
		    parent::__construct( 'Recipe_Types_Widget', __('Quick Recipe: Recipe Types', 'quickrecipe'), $widget_ops );
	    }

        function widget($args,  $instance)
        {
            extract($args);

            $title = apply_filters('widget_title', $instance['title']);
            if ( empty($title) )
                $title = false;

            $show_count = 0; // 1 for yes, 0 for no
            $pad_counts = 0; // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => 'recipe_type',
                'title_li'     => '',
                'show_count' => $show_count,
                'pad_counts' => $pad_counts,
            );

		    echo $before_widget;
		    echo !empty($title) ? $before_title .  esc_html($title) . $after_title : '';

            echo '<ul>';
            wp_list_categories( $args );
            echo '</ul>';

            echo $after_widget;
        }


        function form($instance)
        {
            $instance = wp_parse_args( (array) $instance, array( 'title' => __('Recipe Types', 'quickrecipe') ) );

            $title = $instance['title'];

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title', 'quickrecipe'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
        <?php
        }


        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            return $instance;
        }

    }
}
?>