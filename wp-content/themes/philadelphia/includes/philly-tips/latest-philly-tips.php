<?php
if ( ! defined( 'ABSPATH' ) ) exit;
$latest_tip_limit = get_option('quicktip_latest_tip_limit', 16); 
$latest_tip_title = get_option('quicktip_latest_tip_title', ''); 
$latest_tip_title_desc = get_option('quicktip_latest_tip_title_desc', ''); 
$tip_string = '';
if (isset($_GET['sort'])) {
    $tip_string = wp_kses($_GET['sort'], '');    
}
?>
<?php
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$ids = get_users( array('role__not_in' => 'subscriber' ,'fields' => 'ID') );
if(!empty($tip_string)) {
    $query_args = array(
        'post_type'=>'tips',
        'posts_per_page' => 10,
        'post_status' => 'publish',                                        
        'paged' => $paged,
        'orderby' => 'title',
        'order'   => 'ASC',
        'author' => implode(',', $ids)
        );
}else{
    $query_args = array(
        'post_type'=>'tips',
        'posts_per_page' => 10,
        'post_status' => 'publish',                                        
        'paged' => $paged,
        'author' => implode(',', $ids)
        );
}

$query = new WP_Query($query_args);
if ( $query->have_posts() ) : ?>
    <?php if (ICL_LANGUAGE_CODE=='en') { ?>
        <div class="recipes-filter">
            <select name="recipe-sort" onchange="location = this.value;">
                <option>Sort by</option>
                <option value="/philly-tips/?sort=name">Name</option>
                <option value="/philly-tips/?sort=rating">Rating</option>
                <option value="/philly-tips/">Newest</option>
            </select>
        </div>
    <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
        <div class="recipes-filter">
            <select name="recipe-sort" onchange="location = this.value;">
                <option>رتب</option>
                <option value="/وصفات/?lang=ar&sort=name">رتب</option>
                <option value="/وصفات/?lang=ar&sort=rating">رتب</option>
                <option value="/وصفات/?lang=ar">رتب</option>
            </select>
        </div>
    <?php } ?>
    <?php               
    while($query->have_posts()) :
        $query->the_post();
        $image = get_field('tip_photo');
    ?>
        <div class="columns large-6 small-12  load-more-article recipes-grid-element" style="background-image: url('<?php echo $image; ?>');">
            <div class="row full-width grid-inner">
                <div class="column large-6 small-6 panel">
                </div>
                <div class="column large-6 small-6 transparent-block panel">
                    <h2><?php the_title(); ?></h2>
                    <?php if (ICL_LANGUAGE_CODE=='en') { ?>
                        <a href='<?php echo get_permalink(); ?>'  class="transparent-button">START TIP</a>
                    <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
                        <a href='<?php echo get_permalink(); ?>'  class="transparent-button">بدء الطهي</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php endwhile; ?>

    <div class="row small-12">
        <div class="pagination">
            <?php
            $total_pages = $query->max_num_pages;
            $big = 999999999;
            if ($total_pages > 1){
                $current_page = max(1, get_query_var('paged'));
                echo paginate_links(array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'current' => $current_page,
                    'total' => $total_pages,
                    'prev_text'    => __('« prev'),
                    'next_text'    => __('next »'),
                    ));
            } 
            ?>
        </div>
    </div>
<?php else :?>
    <article <?php post_class(); ?>>
        <?php if (ICL_LANGUAGE_CODE=='en') { ?>
            <p class="noarticle"><?php esc_html_e( 'Latest tips are not available.', 'quickrecipe' ); ?></p>
        <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
            <p class="noarticle"><?php esc_html_e( 'غينيا واستمر العصبة ضرب قد. وباءت الأمريكي الأوربيين هو', 'quickrecipe' ); ?></p>
        <?php } ?>
    </article><!-- /.post -->
<?php endif; ?>
<?php wp_reset_postdata();?>
<!-- end:latest -->

