<?php
global $post;
$tip_type_terms = get_the_terms( $post->ID, 'tip_type' );
$tip_type_slugs = array();
if( $tip_type_terms && !is_wp_error( $tip_type_terms ))
{
	$tip_type_slugs = array();
	foreach($tip_type_terms as $term)
	{
		$tip_type_slugs[] = $term->slug;
	}
}

$occassion_terms = get_the_terms( $post->ID, 'occassion' );
$occassion_slugs = array();
if( $occassion_terms && !is_wp_error( $occassion_terms ))
{
	$occassion_slugs = array();
	foreach($occassion_terms as $term)
	{
		$occassion_slugs[] = $term->slug;
	}
}

$query_args = array(
	'post_type' => 'recipe',
	'posts_per_page' => 2,
	'post__not_in' => array($post->ID),
	'tax_query' => array(
		'relation' => 'OR',
		array(
			'taxonomy' => 'recipe_type',
			'field' => 'slug',
			'terms' => $tip_type_slugs
			),
		array(
			'taxonomy' => 'course',
			'field' => 'slug',
			'terms' => $occassion_slugs
			)
		),
	'meta_query' => array(
		array(
			'key' => '_thumbnail_id',
			'value' => '0',
			'compare' => '>',
			'type' => 'NUMERIC'
			)
		)
	);

$the_query = new WP_Query($query_args);
if ( $the_query->have_posts() ) { ?>
	<div class="row recipe-grid ymal-recipes align-center">
		<div class="columns large-10 small-12 ymal-headline">
			YOU MAY ALSO LIKE
		</div>
		<?php
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			$term_names = wp_get_post_terms($post->ID, 'occassion', array("fields" => "names"));
			$term_list = array();
			if (is_array($term_names)) {
				foreach($term_names as $term_name) {
					$term_list[] = $term_name;
				}
				$term_name_list = join( ", ", $term_list );
			} else {
				$term_name_list = '';
			}
			?>
			<div class="columns large-6 small-12 recipes-grid-element"
			style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID); ?>);">
				<div class="row full-width grid-inner">
					<div class="column large-6 small-6 panel"></div>
					<div class="column large-6 small-6 transparent-block panel">
						<h2><?php the_title(); ?></h2>
						<a href="<?php echo esc_url(get_permalink( $post->ID )); ?>" class="transparent-button">START TIP</a>
					</div>
				</div>
			</div>
		<?php endwhile;	?>
	</div>
<?php }
wp_reset_postdata();
?>