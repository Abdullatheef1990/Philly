<?php
if ( ! defined( 'ABSPATH' ) ) exit;

$search_string = '';
if (isset($_GET['s'])) {
	$search_string = wp_kses($_GET['s'], '');	
}

$tip_type = 0;
if (isset($_GET['tip_type'])) {
	$tip_type = intval(wp_kses($_GET['tip_type'], '0'));
}

$occassion = 0;
if (isset($_GET['occassion'])) {
	$occassion = intval(wp_kses($_GET['occassion'], '0'));
}


if ( function_exists( 'quickrecipe_create_taxonomies' ) ) { ?>
    <?php if (ICL_LANGUAGE_CODE=='en') { ?>
	<div class="row search-form">
		<div class="columns large-3 small-12">
            <?php
            $tip_type_args = array(
				'show_option_none' => __( 'Choose Tip Type', 'quicktip' ),
				'show_count'       => 0,
			    'option_none_value'  => '0',
			    'hide_empty'         => 0, 
				'orderby'          => 'SLUG',
				'echo'             => 1,
			    'selected'           => $tip_type,
			    'name'             => 'tip_type',
			    'id'               => '',
			    'class'            => '',
				'hierarchical'     => 1,							
			    'depth'            => 0,
			    'tab_index'        => 1,
			    'taxonomy'         => 'tip_type',
			);
    		wp_dropdown_categories( $tip_type_args );
			?>
		</div>
        <div class="columns large-3 small-12">
			<?php
			$occassion_args = array(
				'show_option_none' => __( 'Choose Occassion', 'quicktip' ),
				'show_count'       => 0,
			    'option_none_value'  => '0',
			    'hide_empty'         => 0, 
				'orderby'          => 'name',
				'echo'             => 1,
			    'selected'           => $occassion,
			    'name'             => 'occassion',
			    'id'               => '',
			    'class'            => '',
			    'depth'            => 0,
			    'tab_index'        => 0,
			    'taxonomy'         => 'occassion',
			);
    		wp_dropdown_categories( $occassion_args );
			?>
		</div>
        <div class="columns large-6 small-12">
            <input type="text"  placeholder="<?php esc_html_e( 'Search by Keyword', 'quickrecipe' ) ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
		</div>
	</div>
	<?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
		<div class="row search-form">
	    	<div class="columns large-3 small-12">
				<?php
				$tip_type_args = array(
					'show_option_none' => __( 'مضغة خفيف', 'quickrecipe' ),
					'show_count'       => 0,
				    'option_none_value'  => '0',
				    'hide_empty'         => 0, 
					'orderby'          => 'SLUG',
					'echo'             => 1,
				    'selected'           => $tip_type,
				    'name'             => 'tip_type',
				    'id'               => '',
				    'class'            => '',
					'hierarchical'     => 1,							
				    'depth'            => 0,
				    'tab_index'        => 1,
				    'taxonomy'         => 'tip_type',
				);
	    		wp_dropdown_categories( $tip_type_args );
				?>
			</div>
	        <div class="columns large-3 small-12">
				<?php
				$occassion_args = array(
					'show_option_none' => __( 'قدم خدمة', 'quicktip' ),
					'show_count'       => 0,
				    'option_none_value'  => '0',
				    'hide_empty'         => 0, 
					'orderby'          => 'name',
					'echo'             => 1,
				    'selected'           => $occassion,
				    'name'             => 'occassion',
				    'id'               => '',
				    'class'            => '',
				    'depth'            => 0,
				    'tab_index'        => 0,
				    'taxonomy'         => 'occassion',
				);
	    		wp_dropdown_categories( $occassion_args );
				?>
			</div>
			<div class="columns large-6 small-12">
	            <input type="text"  placeholder="<?php esc_html_e( 'البحث عن طريق الكلمات الرئيسية', 'quickrecipe' ) ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
	        </div>
		</div>
	<?php } ?>
<?php } ?>