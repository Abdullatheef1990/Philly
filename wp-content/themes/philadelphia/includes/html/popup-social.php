<?php
if ( ! defined( 'ABSPATH' ) ) exit;
global $post;
?>
    <div id="popup-social<?php echo esc_attr($post->ID); ?>" class="overlay">
	    <div class="popup">
		    <h2><?php esc_html_e( 'Share this recipe', 'quickrecipe' ); ?></h2>
		    <a class="close-popup" href="#">&times;</a>
		    <div class="content">
			    <p><?php the_title(); ?></p>
                <?php
	            //if(is_singular() || is_home()){
	
		            // Get current page URL 
		            $postURL = urlencode(get_permalink());
 
		            // Get current page title
		            $postTitle = str_replace( ' ', '%20', get_the_title());
		
		            // Get Post Thumbnail for pinterest
                    $postThumbnailId = get_post_thumbnail_id($post->ID);
                    if (!empty($postThumbnailId)) {
		                $postThumbnail = wp_get_attachment_image_src($postThumbnailId, 'full');
		                $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$postURL.'&amp;media='.$postThumbnail[0].'&amp;description='.$postTitle;
                    } else {
                        $pinterestURL = '';
                    }
		            // Construct sharing URL without using any script
		            $twitterURL = 'https://twitter.com/intent/tweet?text='.esc_attr($postTitle).'&amp;url='.$postURL;
		            $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$postURL;
		            $googleURL = 'https://plus.google.com/share?url='.$postURL;
		            $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$postURL.'&amp;title='.esc_attr($postTitle);
		            $whatsappURL = 'whatsapp://send?text='.esc_attr($postTitle). '' . $postURL;
		            $redditURL = 'http://www.reddit.com/submit?url='.$postURL;
		            $stumbleuponURL = 'http://www.stumbleupon.com/submit?url='.$postURL;
		            $tumblrURL = 'http://www.tumblr.com/share/link?url='.$postURL;
		            $diggURL = 'http://digg.com/submit?url='.$postURL;
		            $deliciousURL = 'http://del.icio.us/post?url='.$postURL;
		
                    
		            // Add sharing button at the end of page/page content
		            echo '<div class="social-content">';
		            echo '<ul class="social-icons">';
		            echo '<li><a class="twitter" href="'. esc_url($twitterURL) .'" target="_blank"><i class="fa fa-twitter"></i></a></li>';
		            echo '<li><a class="facebook" href="'.esc_url($facebookURL).'" target="_blank"><i class="fa fa-facebook"></i></a></li>';
		            //echo '<li><a class="whatsapp" href="'.esc_url($whatsappURL).'" target="_blank"><i class="fa fa-whatsapp"></i></a></li>';
		            echo '<li><a class="google-plus" href="'.esc_url($googleURL).'" target="_blank"><i class="fa fa-google-plus"></i></a></li>';
		            //echo '<li><a class="tumblr" href="'.esc_url($tumblrURL).'" target="_blank"><i class="fa fa-tumblr"></i></a></li>';
		            //echo '<li><a class="reddit" href="'.esc_url($redditURL).'" target="_blank"><i class="fa fa-reddit"></i></a></li>';
		            //echo '<li><a class="stumbleupon" href="'.esc_url($stumbleuponURL).'" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>';
		            //echo '<li><a class="digg" href="'.esc_url($diggURL).'" target="_blank"><i class="fa fa-digg"></i></a></li>';
		            //echo '<li><a class="linkedin" href="'.esc_url($linkedInURL).'" target="_blank"><i class="fa fa-linkedin"></i></a></li>';
		            //echo '<li><a class="delicious" href="'.esc_url($deliciousURL).'" target="_blank"><i class="fa fa-delicious"></i></a></li>';
		            echo '<li><a class="pinterest" href="'.esc_url($pinterestURL).'" target="_blank"><i class="fa fa-pinterest"></i></a></li>';
		            echo '</ul>';
		            echo '</div>';
		
                /*
	            }else{
	            }
                */
                ?>
		    </div>
	    </div>
    </div>
