<?php
// Add fields after default fields above the comment box, always visible

add_action( 'comment_form_logged_in_after', 'comment_custom_fields' );
add_action( 'comment_form_after_fields', 'comment_custom_fields' );

function comment_custom_fields ($rating = 0) {
	/*
    if (( is_singular( 'recipe' ))) {
	    echo '<p class="comment-form-rating">'.
	    '<label>'. __('Rating', 'quickrecipe') . ' <span class="required">*</span></label>
	    <span class="star-cb-group">';
	
        echo '<input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" /><label for="rating-0">0</label>';
	    for( $i=5; $i >= 1; $i-- ) {
            $checked = ($rating == $i) ? "checked='checked'" : '';
            echo '<input type="radio" id="rating-'. $i .'" name="rating" value="'. $i .'" '. $checked .' /><label for="rating-'. $i .'">'. $i .'</label>';
        }
	    echo'</span></p>';
    }*/

}


// Save the comment meta data along with comment

add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {

	if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '') )
	$rating = wp_filter_nohtml_kses($_POST['rating']);
	add_comment_meta( $comment_id, 'rating', $rating );
}




// Add the comment meta (saved earlier) to the comment text 
// You can also output the comment meta values directly in comments template  

add_filter( 'comment_text', 'modify_comment');
function modify_comment( $text ){
    $rating = get_comment_meta( get_comment_ID(), 'rating', true ) ;

	if( $rating ) {
        $total = 5;
        $comment_rating = '<div class="user-ratings">';
        for($i = 0; $i < $total; $i++) {
            if ($rating - $i >= 1) {
                // one or more full stars left
                $comment_rating .= '<i class="fa fa-star"></i> ';

            } else if (round($rating - $i) == 1) {
                // half a star left
                $comment_rating .= '<i class="fa fa-star-half-o"></i> ';

            } else {
                // no stars left
                $comment_rating .= '<i class="fa fa-star-o"></i> ';
            }
        }
        $comment_rating .= '</div>';
        $text = $comment_rating . $text;

        return $text;
    	
	} else {
		return $text;		
	}	 

}



function average_rating() {
    global $wpdb;
    $post_id = get_the_ID();

    $ratings = $wpdb->get_results( 
                    $wpdb->prepare("
                        SELECT $wpdb->commentmeta.meta_value
                          FROM $wpdb->commentmeta INNER JOIN $wpdb->comments on $wpdb->comments.comment_id=$wpdb->commentmeta.comment_id
                         WHERE $wpdb->commentmeta.meta_key = 'rating' 
                           AND $wpdb->comments.comment_post_id= %d 
                           AND $wpdb->comments.comment_approved = 1                    
                    ", $post_id) 
                 );

    $counter = 0;
    $average_rating = 0;
    $str = '';
    if ($ratings) {
        foreach ($ratings as $rating) {
            $average_rating = $average_rating + $rating->meta_value;
            $counter++;
        } 
        
        $rating = (round(($average_rating/$counter)*2,0)/2 );

        $total = 5;

        for($i = 0; $i < $total; $i++) {
            if ($rating - $i >= 1) {
                // one or more full stars left
                $str .= '<i class="fa fa-star"></i> ';

            } else if (round($rating - $i) == 1) {
                // half a star left
                $str .= '<i class="fa fa-star-half-o"></i> ';

            } else {
                // no stars left
                $str .= '<i class="fa fa-star-o"></i> ';
            }
        }
        return $str;
    } else {
        return '';
    }
}
