<?php
add_filter( 'rwmb_meta_boxes', 'QUICK_RECIPE_register_meta_boxes' );

function QUICK_RECIPE_register_meta_boxes( $meta_boxes )
{
    

    $meta_boxes = array();

    $meta_boxes[] = array(
	    'id'		=> 'recipe_additional',
	    'title'		=> __('Recipe Information', 'quickrecipe'),
	    'pages'		=> array( 'recipe' ),
        'context'  => 'normal',
	    'priority' => 'high',
	    'fields'	=> array(
		    array(
			    'name'		=> __('Recipe Cover Image', 'quickrecipe'),
			    'id'		=> 'quickrecipe_cover_image',
			    'desc'		=> __('Select full width image for recipe cover.', 'quickrecipe'),
			    'clone'		=> false,
			    'type'		=> 'image_advanced',
			    'std'		=> ''
		    ),
		    array(
			    'name'		=> __('Preparation Time', 'quickrecipe'),
			    'id'		=> 'quickrecipe_prep_time',
			    'desc'		=> __('You can enter a value in minutes between 1 and 720.', 'quickrecipe'),
			    'clone'		=> false,
			    'type'		=> 'text',
			    'std'		=> ''
		    ),
		    array(
			    'name'		=> __('Cooking Time', 'quickrecipe'),
			    'id'		=> 'quickrecipe_cook_time',
			    'desc'		=> __('You can enter a value in minutes between 1 and 720.', 'quickrecipe'),
			    'clone'		=> false,
			    'type'		=> 'text',
			    'std'		=> ''
		    ),
		    array(
			    'name'		=> __('Ready Time', 'quickrecipe'),
			    'id'		=> 'quickrecipe_ready_in',
			    'desc'		=> __('You can enter a value in minutes between 1 and 720.', 'quickrecipe'),
			    'clone'		=> false,
			    'type'		=> 'text',
			    'std'		=> ''
		    ),
		    array(
			    'name'		=> __('Yield', 'quickrecipe'),
			    'id'		=> 'quickrecipe_yield',
			    'clone'		=> false,
			    'type'		=> 'text',
			    'std'		=> ''
		    ),
		    array(
			    'name'		=> __('Servings', 'quickrecipe'),
			    'id'		=> 'quickrecipe_servings',
			    'desc'		=> __('How many portions/servings does the recipe yield?', 'quickrecipe'),
			    'clone'		=> false,
			    'type'		=> 'text',
			    'std'		=> ''
		    ),
		    array(
			    'name'		=> __('Ingredients', 'quickrecipe'),
			    'id'		=> 'quickrecipe_ingredients',
			    'desc'		=> __('Add ingredient, one step at a time.', 'quickrecipe'),
			    'clone'		=> true,
			    'type'		=> 'textarea',
			    'std'		=> '',
                'rows'      => 1
		    ),
		    array(
			    'name'		=> __('Steps', 'quickrecipe'),
			    'id'		=> 'quickrecipe_method_steps',
			    'desc'		=> __('Add new instructions, one step at a time.', 'quickrecipe'),
			    'clone'		=> true,
			    'type'		=> 'textarea',
			    'std'		=> ''
		    ),
            array(
                'name'		=> __('Nutrition', 'quickrecipe'),
                'id'		=> 'quickrecipe_nutrition',
                'desc'		=> __('Enter the name of nutrition. Use a colon to separate name and value(Carbohydrate: 150g).<br>Click the + button on the right to add new nutrition', 'quickrecipe'),
                'clone'		=> true,
                'type'		=> 'text',
                'std'		=> ''
            ),
            array(
                'name'		=> __('Editor\'s Choice Recipes', 'quickrecipe'),
                'desc'      =>  __('<strong>Editor\'s Choice Recipes</strong>', 'quickrecipe'),
                'id'		=> 'quickrecipe_editor_choice_recipe',
                'type'		=> 'radio',
                'options'   => array( 'true' => __('Yes ', 'quickrecipe'), 'false'   =>  __('No ', 'quickrecipe')),
                'std'   =>  'off'
            ),
		    array(
			    'name'		=> __(' Editor\'s Choice Recipe Cover', 'quickrecipe'),
			    'id'		=> 'quickrecipe_editor_choice_recipe_image',
			    'desc'		=> __('Select full width image for recipe cover.', 'quickrecipe'),
			    'clone'		=> false,
			    'type'		=> 'image_advanced',
			    'std'		=> ''
		    ),

            
            array(
                'name'		=> __('Is Recipe Include a Video', 'quickrecipe'),
                'desc'      =>  __('if you select a video, attachment images would not be displayed.', 'quickrecipe'),
                'id'		=> 'quickrecipe_is_include_a_video',
                'type'		=> 'radio',
                'options'   => array( 'on' => __('Yes ', 'quickrecipe'), 'off'   =>  __('No ', 'quickrecipe')),
                'std'   =>  'off'
            ),
            
            array(
                'name'		=> __('Video URL', 'quickrecipe'),
                'id'		=> 'quickrecipe_embed_a_video',
                'desc'		=> __('Paste the video embed code into the embed code field.', 'quickrecipe'),
                'clone'		=> false,
                'type'		=> 'textarea',
                'std'		=> ''
            ),
            array(
			    'name'	=> __('Recipe Images', 'quickrecipe'),
			    'desc'	=> __('Take pictures and upload your own recipes.', 'quickrecipe'),
			    'id'	=> 'quickrecipe_more_images_recipe',
			    'type'	=> 'image_advanced'
		    )
	    )
    );

    $meta_boxes[] = array(
	    'id'		=> 'testimonial',
	    'title'		=> __('Recipe Information', 'quickrecipe'),
	    'pages'		=> array( 'testimonial' ),
	    'priority' => 'high',
	    'fields'	=> array(
		    array(
			    'name'		=> __('Rating', 'quickrecipe'),
			    'id'		=> 'quickrecipe_rating',
			    'desc'		=> __('Rating of whom is giving the testimonial', 'quickrecipe'),
			    'clone'		=> false,
			    'type'		=> 'select',
			    'options'   => array( '1' => __('1 Star', 'quickrecipe'), '2'   =>  __('2 Stars', 'quickrecipe'), '3'   =>  __('3 Stars', 'quickrecipe'), '4'   =>  __('4 Stars', 'quickrecipe'), '5'   =>  __('5 Stars', 'quickrecipe')),
			    'placeholder'=> __('Please select...', 'quickrecipe' ),
			    'std'		=> ''
		    ),
	    )
    );

    $posts   = get_posts(
        array(
            'post_type'      => 'recipe',
            'posts_per_page' => 500,
            'post_status'    => 'publish'
        )
    );

    $options = array();
    foreach ($posts as $post) {
        if ($post->post_title !== "") {
            $options[$post->ID] = $post->post_title;
        }
    }

    // 1st meta box
    $meta_boxes[] = array(
        'id'       => 'homepage_slide',
        'title'    => __( 'Select Related Page', 'quickrecipe' ),
        'pages'    => array('homepage_slide'),
        'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name'    => 'Recipe Page',
                'id'      => 'quickrecipe_related_link',
                'type'    => 'select_advanced',
                'options' => $options,
                'clone'   => false,
                'placeholder' => __('Please Select a Page', 'quickrecipe' )
            ),
        )
    );

    /********/
    $pattern = '/\.(flaticon-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
    //$subject = file_get_contents(get_template_directory_uri() . '/css/flaticon.css');

    ob_start();
    include  get_template_directory()  .'/css/flaticon.css';
    $subject = ob_get_contents();
    ob_end_clean();

    preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

    $icons = array();

    foreach($matches as $match){
        $icons[$match[1]] = $match[1];
    }
    
    $meta_boxes[] = array(
        'id'       => 'we_offer',
        'title'    => __( 'Select Related Page', 'quickrecipe' ),
        'pages'    => array('we_offer'),
        'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name'    => __('Recipe Page', 'quickrecipe'),
                'id'      => 'quickrecipe_we_offer',
                'type'    => 'select_advanced',
                'options' => $icons,
                'clone'   => false,
                'placeholder' => __('Please Select a Page', 'quickrecipe' )
            ),
        )
    );

    return $meta_boxes;
}