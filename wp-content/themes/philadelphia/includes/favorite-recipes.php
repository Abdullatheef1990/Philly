<?php
    if ( !defined( 'MY_FAVORITE_RECIPES' ) )	
	    define ( 'MY_FAVORITE_RECIPES', 'quickrecipe_favorite_recipes' );

	function quickrecipe_is_favorite($recipe_id = 0, $user_id = 0) {
		if ( ! $recipe_id ) {
			$recipe_id = get_the_ID();
		}

		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}
		
		$favorite_ids = quickrecipe_get_user_favorites($user_id);
		
		if ($favorite_ids) {
			foreach ($favorite_ids as $favorite_id) {
				if ($recipe_id == $favorite_id) 
					return true;
			}
		}
		return false;
	}

	function quickrecipe_add_to_favorite($recipe_id, $user_id) {		
	
		if (!quickrecipe_is_favorite($recipe_id, $user_id)) {
		
			$favorite_ids = quickrecipe_get_user_favorites($user_id);
			
			if (!$favorite_ids)
				$favorite_ids = array();
				
			$favorite_ids[] = $recipe_id;	
			
			update_user_meta( $user_id, MY_FAVORITE_RECIPES, $favorite_ids );		
			
			$count = quickrecipe_get_favorited_count($recipe_id);
			if (!$count)
				$count = 0;
			$count++;
			
			update_post_meta($recipe_id, MY_FAVORITE_RECIPES, $count);

		}
		return 0;
	}
	
	function quickrecipe_remove_from_favorite($recipe_id, $user_id) {		
	
		if (quickrecipe_is_favorite($recipe_id, $user_id)) {
		
			$favorite_ids = quickrecipe_get_user_favorites($user_id);
			
			if (!$favorite_ids)
				return 0;
				
			if(($key = array_search($recipe_id, $favorite_ids)) !== false) {
				unset($favorite_ids[$key]);
			}
			
			update_user_meta( $user_id, MY_FAVORITE_RECIPES, $favorite_ids );		
			
			$count = quickrecipe_get_favorited_count($recipe_id);
			if (!$count)
				$count = 0;
			else
				$count--;
			
			update_post_meta($recipe_id, MY_FAVORITE_RECIPES, $count);

		}
		return 0;
	}
		
	function quickrecipe_get_user_favorites( $user_id ) {	
	
		return get_user_meta($user_id, MY_FAVORITE_RECIPES, true);	
	}
	
	function quickrecipe_get_favorited_count ( $recipe_id ) {
	
		$count = get_post_meta($recipe_id, MY_FAVORITE_RECIPES, true);
		if (!$count) 
			$count = 0;
		
		return $count;		
	}

    if (! function_exists('quickrecipe_get_favorite_button')) {
	    function quickrecipe_get_favorite_button() {
            global $post;

		    $html = '';

		    $class 	= 'btn-success' . ( quickrecipe_is_favorite() ? ' is-favorite' : '' );
		    $name 	= quickrecipe_is_favorite() ? 'quickrecipe_remove_from_favorite' : 'quickrecipe_add_to_favorite';
		    $text 	= quickrecipe_is_favorite() ? __( 'Unfavorite', 'quickrecipe' ) : __( 'Favorite', 'quickrecipe' );
		
		    $html   .= '<form method="post" class="favorite_user_post" action="'. get_permalink() .'">';
		    $html   .= '<button type="submit" name="'. $name .'" value="'. get_the_ID() .'" class="'. $class .' favorite-btn recipe-video-btn">';
		    $html   .= '<i class="fa fa-heart"></i> ';
		    $html   .= $text;
		    $html   .= '</button></form>';
		
		    return $html;
	    }
    }

    if (! function_exists('quickrecipe_get_form_request')) {
	    function quickrecipe_get_form_request() {

	        if ( ! empty( $_REQUEST['quickrecipe_add_to_favorite'] ) ) {
	    	    if ( is_user_logged_in() ) {
	    		    $recipe_id = absint( $_REQUEST['quickrecipe_add_to_favorite'] );
	    		    quickrecipe_add_to_favorite( $recipe_id, get_current_user_id() );
	    	    } else {
                    auth_redirect();
	    	    }
	        }

	        if ( ! empty( $_REQUEST['quickrecipe_remove_from_favorite'] ) ) {
	    	    $recipe_id = absint( $_REQUEST['quickrecipe_remove_from_favorite'] );
	    	    quickrecipe_remove_from_favorite( $recipe_id, get_current_user_id() );
	        }
	    }
	    add_action( 'init', 'quickrecipe_get_form_request' );
    }
?>