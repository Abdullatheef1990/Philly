<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The default template for displaying content
 */



$term_names = wp_get_post_terms($post->ID, 'course', array("fields" => "names"));
$term_list = array();
 
foreach($term_names as $term_name) {
    $term_list[] = $term_name;
}
$term_name_list = join( ", ", $term_list );

?>
    <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="item-container">
        <div class="item-img-box">
		<?php
		if ( has_post_thumbnail( $post->ID ) ) {
			echo get_the_post_thumbnail( $post->ID, array(400,400) );
		}
		?>
        <a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><div class="hover-button"><?php esc_html_e('View Recipe', 'quickrecipe'); ?></div></a>
        </div>
        <div class="item-meta-box">
        <span><i class="fa fa-clock-o"></i> <?php $prep_time = intval( get_post_meta(get_the_ID(), 'quickrecipe_prep_time', true) ); echo quickrecipe_get_mins_to_hours($prep_time); ?></span>
        <span><i class="fa fa-user"></i> <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true); if (!empty($servings)) { echo sprintf(__('%s People', 'quickrecipe'), $servings); } ?></span>
        <span><i class="fa fa-spoon"></i> <?php $yield = get_post_meta($post->ID, 'quickrecipe_yield', true); if (!empty($servings)) { echo sprintf(__('%s Yield', 'quickrecipe'), $yield); } ?></span>
        </div>
        <div class="item-content-box">
        <h3><a href="<?php the_permalink()?>"><?php echo wp_trim_words(get_the_title(), 5, '...'); ?></a> <small><?php echo esc_html($term_name_list); ?></small></h3>
        <div class="avatar">
            <div class="clearfix">
                <div class="pull-left">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ), '25' ); ?>
                    <?php esc_html_e('By ', 'quickrecipe'); ?> <?php the_author_posts_link(); ?>
                </div>
                <span class="user-ratings pull-right">
                    <?php echo average_rating(); ?>
                </span>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- break -->

