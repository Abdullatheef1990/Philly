<?php

if( !function_exists('quickrecipe_theme_general_settings') ):
function quickrecipe_theme_general_settings(){
    

    $logo_image       			  =   esc_html( get_option('quickrecipe_logo_image','') );
    $footer_logo_image 			  =   esc_html( get_option('quickrecipe_footer_logo_image','') );
    $favicon_image     			  =   esc_html( get_option('quickrecipe_favicon_image','') ); 
	$footer_copyright_message_en     =   esc_html( get_option('quickrecipe_footer_copyright_message_en','') ); 
	$footer_copyright_message_ar     =   esc_html( get_option('quickrecipe_footer_copyright_message_ar','') ); 
    ?>
    <div class="tab-container">
    <h1 class="tab-h1"><?php esc_html_e('General Settings','quickrecipe'); ?></h1>
    <table class="form-table">
        <tr valign="top">
            <th scope="row"><label for="logo_image"><?php esc_html_e('Site Logo','quickrecipe'); ?></label></th>
            <td>
	            <input id="logo_image" type="text" class="img" size="36" name="logo_image" value="<?php echo esc_attr($logo_image); ?>" />
		        <input id="logo_image_button" type="button"  class="upload_button button" value="<?php esc_html_e('Upload Logo','quickrecipe'); ?>" />
            </td>
        </tr> 
        
         <tr valign="top">
            <th scope="row"><label for="footer_logo_image"><?php esc_html_e('Retina ready logo','quickrecipe'); ?></label></th>
            <td>
	            <input id="footer_logo_image" type="text" class="img" size="36" name="footer_logo_image" value="<?php echo esc_attr($footer_logo_image); ?>" />
		        <input id="footer_logo_image_button" type="button"  class="upload_button button" value="<?php esc_html_e('Upload Logo','quickrecipe'); ?>" />
            </td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="favicon_image"><?php esc_html_e('Favicon','quickrecipe'); ?></label></th>
            <td>
	            <input id="favicon_image" type="text" size="36" class="img" name="favicon_image" value="<?php echo esc_attr($favicon_image); ?>" />
		        <input id="favicon_image_button" type="button"  class="upload_button button" value="<?php esc_html_e('Upload Favicon','quickrecipe'); ?>" />
            </td>
        </tr> 
               
        <tr valign="top">
            <th scope="row"><label for="favicon_image"><?php esc_html_e('Footer copyright message','quickrecipe'); ?></label></th>
            <td>
            <?php if (ICL_LANGUAGE_CODE=='en') { ?>
         <input id="footer_copyright_message" type="text" size="36" class="img" name="footer_copyright_message_en" value="<?php echo esc_attr($footer_copyright_message_en); ?>" />
    
  <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>  
    
          <input id="footer_copyright_message" type="text" size="36" class="img" name="footer_copyright_message_ar" value="<?php echo esc_attr($footer_copyright_message_ar); ?>" />
    
<?php } ?>
	      
		        
            </td>
        </tr> 
                
        </table>
        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button-primary" value="<?php esc_html_e('Save Changes','quickrecipe'); ?>" />
        </p>    
    </div>
    <?php
}
endif; 



//  WP Settings
if( !function_exists('quickrecipe_theme_wp_settings') ):
function quickrecipe_theme_wp_settings(){
    $login_page_url = get_option('quickrecipe_login_page_url');
    $login_page_option = quickrecipe_page_select($login_page_url);

    $register_page_url = get_option('quickrecipe_register_page_url');
    $register_page_option = quickrecipe_page_select($register_page_url);

    $redirect_to_after_login = get_option('quickrecipe_redirect_to_after_login');
    $redirect_to_after_login_option = quickrecipe_page_select($redirect_to_after_login);

    $redirect_to_after_logout = get_option('quickrecipe_redirect_to_after_logout');
    $redirect_to_after_logout_option = quickrecipe_page_select($redirect_to_after_logout);

    $lost_password_page_url = get_option('quickrecipe_lost_password_page_url');
    $reset_password_page_option = quickrecipe_page_select($lost_password_page_url);

    $forgot_username_page_url = get_option('quickrecipe_forgot_username_page_url');
    $forgot_username_page_option = quickrecipe_page_select($forgot_username_page_url);

    $terms_page_url = get_option('quickrecipe_terms_page_url');
    $terms_page_option = quickrecipe_page_select($terms_page_url);

    $privacy_policy_page_url = get_option('quickrecipe_privacy_policy_page_url');
    $privacy_policy_page_option = quickrecipe_page_select($privacy_policy_page_url);

    $submit_recipe_url = get_option('quickrecipe_submit_recipe_url');
    $submit_recipe_option = quickrecipe_page_select($submit_recipe_url);

    $submit_testimonial_url = get_option('quickrecipe_submit_testimonial_url');
    $submit_testimonial_option = quickrecipe_page_select($submit_testimonial_url);

    $list_recipe_url = get_option('quickrecipe_list_recipe_url');
    $list_recipe_option = quickrecipe_page_select($list_recipe_url);

    $list_testimonial_url = get_option('quickrecipe_list_testimonial_url');
    $list_testimonial_option = quickrecipe_page_select($list_testimonial_url);

    $list_my_favorite_url = get_option('quickrecipe_list_my_favorite_url');
    $list_my_favorite_option = quickrecipe_page_select($list_my_favorite_url);

    $edit_my_profile_url = get_option('quickrecipe_edit_my_profile_url');
    $edit_my_profile_option = quickrecipe_page_select($edit_my_profile_url);

    $yes_no_array    =   array( '1' => esc_html__('yes','quickrecipe'), '0' => esc_html__('no','quickrecipe'));
    $enable_custom_login_option='';
    $enable_custom_login = get_option('quickrecipe_enable_custom_login','');

    foreach($yes_no_array as $key => $value){
            $enable_custom_login_option .= '<option value="' . esc_attr($key) . '"';
            if ($enable_custom_login == $key){
                    $enable_custom_login_option .= ' selected="selected" ';
            }
            $enable_custom_login_option .= '>'.esc_attr($value).'</option>';
    }

    
    echo '<div class="tab-container">';
    echo '<h1 class="tab-h1">'.esc_html__('Page Settings','quickrecipe').'</h1>';  
    echo '<table class="form-table">
        <tr valign="top">
            <th scope="row">
                <label for="enable_custom_login">'.esc_html__('Enable Custom Login','quickrecipe').'</label>
            </th>
            <td> 
                <select id="enable_custom_login" name="enable_custom_login">'.$enable_custom_login_option.'</select>
            </td>
        </tr>
       
        <tr valign="top">
            <th scope="row">
                <label for="login_page_url">'.esc_html__('Login Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="login_page_url" name="login_page_url">'.$login_page_option.'</select> 
            </td>
        </tr>

        <tr valign="top">
            <th scope="row">
                <label for="register_page_url">'.esc_html__('Register Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="register_page_url" name="register_page_url">'.$register_page_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="lost_password_page_url">'.esc_html__('Reset Password Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="lost_password_page_url" name="lost_password_page_url">'.$reset_password_page_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="forgot_username_page_url">'.esc_html__('Forgot Username Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="forgot_username_page_url" name="forgot_username_page_url">'.$forgot_username_page_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="terms_page_url">'.esc_html__('Terms & Conditions Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="terms_page_url" name="terms_page_url">'.$terms_page_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="privacy_policy_page_url">'.esc_html__('Privacy Policy Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="privacy_policy_page_url" name="privacy_policy_page_url">'.$privacy_policy_page_option.'</select> 
            </td>
        </tr>        
        
        <tr valign="top">
            <th scope="row">
                <label for="redirect_to_after_login">'.esc_html__('Redirect to Page After Login','quickrecipe').'</label>
            </th>
            <td> 
                <select id="redirect_to_after_login" name="redirect_to_after_login">'.$redirect_to_after_login_option.'</select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row">
                <label for="redirect_to_after_logout">'.esc_html__('Redirect to Page After Logout','quickrecipe').'</label>
            </th>
            <td> 
                <select id="redirect_to_after_logout" name="redirect_to_after_logout">'.$redirect_to_after_logout_option.'</select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row">
                <label for="submit_recipe_url">'.esc_html__('Submit Recipe Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="submit_recipe_url" name="submit_recipe_url">'.$submit_recipe_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="submit_testimonial_url">'.esc_html__('Submit Testimonial Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="submit_testimonial_url" name="submit_testimonial_url">'.$submit_testimonial_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="list_recipe_url">'.esc_html__('List Recipe Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="list_recipe_url" name="list_recipe_url">'.$list_recipe_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="list_testimonial_url">'.esc_html__('List Testimonial url','quickrecipe').'</label>
            </th>
            <td>
                <select id="list_testimonial_url" name="list_testimonial_url">'.$list_testimonial_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="list_my_favorite_url">'.esc_html__('List My Favorite Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="list_my_favorite_url" name="list_my_favorite_url">'.$list_my_favorite_option.'</select> 
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row">
                <label for="edit_my_profile_url">'.esc_html__('Edit My Profile Page','quickrecipe').'</label>
            </th>
            <td>
                <select id="edit_my_profile_url" name="edit_my_profile_url">'.$edit_my_profile_option.'</select> 
            </td>
        </tr>        
                
        </table>
    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button-primary" value="'.esc_html__('Save Changes','quickrecipe').'" />
    </p>    
    ';
    
 echo '</div>';   
}
endif;




// Social $  Contact
if( !function_exists('quickrecipe_theme_social') ):
function quickrecipe_theme_social(){
    

    $email_address   =   get_option('quickrecipe_email_address','');
    $facebook_link   =   get_option('quickrecipe_facebook_link','');
    $twitter_link    =   get_option('quickrecipe_twitter_link','');
    $google_link     =   get_option('quickrecipe_google_link','');
    $linkedin_link   =   get_option('quickrecipe_linkedin_link','');
    $pinterest_link  =   get_option('quickrecipe_pinterest_link','');    
    $vimeo_link      =   get_option('quickrecipe_vimeo_link','');
    $instagramm_link =   get_option('quickrecipe_instagramm_link','');    
    $youtube_link    =   get_option('quickrecipe_youtube_link','');    
    
    echo '<div class="tab-container">';
    echo '<h1 class="tab-h1">'.esc_html__('Social & Subscribe','quickrecipe').'</h1>';
    
    echo '<table class="form-table">     
        
    	<tr valign="top">
            <th scope="row"><label for="email_address">'.esc_html__('Email','quickrecipe').'</label></th>
            <td>  <input id="email_address" type="text" size="36" name="email_address" value="'.esc_attr($email_address).'" /></td>
        </tr>    
                        
        <tr valign="top">
            <th scope="row"><label for="facebook_link">'.esc_html__('Facebook Link','quickrecipe').'</label></th>
            <td>  <input id="facebook_link" type="text" size="36" name="facebook_link" value="'.esc_attr($facebook_link).'" /></td>
        </tr>        
        
        <tr valign="top">
            <th scope="row"><label for="twitter_link">'.esc_html__('Twitter Page Link','quickrecipe').'</label></th>
            <td>  <input id="twitter_link" type="text" size="36" name="twitter_link" value="'.esc_attr($twitter_link).'" /></td>
        </tr>
         
        <tr valign="top">
            <th scope="row"><label for="google_link">'.esc_html__('Google+ Link','quickrecipe').'</label></th>
            <td>  <input id="google_link" type="text" size="36" name="google_link" value="'.esc_attr($google_link).'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="pinterest_link">'.esc_html__('Pinterest Link','quickrecipe').'</label></th>
            <td>  <input id="pinterest_link" type="text" size="36" name="pinterest_link" value="'.esc_attr($pinterest_link).'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="linkedin_link">'.esc_html__('Linkedin Link','quickrecipe').'</label></th>
            <td>  <input id="linkedin_link" type="text" size="36" name="linkedin_link" value="'.esc_attr($linkedin_link).'" /></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="vimeo_link">'.esc_html__('Vimeo Link','quickrecipe').'</label></th>
            <td>  <input id="vimeo_link" type="text" size="36" name="vimeo_link" value="'.esc_attr($vimeo_link).'" /></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="youtube_link">'.esc_html__('Youtube Link','quickrecipe').'</label></th>
            <td>  <input id="youtube_link" type="text" size="36" name="youtube_link" value="'.esc_attr($youtube_link).'" /></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="instagramm_link">'.esc_html__('Instagramm Link','quickrecipe').'</label></th>
            <td>  <input id="instagramm_link" type="text" size="36" name="instagramm_link" value="'.esc_attr($instagramm_link).'" /></td>
        </tr>
        
    </table>
    <p class="submit">
      <input type="submit" name="submit" id="submit" class="button-primary"  value="'.esc_html__('Save Changes','quickrecipe').'" />
    </p>';
echo '</div>';
}
endif; // end   quickrecipe_theme_social  


if( !function_exists('quickrecipe_theme_contact') ):
function quickrecipe_theme_contact(){
    

    $phone_number               =   get_option('quickrecipe_phone_number','');
    $mobile_no                  =   get_option('quickrecipe_mobile_no','');
    $company_name               =   get_option('quickrecipe_company_name','');
    $company_address            =   get_option('quickrecipe_company_address','');
    $email_address              =   get_option('quickrecipe_email_address','');
    $longitude                  =   get_option('quickrecipe_longitude');
    $latitude                   =   get_option('quickrecipe_latitude');
    $map_height                 =   get_option('quickrecipe_map_height');
    

    
    echo '<div class="tab-container">';
    echo '<h1 class="tab-h1">'.esc_html__('Contact Page','quickrecipe').'</h1>';
    
    echo '<table class="form-table">     
        
        <tr valign="top">
            <th scope="row"><label for="company_name">'.esc_html__('Company Name','quickrecipe').'</label></th>
            <td>  <input id="company_name" type="text" size="36" name="company_name" value="'.esc_attr($company_name).'" /></td>
        </tr>   

        <tr valign="top">
            <th scope="row"><label for="company_name">'.esc_html__('Company Address','quickrecipe').'</label></th>
            <td>  <input id="company_address" type="text" size="36" name="company_address" value="'.esc_attr($company_address).'" /></td>
        </tr>   
        
    	<tr valign="top">
            <th scope="row"><label for="email_address">'.esc_html__('Email','quickrecipe').'</label></th>
            <td>  <input id="email_address" type="text" size="36" name="email_address" value="'.esc_attr($email_address).'" /></td>
        </tr>    
                
        <tr valign="top">
            <th scope="row"><label for="phone_number">'.esc_html__('Phone Number','quickrecipe').'</label></th>
            <td>  <input id="phone_number" type="text" size="36" name="phone_number" value="'.esc_attr($phone_number).'" /></td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="mobile_no">'.esc_html__('Mobile','quickrecipe').'</label></th>
            <td>  <input id="mobile_no" type="text" size="36" name="mobile_no" value="'.esc_attr($mobile_no).'" /></td>
        </tr> 

      

        <tr valign="top">
            <th scope="row"></th>
            <td>'.esc_html__('The Google Maps JavaScript API v3 does not require an API key to function correctly. However, we strongly encourage you to get  an APIs Console key and post the code in Theme Options.','quickrecipe').'.</td>
        </tr>

        
    </table>
    <p class="submit">
      <input type="submit" name="submit" id="submit" class="button-primary"  value="'.esc_html__('Save Changes','quickrecipe').'" />
    </p>';
echo '</div>';
}
endif;

// Apperance
if( !function_exists('quickrecipe_theme_apperance') ):
function quickrecipe_theme_apperance(){
    

    $latest_recipe_limit        =   intval   ( get_option('quickrecipe_latest_recipe_limit','') );
    $latest_recipe_title        =   esc_attr ( get_option('quickrecipe_latest_recipe_title','') );
    $latest_recipe_title_desc   =   esc_attr ( get_option('quickrecipe_latest_recipe_title_desc','') );
    $most_favorited_limit       =   intval   ( get_option('quickrecipe_most_favorited_limit','') );
    $most_favorited_title       =   esc_attr ( get_option('quickrecipe_most_favorited_title','') );
    $most_favorited_title_desc  =   esc_attr ( get_option('quickrecipe_most_favorited_title_desc','') );
    $editor_choice_limit        =   intval   ( get_option('quickrecipe_editor_choice_limit','') );
    $editor_choice_title        =   esc_attr ( get_option('quickrecipe_editor_choice_title','') );
    $editor_choice_title_desc   =   esc_attr ( get_option('quickrecipe_editor_choice_title_desc','') );
    $testimonials_limit         =   intval   ( get_option('quickrecipe_testimonials_limit','') );
    $testimonials_title         =   esc_attr ( get_option('quickrecipe_testimonials_title','') );
    $testimonials_title_desc    =   esc_attr ( get_option('quickrecipe_testimonials_title_desc','') );
    $most_read_limit            =   intval   ( get_option('quickrecipe_most_read_limit','') );
    $most_read_title            =   esc_attr ( get_option('quickrecipe_most_read_title','') );
    $most_read_title_desc       =   esc_attr ( get_option('quickrecipe_most_read_title_desc','') );
    $offers_limit               =   intval   ( get_option('quickrecipe_offers_limit','') );
    $offers_title               =   esc_attr ( get_option('quickrecipe_offers_title','') );
    $offers_title_desc          =   esc_attr ( get_option('quickrecipe_offers_title_desc','') );
    $our_editors_limit          =   intval   ( get_option('quickrecipe_our_editors_limit','') );
    $our_editors_title          =   esc_attr ( get_option('quickrecipe_our_editors_title','') );
    $our_editors_title_desc     =   esc_attr ( get_option('quickrecipe_our_editors_title_desc','') );
    $latest_news_limit            =   intval   ( get_option('quickrecipe_latest_news_limit','') );
    $latest_news_title            =   esc_attr ( get_option('quickrecipe_latest_news_title','') );
    $latest_news_title_desc       =   esc_attr ( get_option('quickrecipe_latest_news_title_desc','') );

    $show_search_area_option    ='';
    $show_search_area           = esc_html ( get_option('quickrecipe_show_search_area','') );
    $home_intro                 = esc_html ( get_option('quickrecipe_home_intro','') );

    $banner_image               =   esc_html( get_option('quickrecipe_banner_image','') );

    $show_search_area_option    = quickrecipe_yes_no_select($show_search_area);

    $show_latest_recipes_option = '';
    $show_latest_recipes        = esc_html ( get_option('quickrecipe_show_latest_recipes','') );    
    $show_latest_recipes_option = quickrecipe_yes_no_select($show_latest_recipes);
    
    $show_top_bar_user_login_option ='';
    $show_top_bar_user_login_status = esc_html ( get_option('quickrecipe_show_top_bar_user_login','') );    
    $show_top_bar_user_login_option = quickrecipe_yes_no_select($show_top_bar_user_login_status);

    $show_testimonials_option   ='';
    $show_testimonials          = esc_html ( get_option('quickrecipe_show_testimonials','') );    
    $show_testimonials_option   = quickrecipe_yes_no_select($show_testimonials);

    $show_editor_choice_option  ='';
    $show_editor_choice         = esc_html ( get_option('quickrecipe_show_editor_choice','') );    
    $show_editor_choice_option  = quickrecipe_yes_no_select($show_editor_choice);

    $show_our_editors_option  ='';
    $show_our_editors         = esc_html ( get_option('quickrecipe_show_our_editors','') );    
    $show_our_editors_option  = quickrecipe_yes_no_select($show_our_editors);

    $show_most_favorited_option ='';
    $show_most_favorited        = esc_html ( get_option('quickrecipe_show_most_favorited','') );    
    $show_most_favorited_option = quickrecipe_yes_no_select($show_most_favorited);

    $show_offers_option ='';
    $show_offers        = esc_html ( get_option('quickrecipe_show_offers','') );    
    $show_offers_option = quickrecipe_yes_no_select($show_offers);

    $show_most_read_option      ='';
    $show_most_read             = esc_html ( get_option('quickrecipe_show_most_read','') );    
    $show_most_read_option      = quickrecipe_yes_no_select($show_most_read);

    $show_latest_news_option = '';
    $show_latest_news        = esc_html ( get_option('quickrecipe_show_latest_news','') );    
    $show_latest_news_option = quickrecipe_yes_no_select($show_latest_news);
     
    echo '<div class="tab-container">';
    echo '<h1 class="tab-h1">'.esc_html__('Appearance','quickrecipe').'</h1>';
    echo '<table class="form-table">     
                         
        <tr valign="top">
            <th scope="row"><label for="show_top_bar_user_login">'.esc_html__('Show wp admin Bar in Theme?','quickrecipe').' </label></th>
               <td> 
               <select id="show_top_bar_user_login" name="show_top_bar_user_login">
                    '.$show_top_bar_user_login_option.'
		       </select>
            </td>
        </tr>      
        
        <tr valign="top">
            <th scope="row"><label for="show_search_area">'.esc_html__('Show Search Area?','quickrecipe').' </label></th>
               <td>
               <select id="show_search_area" name="show_search_area">
                    '.$show_search_area_option.'
		        </select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="home_intro">'.esc_html__('Homepage Introduction Text','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="home_intro" name="home_intro">'.$home_intro.'</textarea></td>
        </tr>

        <tr valign="top">
            <th colspan="2" class="section-title">  
                <h2 class="section-title">'.esc_html__('Latest Recipes','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_latest_recipes">'.esc_html__('Show Latest Recipes?','quickrecipe').' </label></th>
               <td> 
                    <select id="show_latest_recipes" name="show_latest_recipes">
                    '.$show_latest_recipes_option.'
		            </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="latest_recipe_title">'.esc_html__('Latest Recipe Title','quickrecipe').'</label></th>
            <td>  <input id="latest_recipe_title" type="text" size="36" name="latest_recipe_title" value="'.$latest_recipe_title.'" /></td>
        </tr>
         
         <tr valign="top">
            <th scope="row"><label for="latest_recipe_title_desc">'.esc_html__('Latest Recipe Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="latest_recipe_title_desc" name="latest_recipe_title_desc">'.$latest_recipe_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="latest_recipe_limit">'.esc_html__('Latest Recipe Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="latest_recipe_limit" name="latest_recipe_limit" value="' . $latest_recipe_limit . '">   
            </td>
        </tr> 
              
        <tr valign="top">
            <th colspan="2">  
                <h2 class="section-title">'.esc_html__('Most Favorited Recipe','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_most_favorited">'.esc_html__('Show Most Favorited Recipes?','quickrecipe').' </label></th>
               <td> 
                    <select id="show_most_favorited" name="show_most_favorited">
                    '.$show_most_favorited_option.'
		            </select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="most_favorited_title">'.esc_html__('Most Favorited Recipe Title','quickrecipe').'</label></th>
            <td>  
                <input id="most_favorited_title" type="text" size="36" name="most_favorited_title" value="'.$most_favorited_title.'" />
            </td>
        </tr>
         
         <tr valign="top">
            <th scope="row"><label for="most_favorited_title_desc">'.esc_html__('Most Favorited Recipe Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="most_favorited_title_desc" name="most_favorited_title_desc">'.$most_favorited_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="most_favorited_limit">'.esc_html__('Most Favorited Recipe Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="most_favorited_limit" name="most_favorited_limit" value="' . $most_favorited_limit . '">   
            </td>
        </tr> 

        <tr valign="top">
            <th colspan="2">  
                <h2 class="section-title">'.esc_html__('Offers','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_offers">'.esc_html__('Show Offers Section?','quickrecipe').' </label></th>
               <td> 
                    <select id="show_offers" name="show_offers">
                    '.$show_offers_option.'
		            </select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="offers_title">'.esc_html__('Offers Title','quickrecipe').'</label></th>
            <td>  
                <input id="offers_title" type="text" size="36" name="offers_title" value="'.$offers_title.'" />
            </td>
        </tr>
         
         <tr valign="top">
            <th scope="row"><label for="offers_title_desc">'.esc_html__('Offers Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="offers_title_desc" name="offers_title_desc">'.$offers_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="offers_limit">'.esc_html__('Offers Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="offers_limit" name="offers_limit" value="' . $offers_limit . '">   
            </td>
        </tr> 

        <tr valign="top">
            <th colspan="2">  
                <h2 class="section-title">'.esc_html__('Our Editors','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_our_editors">'.esc_html__('Show Our Editors?','quickrecipe').' </label></th>
               <td> 
                    <select id="show_our_editors" name="show_our_editors">
                    '.$show_our_editors_option.'
		            </select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="our_editors_title">'.esc_html__('Our Editors Title','quickrecipe').'</label></th>
            <td>  
                <input id="our_editors_title" type="text" size="36" name="our_editors_title" value="'.$our_editors_title.'" />
            </td>
        </tr>
         
         <tr valign="top">
            <th scope="row"><label for="our_editors_title_desc">'.esc_html__('Our Editors Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="our_editors_title_desc" name="our_editors_title_desc">'.$our_editors_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="our_editors_limit">'.esc_html__('Our Editors Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="our_editors_limit" name="our_editors_limit" value="' . $our_editors_limit . '">   
            </td>
        </tr> 

        <tr valign="top">
            <th colspan="2" class="section-title">  
                <h2 class="section-title">'.esc_html__('Editor\'s Choice Recipe','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_editor_choice">'.esc_html__('Show Editor\'s Choice Recipe?','quickrecipe').' </label></th>
               <td> 
                    <select id="show_editor_choice" name="show_editor_choice">
                    '.$show_editor_choice_option.'
		            </select>
            </td>
        </tr>

         <tr valign="top">
            <th scope="row"><label for="editor_choice_title">'.esc_html__('Editor\'s Choice Recipe Title','quickrecipe').'</label></th>
            <td>  <input id="editor_choice_title" type="text" size="36" name="editor_choice_title" value="'.$editor_choice_title.'" /></td>
        </tr>
         
         <tr valign="top">
            <th scope="row"><label for="editor_choice_title_desc">'.esc_html__('Editor\'s Choice Recipe Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="editor_choice_title_desc" name="editor_choice_title_desc">'.$editor_choice_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="editor_choice_limit">'.esc_html__('Editor\'s Choice Recipe Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="editor_choice_limit" name="editor_choice_limit" value="' . $editor_choice_limit . '">   
            </td>
        </tr> 

        <tr valign="top">
            <th colspan="2">  
                <h2 class="section-title">'.esc_html__('Most Read Recipe','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_most_read">'.esc_html__('Show Most Read recipes?','quickrecipe').' </label></th>
               <td> 
                    <select id="show_most_read" name="show_most_read">
                    '.$show_most_favorited_option.'
		            </select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="most_read_title">'.esc_html__('Most Read Recipe Title','quickrecipe').'</label></th>
            <td>  
                <input id="most_read_title" type="text" size="36" name="most_read_title" value="'.$most_read_title.'" />
            </td>
        </tr>
         
         <tr valign="top">
            <th scope="row"><label for="most_read_title_desc">'.esc_html__('Most Read Recipe Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="most_read_title_desc" name="most_read_title_desc">'.$most_read_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="most_read_limit">'.esc_html__('Most Read Recipe Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="most_read_limit" name="most_read_limit" value="' . $most_read_limit . '">   
            </td>
        </tr> 

        <tr valign="top">
            <th colspan="2">  
                <h2 class="section-title">'.esc_html__('Top Recipes','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_latest_news">'.esc_html__('Latest News?','quickrecipe').' </label></th>
               <td> 
                    <select id="show_latest_news" name="show_latest_news">
                    '.$show_latest_news_option.'
		            </select>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="latest_news_title">'.esc_html__('Latest News Title','quickrecipe').'</label></th>
            <td>  
                <input id="latest_news_title" type="text" size="36" name="latest_news_title" value="'.$latest_news_title.'" />
            </td>
        </tr>
         
         <tr valign="top">
            <th scope="row"><label for="latest_news_title_desc">'.esc_html__('Latest News Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="latest_news_title_desc" name="latest_news_title_desc">'.$latest_news_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="latest_news_limit">'.esc_html__('Latest News Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="latest_news_limit" name="latest_news_limit" value="' . $latest_news_limit . '">   
            </td>
        </tr> 

        <tr valign="top">
            <th colspan="2" class="section-title">  
                <h2 class="section-title">'.esc_html__('Testimonials','quickrecipe').'</h2>
            </th>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="show_testimonials">'.esc_html__('Show Testimonials?','quickrecipe').' </label></th>
               <td> <select id="show_testimonials" name="show_testimonials">
                    '.$show_testimonials_option.'
		 </select>
            </td>
        </tr>        

        <tr valign="top">
            <th scope="row"><label for="testimonials_title">'.esc_html__('Testimonial Title','quickrecipe').'</label></th>
            <td>  <input id="testimonials_title" type="text" size="36" name="testimonials_title" value="'.$testimonials_title.'" /></td>
        </tr>
         
        <tr valign="top">
            <th scope="row"><label for="testimonials_title_desc">'.esc_html__('Testimonial Title Description','quickrecipe').'</label></th>
            <td><textarea cols="57" rows="2" id="testimonials_title_desc" name="testimonials_title_desc">'.$testimonials_title_desc.'</textarea></td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="testimonials_limit">'.esc_html__('Testimonial Limit','quickrecipe').'</label></th>
            <td>
                <input type="text" id="testimonials_limit" name="testimonials_limit" value="' . $testimonials_limit . '">   
            </td>
        </tr> 

        <tr valign="top">
            <th scope="row"><label for="banner_image">'.esc_html__('Your Banner','quickrecipe').'</label></th>
            <td>
	            <input id="banner_image" type="text" class="img" size="36" name="banner_image" value="'.$banner_image.'" />
		        <input id="banner_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Banner','quickrecipe').'" />
            </td>
        </tr> 

    </table>
    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button-primary"  value="'.esc_html__('Save Changes','quickrecipe').'" />
    </p>';
    echo '</div>';
}
endif;

if( !function_exists('quickrecipe_update_theme_option') ):
function quickrecipe_update_theme_option() 
{  
   

   if($_SERVER['REQUEST_METHOD'] === 'POST')
   {	      
        foreach($_POST as $variable=>$value)
        {	
            if ($variable!='submit')
            {
                update_option( 'quickrecipe_'.$variable, $value );
            }	
        }  
    }
    

$active_tab = isset( $_GET[ 'tab' ] ) ? wp_kses( $_GET[ 'tab' ],array() ) : 'general_settings';  

echo ' <div class="tab-wrap">
        <form method="post" action="">
        <div class="tab-wrapper-container">
        <div class="tab-wrapper">';
            echo '<div class="theme-logo"><img src="'.esc_url(get_template_directory_uri()).'/img/theme-logo.png"></div>';
            
            $tab_item_url = 'themes.php?page=includes/theme-options.php&tab=';
            echo '<div class="tab-item '; 
            echo esc_attr($active_tab) == 'general_settings'  ? 'tab-active' : '';
            echo '"><a href="'.esc_url($tab_item_url).'general_settings"><i class="fa fa-wrench"></i> '.esc_html__('General Settings','quickrecipe').'</a></div>';

            echo '<div class="tab-item ';
            echo esc_attr($active_tab) == 'wp_settings' ? 'tab-active' : ''; 
            echo'"><a href="'.esc_url($tab_item_url).'wp_settings"><i class="fa fa-cog"></i> '.esc_html__('Page Settings','quickrecipe').'</a></div>';
                
            echo '<div class="tab-item ';
            echo esc_attr($active_tab) == 'social_subscribe' ? 'tab-active' : ''; 
            echo'"><a href="'.esc_url($tab_item_url).'social_subscribe"><i class="fa fa-share"></i> '.esc_html__('Social & Subscribe','quickrecipe').'</a></div>';
           
            echo '<div class="tab-item ';
            echo esc_attr($active_tab) == 'appearance' ? 'tab-active' : ''; 
            echo'"><a href="'.esc_url($tab_item_url).'appearance"><i class="fa fa-desktop"></i> '.esc_html__('Appearance','quickrecipe').'</a></div>';
                                                                                    
            echo '<div class="tab-item ';
            echo esc_attr($active_tab) == 'contact_page' ? 'tab-active' : ''; 
            echo'"><a href="'.esc_url($tab_item_url).'contact_page"><i class="fa fa-map-marker"></i> '.esc_html__('Contact Page','quickrecipe').'</a></div>';

       echo '</div>';

   

    switch ($active_tab) {
        case "general_settings":
            quickrecipe_theme_general_settings();
            break;
        case "wp_settings":
            quickrecipe_theme_wp_settings();
            break;
        case "social_subscribe":
            quickrecipe_theme_social();
            break;
        case "contact_page":
            quickrecipe_theme_contact();
            break;
        case "appearance":
            quickrecipe_theme_apperance();
            break;
    }        
                   
echo '</div></form></div>';
}
endif;


if( !function_exists('quickrecipe_page_select') ):
    function quickrecipe_page_select($default = '')
    {
        $page_options = '';
        $pages = get_pages(); 
        foreach($pages as $page)
        {
		    $selected = ($page->ID == $default) ? ' selected="selected"' : '';
		    $page_options .= '<option value="' . $page->ID . '"' . $selected . '>' . esc_attr($page->post_title) . '</option>';
        }
        return $page_options;
    }
endif;


function quickrecipe_yes_no_select($default = '')
{
    $yes_no_array = array( '1' => esc_html__('yes','quickrecipe'), '0' => esc_html__('no','quickrecipe'));

	$yes_no_options = '';
    foreach ($yes_no_array as $key => $value)
    {
		$selected = ($key == $default) ? ' selected="selected"' : '';
		$yes_no_options .= '<option value="' . $key . '"' . $selected . '>' . esc_attr($value) . '</option>';
    }

	return $yes_no_options;
}

?>