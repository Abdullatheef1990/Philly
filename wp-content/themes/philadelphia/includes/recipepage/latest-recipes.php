<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$latest_recipe_limit = get_option('quickrecipe_latest_recipe_limit', 16); 
$latest_recipe_title = get_option('quickrecipe_latest_recipe_title', ''); 
$latest_recipe_title_desc = get_option('quickrecipe_latest_recipe_title_desc', ''); 
$recipe_string = '';
if (isset($_GET['sort'])) {
	$recipe_string = wp_kses($_GET['sort'], '');	
}
?>

    
		<?php

		

//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
 $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
/*
$query= new WP_Query(array(
    'post_type'=>'recipe',
    'posts_per_page' => 10,
    'paged' => $paged,
));

        if($query->have_posts()) :
    while($query->have_posts())  : $query->the_post();
*/
if(!empty($recipe_string)) {
			$query_args = array(
                               'post_type'=>'recipe',
								'posts_per_page' => 10,
                                'post_status' => 'publish',                                     						
                                'paged' => $paged,
								'orderby' => 'title',
								'order'   => 'ASC', 
        					);
}else{
	$query_args = array(
                               'post_type'=>'recipe',
								'posts_per_page' => 10,
                                'post_status' => 'publish',                                     						
                                'paged' => $paged,
        					);
}

		  
		 $query=new WP_Query($query_args);
  if ( $query->have_posts() ) : 
?>


      <?php if (ICL_LANGUAGE_CODE=='en') { ?>

<div class="recipes-filter">
        <select name="recipe-sort" onchange="location = this.value;">
            <option>Sort by</option>
            <option value="/recipes/">Latest</option>
            <option value="/recipes/?sort=name">Name</option>
        </select>
    </div>
      
<?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
	<div class="recipes-filter">
        <select name="recipe-sort" onchange="location = this.value;">
            <option>رتب</option>
            <option value="/وصفات/?lang=ar">رتب</option>
            <option value="/وصفات/?lang=ar&sort=name">رتب</option>
        </select>
    </div>
      
<?php } ?>
<?php	            
	            	 	 while($query->have_posts()) :
		            $query->the_post();
?>
         <?php /*?><a href='<?php echo get_permalink(); ?>' ><?php */?>
           <div class="columns large-6 small-12  load-more-article recipes-grid-element" style="background-image: url('<?php echo get_the_post_thumbnail_url( $post->ID, 'large' ); ?>');">
            <div class="row full-width grid-inner">
                <div class="column large-6 small-6 panel">

                </div>
                <div class="column large-6 small-6 transparent-block panel">

                    <h2><?php the_title(); ?></h2>
					<?php if (ICL_LANGUAGE_CODE=='en') { ?>

 <a href='<?php echo get_permalink(); ?>'  class="transparent-button">START COOKING</a>
<?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
	 <a href='<?php echo get_permalink(); ?>'  class="transparent-button">بدء الطهي</a>
<?php } ?>
                   

                </div>
            </div>

        </div>
		<?php /*?>	</a><?php */?>
        
        
        <?php
			 endwhile;
?>

       <div class="row small-12">
         <div class="pagination">
          <?php
           // wp_reset_postdata();
			$total_pages = $query->max_num_pages;
			 $big = 999999999;
    if ($total_pages > 1){

        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        	//'format' => '?paged=%#%',
        	//'current' => max( 1, get_query_var('paged') ),
        	//'total' => @$query->max_num_pages,
        	//'type'  => 'array',
			
            //'base' => get_pagenum_link(1) . '%_%',
            //'format' => 'page/%#%',
			 //'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
            'prev_text'    => __('« prev'),
            'next_text'    => __('next »'),
        ));
		
		
    } 
			 ?>
		   </div></div>
       <?php else :?>
	        <article <?php post_class(); ?>>
            <?php if (ICL_LANGUAGE_CODE=='en') {?>

			          <p class="noarticle"><?php esc_html_e( 'Latest recipes are not available.', 'quickrecipe' ); ?></p>
<?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
	<p class="noarticle"><?php esc_html_e( 'غينيا واستمر العصبة ضرب قد. وباءت الأمريكي الأوربيين هو', 'quickrecipe' ); ?></p>
<?php } ?>
	  
	            
	            
	              
	        </article><!-- /.post -->
       <?php endif; ?>
<?php wp_reset_postdata();?>

     
  
      
        
        <!-- end:latest -->
      
