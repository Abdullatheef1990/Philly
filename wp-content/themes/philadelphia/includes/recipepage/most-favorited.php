<?php
if ( ! defined( 'ABSPATH' ) ) exit;



$most_favorited_limit = get_option('quickrecipe_most_favorited_limit', 3); 
$most_favorited_title = get_option('quickrecipe_most_favorited_title', ''); 
$most_favorited_title_desc = get_option('quickrecipe_most_favorited_title_desc', ''); 

?>

    <div id="most-rated">
      <div class="container">

        <!-- begin:for-sale -->
        <div class="row">
          <div class="col-md-12">
            <?php if ( !empty($most_favorited_title) != '' ): ?>
            <div class="heading">
              <?php
	              if ( !empty($most_favorited_title) ): ?><h2><?php echo esc_attr( stripslashes( $most_favorited_title ) ); ?></h2><?php endif;
	              if ( !empty($most_favorited_title_desc) ): ?><p><?php echo esc_attr( stripslashes( $most_favorited_title_desc ) ); ?></p><?php endif; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
		<?php
        $query_args = array(
        					'post_type' => 'recipe', 
        					'posts_per_page' => intval( $most_favorited_limit ),
	                        'orderby'    => 'meta_value',
	                        'order'      => 'DESC',
	                        'meta_query' => array(
		                        array(
			                        'key'     => 'quickrecipe_editor_choice_recipe',
			                        'value'   => 'true',
			                        'compare' => 'NOT IN',
		                        ),
		                        array(
			                        'key'     => MY_FAVORITE_RECIPES,
			                        'value'   => '0',
			                        'compare' => '>',
		                        ),
	                        )
        				);
        	
        $query=new WP_Query($query_args);
        	
        if ( $query->have_posts() ) {
            while($query->have_posts()){
	            $query->the_post();
                $term_names = wp_get_post_terms($post->ID, 'course', array("fields" => "names"));
                $term_list = array();
                if (is_array($term_names)) {
                    foreach($term_names as $term_name) {
                        $term_list[] = $term_name;
                    }
                    $term_name_list = join( ", ", $term_list );
                } else {
                    $term_name_list = '';
                }
		?>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="item-container">
              <div class="item-img-box">
				<?php
				if ( has_post_thumbnail( $post->ID ) ) {
					echo get_the_post_thumbnail( $post->ID, 'large' );
				}
				?>
                <a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><div class="hover-button"><?php esc_html_e('View Recipe', 'quickrecipe'); ?></div></a>
              </div>
              <div class="item-meta-box">
                <span><i class="fa fa-clock-o"></i> <?php $prep_time = intval( get_post_meta(get_the_ID(), 'quickrecipe_prep_time', true) ); echo esc_attr(quickrecipe_get_mins_to_hours($prep_time)); ?></span>
                <span><i class="fa fa-user"></i> <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true); if (!empty($servings)) { echo sprintf(wp_kses(__('%s People', 'quickrecipe'), ''), esc_attr($servings)); } ?></span>
                <span><i class="fa fa-spoon"></i> <?php $yield = get_post_meta($post->ID, 'quickrecipe_yield', true); if (!empty($servings)) { echo sprintf(wp_kses(__('%s Yield', 'quickrecipe'), ''), esc_attr($yield)); } ?></span>
                <span><i class="fa fa-comments"></i> <?php comments_number( 'no responses', 'one response', '% responses' ); ?></span>
              </div>
              <div class="item-content-box">
                <h3><a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><?php the_title(); ?></a> <small><?php echo esc_html($term_name_list); ?></small></h3>
                <p><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></p>
                <div class="avatar">
                    <div class="clearfix">
                        <div class="pull-left">
                            <?php echo get_avatar( get_the_author_meta( 'ID' ), '25' ); ?>
                            <?php esc_html_e('By ', 'quickrecipe'); ?> <?php the_author_posts_link(); ?>
                        </div>
                        <span class="user-ratings pull-right">
                            <?php echo average_rating(); ?>
                        </span>
                    </div>
                </div>
              </div>
              <?php
              $postURL = urlencode(get_permalink());
              $postTitle = str_replace( ' ', '%20', get_the_title());
              $mailto = "mailto:?subject=".$postTitle."&amp;body=".$postTitle.":".$postURL;
              ?>
              <div class="item-button-box">
              <?php
              if (quickrecipe_is_favorite($post->ID)) {
              ?>              
                <a href="<?php echo esc_url(get_permalink( $post->ID ) . '?quickrecipe_remove_from_favorite=') . $post->ID; ?>" title="<?php esc_html_e( 'Remove from favorites', 'quickrecipe' ); ?>"><i class="fa fa-heart-o"></i></a>
              <?php
              } else {
              ?>
                <a href="<?php echo esc_url(get_permalink( $post->ID ) . '?quickrecipe_add_to_favorite=' . $post->ID); ?>" title="<?php esc_html_e( 'Add to favorites', 'quickrecipe' ); ?>"><i class="fa fa-heart"></i></a>
              <?php } ?>
                <a href="#popup-social<?php echo esc_attr($post->ID); ?>" title="<?php esc_html_e( 'Share', 'quickrecipe' ); ?>"><i class="fa fa-share"></i></a>
                <a href="<?php echo esc_url($mailto); ?>" title="<?php esc_html_e( 'Send E-mail', 'quickrecipe' ); ?>"><i class="fa fa-envelope"></i></a>
                <a href="<?php comments_link(); ?>" title="<?php esc_html_e( 'Add Comment', 'quickrecipe' ); ?>"><i class="fa fa-comment"></i></a>
              </div>
            </div>
          </div>
          <!-- break -->
            <?php
                get_template_part( 'includes/html/popup-social' );
        	}
            wp_reset_postdata();

        } else {
        // In the "Theme Options > Appearance > Most Favorited Recipe" section, select the "Yes" option from the list to show it on the homepage
            ?>
	        <article <?php post_class(); ?>>
	            <p class="noarticle"><?php esc_html_e( 'Most favored recipes are not available.', 'quickrecipe' ); ?></p>
	        </article>
	    <?php } ?> 

        </div>
        <!-- end:for-sale -->
      </div>
    </div>
    <!-- end:content -->

