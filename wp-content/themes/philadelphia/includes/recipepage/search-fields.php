<?php
if ( ! defined( 'ABSPATH' ) ) exit;


$search_string = '';
if (isset($_GET['s'])) {
	$search_string = wp_kses($_GET['s'], '');	
}

$recipe_type = 0;
if (isset($_GET['recipe-type'])) {
	$recipe_type = intval(wp_kses($_GET['recipe-type'], '0'));
}

$cuisine = 0;
if (isset($_GET['cuisine'])) {
	$cuisine = intval(wp_kses($_GET['cuisine'], '0'));
}

$course = 0;
if (isset($_GET['course'])) {
	$course = intval(wp_kses($_GET['course'], '0'));
}

$season = 0;
if (isset($_GET['season'])) {
	$season = intval(wp_kses($_GET['season'], '0'));
}

$ingredient = 0;
if (isset($_GET['ingredient'])) {
	$ingredient = intval(wp_kses($_GET['ingredient'], '0'));
}

$allergen = 0;
if (isset($_GET['allergen'])) {
	$allergen = intval(wp_kses($_GET['allergen'], '0'));
}

$difficulty = 0;
if (isset($_GET['difficulty_level'])) {
	$difficulty = intval(wp_kses($_GET['difficulty_level'], '0'));
}

if ( function_exists( 'quickrecipe_create_taxonomies' ) ) {
?>
      <?php if (ICL_LANGUAGE_CODE=='en') { ?>

     <div class="row search-form">
            <div class="columns large-3 small-12">

                    <?php
	
						$course_args = array(
		'show_option_none' => __( 'Choose Category', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'SLUG',
		'echo'             => 1,
	    'selected'           => $course,
	    'name'             => 'course',
	    'id'               => '',
	    'class'            => '',
		'hierarchical'     => 1,							
	    'depth'            => 0,
	    'tab_index'        => 1,
	    'taxonomy'         => 'course',
	);
    wp_dropdown_categories( $course_args );
	?>
	
					

            </div>
            <div class="columns large-3 small-12">

                 <?php
	
						$season_args = array(
		'show_option_none' => __( 'Choose Season', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'name',
		'echo'             => 1,
	    'selected'           => $season,
	    'name'             => 'season',
	    'id'               => '',
	    'class'            => '',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'season',
	);
    wp_dropdown_categories( $season_args );
	?>
	

            </div>
            
             <div class="columns large-6 small-12">

                
               
                
                <input type="text"  placeholder="<?php esc_html_e( 'Search by Keyword', 'quickrecipe' ) ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">

           
            </div>
           
        </div>

    
<?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
	 <div class="row search-form">
            <div class="columns large-3 small-12">

                    <?php
	
						$course_args = array(
		'show_option_none' => __( 'مضغة خفيف', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'SLUG',
		'echo'             => 1,
	    'selected'           => $course,
	    'name'             => 'course',
	    'id'               => '',
	    'class'            => '',
		'hierarchical'     => 1,							
	    'depth'            => 0,
	    'tab_index'        => 1,
	    'taxonomy'         => 'course',
	);
    wp_dropdown_categories( $course_args );
	?>
	
					

            </div>
            <div class="columns large-3 small-12">

                 <?php
	
						$season_args = array(
		'show_option_none' => __( 'قدم خدمة', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'name',
		'echo'             => 1,
	    'selected'           => $season,
	    'name'             => 'season',
	    'id'               => '',
	    'class'            => '',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'season',
	);
    wp_dropdown_categories( $season_args );
	?>
	

            </div>
            
             <div class="columns large-6 small-12">

                
               
                
                <input type="text"  placeholder="<?php esc_html_e( 'البحث عن طريق الكلمات الرئيسية', 'quickrecipe' ) ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">      

           
            </div>
           
        </div>

          
<?php } ?>
<?php /*
<div class="col-md-3 col-sm-3 col-xs-6">
    <div class="form-group">
    <?php 
    $recipe_type_args = array(
	    'show_option_all'    => '',
	    'show_option_none'   => __(' - Choose Category - ','quickrecipe'),
	    'option_none_value'  => '0',
	    'orderby'            => 'NAME', 
	    'order'              => 'ASC',
	    'show_count'         => 0,
	    'hide_empty'         => 0, 
	    'child_of'           => 0,
	    'exclude'            => '',
	    'echo'               => 1,
	    'selected'           => $recipe_type,
	    'hierarchical'       => 0, 
	    'name'               => 'recipe-type',
	    'id'                 => '',
	    'class'              => 'form-control',
	    'depth'              => 0,
	    'tab_index'          => 0,
	    'taxonomy'           => 'recipe_type',
	    'hide_if_empty'      => false,
	    'value_field'	     => 'term_id',	
    ); 
    wp_dropdown_categories( $recipe_type_args );
    ?>
    </div>
   

     <div class="form-group">
	<?php
	$cuisine_args = array(
		'show_option_none' => __( ' - Choose Cuisine - ', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'name',
		'echo'             => 1,
	    'selected'           => $cuisine,
	    'name'             => 'cuisine',
	    'id'               => '',
	    'class'            => 'form-control',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'cuisine',
	);
    wp_dropdown_categories( $cuisine_args );
	?>
    </div>
	
</div>
*/?>
<!-- break -->
 <?php /*
<div class="col-md-3 col-sm-3 col-xs-6">
    <div class="form-group">
	<?php
	$course_args = array(
		'show_option_none' => __( ' - Choose Course -', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'SLUG',
		'echo'             => 1,
	    'selected'           => $course,
	    'name'             => 'course',
	    'id'               => '',
	    'class'            => 'form-control',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'course',
	);
    wp_dropdown_categories( $course_args );
	?>
    </div>
   
    <div class="form-group">
	<?php
	$season_args = array(
		'show_option_none' => __( ' - Choose Season - ', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'name',
		'echo'             => 1,
	    'selected'           => $season,
	    'name'             => 'season',
	    'id'               => '',
	    'class'            => 'form-control',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'season',
	);
    wp_dropdown_categories( $season_args );
	?>
    </div>
	
</div>
*/?>
<!-- break -->
<?php /*
<div class="col-md-3 col-sm-3 col-xs-6">
    <div class="form-group">
	<?php
	$season_args = array(
		'show_option_none' => __( ' - Choose Allergen - ', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'name',
		'echo'             => 1,
	    'selected'           => $allergen,
	    'name'             => 'allergen',
	    'id'               => '',
	    'class'            => 'form-control',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'allergen',
	);
    wp_dropdown_categories( $season_args );
	?>
    </div>
    <div class="form-group">
	<?php
	$ingredient_args = array(
		'show_option_none' => __( ' - Choose Ingredient - ', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'name',
		'echo'             => 1,
	    'selected'           => $ingredient,
	    'name'             => 'ingredient',
	    'id'               => '',
	    'class'            => 'form-control',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'ingredient',
	);
    wp_dropdown_categories( $ingredient_args );
	?>
    </div>
</div>
*/ ?>
<!-- break -->
 <?php /*?>
 <div class="col-md-3 col-sm-3 col-xs-6">
   <div class="form-group">
	<?php
	$difficulty_level_args = array(
		'show_option_none' => __( ' - Choose Difficulty - ', 'quickrecipe' ),
		'show_count'       => 0,
	    'option_none_value'  => '0',
	    'hide_empty'         => 0, 
		'orderby'          => 'name',
		'echo'             => 1,
	    'selected'           => $difficulty,
	    'name'             => 'difficulty_level',
	    'id'               => '',
	    'class'            => 'form-control',
	    'depth'            => 0,
	    'tab_index'        => 0,
	    'taxonomy'         => 'difficulty_level',
	);
    wp_dropdown_categories( $difficulty_level_args );
	?>
    </div>
    <div class="form-group">
    <input class="form-control" placeholder="<?php esc_html_e( 'Search by Keyword', 'quickrecipe' ) ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
    </div>
</div>     <?php */?>     
<?php 
} else {
?>
<?php /*?><div class="col-md-12 col-sm-12 col-xs-6">
    <div class="form-group">
    <input class="form-control" placeholder="<?php esc_html_e( 'Search by Keyword', 'quickrecipe' ) ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
    </div>
</div> <?php */?>
<?php
}
?>