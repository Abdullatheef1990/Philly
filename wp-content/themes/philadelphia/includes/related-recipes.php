<?php
global $post;

$recipe_type_terms = get_the_terms( $post->ID, 'recipe_type' );
$recipe_type_slugs = array();
if( $recipe_type_terms && !is_wp_error( $recipe_type_terms ))
{
	$recipe_type_slugs = array();
	foreach($recipe_type_terms as $term)
	{
		$recipe_type_slugs[] = $term->slug;
	}
    
}

$course_terms = get_the_terms( $post->ID, 'course' );
$course_slugs = array();
if( $course_terms && !is_wp_error( $course_terms ))
{
	$course_slugs = array();
	foreach($course_terms as $term)
	{
		$course_slugs[] = $term->slug;
	}
}


$cuisine_terms = get_the_terms( $post->ID, 'cuisine' );
$cuisine_slugs = array();
if( $cuisine_terms && !is_wp_error( $cuisine_terms ))
{
	$cuisine_slugs = array();
	foreach($cuisine_terms as $term)
	{
		$cuisine_slugs[] = $term->slug;
	}
}

$query_args = array(
					'post_type' => 'recipe',
					'posts_per_page' => 3,
					'post__not_in' => array($post->ID),
					'tax_query' => array(
										'relation' => 'OR',
										array(
											'taxonomy' => 'recipe_type',
											'field' => 'slug',
											'terms' => $recipe_type_slugs
										),
										array(
											'taxonomy' => 'course',
											'field' => 'slug',
											'terms' => $course_slugs
										),
										array(
											'taxonomy' => 'cuisine',
											'field' => 'slug',
											'terms' => $cuisine_slugs
										)
									),
					'meta_query' => array(
		                                    array(
			                                    'key'     => 'quickrecipe_editor_choice_recipe',
			                                    'value'   => 'true',
			                                    'compare' => 'NOT IN',
		                                    ),
											array(
												'key' => '_thumbnail_id',
												'value' => '0',
												'compare' => '>',
												'type' => 'NUMERIC'
											)
									)
					);

$the_query = new WP_Query($query_args);

if ( $the_query->have_posts() )
{
	?>
                        <h3 class="section-title"><?php esc_html_e( 'You may also like', 'quickrecipe' ); ?></h3>
                        <div class="row">
				          <?php
					        while ( $the_query->have_posts() ) :
						        $the_query->the_post();

                                $term_names = wp_get_post_terms($post->ID, 'course', array("fields" => "names"));
                                $term_list = array();
                                
                                if (is_array($term_names)) {
                                    foreach($term_names as $term_name) {
                                        $term_list[] = $term_name;
                                    }
                                    $term_name_list = join( ", ", $term_list );
                                } else {
                                    $term_name_list = '';
                                }

						  ?>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="item-container">
                              <div class="item-img-box">
						        <?php
						        if ( has_post_thumbnail( $post->ID ) ) {
							        echo get_the_post_thumbnail( $post->ID, array(400,400) );
						        }
						        ?>
                                <a href="<?php echo esc_url(get_permalink( $post->ID )); ?>"><div class="hover-button"><?php esc_html_e('View Recipe', 'quickrecipe'); ?></div></a>
                              </div>
                              <div class="item-meta-box">
                                <span><i class="fa fa-clock-o"></i> <?php $prep_time = intval( get_post_meta(get_the_ID(), 'quickrecipe_prep_time', true) ); echo quickrecipe_get_mins_to_hours($prep_time); ?></span>
                                <span><i class="fa fa-user"></i> <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true); if (!empty($servings)) { echo sprintf(__('%s People', 'quickrecipe'), esc_attr($servings)); } ?></span>
                                <span><i class="fa fa-spoon"></i> <?php $yield = get_post_meta($post->ID, 'quickrecipe_yield', true); if (!empty($servings)) { echo sprintf(__('%s Yield', 'quickrecipe'), esc_attr($yield)); } ?></span>
                              </div>
                              <div class="item-content-box">
                                <h3><a href="<?php the_permalink()?>"><?php the_title(); ?></a> <small><?php echo esc_html($term_name_list); ?></small></h3>
                                <div class="avatar">
                                    <div class="clearfix">
                                        <div class="pull-left">
                                            <?php echo get_avatar( get_the_author_meta( 'ID' ), '25' ); ?>
                                            <?php esc_html_e('By ', 'quickrecipe'); ?> <?php the_author_posts_link(); ?>
                                        </div>
                                        <span class="user-ratings pull-right">
                                            <?php echo average_rating(); ?>
                                        </span>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- break -->
                        <?php
					        endwhile;
				        ?>
                        </div>
<?php
}
wp_reset_postdata();
?>