<?php 
/*

* Template Name: List My Recipes AR

*/ 



$page_id = $post->ID;
if (!is_user_logged_in()) { auth_redirect(); }
get_header(); 

$page_by_title = get_page_by_title( get_the_title() );
$the_excerpt = (!empty($page_by_title->post_excerpt)) ? $page_by_title->post_excerpt : $page_by_title->post_content;

$slide_intro = get_field('vp_header_title');		
$slide_image = get_field('vp_header_image');
?>
   <div class="row full-width header-box">
	<div class="main-image" style="background-image: url(<?php echo $slide_image; ?>)">
		<div class="master-slider ms-skin-default" id="masterslider">
			<div class="ms-slide">
				<img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt=""/>
				<div class="row main-container align-center full-width ms-layer ms-caption">
					<div class="column large-6 column align-self-middle">
						<?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>
                        
 					<?php } ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>

   
    <!-- begin:content -->
    <div id="content" class="user-account-box row">
      <div class="columns full-width user-account-header">MY RECIPES
      <?php /*?><h2><?php the_title(); ?></h2>
					<p><?php echo esc_html($the_excerpt); ?></p><?php */?>
      </div>
      <div class="columns large-9 small-12 ">
      <form>
        <div class="row search-form full-width">

            

        </div>
        </form>
           <div class="row">
          		
				<div class="columns large-12 small-12 recipe-grid ">
				<div class="row load-more-wrapper">
				<?php
				if(is_user_logged_in())
				{
			        $query = new WP_Query(array('post_type' => 'recipe', 'posts_per_page' =>'-1', 'author' => get_current_user_id(), 'post_status' => array('publish', 'pending', 'draft', 'private', 'trash') ) );
			        if ($query->have_posts()) : 
                ?>
                 
                 
                  <?php while ($query->have_posts()) : $query->the_post(); ?>
                  <div class="columns large-6 small-12 recipes-grid-element load-more-article" style="background-image: url('<?php echo get_the_post_thumbnail_url( $post->ID); ?>');">
                    <div class="row full-width grid-inner">
                       
                        <div class="column large-6 small-6 transparent-block panel">

                            <h2><?php echo get_the_title(); ?></h2>

                            <a href="<?php echo esc_url( get_permalink() ); ?>" class="transparent-button">START COOKING</a>
                            <div class="member-controls">
                                <?php 
                                $submit_recipe_page_id = get_option('quickrecipe_submit_recipe_url', '');
                                $edit_post = add_query_arg('post', get_the_ID(), get_permalink($submit_recipe_page_id)); 
                            ?>
							<a href="<?php echo esc_url($edit_post); ?>" class="btn-success small"><i class="fa fa-pencil"></i> <?php esc_html_e( 'Edit', 'quickrecipe' ); ?></a>
                        <?php if( !(get_post_status() == 'trash') ) : ?>

						            <a onclick="return confirm('Are you sure you wish to delete post: <?php echo esc_attr(get_the_title()); ?>?')" href="<?php echo esc_url(get_delete_post_link( get_the_ID() )); ?>" class="btn-warning small"><i class="fa fa-close"></i> <?php esc_html_e( 'Delete', 'quickrecipe' ); ?></a>

					            <?php endif; ?>
							</div>
                        </div>
                    </div>

                </div>
                
                <?php endwhile; ?>
               
	            <?php else: ?>
	                <strong><?php esc_html_e( 'Sorry, no recipe available at this time.', 'quickrecipe' ); ?></strong>
	            <?php endif; ?>

				<?php } ?>
			   </div>
				</div>
				<!-- end:content -->
		
        </div>
      </div>
     	
          <!-- begin:sidebar -->
           <div class="columns large-3 small-12">
          <?php quickrecipe_get_user_sidebar($page_id); ?>
          </div>
          <!-- end:sidebar -->
    </div>
    <!-- end:content -->

<?php get_footer(); ?>