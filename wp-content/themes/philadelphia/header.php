 <?php
if ( ! defined( 'ABSPATH' ) ) exit;
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php if (ICL_LANGUAGE_CODE=='en') {echo 'en ltr'; } elseif (ICL_LANGUAGE_CODE=='ar'){	echo 'ar rtl'; } ?>">
<head>
	
	
	
<?php echo '<meta http-equiv="Content-Type" content="'. esc_attr( get_bloginfo( 'html_type' ) ) . '; charset=' . esc_attr( get_bloginfo( 'charset' ) ) . '" />' . "\n"; ?>
<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
<?php 
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
    $site_favicon = get_option( 'quickrecipe_favicon_image' );
    if(!empty($site_favicon)){
        echo '<link rel="shortcut icon" href="'.esc_url($site_favicon).'" />';
    }
}
?>
<?php wp_head(); ?>
</head>
<body id="top" <?php body_class(); ?>>
	
	<header>
		<!-- begin:topbar -->
		<?php if (ICL_LANGUAGE_CODE=='en') { ?>
   
    <div class="full-width top-nav-bar">
        <div class="row">
            <div class="column large-12 top-nav-bar-wrapper">
				<ul class="social-media-links hide-for-small-only">
               	 <?php quickrecipe_get_social_icons();  ?> </ul>

             
				<ul class="top-nav-links">
					   <li class="search-link hide-for-medium hide-for-large">
                        <a href="#"><i class="fa fa-search"></i> SEARCH</a>
                    </li>
                    <li class="search-link hide-for-small-only">
                       <form id="search_form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
    <label class="search-box">
		 
		<input type="search" class="form-control input-lg" id="searchDesktop" placeholder="<?php echo esc_attr_x( 'SEARCH', 'placeholder', 'quickrecipe' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
		<button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
    </label>
   
</form>
                    </li>
                    
               <?php if (is_user_logged_in()) { 
                $current_user = wp_get_current_user();
                    $my_profile_page_id = get_option('quickrecipe_edit_my_profile_url', '');
                    $my_profile_page_url = get_permalink($my_profile_page_id);
                ?>
                <!-- dev code start here -->
                <li><div class="dropdown">
                  <button class="dropbtn btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-user"></i> <?php echo esc_html($current_user->display_name); ?> <span class="caret"></span>
                    
                  </button>
                  <?php
                        $recipe_current_url = get_permalink( 103 );
                        $submit_recipe_url = get_permalink( 103 );

                        $tip_current_url = get_permalink( 1572 );
                        $submit_tip_url = get_permalink( 1572 );

                        $my_tip_current_url = get_permalink( 1576 );
                        $my_submit_tip_url =  get_permalink( 1576 );

                        $list_recipe_current_url = get_permalink( 101 );
                        $list_recipe_url = get_permalink( 101 );


                        $view_profile_current_url = get_permalink( 99 );
                        $view_profile_url = get_permalink( 99 );



                        ?>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                     <li class="menu-item" <?php echo esc_url($view_profile_current_url) == esc_url($view_profile_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($view_profile_url); ?>" title="<?php esc_attr_e('View Profile', 'quickrecipe'); ?>"><i class="fa fa-book"></i><?php esc_html_e('View Profile', 'quickrecipe'); ?></a></li>
                                 <li  class="menu-item" <?php echo esc_url($list_recipe_current_url) == esc_url($list_recipe_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($list_recipe_url); ?>" title="<?php esc_attr_e('Recipes I', 'quickrecipe'); ?>"><i class="fa fa-heart"></i><?php esc_html_e('Recipes I', 'quickrecipe'); ?><i class="fa fa-heart"></i></a>
                                </li>
                                <li  class="menu-item" <?php echo esc_url($list_recipe_current_url) == esc_url($submit_tip_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($submit_tip_url); ?>" title="<?php esc_attr_e('Tips I', 'quickrecipe'); ?>"><i class="fa fa-heart"></i><?php esc_html_e('Tips I', 'quickrecipe'); ?><i class="fa fa-heart"></i></a>
                                </li>
                                <li  class="menu-item" <?php echo esc_url($recipe_current_url) == esc_url($submit_recipe_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($submit_recipe_url); ?>" title="<?php esc_attr_e('My Recipe', 'quickrecipe'); ?>"><i class="fa fa-pencil"></i><?php esc_html_e('My Recipe', 'quickrecipe'); ?></a>
                                </li>
                                <li  class="menu-item" <?php echo esc_url($my_tip_current_url) == esc_url($my_submit_tip_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($my_submit_tip_url); ?>" title="<?php esc_attr_e('My Tip', 'quickrecipe'); ?>"><i class="fa fa-pencil"></i><?php esc_html_e('My Tip', 'quickrecipe'); ?></a>
                                </li>

                                 <li class="menu-item"><a href="<?php echo wp_logout_url(); ?>" title="<?php esc_attr_e('Logout', 'quickrecipe'); ?>"><i class="fa fa-sign-out"></i><?php esc_html_e('  Logout', 'quickrecipe'); ?></a></li>
                               
                            
                  </ul>
                </div></li>
    	               
                <!-- dev code end here -->
    	                <li><a href="<?php echo wp_logout_url( esc_url( home_url( '/' ) ) ); ?>"><i class="fa fa-sign-out"></i> <?php esc_html_e('Logout', 'quickrecipe'); ?></a></li>
                <?php } else { get_template_part('ajax', 'auth'); ?>            	
                        <li ><a class="login_button" id="show_login" href="<?php echo esc_url( wp_login_url( home_url( '/' ) ) ); ?>" alt="<?php esc_attr_e( 'Login', 'quickrecipe' ); ?>"><i class="fa fa-key"></i> <?php esc_html_e('Log In', 'quickrecipe'); ?></a></li>
                        <li><a class="login_button" id="show_signup" href="<?php echo esc_url( wp_registration_url() ); ?>"><i class="fa fa-lock"></i> <?php esc_html_e('Register', 'quickrecipe'); ?></a></li>
                <?php } ?>
                
					<li class="language-selector">
                        <?php do_action('wpml_add_language_selector'); ?>
                    </li>
            </ul>
            </div>
            <div class="column small-12 mobile-search-bar-wrapper hide-for-medium hide-for-large">
                  <form id="search_form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
               <label class="search-box">   
		 
		<input type="search" class="form-control input-lg" id="searchDesktop" placeholder="<?php echo esc_attr_x( 'SEARCH', 'placeholder', 'quickrecipe' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
		<button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
						<span class="close">×</span>
    </label>
    <input type="hidden" name="post_type" value="post">
               <input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
                </form>

            </div>
        </div>
    </div>
    

 <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
<div class="full-width top-nav-bar">
        <div class="row">
            <div class="column large-12 top-nav-bar-wrapper">
				<ul class="social-media-links hide-for-small-only">
               	 <?php quickrecipe_get_social_icons();  ?> </ul>

             
				<ul class="top-nav-links">
					   <li class="search-link hide-for-medium hide-for-large">
                        <a href="#"><i class="fa fa-search"></i> البحث</a>
                    </li>
                    <li class="search-link hide-for-small-only">
                       <form id="search_form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
    <label class="search-box">
		 
		<input type="search" class="form-control input-lg" id="searchDesktop" placeholder="<?php echo esc_attr_x( 'البحث', 'placeholder', 'quickrecipe' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
		<button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
    </label>
   
</form>
                    </li>
                   
                <?php if (is_user_logged_in()) { 
                    $my_profile_page_id = get_option('quickrecipe_edit_my_profile_url', '');
                    $my_profile_page_url = get_permalink($my_profile_page_id);
                ?>
				  		 <!-- dev code start here -->
                <li><div class="dropdown">
                  <button class="dropbtn btn btn-default dropdown-toggle" type="button" id="dropdownMenu-ar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-user"></i> ملفي-الشخصي <span class="caret"></span>
                    
                  </button>
                  <?php
                        $recipe_current_url = get_permalink( 103 );
                        $submit_recipe_url = get_permalink( 103 );

                        $tip_current_url = get_permalink( 1572 );
                        $submit_tip_url = get_permalink( 1588 );

                        $my_tip_current_url = get_permalink( 1576 );
                        $my_submit_tip_url =  get_permalink( 1590 );

                        $list_recipe_current_url = get_permalink( 101 );
                        $list_recipe_url = get_permalink( 101 );


                        $view_profile_current_url = get_permalink( 99 );
                        $view_profile_url = get_permalink( 99 );



                        ?>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu-ar">
                     <li><a href="<?php echo esc_url($my_profile_page_url); ?>"><i class="fa fa-user"></i> <?php esc_html_e('ملفي-الشخصي', 'quickrecipe'); ?></a></li>
                                 <li  class="menu-item" <?php echo esc_url($list_recipe_current_url) == esc_url($list_recipe_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($list_recipe_url); ?>" title="<?php esc_attr_e('وصفتي-المفضلة', 'quickrecipe'); ?>"><i class="fa fa-heart"></i><?php esc_html_e('وصفتي-المفضلة', 'quickrecipe'); ?><i class="fa fa-heart"></i></a>
                                </li>
                                <li  class="menu-item" <?php echo esc_url($list_recipe_current_url) == esc_url($submit_tip_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($submit_tip_url); ?>" title="<?php esc_attr_e('نصائح المفضلة', 'quickrecipe'); ?>"><i class="fa fa-heart"></i><?php esc_html_e('نصائح المفضلة', 'quickrecipe'); ?><i class="fa fa-heart"></i></a>
                                </li>
                                <li  class="menu-item" <?php echo esc_url($recipe_current_url) == esc_url($submit_recipe_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($submit_recipe_url); ?>" title="<?php esc_attr_e('وصفاتي', 'quickrecipe'); ?>"><i class="fa fa-pencil"></i><?php esc_html_e('وصفاتي', 'quickrecipe'); ?></a>
                                </li>
                                <li  class="menu-item" <?php echo esc_url($my_tip_current_url) == esc_url($my_submit_tip_url) ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo esc_url($my_submit_tip_url); ?>" title="<?php esc_attr_e('نصائحي', 'quickrecipe'); ?>"><i class="fa fa-pencil"></i><?php esc_html_e('نصائحي', 'quickrecipe'); ?></a>
                                </li>

                                 <li class="menu-item"><a href="<?php echo wp_logout_url(); ?>" title="<?php esc_attr_e('الخروج', 'quickrecipe'); ?>"><i class="fa fa-sign-out"></i><?php esc_html_e('الخروج', 'quickrecipe'); ?></a></li>
                               
                            
                  </ul>
                </div></li>
                       
                <!-- dev code end here -->
    	                <li><a href="<?php echo wp_logout_url( esc_url( home_url( '/?lang=ar' ) ) ); ?>"><i class="fa fa-sign-out"></i> <?php esc_html_e('تسجيل الخروج', 'quickrecipe'); ?></a></li>
                <?php } else { get_template_part('ajax', 'auth'); ?>            	
                        <li ><a class="login_button" id="show_login" href="<?php echo esc_url( wp_login_url( home_url( '/' ) ) ); ?>" alt="<?php esc_attr_e( 'تسجيل الدخول', 'quickrecipe' ); ?>"><i class="fa fa-key"></i> <?php esc_html_e('تسجيل الدخول', 'quickrecipe'); ?></a></li>
                        <li><a class="login_button" id="show_signup" href="<?php echo esc_url( wp_registration_url() ); ?>"><i class="fa fa-lock"></i> <?php esc_html_e('التسجيل', 'quickrecipe'); ?></a></li>
				<?php }; ?>
					<li class="language-selector">
                        <?php do_action('wpml_add_language_selector'); ?>
                    </li>
            </ul>
            </div>
            <div class="column small-12 mobile-search-bar-wrapper hide-for-medium hide-for-large">
                  <form id="search_form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
               <label class="search-box">   
		 
		<input type="search" class="form-control input-lg" id="searchDesktop" placeholder="<?php echo esc_attr_x( 'البحث', 'placeholder', 'quickrecipe' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
		<button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
						<span class="close">×</span>
    </label>
    <input type="hidden" name="post_type" value="post">
               <input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
                </form>

            </div>
        </div>
    </div>
<?php }; ?>
 <!-- end:topbar -->
	 <!-- begin:navbar -->
			
    <div class="row main-nav-bar">

        

        <a class="mobile-menu-icon hide-for-medium hide-for-large">
            <div id="navButton" class="mobile-menu-button">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>
        </a>


        

        <div class="column large-2 medium-2 logo-wrapper">
            <div class="logo">
                <?php
           $logo = get_option('quickrecipe_logo_image','');
				
           $logo = ( isset( $logo ) && $logo != '' ) ? $logo : esc_url( get_template_directory_uri() . '/img/logo.png' );

	       if ( is_ssl() ) { 
               $logo = str_replace( 'http://', 'https://', $logo ); 
           }
           ?>
            <a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'description' ) ); ?>">
		       <img src="<?php echo esc_url( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
	       </a>
            </div>
        </div>

        <div class="column large-10 medium-10 main-menu-wrapper">

            <div class="column large-12 main-nav-bar">

	<div class="main-nav">

					 <?php
			wp_nav_menu(array(
				'theme_location' => 'primary-menu',
				'items_wrap'     => '<ul id="%1$s" class="main-nav-links ">%3$s</ul>',
				'fallback_cb'	 => 'quickrecipe_menu_message'
			));

            ?>
                  
				</div>
            </div>
        </div>
    </div>
     <!-- end:navbar -->
     
</header>

	
	
    

   
   
   
<?php
if ( is_archive() ) {
    // Banner Title
    $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

    if (!is_object($current_term)) {
        $quickrecipe_title = esc_html__( 'Archive', 'quickrecipe' );;
        $quickrecipe_image_url = quickrecipe_get_default_banner();
    } else {
        $quickrecipe_title = (!empty($current_term->name)) ? $current_term->name : esc_html__('All Recipes', 'quickrecipe');

        // Banner Image
        $term_meta = get_option( "taxonomy_$current_term->term_id" );
        if ( !empty($term_meta['custom_term_meta']) )
        {
            $quickrecipe_image_url = $term_meta['custom_term_meta'];
        }
        else
        {
            $quickrecipe_image_url = quickrecipe_get_default_banner();
        }
    }
} else {
    $quickrecipe_title = get_the_title();
    $quickrecipe_thumbnail_id = get_post_thumbnail_id();
    if (!empty($quickrecipe_thumbnail_id)) {
        $quickrecipe_image_url = wp_get_attachment_image_src($quickrecipe_thumbnail_id, 'large');
        $quickrecipe_image_url = $quickrecipe_image_url[0];
    } else {
        $quickrecipe_image_url = get_template_directory_uri() . '/img/img01.jpg';
    }
}
/*
if (!is_single() && !is_search() && !is_front_page()) {
?>
    <!-- begin:header -->
    <div id="header" class="page-heading" style="background-image: url(<?php echo esc_url($quickrecipe_image_url); ?>);">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 col-sm-12">
            <div class="page-title">
		        <?php if ( is_404() ) : ?>
			        <h2><?php esc_html_e( '404 Error!', 'quickrecipe' ); ?></h2>
		        <?php elseif ( is_home() ) : ?>
			        <h2><?php esc_html_e( 'Home Page', 'quickrecipe' ); ?></h2>
		        <?php elseif ( is_author() ) : ?>
			        <?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>
			        <h2><?php esc_html_e( 'Author : ', 'quickrecipe' ); ?><span><?php echo esc_html($curauth->display_name); ?></span></h2>
		        <?php elseif ( is_category() ) : ?>
			        <h2><?php esc_html_e( 'Category : ', 'quickrecipe' ); ?><span><?php echo esc_html(single_cat_title( '', false )); ?></span></h2>
		        <?php elseif ( is_tag() ) : ?>
			        <h2><?php esc_html_e( 'Tag : ', 'quickrecipe' ); ?><span><?php echo esc_html(single_cat_title( '', false )); ?></span></h2>
		        <?php else : ?>
			        <h2><?php echo esc_html($quickrecipe_title); ?></h2>
		        <?php endif; ?>
            </div>
            <?php quickrecipe_breadcrumbs(); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- end:header -->
<?php
    }
	*/
?>