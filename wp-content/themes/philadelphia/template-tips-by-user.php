<?php
/*
* Template Name: Tips-By-User
*/
// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header();
$slide_intro = get_field('page_header_title');      
$slide_image = get_field('page_header_image');
$slide_video = get_field('page_header_yt_video_id');
?>  
<div class="row full-width header-box">     
    <div class="main-image">    
        <div class="master-slider ms-skin-default" id="recipe-home-masterslider">
            <div class="ms-slide">
                <img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt="<?php echo $slide_intro; ?>"/>
                <div class="row main-container align-center full-width ms-layer ms-caption">
                    <div class="column large-6 column align-self-middle">
                        <?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>
                        <?php } ?>
                    </div>
                </div>
                <?php if ($slide_video!=''){ ?>
                <iframe class="yt-video" src="https://www.youtube.com/embed/<?php echo $slide_video; ?>?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=<?php echo $slide_video; ?>&modestbranding=1" frameborder="0" allowfullscreen></iframe>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- begin:header -->
<div class="row">
    <div class="recipe-search-box small-12 columns align-center">
        <?php get_template_part( 'includes/philly-tips/search-area-by-user' ); ?>
    </div>   
    <div class="columns large-12 recipes-headline">
        <?php if (ICL_LANGUAGE_CODE=='en') { 
            the_field('user_tips_title', 1570); 
        } elseif (ICL_LANGUAGE_CODE=='ar'){ 
            the_field('user_tips_title', 556); 
        } ?>
    </div>
</div>  
<div class="row recipes-grid all-recipes load-more-wrapper">
    <?php 
    get_template_part( 'includes/philly-tips/latest-philly-tips-by-user' ); 
    ?>
</div>     
<?php
get_footer(); 
?>