<?php
if ( ! defined( 'ABSPATH' ) ) exit;

// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header();
if ( have_posts() ) { $count = 0;
    while ( have_posts() ) { the_post(); $count++;
        $cover_image = get_post_meta($post->ID, 'quickrecipe_cover_image', true);
        if (!empty($cover_image)) {
            $cover_image_url = wp_get_attachment_image_src( $cover_image, 'large');
        }
        else {
            $cover_image_url[0] = get_template_directory_uri() . '/img/img01.jpg';   
        }
        $is_tips = ( is_singular( 'tips' ) ) ? true : false;
        ?>
        <!-- begin:header -->
        <?php
        $image = get_field('tip_photo');
        if($image == "") {
            $image = get_template_directory_uri() . '/img/img01.jpg';
        }
        ?>
        <div class="row full-width header-box">
            <div class="main-image" style="background-image: url('<?php echo $image; ?>');">
                <div class="row main-container align-center full-width">
                    <div class="column large-6 column align-self-middle">
                        <h1 class="heading"><?php the_title(); ?></h1>
                        <?php if( get_author_role()=='editor' || get_author_role()=='administrator' ) { } else{ ?> 
                            <p class="recipe-date"><?php the_date(); ?></p>
                            <?php /*?><div class="recipe-submitted-img">
                            <img src=""/>
                            </div><?php */?>
                            <p class="submitted-by">By <?php the_author(); ?> </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:header -->
        <!-- begin:content -->
        <div class="row recipe-video align-center">
            <div class="columns large-10 small-12">
                <div id="myCarousel-tips" class="carousel slide" data-ride="carousel">
                     <ol class="carousel-indicators">
                        <?php 
                        $thumbnail = get_the_post_thumbnail_url();
                        $embedCode = get_field('tip_video'); 
                        $sliders = array(
                            'image' => $thumbnail,
                            'video' => $embedCode
                            );
                        $sliderCount = count(array_filter($sliders));
                        for ($i=0; $i < $sliderCount; $i++) { 
                            if($i == 0) { $activeClass = "active"; } else { $activeClass = ""; } ?>
                            <li data-target="#myCarousel-tips" data-slide-to="<?php echo $i; ?>" class="<?php echo $activeClass; ?>"></li>
                        <?php } ?>
                    </ol> 
                    <div class="carousel-inner">
                        <?php 
                        $j = 0;
                        foreach (array_filter($sliders) as $key => $value) {
                            $j++;
                            if($j == 1) { $class = "active"; } else { $class = ""; } ?>
                            
                                <?php if($key == 'video') { ?>
                                <div class="item">
                                    <iframe src="http://www.youtube.com/embed/<?php echo $value; ?>?controls=0" frameborder="0" allowfullscreen=""></iframe>
                                </div>    
                                <?php } else { ?>
                                <div class="item" style="background-image: url(<?php echo $value; ?>)">
                                </div>
                                <?php } ?>
                            
                        <?php } ?>
                    </div> 
                    <!-- Wrapper for slides -->
                    <!-- Left and right controls -->
                    <!-- <a class="left carousel-control" href="#myCarousel-tips" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel-tips" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a> -->
                </div>
            </div>
            <div class="columns large-4 small-12 recipe-servings-box">
                <div class="recipe-servings-wrapper">
                    <?php $servings = get_post_meta($post->ID, 'quickrecipe_servings', true);
		             if (!empty($servings)) {			
				    ?>
    			        <div class="recipe-servings">	
    			            <?php for ( $count=1; $count<=sprintf(wp_kses(__('%s', 'quickrecipe'), ''), esc_attr($servings)); $count++) { ?>
                                <div class="recipe-serving"></div>
                            <?php } ?>
                        </div>      
                        <div class="recipe-servings-caption"> 
                            <?php if (ICL_LANGUAGE_CODE=='en') { ?>
                            <?php echo sprintf(wp_kses(__('Serves %s', 'quickrecipe'), ''), esc_attr($servings)); ?>
                            <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
                                <?php echo sprintf(wp_kses(__('المحتوى %s', 'quickrecipe'), ''), esc_attr($servings)); ?>
                            <?php } ?>                    
                        </div>            
                    <?php } ?>	
                </div>
            </div>  
            <div class="columns large-6 small-12 recipe-buttons-box">
                <?php 
                $postURL = urlencode(get_permalink());
                $postTitle = str_replace( ' ', '%20', get_the_title());
                $mailto = "mailto:?subject=".str_replace( ' ', '%20', get_bloginfo('name')).'-'.$postTitle."&amp;body=".$postTitle.":".$postURL; 
                ?>             
                <ul class="recipe-video-buttons">
                    <li>
                        <?php if ( $is_tips ) echo quickrecipe_get_favorite_button(); ?>
		            </li>
                    <li>
                        <a href="#popup-social<?php echo esc_attr($post->ID); ?>" class="share recipe-video-btn">
                        <i class="fa fa-share-square-o"></i> 
                        <?php if (ICL_LANGUAGE_CODE=='en') { 
               					esc_html_e( 'SHARE', 'quickrecipe' );  
						} 
						elseif (ICL_LANGUAGE_CODE=='ar'){ 
							 	esc_html_e( 'حقيقة', 'quickrecipe' ); 
						} ?></a>
                        <?php get_template_part( 'includes/html/popup-social' ); ?>
                    </li>
                    <li>
                        <a href="<?php echo esc_url($mailto); ?>" class="email recipe-video-btn"><i class="fa fa-envelope"></i> <?php if (ICL_LANGUAGE_CODE=='en') { 
               					esc_html_e( 'E-Mail', 'quickrecipe' );  
						} 
						 elseif (ICL_LANGUAGE_CODE=='ar'){ 
							 	esc_html_e( 'حقيقة', 'quickrecipe' ); 
						} ?></a>
                    </li>
                    <li>
               	        <a id="printcontent" rel="nofollow" class="print print-friendly recipe-video-btn" href="javascript:void(0);"><i class="fa fa-print"></i> <?php if (ICL_LANGUAGE_CODE=='en') { 
               					esc_html_e( 'PRINT', 'quickrecipe' );  
						} 
						elseif (ICL_LANGUAGE_CODE=='ar'){ 
							 	esc_html_e( 'حقيقة', 'quickrecipe' ); 
						} ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <?php /******* RECIPE DECSRIPTION  **********/?>
        <div class="row align-center">
            <div class="columns large-10 small-12 printable">
                <?php 
                if ( $is_tips ) {
                ?>
                
                <!--  <div class="recipe-title"> -->
    			<!-- <img src="<?php echo get_template_directory_uri(); ?>/img/desc-icon.png" class="title_icon"/> -->
                <?php /*?><?php if (ICL_LANGUAGE_CODE=='en') { 
       					esc_html_e( 'Recipe Description', 'quickrecipe' );  
    			} 
    			 elseif (ICL_LANGUAGE_CODE=='ar'){ 
    				 	esc_html_e( 'حقيقة مثبتة منذ زمن', 'quickrecipe' ); 
    			} ?><?php */?>
    		 	<?php //the_title(); ?>
		        <!-- </div> -->
                <?php } ?>
                <div class="recipe-title">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/desc-icon.png" class="title_icon"/>
                    <?php the_title(); ?>
                </div>
                <div class="recipe-description">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <?php /******* INGREDIENTS/INSTRUCTION  **********/?>
        <div class="row align-center">
            <!-- <div class="columns large-4 small-12 recipe-ingredients">
                <h2><img src="<?php echo get_template_directory_uri(); ?>/img/ingredients-icon.png" class="title_icon"/> <?php if (ICL_LANGUAGE_CODE=='en') { 
                       					esc_html_e( 'INGREDIENTS', 'quickrecipe' );
        						} 
        						 elseif (ICL_LANGUAGE_CODE=='ar'){ 
        							 	esc_html_e( 'حقيقة مثبتة منذ زمن', 'quickrecipe' ); 
        						} ?></h2>
                 <?php echo quickrecipe_list_ingredients(); ?>
            </div> -->
            <div class="columns large-10 small-12 printable recipe-instruction">
                <h2><img src="<?php echo get_template_directory_uri(); ?>/img/instructions-icon.png" class="title_icon"/>
                <?php if (ICL_LANGUAGE_CODE=='en') { 
       					esc_html_e( 'INSTRUCTION', 'quickrecipe' ); 
				} 
				 elseif (ICL_LANGUAGE_CODE=='ar'){ 
					 	esc_html_e( 'حقيقة مثبتة منذ زمن', 'quickrecipe' ); 
				} ?></h2>
                <!-- <?php echo quickrecipe_list_method_steps(); ?> -->
                <?php
                // check if the repeater field has rows of data
                if( have_rows('tip_steps') ): ?>
                    <ol>
                        <?php while ( have_rows('tip_steps') ) : the_row(); ?>
                            <li><?php the_sub_field('steps'); ?></li>
                        <?php endwhile; ?>
                    </ol>
                <?php else : endif; ?>
            </div>
        </div>
        <?php /******* NEXT PREVIOS RECIPES **********/?>
        <div class="row recipe-grid prev-next-recipes align-center hide-for-small-only">
            <?php
			$prev_post = get_previous_post();
            $prev_label = ( $is_tips ) ? esc_html__('Previous:', 'quickrecipe') : esc_html__('Previous Article:', 'quickrecipe');
					
			if (!empty( $prev_post )): ?>
                <div class="columns large-6 small-12 recipes-grid-element"
             style="background-image: url('<?php echo get_the_post_thumbnail_url($prev_post->ID); ?>')">
                    <div class="row full-width grid-inner">
                        <div class="column large-6 small-6 transparent-block panel">
                            <h2><?php echo esc_attr( get_the_title($prev_post)  ); ?></h2>
                            <a href="<?php echo esc_url(get_permalink( $prev_post->ID )); ?>" class="transparent-button">PREVIOUS</a>
                        </div>
                        <div class="column large-6 small-6 panel"></div>
                    </div>
                </div>
            <?php endif; ?>
            <?php
			$next_post = get_next_post();
            $next_label = ( $is_tips ) ? esc_html__('Next:', 'quickrecipe') : esc_html__('Next Article:', 'quickrecipe');
			if (!empty( $next_post )): ?>	
                <div class="columns large-6 small-12 recipes-grid-element"
                     style="background-image: url('<?php echo get_the_post_thumbnail_url($prev_post->ID); ?>')">
                    <div class="row full-width grid-inner">
                        <div class="column large-6 small-6 panel"></div>
                        <div class="column large-6 small-6 transparent-block panel">
                            <h2><?php echo esc_attr( get_the_title($next_post)  ); ?></h2>
                            <a href="<?php echo esc_url(get_permalink( $next_post->ID )); ?>" class="transparent-button">NEXT</a>
            			</div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="row recipe-grid prev-next-recipes align-center hide-for-medium hide-for-large">
            <?php
			$prev_post = get_previous_post();
            $prev_label = ( $is_tips ) ? esc_html__('Previous:', 'quickrecipe') : esc_html__('Previous Article:', 'quickrecipe');
					
			if (!empty( $prev_post )): ?>
                <div class="columns small-6 prev-btn">
                    <a href="">PREVIOUS RECIPE</a>
                </div>
            <?php endif; ?>
            <?php
			$next_post = get_next_post();
            $next_label = ( $is_tips ) ? esc_html__('Next:', 'quickrecipe') : esc_html__('Next Article:', 'quickrecipe');
			if (!empty( $next_post )): ?>	
                <div class="columns small-6 next-btn">
                    <a href="">NEXT RECIPE</a>
                </div>
            <?php endif; ?>
        </div>
        <?php /******* COMMENTS **********/?>
        <?php comments_template(); ?>
        <?php /******* YOU MAY ALSO LIKE **********/?>
        <?php   get_template_part('includes/recipepage/related-recipes'); ?>
        <?php /******* BE OUR CHEF **********/?>   
        <?php
        if (ICL_LANGUAGE_CODE=='ar') {
		    $be_chef_title = get_field('be_chef_title', 556);
			$be_chef_intro = get_field('be_chef_intro', 556);
            $be_chef_link_title = get_field('be_chef_link_title', 556);
            $be_chef_link_url = get_field('be_chef_link_url', 556);
			$be_chef_image = get_field('be_chef_image', 556);
        } elseif (ICL_LANGUAGE_CODE=='en'){
			$be_chef_title = get_field('be_chef_title', 315);
			$be_chef_intro = get_field('be_chef_intro', 315);
            $be_chef_link_title = get_field('be_chef_link_title', 315);
            $be_chef_link_url = get_field('be_chef_link_url', 315);
			$be_chef_image = get_field('be_chef_image', 315);
	    }
		if (!empty( $be_chef_title )){ ?>
            <!-- <div class="row full-width recipe-grid be-our-chef">
                <div class="columns large-12 small-12 recipes-grid-element"
            style="background-image: url(' <?php echo $be_chef_image ?>');">
                    <div class="row full-width grid-inner">
                        <div class="column large-6 small-6 panel"></div>
                        <div class="column large-6 small-6 transparent-block panel">
                            <div class="column large-7">
                                <h2><?php echo $be_chef_title ?></h2>
                                <p> <?php echo $be_chef_intro ?></p>
                                <?php if (!empty( $be_chef_link_title )){ ?> 
                                <a href="<?php echo $be_chef_link_url ?>" class="transparent-button"><?php echo $be_chef_link_title ?></a> 
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        <?php } ?>
    <?php } ?> 
<?php }?>
<?php // get_sidebar(); ?>
<script type="text/javascript">
    jQuery('#myCarousel-tips .item:first-child').addClass('active');
</script>
<?php get_footer(); ?>
