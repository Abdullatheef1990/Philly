<?php
//Set content width
$content_width=1140;

include( get_template_directory() . '/includes/register-meta-boxes.php');
include( get_template_directory() . '/includes/theme-options.php');
include( get_template_directory() . '/includes/favorite-recipes.php');
include( get_template_directory() . '/includes/comment-review.php');
include( get_template_directory() . '/includes/breadcrumb.php');
include( get_template_directory() . '/includes/required-plugin.php');

if ( function_exists( 'wp_nav_menu') ) {
	add_theme_support( 'nav-menus' );
	register_nav_menus( array( 'primary-menu' => __( 'Primary Menu', 'quickrecipe' ) ) );
	register_nav_menus( array( 'top-menu' => __( 'Top Menu', 'quickrecipe' ) ) );
}

/*
Register Fonts
*/
function quickrecipe_fonts_url() {
    $font_url = '';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'quickrecipe' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Lora:400,400i,700,700i|Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i|PT Serif:400,400i,700,700i&subset=latin-ext' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}
    
function quickrecipe_enqeue_scripts() 
{
	wp_enqueue_script('validation', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), null, true);	
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), null, true);	
	wp_enqueue_script('bootstrap-filestyle', get_template_directory_uri() . '/js/bootstrap-filestyle.min.js', array('jquery'), null, true);
	wp_enqueue_script('html5shiv', get_template_directory_uri() . '/js/html5shiv.js', array('jquery'), null, true);
	wp_enqueue_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array('jquery'), null, true);	
    wp_enqueue_script('easing', get_template_directory_uri() . '/js/jquery.easing.js', array('jquery'), null, true);
	wp_enqueue_script('jcarousel', get_template_directory_uri() . '/js/jquery.jcarousel.min.js', array('jquery'), null, true);	
	wp_enqueue_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), null, true);	
	wp_enqueue_script('quickrecipe-script', get_template_directory_uri() . '/js/script.js', array('jquery'), null, true);	
	wp_enqueue_script('owlcarousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), null, true);
    wp_enqueue_script('fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), null, true);
	wp_enqueue_script('foundation', get_template_directory_uri() . '/js/vendor/foundation.js', array('jquery'), null, true);
	wp_enqueue_script('master slider', get_template_directory_uri() . '/js/vendor/masterslider.min.js', array('jquery'), null, true);
	wp_enqueue_script('all', get_template_directory_uri() . '/js/all.js', array('jquery'), null, true);
	
    if ( is_page( 'contact' ) ) {    
	  //  wp_enqueue_script('quickrecipe-gmap3', get_template_directory_uri() . '/js/gmap3.min.js', array('jquery'), null, true);
	  //  wp_enqueue_script('quickrecipe-google-maps', '//maps.google.com/maps/api/js?sensor=false', array('jquery'), null, true);
	    wp_enqueue_script('quickrecipe-contact', get_template_directory_uri() . '/js/contact.js', array('jquery'), null, true);
    }

	if ( !is_admin() ) {
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), null, 'all' );
		wp_enqueue_style( 'quickrecipe-responsive', get_template_directory_uri() . '/css/responsive.css' );
		wp_enqueue_style( 'flaticon', get_template_directory_uri() . '/css/flaticon.css' );
        wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css' );
	    wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css' );
	    wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/css/owl.theme.css' );
        wp_enqueue_style( 'quickrecipe-star-rating', get_template_directory_uri() . '/css/star-rating.css' );
	    wp_enqueue_style( 'quickrecipe-login-user', get_template_directory_uri() . '/css/login.css' );
        wp_enqueue_style( 'quickrecipe-fonts', quickrecipe_fonts_url(), array(), '1.0.0' );
		wp_enqueue_style( 'fondation', get_template_directory_uri() . '/css/vendor/foundation-flex.min.css' );
		wp_enqueue_style( 'master slider', get_template_directory_uri() . '/css/vendor/masterslider/masterslider.css' );
	    wp_enqueue_style( 'master slider skin', get_template_directory_uri() . '/css/vendor/masterslider/skins/default/style.css' );
		//wp_enqueue_style( 'styles', get_template_directory_uri() . '/css/style-en.css' );
		//wp_enqueue_style( 'custom en styles', get_template_directory_uri() . '/css/custom-en.css' );
		//wp_enqueue_style( 'quickrecipe-style', get_stylesheet_uri() );
		
		if (ICL_LANGUAGE_CODE=='ar') { 
			 wp_enqueue_style( 'styles', get_template_directory_uri() . '/css/style-ar.css' );
		wp_enqueue_style( 'custom ar styles', get_template_directory_uri() . '/css/custom-ar.css' );
  
  		}else if (ICL_LANGUAGE_CODE=='en') { 
		wp_enqueue_style( 'styles', get_template_directory_uri() . '/css/style-en.css' );
		wp_enqueue_style( 'custom en styles', get_template_directory_uri() . '/css/custom-en.css' );
		}
		 
	}
}
add_action('wp_enqueue_scripts', 'quickrecipe_enqeue_scripts');

add_action('after_setup_theme', 'quickrecipe_init');
if( !function_exists('quickrecipe_init') ):
    function quickrecipe_init() {
        add_theme_support( 'title-tag' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
        add_theme_support( 'custom-background' );
        add_editor_style('/css/editor-style.css');

        $defaults = array(
            'default-image'          => '',
            'random-default'         => false,
            'width'                  => 0,
            'height'                 => 0,
            'flex-height'            => false,
            'flex-width'             => false,
            'default-text-color'     => '',
            'header-text'            => true,
            'uploads'                => true,
            'wp-head-callback'       => '',
            'admin-head-callback'    => '',
            'admin-preview-callback' => '',
        );
        add_theme_support( 'custom-header', $defaults );
        /*	Load Text Domain */
        load_theme_textdomain('quickrecipe', get_template_directory() . '/languages');
    }
endif;

function quickrecipe_viewport_meta(){
    ?> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <?php
}

add_filter('wp_head', 'quickrecipe_viewport_meta');

function quickrecipe_wp_admin_style() {
    wp_register_style( 'wp_admin_css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
    wp_enqueue_style( 'wp_admin_css' );
    wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css' );
	wp_enqueue_style( 'font-awesome' );

    wp_enqueue_script('admin', get_template_directory_uri().'/js/admin.js',array('jquery'), '1.0', true); 
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');

}
add_action( 'admin_enqueue_scripts', 'quickrecipe_wp_admin_style' );

if (is_admin()) {
    add_action('admin_menu', 'quickrecipe_admin_menu');
}

if( !function_exists('quickrecipe_admin_menu') ):

    function quickrecipe_admin_menu() {
        add_theme_page('Quick Recipe Theme Options', __('Theme Options', 'quickrecipe'), 'administrator', 'includes/theme-options.php', 'quickrecipe_update_theme_option' );
    }
    
endif;


if(!function_exists('quickrecipe_insert_attachment')){
    function quickrecipe_insert_attachment($file_handler,$post_id,$setthumb = false )
    {
        // check to make sure its a successful upload
        if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        $attach_id = media_handle_upload( $file_handler, $post_id );

        if ($setthumb) update_post_meta($post_id,'_thumbnail_id',$attach_id);

        return $attach_id;
    }
}

if(!function_exists('quickrecipe_list_ingredients')){
    function quickrecipe_list_ingredients() {
        global $post;

        $ingredients = get_post_meta($post->ID, 'quickrecipe_ingredients', true);
        if (empty($ingredients)) 
            $count = 0;
        else
            $count = count($ingredients); 
        
        $html = '';

        if( $count > 0 )
        {
            $html .= '<ul class="ingredient-check">';
            foreach($ingredients as $ingredient){
                $ingredient_id = '$ingredient_'.$count++;
                $html .= '<li>';
                $html .= '  <span>';
                $html .= '      <input type="checkbox" value="">';
                $html .= '  </span>'.$ingredient;
                $html .= '</li>';
            }

            $html .= '</ul>';
        }
        else
        {
            $html .= '<p>';
            $html .= esc_html__('Ingredients is not available.','quickrecipe');
            $html .= '</p>';
        }

        return $html;
    }
}

if(!function_exists('quickrecipe_list_method_steps')){
    function quickrecipe_list_method_steps() {

        global $post;

        $method_steps = get_post_meta($post->ID, 'quickrecipe_method_steps', true);

        if (empty($method_steps)) 
            $count = 0;
        else
            $count = count($method_steps);

        if( $count >= 1 )
        {
            ?>
            <ol>
            <?php
            foreach($method_steps as $step){
            ?>
                <li><?php echo esc_html($step); ?></li>
            <?php
            }
            ?>
            </ol>
            <?php
        } else {
        ?>
            <p><?php esc_html_e('Steps is not available.','quickrecipe'); ?></p>
        <?php
        }
    }
}

if(!function_exists('quickrecipe_list_nutritions')){
    function quickrecipe_list_nutritions() {
        global $post;

        $nutritions = get_post_meta($post->ID, 'quickrecipe_nutrition', true);

        if( count($nutritions) && !empty($nutritions) )
        {
	        ?>
	        <div class="nutritional">
			        <ul>
			        <?php
                    foreach($nutritions as $nutrition){
                        $nutrition_item = explode(":", $nutrition);
					        ?>
						        <li class="nutrition">
								        <?php echo esc_html($nutrition_item[0]); ?> <strong class="value"><?php echo esc_html($nutrition_item[1]); ?></strong>
						        </li>
					        <?php
				        }
			        ?>
			        </ul>
	        </div>
	        <?php
        }
        else
        {
        ?>
            <p><?php esc_html_e('Nutritions is not available.','quickrecipe'); ?></p>
        <?php
        }
    }
}

// Comment function

function quickrecipe_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) {
		case 'pingback' :
		case 'trackback' :
	?>
	
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
	
		<?php esc_html__( 'Pingback:', 'quickrecipe' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'quickrecipe' ), '<span class="edit-link">', '</span>' ); ?>
		
	</li>
	<?php
			break;
		default :
		global $post;
	?>
    <li id="comment-<?php echo esc_html($comment->comment_ID); ?>">
        <div class="comment">
            <div class="comment-avatar">
                <?php echo get_avatar( $comment, 96 ); ?>
            </div>
            <div class="comment-body">
                <span class="comment-author"><?php echo get_comment_author_link(); ?> -</span> <span class="comment-date"><?php echo get_comment_date() .  esc_html__( ' at ', 'quickrecipe' ) . get_comment_time() ?></span>
                <?php comment_text(); ?>
                <?php edit_comment_link( esc_html__( 'Edit', 'quickrecipe' ), '', '' ); ?>
                <?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'quickrecipe' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div>
        </div>

	<?php
		break;
	}
}

function quickrecipe_enqueue_comments_reply() {
    if ( (!is_admin()) && is_singular() && comments_open() && get_option('thread_comments') ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'comment_form_before', 'quickrecipe_enqueue_comments_reply' );

function quickrecipe_get_mins_to_hours($time) {
    settype($time, 'integer');
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    if ($hours > 0 && $minutes > 0) {
        return sprintf(wp_kses(__('%02d h %02d mins', 'quickrecipe'), ''), $hours, $minutes);
    }
    else if ($hours > 0) {
        return sprintf(wp_kses(__('%02d hour', 'quickrecipe'), ''), $hours, $minutes);
    }
    else {
        return sprintf(wp_kses(__('%02d mins', 'quickrecipe'), ''), $minutes);
    }
}


if(!function_exists('quickrecipe_get_default_banner')){
    function quickrecipe_get_default_banner(){
        
        $banner_image_path = get_option('quickrecipe_banner_image');
        return empty($banner_image_path)? get_template_directory_uri().'/img/img01.jpg' :$banner_image_path;
    }
}

// Add term page
function quickrecipe_taxonomy_add_new_meta_field() {
	?>
	<div class="form-field">
		<label for="term_meta[custom_term_meta]"><?php esc_html_e( 'Image', 'quickrecipe' ); ?></label>
		<input type="text" name="term_meta[custom_term_meta]" id="custom_term_image_meta" value="">
        <input id="custom_term_image_button" type="button"  class="upload_button button" value="<?php esc_html_e( 'Upload Image', 'quickrecipe' ); ?>" />
		<p class="description"><?php esc_html_e( 'Upload a image','quickrecipe' ); ?></p>
	</div>
<?php
}
add_action( 'ingredient_add_form_fields', 'quickrecipe_taxonomy_add_new_meta_field', 10, 2 );
add_action( 'cuisine_add_form_fields', 'quickrecipe_taxonomy_add_new_meta_field', 10, 2 );
add_action( 'recipe_type_add_form_fields', 'quickrecipe_taxonomy_add_new_meta_field', 10, 2 );
add_action( 'course_add_form_fields', 'quickrecipe_taxonomy_add_new_meta_field', 10, 2 );

// Edit term page
function quickrecipe_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php esc_html_e( 'Image', 'quickrecipe' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[custom_term_meta]" id="custom_term_image_meta" value="<?php echo esc_attr( $term_meta['custom_term_meta'] ) ? esc_attr( $term_meta['custom_term_meta'] ) : ''; ?>">
            <input id="custom_term_image_button" type="button"  class="upload_button button" value="<?php esc_html_e( 'Upload Image', 'quickrecipe' ); ?>" />
			<p class="description"><?php esc_html_e( 'Enter a value for this field','quickrecipe' ); ?></p>
		</td>
	</tr>
<?php
}
add_action( 'ingredient_edit_form_fields', 'quickrecipe_taxonomy_edit_meta_field', 10, 2 );
add_action( 'cuisine_edit_form_fields', 'quickrecipe_taxonomy_edit_meta_field', 10, 2 );
add_action( 'recipe_type_edit_form_fields', 'quickrecipe_taxonomy_edit_meta_field', 10, 2 );
add_action( 'course_edit_form_fields', 'quickrecipe_taxonomy_edit_meta_field', 10, 2 );

// Save extra taxonomy fields callback function.
function quickrecipe_save_taxonomy_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  
add_action( 'edited_ingredient', 'quickrecipe_save_taxonomy_meta', 10, 2 );  
add_action( 'create_ingredient', 'quickrecipe_save_taxonomy_meta', 10, 2 );

add_action( 'edited_cuisine', 'quickrecipe_save_taxonomy_meta', 10, 2 );  
add_action( 'create_cuisine', 'quickrecipe_save_taxonomy_meta', 10, 2 );

add_action( 'edited_recipe_type', 'quickrecipe_save_taxonomy_meta', 10, 2 );  
add_action( 'create_recipe_type', 'quickrecipe_save_taxonomy_meta', 10, 2 );

add_action( 'edited_course', 'quickrecipe_save_taxonomy_meta', 10, 2 );  
add_action( 'create_course', 'quickrecipe_save_taxonomy_meta', 10, 2 );

function quickrecipe_get_user_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
}

function quickrecipe_get_user_sidebar($page_id = 0){
    
    $page_custom_fields = get_post_custom( $page_id );
    $current_url = get_permalink( $page_id );
    
    $submit_recipe_page_id = get_option('quickrecipe_submit_recipe_url', '');
    $submit_recipe_url = get_permalink($submit_recipe_page_id);

 //dev code start here
    $submit_tips_url = get_permalink(1559);
//dev code end here
    $submit_testimonial_page_id = get_option('quickrecipe_submit_testimonial_url', '');
    $submit_testimonial_url = get_permalink($submit_testimonial_page_id);
    $list_recipe_page_id = get_option('quickrecipe_list_recipe_url', '');
    $list_recipe_url = get_permalink($list_recipe_page_id);
    $list_testimonial_page_id = get_option('quickrecipe_list_testimonial_url', '');
    $list_testimonial_url = get_permalink($list_testimonial_page_id);
    $list_my_favorite_page_id = get_option('quickrecipe_list_my_favorite_url', '');
    $list_my_favorite_url = get_permalink($list_my_favorite_page_id);
    $edit_my_profile_page_id = get_option('quickrecipe_edit_my_profile_url', '');
    $edit_my_profile_url = get_permalink($edit_my_profile_page_id);

    ?>
        

                   <?php if (ICL_LANGUAGE_CODE=='en') { ?>
                    
                    <ul class="user-account-menu">
                      <li class="user-pic-name"><?php
                        if ( is_user_logged_in() ):
                            $current_user = wp_get_current_user();

                        if ( ($current_user instanceof WP_User) ) {
                            echo '<div class="user-profile-pic">' . get_avatar( $current_user->user_email, 290 ) . '</div>';
                            echo '<div class="user-name">' . esc_html($current_user->display_name) . '</div>' ;
                        }
                        endif;
						  ?></li>
          <!-- dev code star here -->
				        <!-- <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($submit_recipe_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($submit_recipe_url); ?>" title="<?php esc_attr_e('My Recipe', 'quickrecipe'); ?>"><i class="fa fa-plus-circle"></i><?php esc_html_e('My Recipe', 'quickrecipe'); ?></a></li> -->
                
            
                        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($list_recipe_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($list_recipe_url); ?>" title="<?php esc_attr_e('My Recipes', 'quickrecipe'); ?>"><i class="fa fa-pencil"></i><?php esc_html_e('My Recipes', 'quickrecipe'); ?></a></li>

                        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($submit_tips_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($submit_tips_url); ?>" title="<?php esc_attr_e('My Tips', 'quickrecipe'); ?>"><i class="fa fa-pencil"></i><?php esc_html_e('My Tips', 'quickrecipe'); ?></a></li>
                
				        
				      	<?php /*?><li  class="menu-item" <?php echo esc_url($current_url) == esc_url($submit_testimonial_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($submit_testimonial_url); ?>" title="<?php esc_attr_e('Submit Testimonial', 'quickrecipe'); ?>"><i class="fa fa-plus-circle"></i><?php esc_html_e('Submit Testimonial', 'quickrecipe'); ?></a></li>
				        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($list_testimonial_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($list_testimonial_url); ?>" title="<?php esc_attr_e('List Testimonial', 'quickrecipe'); ?>"><i class="fa fa-comment"></i><?php esc_html_e('List My Testimonials', 'quickrecipe'); ?></a></li><?php */?>
				        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($list_my_favorite_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($list_my_favorite_url); ?>" title="<?php esc_attr_e('My Favorite Recipes', 'quickrecipe'); ?>"><i class="fa fa-heart"></i> Recipes I <span class=heart_icon_text>&#9829;</span></a></li>
                        
				        <li class="menu-item" <?php echo esc_url($current_url) == esc_url($edit_my_profile_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($edit_my_profile_url); ?>" title="<?php esc_attr_e('View My Profile', 'quickrecipe'); ?>"><i class="fa fa-book"></i><?php esc_html_e(' View My Profile', 'quickrecipe'); ?></a></li>
				        <li class="menu-item"><a href="<?php echo wp_logout_url(); ?>" title="<?php esc_attr_e('Logout', 'quickrecipe'); ?>"><i class="fa fa-sign-out"></i><?php esc_html_e(' Logout', 'quickrecipe'); ?></a></li>
                   		<li class="menu-last-item"></li>
                    </ul>
                  <?php } elseif (ICL_LANGUAGE_CODE=='ar'){ ?>
 				
 				   <ul class="user-account-menu">
                      <li class="user-pic-name"><?php
                        if ( is_user_logged_in() ):
                            $current_user = wp_get_current_user();

                        if ( ($current_user instanceof WP_User) ) {
                            echo '<div class="user-profile-pic">' . get_avatar( $current_user->user_email, 290 ) . '</div>';
                            echo '<div class="user-name">' . esc_html($current_user->display_name) . '</div>' ;
                        }
                        endif;
						  ?></li>
				        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($submit_recipe_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($submit_recipe_url); ?>" title="<?php esc_attr_e('تحميل الوصفة', 'quickrecipe'); ?>"><i class="fa fa-plus-circle"></i><?php esc_html_e('تحميل الوصفة', 'quickrecipe'); ?></a></li>
				        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($list_recipe_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($list_recipe_url); ?>" title="<?php esc_attr_e('وصفاتي', 'quickrecipe'); ?>"><i class="fa fa-book"></i><?php esc_html_e('وصفاتي', 'quickrecipe'); ?></a></li>
				        <?php /*?><li  class="menu-item" <?php echo esc_url($current_url) == esc_url($submit_testimonial_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($submit_testimonial_url); ?>" title="<?php esc_attr_e('Submit Testimonial', 'quickrecipe'); ?>"><i class="fa fa-plus-circle"></i><?php esc_html_e('Submit Testimonial', 'quickrecipe'); ?></a></li>
				        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($list_testimonial_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($list_testimonial_url); ?>" title="<?php esc_attr_e('List Testimonial', 'quickrecipe'); ?>"><i class="fa fa-comment"></i><?php esc_html_e('List My Testimonials', 'quickrecipe'); ?></a></li>
				        <?php */?>
				        <li  class="menu-item" <?php echo esc_url($current_url) == esc_url($list_my_favorite_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($list_my_favorite_url); ?>" title="<?php esc_attr_e('وصفتي المفضلة', 'quickrecipe'); ?>"><i class="fa fa-heart"></i><?php esc_html_e(' وصفتي المفضلة', 'quickrecipe'); ?></a></li>
				        <li class="menu-item" <?php echo esc_url($current_url) == esc_url($edit_my_profile_url) ? 'class="active"' : ''; ?>><a href="<?php echo esc_url($edit_my_profile_url); ?>" title="<?php esc_attr_e('Edit My Profile', 'quickrecipe'); ?>"><i class="fa fa-pencil"></i><?php esc_html_e(' تعديل ملفي الشخصي', 'quickrecipe'); ?></a></li>
				        <li class="menu-item"><a href="<?php echo wp_logout_url(); ?>" title="<?php esc_attr_e('تسجيل الخروج', 'quickrecipe'); ?>"><i class="fa fa-sign-out"></i><?php esc_html_e(' تسجيل الخروج', 'quickrecipe'); ?></a></li>
                   		<li class="menu-last-item"></li>
                    </ul>
 				<!-- dev code end here -->
				  <?php } ?>

              
    <?php
}

function quickrecipe_get_author_social($user_id){
    $author_url = get_the_author_meta('user_url', $user_id); 
								
	$author_url = preg_replace('#^https?://#', '', rtrim($author_url,'/'));
						
	$author_facebook = get_the_author_meta('facebook', $user_id); 
																	
	if (!empty($author_facebook)) : ?>
									
		<a class="link-facebook" target="_blank" title="<?php echo esc_attr($author_facebook) . ' '; ?><?php esc_html_e('on Facebook','quickrecipe'); ?>" href="<?php echo esc_url($author_facebook); ?>"><i class="fa fa-facebook"></i></a>

	<?php endif;
									
	$author_twitter = get_the_author_meta('twitter', $user_id); 
																	
	if (!empty($author_twitter)) : ?>
									
		<a class="link-twitter" target="_blank" title="<?php echo esc_attr('@'.$author_twitter) . ' '; ?><?php esc_html_e('on Twitter','quickrecipe'); ?>" href="<?php echo esc_url('http://www.twitter.com/'.$author_twitter); ?>"><i class="fa fa-twitter"></i></a>

	<?php endif;

	$author_gplus = get_the_author_meta('google', $user_id); 
																	
	if (!empty($author_gplus)) : ?>
									
		<a class="link-google" target="_blank" title="<?php echo esc_attr($author_gplus) . ' '; ?><?php esc_html_e('on google+','quickrecipe'); ?>" href="<?php echo esc_url($author_gplus); ?>"><i class="fa fa-google-plus"></i></a>

										
	<?php endif;
									
	$author_linkedin = get_the_author_meta('linkedin', $user_id); 
																	
	if (!empty($author_linkedin)) : ?>
									
		<a class="link-linkedin" target="_blank" title="<?php echo esc_attr($author_linkedin) . ' '; ?><?php esc_html_e('on Linkedin','quickrecipe'); ?>" href="<?php echo esc_url($author_linkedin); ?>"><i class="fa fa-linkedin"></i></a>
																														
	<?php endif;
									
	$author_pinterest = get_the_author_meta('pinterest', $user_id); 
																	
	if (!empty($author_pinterest)) : ?>
									
		<a class="link-pinterest" target="_blank" title="<?php echo esc_attr($author_pinterest) . ' '; ?><?php esc_html_e('on Pinterest','quickrecipe'); ?>" href="<?php echo esc_url($author_pinterest); ?>"><i class="fa fa-pinterest-p"></i></a>

										
	<?php endif;
									
	$author_mail = get_the_author_meta('email', $user_id); 
									
	$show_mail = get_the_author_meta('showemail');
																	
	if ( !empty($author_mail) && ($show_mail == "yes") ) : ?>
									
		<a class="link-email" title="<?php echo esc_attr($author_mail); ?>" href="mailto:<?php echo antispambot($author_mail, 1); ?>"><i class="icon-mail-2"></i></a>
																				
	<?php endif;
}

add_filter('nav_menu_css_class' , 'quickrecipe_special_nav_class' , 10 , 2);
function quickrecipe_special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

function quickrecipe_pagination_links() {
    global $query;

    $big = 999999999; // need an unlikely integer

    $pages = paginate_links( array(
        //'base' => @add_query_arg('paged','%#%'),
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => @$query->max_num_pages,
        'type'  => 'array',
    ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<div class="row small-12"><ul class="pagination">';
        foreach ( $pages as $page ) {
                echo "<li>".$page."</li>";
        }
        echo '</ul></div>';
    }
}

// function to display number of posts.
function quickrecipe_get_post_views($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if (empty($count)) {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return esc_html__( '0 View', 'quickrecipe' );
    }
    return sprintf( wp_kses( __( '%02d Views', 'quickrecipe' ), '' ), $count );
}

// function to count views.
function quickrecipe_set_post_views($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

if ( !function_exists( 'quickrecipe_post_thumbnail' ) ) {
    function quickrecipe_post_thumbnail() {
	    if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		    return;
	    }

	    if ( is_singular() ) :
	    ?>

	    <div class="post-thumbnail">
		    <?php the_post_thumbnail(); ?>
	    </div><!-- .post-thumbnail -->

	    <?php else : ?>

	    <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		    <?php
			    the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
		    ?>
	    </a>

	    <?php endif; // End is_singular()
    }
}

// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'quickrecipe_posts_column_views');
add_action('manage_posts_custom_column', 'quickrecipe_posts_custom_column_views',5,2);
function quickrecipe_posts_column_views($defaults){
    $defaults['post_views'] = esc_html__('Views', 'quickrecipe');
    return $defaults;
}
function quickrecipe_posts_custom_column_views($column_name, $id){
	if($column_name === 'post_views'){
        echo quickrecipe_get_post_views(get_the_ID());
    }
}



// Add footer widget areas
add_action( 'widgets_init', 'quickrecipe_sidebar_reg' ); 

function quickrecipe_sidebar_reg() {
	register_sidebar(array(
	  'name' => __( 'Footer Menu', 'quickrecipe' ),
	  'id' => 'footer-a',
	  'description' => __( 'Menu will be shown in the left column in the footer.', 'quickrecipe' ),
	  'before_title' => '<h3 class="sub-title">',
	  'after_title' => '</h3>',
	  'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
	  'after_widget' => '</div><div class="clear"></div></div>'
	));	
	register_sidebar(array(
	  'name' => __( 'Footer B', 'quickrecipe' ),
	  'id' => 'footer-b',
	  'description' => __( 'Widgets in this area will be shown in the middle column in the footer.', 'quickrecipe' ),
	  'before_title' => '<h3 class="sub-title">',
	  'after_title' => '</h3>',
	  'before_widget' => '<div class="widget %2$s widget-section categories">',
	  'after_widget' => '</div>'
	));
	register_sidebar(array(
	  'name' => __( 'Footer C', 'quickrecipe' ),
	  'id' => 'footer-c',
	  'description' => __( 'Widgets in this area will be shown in the right column in the footer.', 'quickrecipe' ),
	  'before_title' => '<h3 class="sub-title">',
	  'after_title' => '</h3>',
	  'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
	  'after_widget' => '</div><div class="clear"></div></div>'
	));
	register_sidebar(array(
	  'name' => __( 'Footer D', 'quickrecipe' ),
	  'id' => 'footer-d',
	  'description' => __( 'Widgets in this area will be shown in the right column in the footer.', 'quickrecipe' ),
	  'before_title' => '<h3 class="sub-title">',
	  'after_title' => '</h3>',
	  'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
	  'after_widget' => '</div><div class="clear"></div></div>'
	));
	register_sidebar(array(
	  'name' => __( 'Primary Sidebar', 'quickrecipe' ),
	  'id' => 'primary-sidebar',
	  'description' => __( 'Widgets in this area will be shown in the sidebar.', 'quickrecipe' ),
	  'before_title' => '<div class="widget-header"><h3 class="widget-title">',
	  'after_title' => '</h3></div>',
	  'before_widget' => '<div class="widget widget-sidebar widget-white">',
	  'after_widget' => '</div>'
	));
	register_sidebar(array(
	  'name' => __( 'Secondary Sidebar', 'quickrecipe' ),
	  'id' => 'secondary-sidebar',
	  'description' => __( 'Widgets in this area will be shown in the sidebar.', 'quickrecipe' ),
	  'before_title' => '<h3 class="widget-title">',
	  'after_title' => '</h3>',
	  'before_widget' => '',
	  'after_widget' => ''
	));
}


class Quickrecipe_Nav_Menu_Walker extends Walker_Nav_Menu {


	function start_lvl( &$output, $depth = 0, $args = array() ) {

		$indent = str_repeat( "\t", $depth );
		$submenu = ($depth > 0) ? ' sub-menu' : '';
		$output	   .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";

	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {


		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$li_attributes = '';
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		
		// managing divider: add divider class to an element to get a divider before it.
		$divider_class_position = array_search('divider', $classes);
		if($divider_class_position !== false){
			$output .= "<li class=\"divider\"></li>\n";
			unset($classes[$divider_class_position]);
		}
		
		$classes[] = ($args->has_children) ? 'dropdown' : '';
		$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
		$classes[] = 'menu-item-' . $item->ID;
		if($depth && $args->has_children){
			$classes[] = 'dropdown-submenu';
		}


		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_url( $item->url        ) .'"' : '';
		$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= ($depth == 0 && $args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
		$item_output .= $args->after;


		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		//v($element);
		if ( !$element )
			return;

		$id_field = $this->db_fields['id'];

		//display this element
		if ( is_array( $args[0] ) )
			$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
		else if ( is_object( $args[0] ) )
			$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'start_el'), $cb_args);

		$id = $element->$id_field;

		// descend only when the depth is right and there are childrens for this element
		if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

			foreach( $children_elements[ $id ] as $child ){

				if ( !isset($newlevel) ) {
					$newlevel = true;
					//start the child delimiter
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
				}
				$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
			}
			unset( $children_elements[ $id ] );
		}

		if ( isset($newlevel) && $newlevel ){
			//end the child delimiter
			$cb_args = array_merge( array(&$output, $depth), $args);
			call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
		}

		//end this element
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'end_el'), $cb_args);

	}

}

function quickrecipe_menu_message(){
	echo '<li class="menu-item"><a href="' . esc_url( home_url( '/' ) ) . '">'.__('Please add a menu.','quickrecipe').'</a></li>';
}

function quickrecipe_bootstrap_link_pages( $args = array () ) {
    $defaults = array(
        'before'      => '<p>' . __('Pages:', 'quickrecipe'),
        'after'       => '</p>',
        'before_link' => '',
        'after_link'  => '',
        'current_before' => '',
        'current_after' => '',
        'link_before' => '',
        'link_after'  => '',
        'pagelink'    => '%',
        'echo'        => 1
    );
    $r = wp_parse_args( $args, $defaults );
    $r = apply_filters( 'wp_link_pages_args', $r );
    extract( $r, EXTR_SKIP );
    global $page, $numpages, $multipage, $more, $pagenow;
    if ( ! $multipage )
    {
        return;
    }
    $output = $before;
    for ( $i = 1; $i < ( $numpages + 1 ); $i++ )
    {
        $j       = str_replace( '%', $i, $pagelink );
        $output .= ' ';
        if ( $i != $page || ( ! $more && 1 == $page ) )
        {
            $output .= "{$before_link}" . _wp_link_page( $i ) . "{$link_before}{$j}{$link_after}</a>{$after_link}";
        }
        else
        {
            $output .= "{$current_before}{$link_before}<a>{$j}</a>{$link_after}{$current_after}";
        }
    }
    echo $output . $after;
}

function quickrecipe_get_social_icons() 
{
    $facebook_link      =   get_option('quickrecipe_facebook_link', '');
    $twitter_link       =   get_option('quickrecipe_twitter_link', '');
    $google_link        =   get_option('quickrecipe_google_link', '');
    $linkedin_link      =   get_option('quickrecipe_linkedin_link','');
    $pinterest_link     =   get_option('quickrecipe_pinterest_link','');
    $instagramm_link      =  get_option('quickrecipe_instagramm_link','');
    $vimeo_link      =   get_option('quickrecipe_vimeo_link','');
    $youtube_link      =   get_option('quickrecipe_youtube_link','');

    if($facebook_link != ''){
        echo ' <li><a href="'. esc_url($facebook_link).'"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>';
    }

    if($twitter_link != ''){
        echo ' <li><a href="'.esc_url($twitter_link).'"><i class="fa fa-twitter"></i></a> <span>Twitter</span></li>';
    }
                        
    if($google_link != ''){
        echo ' <li><a href="'. esc_url($google_link).'"><i class="fa fa-google-plus"></i> <span>Google+</span></a></li>';
    }

    if($linkedin_link != ''){
        echo ' <li><a href="'.esc_url($linkedin_link).'"><i class="fa fa-linkedin"></i> <span>Linkedin</span></a></li>';
    }

    if($pinterest_link != ''){
            echo ' <li><a href="'. esc_url($pinterest_link).'"><i class="fa fa-pinterest"></i> <span>Pinterest</span></a></li>';
    }

    if($instagramm_link != ''){
            echo ' <li><a href="'. esc_url($instagramm_link).'"><i class="fa fa-instagram"></i> <span>Instagram</span></a></li>';
    }

    if($vimeo_link != ''){
            echo ' <li><a href="'. esc_url($vimeo_link).'"><i class="fa fa-vimeo"></i> <span>Vimeo</span></a></li>';
    }

    if($youtube_link != ''){
            echo ' <li><a href="'. esc_url($youtube_link).'"><i class="fa fa-youtube-play"></i> <span>YouTube</span></a></li>';
    }

    return;

}

function quickrecipe_get_term_pages( $pterm ) {
?>
    <div id="content">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

            <div id="recipe_categories" class="widget recipe_categories">
                <div class="tm-grid-2x2-widget">
                    <div class="grid-wrap row">
                    <?php
                     $terms = get_terms( $pterm, array('orderby' => 'name', 'pad_counts' => 1) );
                     if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                         $count = 0;
                         foreach ( $terms as $term ) {
                            $count++;
                            $term_meta = get_option( "taxonomy_$term->term_id" );
                            if ( !empty($term_meta['custom_term_meta']) )
                            {
                                $quickrecipe_thumbnail = $term_meta['custom_term_meta'];
                            }
                            else
                            {
                                $quickrecipe_thumbnail = quickrecipe_get_default_banner();
                            }
                            $term_link = get_term_link( $term );
                            // If there was an error, continue to the next term.
                            if ( is_wp_error( $term_link ) ) {
                                continue;
                            }

                            ?>
                            <div class="col-md-3 invert">
                                <a href="<?php echo esc_url( $term_link ); ?>">
                                    <img src="<?php echo esc_url($quickrecipe_thumbnail); ?>">
                                    <div class="article-content">
                                        <h4><?php echo esc_html(wp_trim_words($term->name,4,'...')); ?></h4>
                                        <h6><em><i class="fa fa-book"></i> <?php echo esc_html($term->count); ?> <?php esc_html_e('recipes', 'quickrecipe'); ?></em> | <em><i class="fa fa-comment-o"></i> <?php echo quickrecipe_custom_taxonomy_count('recipe',$term->slug); ?> <?php esc_html_e('comments', 'quickrecipe'); ?></em></h6>
                                    </div>
                                </a>
                            </div>
                            <?php
                         }
                     }
                    ?>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php
}

function quickrecipe_custom_taxonomy_count($ptype,$pterm) {
	global $wpdb;

    $comment_count = $wpdb->get_var( $wpdb->prepare( 
	    "
		    SELECT SUM(comment_count)
		    FROM $wpdb->posts posts, $wpdb->terms terms, $wpdb->term_relationships rels
		    WHERE posts.post_status = 'publish'
            AND posts.ID = rels.object_id
            AND terms.term_id = rels.term_taxonomy_id
            AND post_type = %s 
            AND terms.slug = %s 
	    ", 
	    $ptype,
        $pterm
    ) );

    return $comment_count;
}

// Add theme widgets
include_once(get_template_directory() . '/includes/widgets/footer-recent-recipe-widget.php');
include_once(get_template_directory() . '/includes/widgets/recipe-types-widget.php');
include_once(get_template_directory() . '/includes/widgets/recipe-cuisine-widget.php');
include_once(get_template_directory() . '/includes/widgets/recipe-course-widget.php');
include_once(get_template_directory() . '/includes/widgets/latest-blog-post-widget.php');
include_once(get_template_directory() . '/includes/widgets/related-cuisine-sidebar-widget.php');
include_once(get_template_directory() . '/includes/widgets/recipe-season-widget.php');
include_once(get_template_directory() . '/includes/widgets/footer-about-widget.php');
include_once(get_template_directory() . '/includes/widgets/footer-contact-widget.php');
include_once(get_template_directory() . '/includes/widgets/footer-tagcloud-widget.php');
include_once(get_template_directory() . '/includes/widgets/footer-most-viewed-widget.php');
include_once(get_template_directory() . '/includes/widgets/google-adsense-widget.php');



function pre_wpml_is_translated_post_type ($translated, $type) {
    if ($type === "recipe") {
        $translated = true;
    }
 
    return $translated;
}
add_filter('pre_wpml_is_translated_post_type', 'pre_wpml_is_translated_post_type', 10, 2);


function SearchFilter($query) {
   // If 's' request variable is set but empty
   if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
      $query->is_search = true;
      $query->is_home = false;
   }
   return $query;
}
add_filter('pre_get_posts','SearchFilter');



/**body class **/
function template_chooser($template)   
{    
    global $wp_query;
    /*Customer Code Start - Mansur*/   
    $post_type = (isset($_GET['post_type'])?$_GET['post_type']:"");   
    /*Customer Code End - Mansur*/
    if( $wp_query->is_search && $post_type == 'recipes' )   
    {
        return locate_template('recipe-search.php');
    }
    /*Customer Code Start - Mansur*/
    if( $wp_query->is_search && $post_type == 'tips' )  
    {
        return locate_template('philly-tips-search.php');
    }
    if( $wp_query->is_search && $post_type == 'tips-by-user' )  
    { 
        return locate_template('philly-tips-search-by-user.php');
    }
    /*Customer Code End - Mansur*/       
    return $template;   
}
add_filter('template_include', 'template_chooser');    

add_filter( 'body_class','body_classes' );
function body_classes( $classes ) {
 
	
	if ( is_front_page() ){
        $classes[] = 'homepage';
	}
	if( is_page_template('template-philadelphia.php') ){
        $classes[] = 'about';
	}
	if( is_page_template('template-contact-form-en.php') || is_page_template('template-contact-form-ar.php') ){
        $classes[] = 'contact';
	}
	if( is_page_template('template-recipes.php') ){
        $classes[] = 'recipes';
	}
//dev star code
    if( is_page_template('template-philly-tips.php') ){
         $classes[] = 'recipes';
        $classes[] = 'pilly-tips';
    }
    if( is_page_template('template-submit-tips-en.php') ){
        $classes[] = 'user-account';
        $classes[] = 'user-submit-tips-account';

    }
    if( is_page_template('template-tips-by-user.php') ){
        $classes[] = 'recipes';
        $classes[] = 'tips-by-user';

    }
    if( is_page_template('template-list-my-favorite-tips-en.php') ){
        $classes[] = 'my-fav-tips';
    }
    if( is_page_template('template-list-my-favorite-tips-ar.php') ){
        $classes[] = 'user-account favorites';
    }
    if( is_page_template('template-list-tips-en.php') ){
        $classes[] = 'my-fav-tips';
    }
    if( is_page_template('template-list-tips-ar.php') ){
        $classes[] = 'user-account favorites';
    }
    if( is_page_template('single-tips.php') ){
        $classes[] = 'recipe-template-default single-recipe';
    }

    //dev end code	
	if( is_page_template('search.php') ){
        $classes[] = 'search-results';
	}
	
	if( is_page_template('template-edit-my-profile-en.php') ||  is_page_template('template-edit-my-profile-ar.php')){
        $classes[] = 'user-account submit-profile';
	}
	
	if( is_page_template('template-submit-recipe-en.php') ||  is_page_template('template-submit-recipe-ar.php')){
        $classes[] = 'user-account submit-recipe';
	}

    /*Customer Code Start - Mansur*/
    if( is_page_template('template-submit-recipe-en.php') ||  is_page_template('template-submit-recipe-ar.php')){
        $classes[] = 'user-account submit-recipe';
    }
    /*Customer Code End - Mansur*/
	
	if( is_page_template('template-list-recipe-en.php') ||  is_page_template('template-list-recipe-ar.php') ||  is_page_template('template-list-my-favorite-en.php') ||  is_page_template('template-list-my-favorite-ar.php')){
        $classes[] = 'user-account favorites';
	}
	
	if( is_page('terms-and-conditions') ){
        $classes[] = 'terms';
	}
	
	
    return $classes;
     
}

/* recipe videos */

function Oembed_youtube_no_title($html,$url,$args){
    $url_string = parse_url($url, PHP_URL_QUERY);
    parse_str($url_string, $id);
    if (isset($id['v'])) {
        return '<iframe width="'.$args['width'].'" height="'.$args['height'].'" src="http://www.youtube.com/embed/'.$id['v'].'?rel=0&showinfo=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>';
    }
    return $html;
}
add_filter('oembed_result','Oembed_youtube_no_title',10,3);

/*sub menu custom class*/

function change_submenu_class($menu) {  
  $menu = preg_replace('/ class="sub-menu"/','/ class="dropdown-menu" /',$menu);  
  return $menu;  
}  
add_filter('wp_nav_menu','change_submenu_class');  



/*get post/recipe author role*/

function get_author_role()
{
    global $authordata;

    $author_roles = $authordata->roles;
    $author_role = array_shift($author_roles);

    return $author_role;
}


/*login errros*/
function no_wordpress_errors(){
  return 'Invalid Credentials';
}
add_filter( 'login_errors', 'no_wordpress_errors' );