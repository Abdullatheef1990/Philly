<?php
if ( ! defined( 'ABSPATH' ) ) exit;

get_header();

?>

<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page404">
                    <div class="titleError"><?php esc_html_e( '404!', 'quickrecipe' ); ?></div>
                    <h2><?php esc_html_e( 'The page you were looking for could not be found.', 'quickrecipe' ); ?></h2>
                    <p>
                        <?php esc_html_e( 'Go back, or return to', 'quickrecipe' ); ?>
                        <a href="<?php echo esc_url( get_home_url() ); ?>"><?php esc_html_e( 'home page', 'quickrecipe' ); ?></a> <?php esc_html_e( 'to choose a new page.', 'quickrecipe' ); ?>
                        <br><?php esc_html_e( 'Please report any broken links to our team.', 'quickrecipe' ); ?>
                    </p>
                    
                    <div>
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>


