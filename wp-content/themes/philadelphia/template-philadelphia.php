<?php
/*
*   Template Name: Philadelphia
*/
// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header();
?>
    
    
   <div class="row full-width header-box">
    <div class="main-image" style="background-image: url(<?php the_field('page_header_image'); ?>)">

        <div class="row main-container align-center full-width">
            <div class="column large-6 column align-self-middle">
                <h1 class="heading">
                   <?php  the_field('page_header_title'); ?>
                </h1>

            </div>
        </div>

    </div>

</div>
  
 



<?php
		
		if( have_rows('about_content_section') ):	   
		 while ( have_rows('about_content_section') ) : the_row();
               
                $about_content_title = get_sub_field('about_content_title');
				$about_title_icon = get_sub_field('about_icon_image');
				$about_content = get_sub_field('about_content');
                $about_banner_image = get_sub_field('about_banner_image');
                
	    ?>
              
             



<div class="row about-text-block align-center">
    <div class="columns large-10 align-self-middle">
        <h2><?php echo $about_content_title; ?>  <img src="<?php echo $about_title_icon; ?>"/></h2>
        <?php echo $about_content; ?>
    </div>

</div>
   
<?php if ($about_banner_image!=""){ ?>
<div class="row full-width about-image-block" style="background-image: url(<?php echo $about_banner_image; ?>)">

</div>                        
	
 		 <?php 
}
    endwhile;

endif;
	  
            wp_reset_postdata();
       
        ?>
         




   
 <?php /* OUR FULL RANGE ENDS STARTS */ ?>

<div class="about-products-slider">
 <div class="row align-center products-slider-headline">

    <div class="columns large-10 small-12">
        <?php  the_field('about_title'); ?>
    </div>

</div>

<div class="row full-width all-products">


     <div class="master-slider ms-skin-default" id="products-masterslider"> 
    
       
	<?php
		
		if( have_rows('about_products_slider') ):	   
		 while ( have_rows('about_products_slider') ) : the_row();
               
                $about_product_slider_title = get_sub_field('about_product_slider_title');
				$about_product_slider_details = get_sub_field('about_product_slider_details');
                $about_product_slider_image = get_sub_field('about_product_slider_image');
                
	    ?>
              
              <div class="ms-slide">

            
            <div class="row main-container align-center full-width ms-layer ms-caption">
                <div class="column large-4 column align-self-middle">


                    <div class="product-img">
                        <img src="<?php echo $about_product_slider_image; ?>"/>
                    </div>


                </div>
                <div class="column large-4 column align-self-middle">
                    <h1 class="heading">
                         <?php echo $about_product_slider_title; ?>
                    </h1>

                    <p class="product-description"> <?php echo $about_product_slider_details; ?></p>

                </div>
            </div>


        </div>
        
               
             
            
 		 <?php 
    
    endwhile;

endif;
	  
            wp_reset_postdata();
       
        ?>
         
      
            
            
    </div>
    
</div>
   
</div>
 <?php /* OUR FULL RANGE ENDS */ ?>




<?php /* OUR WORK VIDEO STARTS */ ?>

<div class="row about-video align-center">
    <div class="columns large-10 small-12 medium-10 about-video-headline">
      <?php  the_field('about_our_work_title'); ?>
    </div>

    <div class="columns large-10 small-12 medium-10">
       <div class="embed-container">
        <?php  the_field('about_our_work_video'); ?>
        </div>

       <?php /*?> <div class="video-view-more-btn">
            <a href="">VIEW MORE</a>
        </div><?php */?>
    </div>

</div>

<?php /* OUR WORK VIDEO ENDS */ ?>


<?php


get_footer(); 
?>