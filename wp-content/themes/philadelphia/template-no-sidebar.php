<?php
// Template Name: Content Page No Sidebar
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Page Template
 */
get_header();
?>
   <?php if(the_field('page_header_image')!='') { ?>
   <div class="row full-width header-box">
    <div class="main-image" style="background-image: url(<?php the_field('page_header_image'); ?>)">

        <div class="row main-container align-center full-width">
            <div class="column large-6 column align-self-middle">
                <h1 class="heading">
                   <?php  the_field('page_header_title'); ?>
                </h1>

            </div>
        </div>

    </div>

</div>
   <?php } ?>
    <!-- begin:content -->
    <div class="row align-center">
           <div class="columns large-12 small-12 headline"> <?php the_title(); ?></div>
             <div class="columns large-12 small-12 content">
            <?php
        	    if ( have_posts() ) {
        		    while ( have_posts() ) { the_post();
											?>
											
                       <?php
                        the_content();
                        wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'quickrecipe' ), 'after' => '</div>' ) );
				    }
			    } else {
		    ?>
            	    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'quickrecipe' ); ?></p>
            <?php } ?>
          </div>
        </div>
     
    <!-- end:content -->

<?php get_footer(); ?>
