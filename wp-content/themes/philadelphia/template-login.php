<?php 
/* Template Name: Login Page */
 
if ( is_user_logged_in() ) {
	wp_redirect( esc_url( get_home_url() ) );
	exit;
}

$errors = array();
$login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;
if ( $login === "failed" ) {
    $errors[] = esc_html__('Invalid username and/or password.', 'quickrecipe');
} elseif ( $login === "false" ) {
    $errors[] = esc_html__('You are logged out.', 'quickrecipe');
}

$enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);

$login_page_id = get_option('quickrecipe_login_page_url', '');
$login_page_url = get_permalink($login_page_id);
$login_page_url = (!$login_page_url || !$enable_custom_login) ? wp_login_url() : $login_page_url;
	
$register_page_id = get_option('quickrecipe_register_page_url', '');
$register_page_url = get_permalink($register_page_id);
$register_page_url = (!$register_page_url || !$enable_custom_login) ? wp_registration_url() : $register_page_url;
	
$lost_password_page_id = get_option('quickrecipe_lost_password_page_url', '');
$lost_password_page_url = get_permalink($lost_password_page_id);
if (!$lost_password_page_url || !$enable_custom_login) {
    $lost_password_page_url = wp_lostpassword_url();
}

get_header(); 
?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
            <div class="login-wrap">
                <div class="login ">
                    <div class="page-header">
                        <h1>
                            <?php esc_html_e('Login', 'quickrecipe'); ?>
                        </h1>
                    </div>
                    <div class="login-description">
				        <p>
				        <?php esc_html_e('Don\'t have an account yet?', 'quickrecipe'); ?> <a href="<?php echo esc_url($register_page_url); ?>"><?php esc_html_e('Sign up', 'quickrecipe'); ?></a>.<br /> <?php esc_html_e('Forgotten your password?', 'quickrecipe'); ?> <a href="<?php echo esc_url($lost_password_page_url); ?>"><?php esc_html_e('Reset it here', 'quickrecipe'); ?></a>.
				        </p>
				        <?php
				        if (count($errors) > 0) {
					        ?>
					        <div class="error">
			                <?php if (!empty($errors)) { ?>
				                <p class="error"><?php echo implode('<br />', $errors); ?></p>
			                <?php } ?> 
					        </div>
					        <?php
				        }
				        ?>
                    </div>
                    <?php
                    $args = array(
                        'redirect' => esc_url( home_url( '/' ) ), 
                        'id_username' => 'user',
                        'id_password' => 'pass',
                       );
                    ?>
                    <?php wp_login_form( $args ); ?>
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- end:content -->
	
	
 <?php get_footer(); ?>