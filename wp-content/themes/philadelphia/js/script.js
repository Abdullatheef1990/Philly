jQuery.noConflict();
jQuery(document).ready(function ($) {
    'use strict';
    $(function () {
        $("#testimonial-slide").owlCarousel({
            loop: true,
            margin: 20,
            navigation: false,
            pagination: true,
            items: 2,
            itemsDesktop: [1199, 2],
            itemsDesktopSmall: [980, 2],
            itemsTablet: [768, 2],
            itemsTabletSmall: false,
            itemsMobile: [479, 1]

        });

        $("#article-slide").owlCarousel({

            loop: true,
            margin: 20,
            navigation: false,
            pagination: true,
            items: 2,
            itemsDesktop: [1199, 2],
            itemsDesktopSmall: [980, 2],
            itemsTablet: [768, 2],
            itemsTabletSmall: false,
            itemsMobile: [479, 1]

        });

    });

    function ingredientCheckbox() {
        if ($('ul.ingredient-check').length) {
            $('ul.ingredient-check').find('li').on('click', function () {
                $(this).toggleClass('active');
                $(this).find('input[type=checkbox]').prop('checked', function () {
                    return !this.checked;
                });
            });

        };
    }

    $('#recipe-form').validate();
    $('.add-more').on('click', function () {
        var parent = $(this).parent('div');
        parent.find('input:first, textarea:first').clone(true).val('').appendTo(parent.find('.more-field'));
    });

    $('input[type=file]').change(function (e) {
        var file_in = jQuery(this);
        file_in.next().html(file_in.val());

    });

    var jcarousel = $('.jcarousel');

    jcarousel
        .on('jcarousel:reload jcarousel:create', function () {
            var width = jcarousel.innerWidth();

            if (width >= 992) {
                width = width / 5;
            } else if (width >= 768) {
                width = width / 4;
            } else if (width >= 480) {
                width = width / 3;
            } else if (width >= 350) {
                width = width / 2;
            }

            jcarousel.jcarousel('items').css('width', width + 'px');
        })
        .jcarousel({
            wrap: 'circular'
        });

    $('.jcarousel-control-prev')
        .jcarouselControl({
            target: '-=1'
        });

    $('.jcarousel-control-next')
        .jcarouselControl({
            target: '+=1'
        });

    $('.jcarousel-pagination')
        .on('jcarouselpagination:active', 'a', function () {
            $(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function () {
            $(this).removeClass('active');
        })
        .on('click', function (e) {
            e.preventDefault();
        })
        .jcarouselPagination({
            perPage: 1,
            item: function (page) {
                return '<a href="#' + page + '">' + page + '</a>';
            }
        });


    /* scrolltop */
    $('.scroltop').on('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
    /* print */
    $('#printcontent').on('click', function (event) {
        w = window.open();
        w.document.write($('.printable').html());
        w.print();
        w.close();
        event.preventDefault();
    });
    ingredientCheckbox();
    /* masonry layout */
    var $container = $('.container-recipe');
    $container.imagesLoaded(function () {
        $container.masonry();
    });

    $(".carousel").fitVids();

    /* carousel single */
    $('#slider-recipe').carousel({
        interval: 6500
    });
});

