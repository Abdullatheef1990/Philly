(function ($) {
"use strict";
    $(document).foundation();


    $('.main-nav-links > li').click(function () {

        if($(this).hasClass('open')){
            $(this).removeClass('open');
        }else {
            $('.main-nav-links > li').removeClass('open');
            $(this).addClass('open');
        }
    });

    $('.mobile-menu-icon').click(function () {
        $(this).hasClass('open') ? $(this).removeClass('open') : $(this).addClass('open');
        $('.main-menu-wrapper').hasClass('open') ? $('.main-menu-wrapper').slideUp().removeClass('open') : $('.main-menu-wrapper').slideDown().addClass('open');
    });

    $('.search-link').click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.mobile-search-bar-wrapper').slideUp();
        }else {
            $(this).addClass('active');
            $('.mobile-search-bar-wrapper').slideDown();
        }
    });

    $('.mobile-search-bar-wrapper .close, .search-link.active').click(function(){
        $('.mobile-search-bar-wrapper').slideUp();
    });

	// homepage slider
    var hp_slider = new MasterSlider();
    hp_slider.setup('home-masterslider' , {
        width:1920,
        height:500,
        layout: "autofill"

    });
    // adds Arrows navigation control to the slider.
    //hp_slider.control('arrows');
	
	// recipe page slider
	var rh_slider = new MasterSlider();
    rh_slider.setup('recipe-home-masterslider' , {
        width:1920,
        height:500,
        layout: "autofill"

    });
    // adds Arrows navigation control to the slider.
    //rh_slider.control('arrows');
	
	// about  us slider
	var prd_slider = new MasterSlider();
    prd_slider.setup('products-masterslider' , {
        width:1920,
        height:500,
        layout: "autofill",
		speed: 5,
		view:"flow",
		instantStartLayers:true,
		
    });
    // adds Arrows navigation control to the slider.
    prd_slider.control('arrows');
	
	 var slider = new MasterSlider();
    slider.setup('masterslider' , {
        width:1920,
        height:500,
        layout: "autofill"

    });
    // adds Arrows navigation control to the slider.
    slider.control('arrows');




})(jQuery);


var scrolltotop = {
    setting: {
        startline: 100,
        scrollto: 0,
        scrollduration: 1000,
        fadeduration: [500, 100]
    },
    controlHTML: '',
    controlattrs: {
        offsetx: 10,
        offsety: 10
    },
    anchorkeyword: '#top',
    state: {
        isvisible: false,
        shouldvisible: false
    },
    scrollup: function() {
        if (!this.cssfixedsupport)
            this.$control.css({
                opacity: 0
            })
        var dest = isNaN(this.setting.scrollto) ? this.setting.scrollto : parseInt(this.setting.scrollto)
        if (typeof dest == "string" && jQuery('#' + dest).length == 1)
            dest = jQuery('#' + dest).offset().top
        else
            dest = 0
        this.$body.animate({
            scrollTop: dest
        }, this.setting.scrollduration);
    },
    keepfixed: function() {
        var $window = jQuery(window)
        var controlx = $window.scrollLeft() + $window.width() - this.$control.width() - this.controlattrs.offsetx
        var controly = $window.scrollTop() + $window.height() - this.$control.height() - this.controlattrs.offsety
        this.$control.css({
            left: controlx + 'px',
            top: controly + 'px'
        })
    },
    togglecontrol: function() {
        var scrolltop = jQuery(window).scrollTop()
        if (!this.cssfixedsupport)
            this.keepfixed()
        this.state.shouldvisible = (scrolltop >= this.setting.startline) ? true : false
        if (this.state.shouldvisible && !this.state.isvisible) {
            this.$control.stop().animate({
                opacity: 1
            }, this.setting.fadeduration[0])
            this.state.isvisible = true
        } else if (this.state.shouldvisible == false && this.state.isvisible) {
            this.$control.stop().animate({
                opacity: 0
            }, this.setting.fadeduration[1])
            this.state.isvisible = false
        }
    },
    init: function() {
        jQuery(document).ready(function($) {
            var mainobj = scrolltotop
            var iebrws = document.all
            mainobj.cssfixedsupport = !iebrws || iebrws && document.compatMode == "CSS1Compat" && window.XMLHttpRequest
            mainobj.$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body')
            mainobj.$control = $('<div id="topcontrol">' + mainobj.controlHTML + '</div>').css({
                position: mainobj.cssfixedsupport ? 'fixed' : 'absolute',
                bottom: mainobj.controlattrs.offsety,
                right: mainobj.controlattrs.offsetx,
                opacity: 0,
                cursor: 'pointer'
            }).attr({
                title: 'Scroll Back to Top'
            }).click(function() {
                mainobj.scrollup();
                return false
            }).appendTo('body')
            if (document.all && !window.XMLHttpRequest && mainobj.$control.text() != '')
                mainobj.$control.css({
                    width: mainobj.$control.width()
                })
            mainobj.togglecontrol()
            $('a[href="' + mainobj.anchorkeyword + '"]').click(function() {
                mainobj.scrollup()
                return false
            })
            $(window).bind('scroll resize', function(e) {
                mainobj.togglecontrol()
            })
        })
    }
}
scrolltotop.init();

(function(){

    $('#recipePhotoUpload').on('change',function() {
        var fullPath = $('#recipePhotoUpload').val();
        if (fullPath) {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            $('#recipePhotoUploadName').val(filename);
        }
    });


})();