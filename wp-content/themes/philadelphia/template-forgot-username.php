<?php
/* Template Name: Forgot Username Page */
 
if ( is_user_logged_in() ) {
	wp_redirect( esc_url( get_home_url() ) );
	exit;
}

$errors = array();
if( isset($_POST['reset']) && $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['forgotusername_field_nonce']) && wp_verify_nonce( $_POST['forgotusername_field_nonce'], 'forgotusername_field' ) ) {
    if(function_exists('quickrecipe_forgot_username')) {
	    $errors = quickrecipe_forgot_username();
    } else {
        $errors[] = esc_html__( 'You should install and activate to quickrecipe addition plugin.', 'quickrecipe' );
    }
}

$enable_custom_login = get_option('quickrecipe_enable_custom_login', 0);

$login_page_id = get_option('quickrecipe_login_page_url', '');
$login_page_url = get_permalink($login_page_id);
if (!$login_page_url || !$enable_custom_login)
	$login_page_url = wp_login_url();

$register_page_id = get_option('quickrecipe_register_page_url', '');
$register_page_url = get_permalink($register_page_id);
if (!$enable_custom_login || !$register_page_url) {
	$register_page_url = wp_registration_url();
}


get_header(); 
?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
            <div class="login-wrap">
                <div class="login ">
                    <div class="page-header">
                        <h1><?php esc_html_e('Remind Username', 'quickrecipe'); ?></h1>
                    </div>
                    <div class="login-description">
				        <p>
				        <?php esc_html_e('Don\'t have an account yet?', 'quickrecipe'); ?> <a href="<?php echo esc_url($register_page_url); ?>"><?php esc_html_e('Sign up', 'quickrecipe'); ?></a>.
				        </p>
				        <p>	
				        <?php esc_html_e('Already a member?', 'quickrecipe'); ?> <?php echo sprintf( wp_kses( __( 'Proceed to <a href="%s">login</a> page', 'quickrecipe' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $login_page_url ) ); ?>.
				        </p>
			        <?php if( isset($errors) && count($errors) > 0){ ?>
				        <p class="error"><?php echo implode('<br />', $errors); ?></p>
			        <?php } ?> 
                    <?php 				
                    if( isset( $_GET['action'] ) && $_GET['action'] == 'notify' ){ 

	                    ?>
	                    <p class="success">
		                    <?php esc_html_e( 'Your username has been sent to your registered email address. Please check your email for further instructions.', 'quickrecipe' ) ?>
	                    </p>
	                    <?php
                    } 
                    ?>		
                    </div>
                    <form id="reset_password_form" method="post" action="<?php get_permalink(); ?>">
                    <fieldset>
                        <div class="form-group">
                            <label for="user_login"><?php esc_html_e('E-mail address', 'quickrecipe'); ?><span class="star">&nbsp;*</span></label>
                            <input type="text" name="user_login" id="user_login" class="form-control input-lg" value="" />
                        </div>
                        <div class="form-group">
                            <div class="form-btn">
				                <?php wp_nonce_field( 'forgotusername_field', 'forgotusername_field_nonce' ) ?>
				                <input type="submit" id="reset" name="reset" value="<?php esc_html_e('Send Username', 'quickrecipe'); ?>" class="btn btn-primary"/>
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- end:content -->



<?php get_footer(); ?>