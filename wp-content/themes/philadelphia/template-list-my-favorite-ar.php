<?php 
/*
* Template Name: List My Favorite AR
*/ 

$page_id = $post->ID;
if (!is_user_logged_in()) { auth_redirect(); }

/* Get user info. */
global $current_user;
wp_get_current_user();


get_header(); 

$page_by_title = get_page_by_title( get_the_title() );
$the_excerpt = (!empty($page_by_title->post_excerpt)) ? $page_by_title->post_excerpt : $page_by_title->post_content;

$slide_intro = get_field('vp_header_title');		
$slide_image = get_field('vp_header_image');

?>
   
   <div class="row full-width header-box">
	<div class="main-image" style="background-image: url(<?php echo $slide_image; ?>)">
		<div class="master-slider ms-skin-default" id="masterslider">
			<div class="ms-slide">
				<img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt=""/>
				<div class="row main-container align-center full-width ms-layer ms-caption">
					<div class="column large-6 column align-self-middle">
						<?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>
                        
 					<?php } ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>
   
   
    <!-- begin:content -->
    <div id="content" class="user-account-box row">
	
			<!-- begin:article -->
			<div class="columns full-width user-account-header"><?php the_title(); ?></div>
     
      <div class="columns large-9 small-12">
        
<div class="row search-form full-width">

            

        </div>
        <div class="row">
            <div class="col-xs-12 single-page">
				
				<div class="columns large-12 small-12 recipe-grid">
                <div class="row">
				
				<?php
				if(is_user_logged_in())
				{
                    $args = array(
	                    'post_type'  => 'recipe',
                        'post_status' => array('publish'),
                        'posts_per_page' =>'-1', 
                        'author' => get_current_user_id(),
	                    'meta_query' => array(
		                    array(
			                    'key'     => MY_FAVORITE_RECIPES,
			                    'value'   => '0',
			                    'compare' => '>',
		                    ),
	                    ),
                    );
                    $query = new WP_Query( $args );
                    if ($query->have_posts()) : 
                ?>
                
            	   <?php while ($query->have_posts()) : $query->the_post(); ?>
                <div class="columns large-6 small-12 recipes-grid-element" style="background-image: url(<?php echo get_the_post_thumbnail_url( $post->ID); ?>);">
                    <div class="row full-width grid-inner">
                        
                        <div class="column large-6 small-6 transparent-block panel">

                            <h2><?php echo get_the_title(); ?></h2>

                            <a href="#" class="transparent-button">START COOKING</a>
							 <?php 
                                $unmark_as_favorite = add_query_arg('post', get_the_ID()); 
                                if(isset($_GET['post'])) {
		
	                                if($_GET['post'] == $post->ID)
	                                {
		                                quickrecipe_remove_from_favorite( $post->ID, get_current_user_id());
	                                }
                                }
                            ?>
							 <div class="member-controls">
                               <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn-success small"><i class="fa fa-pencil"></i> <?php esc_html_e( 'View', 'quickrecipe' ); ?></a>
                                <a onclick="return confirm('Are you sure you want to remove <?php echo esc_attr(get_the_title()); ?> from Favorites?')" href="<?php echo esc_url($unmark_as_favorite); ?>" class="btn-warning small"><i class="fa fa-close"></i><?php esc_html_e( 'Remove', 'quickrecipe' ); ?></a>
							</div>
                        </div>
                    </div>

                </div>
			 <?php endwhile;?>
			     <?php /*   
		        <table>
                    <thead>
			            <tr>
				            <th><?php esc_html_e( 'Image', 'quickrecipe' ); ?></th>
				            <th style="width: 250px"><?php esc_html_e( 'Recipe', 'quickrecipe' ); ?></th>
				            <th><?php esc_html_e( 'Status', 'quickrecipe' ); ?></th>
				            <th><?php esc_html_e( 'Actions', 'quickrecipe' ); ?></th>
			            </tr>
                    </thead>
                    <tbody>
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
			            <tr>
				            <td><?php echo get_the_post_thumbnail( $post->ID, array(48, 48) ); ?></td>
				            <td class="small"><h4><?php echo get_the_title(); ?></h4><?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?></td>
				            <td><?php echo get_post_status( get_the_ID() ); ?></td>

				            <?php 
                                $unmark_as_favorite = add_query_arg('post', get_the_ID()); 
                                if(isset($_GET['post'])) {
		
	                                if($_GET['post'] == $post->ID)
	                                {
		                                quickrecipe_remove_from_favorite( $post->ID, get_current_user_id());
	                                }
                                }
                            ?>

				            <td>
					            <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn-success small"><i class="fa fa-pencil"></i> <?php esc_html_e( 'View', 'quickrecipe' ); ?></a>
                                <a onclick="return confirm('Are you sure you want to remove <?php echo esc_attr(get_the_title()); ?> from Favorites?')" href="<?php echo esc_url($unmark_as_favorite); ?>" class="btn-warning small"><?php esc_html_e( 'Remove', 'quickrecipe' ); ?></a>
				            </td>
			            </tr>
                        <?php endwhile;?>
                    </tbody>
                </table>
                
                	 */?>	
	            <?php else: ?>
                    <strong><?php esc_html_e( 'Sorry, no favorite recipe available at this time.', 'quickrecipe' ); ?></strong>
	            <?php endif; ?>
				<?php } ?>
				</div>
				<!-- end:content -->
			</div>
            </div>
          </div>
          
		</div>
         <!-- begin:sidebar -->
			<div class="columns large-3 small-12">
			<?php quickrecipe_get_user_sidebar(); ?>
			</div>
			<!-- end:sidebar -->
	</div>		
    <!-- end:content -->

<?php get_footer(); ?>