<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: Blog
 */

 get_header();
 
?>
<?php
$quickrecipe_thumbnail_id = get_post_thumbnail_id();
if (!empty($quickrecipe_thumbnail_id)) {
    $quickrecipe_image_url = wp_get_attachment_image_src($quickrecipe_thumbnail_id, 'large');
    $quickrecipe_image_url = $quickrecipe_image_url[0];
} else {
    $quickrecipe_image_url = get_template_directory_uri() . '/img/img01.jpg';
}

if (is_front_page()) {
?>
    <!-- begin:header -->
    <div id="header" class="page-heading" style="background-image: url(<?php echo esc_url($quickrecipe_image_url); ?>);">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 col-sm-12">
            <div class="page-title">
              <h2><?php echo esc_html_e('Home Page','quickrecipe'); ?></h2>
            </div>
            <?php quickrecipe_breadcrumbs(); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- end:header -->
<?php
    }
?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
          <!-- begin:article -->
          <div class="col-md-9">
            <!-- begin:recipe -->
            <div class="row">
            <?php
        	    if ( get_query_var( 'paged') ) { $paged = get_query_var( 'paged' ); } elseif ( get_query_var( 'page') ) { $paged = get_query_var( 'page' ); } else { $paged = 1; }

        	    $query_args = array(
        						    'post_type' => 'post',
        						    'paged' => $paged
        					    );

                $query=new WP_Query($query_args);

        	    if ( $query->have_posts() ) {
                    while($query->have_posts()){
	                    $query->the_post();

					    /* Include the Post-Format-specific template for the content.
					     * If you want to overload this in a child theme then include a file
					     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					     */
					    get_template_part( 'content', get_post_format() );

        		    } // End WHILE Loop
                    wp_reset_postdata();
        	    } else {
            ?>
	        <h1><?php esc_html_e( 'Nothing Found', 'quickrecipe' ); ?></h1>
	        <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords or settings.', 'quickrecipe' ); ?></p>
            <?php } // End IF Statement ?>

            </div>
            <!-- end:recipe -->

            <!-- begin:pagination -->
            <?php quickrecipe_pagination_links( ); ?>
            <!-- end:pagination -->
          </div>
          <!-- end:article -->

          <!-- begin:sidebar -->
          <?php get_sidebar(); ?>
          <!-- end:sidebar -->
                  
        </div>
      </div>
    </div>
    <!-- end:content -->
<?php get_footer(); ?>