<?php
if ( post_password_required() ) {
	return;
}
?>

 
 <div class="comment-box align-center">


	<?php if ( have_comments() ) : ?>
	    <div class="row align-center">

        <div class="columns large-8 small-12 comments-headline">
            COMMENTS
        </div>
        </div>
	<div class="row align-center comment">
		<h2 class="comments-title">
			<?php
				$comments_number = get_comments_number();
				if ( 1 === $comments_number ) {
					/* translators: %s: post title */
					printf( _x( 'One thought on &ldquo;%s&rdquo;', 'comments title', 'quickrecipe' ), get_the_title() );
				} else {
					printf(
						/* translators: 1: number of comments, 2: post title */
						_nx(
							'%1$s thought on &ldquo;%2$s&rdquo;',
							'%1$s thoughts on &ldquo;%2$s&rdquo;',
							$comments_number,
							'comments title',
							'quickrecipe'
						),
						number_format_i18n( $comments_number ),
						get_the_title()
					);
				}
			?>
		</h2>
</div>
	 <div class="row align-center">

       
        <div class="columns large-8 small-12 comments-view-more">
		<?php the_comments_navigation(); ?>
<?php /*?>
		<ol class="comment-list">
			<?php wp_list_comments( array( 'callback' => 'quickrecipe_comment' ) ); ?>
		</ol><!-- .comment-list -->
<?php */?>
	
  
    
    <?php

    function format_comment($comment, $args, $depth) {
    
       $GLOBALS['comment'] = $comment; ?>
       
        <div class="row align-center comment" <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            <div class="columns large-3 small-3 comments-headline">
                <div class="profile-pic"><?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?></div>
                <div class="comment-name"><?php echo get_comment_author() ?></div>
            </div>
		     <div class="columns large-9 small-9 comments-body">
                 <?php if ($comment->comment_approved == '0') : ?>
                <em><php _e('Your comment is awaiting moderation.') ?></em><br />
            <?php endif; ?>
            <?php comment_text(); ?>
          </div>
    </div>
          
        
<?php } ?>
	
	<div class="commentlist">
    <?php wp_list_comments('type=comment&callback=format_comment'); ?>
</div>
		<?php the_comments_navigation(); ?>
		 </div>
</div>
	<?php endif; // Check for have_comments(). ?>
<div class="row align-center add-comment">
<div class="columns large-8 small-12 add-comment-headline">	
	
	<?php
	$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
$fields =  array(
    'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
        '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
    'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
        '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
);?>
 
	<?php if (ICL_LANGUAGE_CODE=='en') { 

$comments_args = array(
    'fields' =>  $fields,
    'title_reply'=>'LEAVE A COMMENT',
    'label_submit' => 'POST'
);
 } elseif (ICL_LANGUAGE_CODE=='ar'){ 
$comments_args = array(
    'fields' =>  $fields,
    'title_reply'=>'اترك تعليقا',
    'label_submit' => 'بريد'
);	
 } 

 
comment_form($comments_args)
	?>
	</div>
</div>
</div>
<!-- .comments-area -->

