<?php
if ( ! defined( 'ABSPATH' ) ) exit;
?>

          <!-- begin:sidebar -->
          <div class="col-md-3 sidebar">
            <?php
            // are we showing a date-based archive?
            if ( is_author() ) {
                //$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
                if ( $author_id = get_query_var( 'author' ) ) { 
                    $curauth = get_user_by( 'id', $author_id ); 
                } else { 
                    $curauth = get_user_by( 'slug', get_query_var( 'author_name' ) ); 
                }
            ?>
            <div class="user-navigation">
                <div class="user-nav-sidebar">
                    <div class="user-avatar">
			            <?php 
                            $avatar_url = quickrecipe_get_user_avatar_url ( get_avatar( get_the_author_meta( 'ID' ), 200 ) ); 
                        ?>
                        <img src="<?php echo esc_url($avatar_url); ?>" class="img-responsive" alt="<?php echo esc_html($curauth->user_title); ?>">
                        <h4><?php echo esc_html($curauth->display_name); ?><small><?php echo esc_html($curauth->user_title); ?></small></h4>
                        <p class="social-icons"><?php echo quickrecipe_get_author_social($curauth->ID); ?></p>
                        <p class="author-content"><?php the_author_meta('description'); ?></p>
                    </div>
                </div>
            </div>
            <?php } ?>

	        <?php if ( is_active_sidebar( 'primary-sidebar' ) ) : ?>

		        <?php dynamic_sidebar( 'primary-sidebar' ); ?>

	        <?php else : ?>
            <?php endif; ?>


	        <?php if ( is_active_sidebar( 'secondary-sidebar' ) && is_single() ) : ?>
    
		        <?php dynamic_sidebar( 'secondary-sidebar' ); ?>
	
	        <?php else : ?>
            <?php endif; ?>
            <!-- break -->            
          </div>
          <!-- end:sidebar -->
