<?php
if ( ! defined( 'ABSPATH' ) ) exit;

get_header(); 

if ( $author_id = get_query_var( 'author' ) ) { 
    $curauth = get_user_by( 'id', $author_id ); 
} else { 
    $curauth = get_user_by( 'slug', get_query_var( 'author_name' ) ); 
}
?>
    <!-- begin:content -->
    <div id="content">
      <div class="container">
        <div class="row">
          <!-- begin:article -->
          <div class="col-md-9">
            <!-- begin:recipe -->
            <div class="row container-recipe">
            <?php
            $args = array(
                'post_type' => 'recipe',
                'posts_per_page' => -1,
                'author' => $curauth->ID
            );

            $query=new WP_Query($args);

            $count = 0;

            if ( $query->have_posts() ) :
                while($query->have_posts()) :
                    $query->the_post();

                    /* Display Recipes for Listing */
                    get_template_part('includes/content-recipe');

                endwhile;
                wp_reset_postdata();
            else:
            ?>
            <span><?php esc_html_e('No recipes currently available for this user.','quickrecipe'); ?></span>
            <?php
            endif;

            //wp_reset_postdata();
            ?>

            </div>
            <!-- end:recipe -->

            <?php quickrecipe_pagination_links( ); ?>

          </div>
          <!-- end:article -->

          <!-- begin:sidebar -->
          <?php get_sidebar(); ?>
          <!-- end:sidebar -->
          
        </div>
      </div>
    </div>
    <!-- end:content -->




<?php get_footer(); ?>