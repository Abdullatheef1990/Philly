<?php
if ( ! defined( 'ABSPATH' ) ) exit;

if(function_exists('quickrecipe_login_register_popup')) {
    quickrecipe_login_register_popup();
}

?>
   
  <div class="row instragram-block align-center">
    <div class="column large-12 medium-10 small-12">
        <span class="social-block-caption"><i class="fa fa-instagram"></i> <?php if (ICL_LANGUAGE_CODE=='en') {the_field('hp_instagram_feed_title', 246); } elseif (ICL_LANGUAGE_CODE=='ar'){	the_field('hp_instagram_feed_title', 552); } ?></span>
<div class="instagram-wrapper">
    <?php echo do_shortcode('[ap_instagram_slider]'); ?>
		</div>
    </div>
</div>

   <footer class="full-width footer">
    <div class="row footer-box">
        <div class="column large-3 small-6 medium-4">
           
             <?php 
                            if ( is_active_sidebar( 'footer-a' ) ) :
                                dynamic_sidebar( 'footer-a' );
		                    else :
		                    endif; 
                            ?>  
        </div>
        <div class="column large-6 medium-4 hide-for-small-only">

        </div>
        <div class="column large-3 small-6 medium-4">
            <ul class="footer-social-menu">
                <li><?php if (ICL_LANGUAGE_CODE=='en') {echo ('FOLLOW US'); } elseif (ICL_LANGUAGE_CODE=='ar'){	echo ('تابعنا'); } ?></li>
                <?php
                    quickrecipe_get_social_icons();
                ?>   

            </ul>
        </div>

    </div>

    <div class="footer-bottom">
        <?php 
		 
		 if (ICL_LANGUAGE_CODE=='en') { 
  $footer_copyright_message = get_option('quickrecipe_footer_copyright_message_en','');
    
} elseif (ICL_LANGUAGE_CODE=='ar'){ 
    
    $footer_copyright_message = get_option('quickrecipe_footer_copyright_message_ar','');
 }
    

		 echo date("Y ") ; echo esc_attr($footer_copyright_message); ?> <?php wp_nav_menu( array(  'menu' => 'Footer Bottom Menu') ); ?> 
		</div>

</footer>

   
   <?php /*?> <footer class="footer">
        <div class="footer-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 blog-col">
                        <div class="footer-col-inner">
		                    <?php 
                            if ( is_active_sidebar( 'footer-a' ) ) :
                                dynamic_sidebar( 'footer-a' );
		                    else :
		                    endif; 
                            ?>           
                        </div>
                    </div>
                    <div class="footer-col col-md-3 col-sm-4 links-col">
                        <div class="footer-col-inner">
		                    <?php if ( is_active_sidebar( 'footer-b' ) ) :
                                dynamic_sidebar( 'footer-b' );
		                    else :
                            ?>
                            <?php endif; ?> 
                        </div>
                    </div>
                    <div class="footer-col col-md-3 col-sm-12 contact-col">
                        <div class="footer-col-inner">
		                    <?php if ( is_active_sidebar( 'footer-c' ) ) : ?>
                                <div class="column-four">
			                        <?php dynamic_sidebar( 'footer-c' ); ?>
                                </div>
		                    <?php else : ?>
                            <?php endif; ?> 
                        </div>
                    </div>
                </div>   
            </div>        
        </div>
        <div class="bottom-bar">
            <div class="container center">                                   
                <ul class="social-icons list-inline">
                <?php
                    quickrecipe_get_social_icons();
                ?>                                      
                </ul> 
            </div>
        </div>
    </footer><?php */?>
<?php wp_footer(); ?>

</body>
</html>