<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The default template for displaying content
 */

?>
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="item-container">
        <div class="item-content-box-list">
        <div class="item-img-box-list">
	    <?php
		    // Post thumbnail.
			if ( has_post_thumbnail( $post->ID ) ) {
				echo get_the_post_thumbnail( $post->ID, array(400,400) );
			}
	        ?>
            <div class="item-meta-box">
            <span><i class="fa fa-comments"></i> <?php esc_html(comments_number( 'no responses', 'one response', '% responses' )); ?></span>
            <span><i class="fa fa-eye"></i> <?php echo esc_html(quickrecipe_get_post_views(get_the_ID())); ?></span>
            <span><i class="fa fa-folder"></i> <?php $category = get_the_category ( get_the_ID() ); echo esc_html($category[0]->cat_name); ?></span>
            </div>
        </div>
        <div class="item-text">

            <div class="item-cat">
            <?php
            $tags = wp_get_post_tags($post->ID);
                if ($tags) {
                    foreach($tags as $tag) {
                        echo '<a href="' . esc_url(get_term_link( $tag, 'post_tag' )) . '" title="' . sprintf( __( "View all posts in %s", 'quickrecipe' ), $tag->name ) . '" ' . '>' . esc_html($tag->name) .'</a> ';
                    }
                }
            ?>                            
            </div>

            <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a> <small><?php esc_html_e( 'By ', 'quickrecipe' ); ?> <?php the_author_posts_link(); ?> - <i class="fa fa-calendar"></i><?php the_time( get_option( 'date_format' ) ); ?></small></h3>
            <?php if (is_sticky()) { echo '<div class="sticky-tag">'.__('Sticky Post','quickrecipe').'</div>'; } ?>
            <p><?php echo esc_html(wp_trim_words(get_the_excerpt(), 30, '...')); ?></p>
            <p><a class="btn btn-success btn-continue" href="<?php the_permalink(); ?>" title="<?php esc_attr_e( 'Continue Reading &rarr;', 'quickrecipe' ); ?>"><?php esc_html_e( 'Continue Reading &rarr;', 'quickrecipe' ); ?></a></p>
        </div>
        </div>
    </div>
    </div>
    <!-- break -->