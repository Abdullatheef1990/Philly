<?php
/*
* Template Name: Philly-Tips
*/
// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header();
$slide_intro = get_field('page_header_title');		
$slide_image = get_field('page_header_image');
$slide_video = get_field('page_header_yt_video_id');
?>  
<div class="row full-width header-box">     
    <div class="main-image">    
        <div class="master-slider ms-skin-default" id="recipe-home-masterslider">
            <div class="ms-slide">
                <img src="<?php echo get_template_directory_uri(); ?>/css/vendor/masterslider/blank.gif" data-src="<?php echo $slide_image; ?>" alt="<?php echo $slide_intro; ?>"/>
                <div class="row main-container align-center full-width ms-layer ms-caption">
                    <div class="column large-6 column align-self-middle">
                        <?php if ($slide_intro!=''){ ?>
                        <h1 class="heading">
                            <?php echo $slide_intro; ?>
                        </h1>
                        <?php } ?>
                    </div>
                </div>
                <?php if ($slide_video!=''){ ?>
                <iframe class="yt-video" src="https://www.youtube.com/embed/<?php echo $slide_video; ?>?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=<?php echo $slide_video; ?>&modestbranding=1" frameborder="0" allowfullscreen></iframe>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- begin:header -->
<div class="row">
    <div class="recipe-search-box small-12 columns align-center">
        <?php get_template_part( 'includes/philly-tips/search-area' ); ?>
    </div>   
    <div class="columns large-12 recipes-headline">
        <?php if (ICL_LANGUAGE_CODE=='en') { 
            the_field('tip_inner_title', 1521); 
        } elseif (ICL_LANGUAGE_CODE=='ar'){ 
            the_field('inner_title', 556); 
        } ?>
    </div>
</div>  
<div class="row recipes-grid all-recipes load-more-wrapper">
    <?php 
    get_template_part( 'includes/philly-tips/latest-philly-tips' );	
    ?>
</div>     
<?php
/*$show_search_area = get_option('quickrecipe_show_search_area', 1); 
$show_latest_recipes = get_option('quickrecipe_show_latest_recipes', 1); 
$show_offers = get_option('quickrecipe_show_offers', 1); 
$show_most_favorited = get_option('quickrecipe_show_most_favorited', 1); 
$show_editor_choice = get_option('quickrecipe_show_editor_choice', 1); 
$show_latest_news = get_option('quickrecipe_show_latest_news', 1); 
$show_our_editors = get_option('quickrecipe_show_our_editors', 1); 
$show_testimonials = get_option('quickrecipe_show_testimonials', 1); 
$show_most_read = get_option('quickrecipe_show_most_read', 1); 

if ($show_search_area) {
    get_template_part( 'includes/mainpage/search-area' );
}

if ($show_latest_recipes) {
    get_template_part( 'includes/mainpage/latest-recipes' );
}

if ($show_offers) {
    get_template_part( 'includes/mainpage/offers' );
}

if ($show_most_favorited) {
    get_template_part( 'includes/mainpage/most-favorited' );
}

if ($show_latest_news) {
    get_template_part( 'includes/mainpage/latest-news' );
}

if ($show_editor_choice) {
    get_template_part( 'includes/mainpage/editor-choice' );
}

if ($show_our_editors) {
    get_template_part( 'includes/mainpage/our-editors' );
}


if ($show_testimonials) {
    get_template_part( 'includes/mainpage/testimonials' );
}

if ($show_most_read) {
    get_template_part( 'includes/mainpage/most-read' );
}*/

get_footer(); 
?>